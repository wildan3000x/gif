#import <Flutter/Flutter.h>
#import <UIKit/UIKit.h>
@import Firebase;

@interface AppDelegate : FlutterAppDelegate
[FIRApp configure];
@end
