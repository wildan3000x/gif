import 'package:flutter/material.dart';
import 'package:gif/auth/profile/profile_page.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/caddie/my_ratings.dart';
import 'package:gif/caddie/play_caddie.dart';
import 'package:gif/socials/home_groups.dart';
import 'package:gif/main.dart';
import 'package:gif/my_games/my_games.dart';

class NavItem {
  final int id;
  final String icon;
  final String text;
  final Widget destination;

  NavItem({this.id, this.icon, this.text, this.destination});

// If there is no destination then it help us
  bool destinationChecker() {
    if (destination != null) {
      return true;
    }
    return false;
  }
}

// If we made any changes here Provider package rebuid those widget those use this NavItems
class NavItems extends ChangeNotifier {
  // By default first one is selected
  int selectedIndex = 0;
  static int selectedpage = 0;

  void changeNavIndex({int index}) {
    selectedIndex = index;
    selectedpage = index;
    // if any changes made it notify widgets that use the value
    notifyListeners();
  }

  List<NavItem> items = [
    NavItem(
      id: 1,
      icon: "assets/svg/home.svg",
      text: "Home",
      destination: HomePage(),
    ),
    NavItem(
      id: 2,
      icon: "assets/svg/games.svg",
      text: "My Games",
      destination: MyGames(),
    ),
    NavItem(
      id: 3,
      icon: "assets/svg/groups.svg",
      text: "My Socials",
      destination: HomeGroups()
    ),
    // NavItem(
    //   id: 4,
    //   icon: "assets/svg/date.svg",
    //   text: "Events",
    // ),
    NavItem(
      icon: "assets/svg/profile.svg",
      text: "Profile",
      destination: ProfilePage(
        privateProfile: true,
      ),
    ),
  ];

  List<NavItem> itemsCaddie = [
    NavItem(
      id: 1,
      icon: "assets/svg/home.svg",
      text: "Home",
      destination: HomeCaddie(),
    ),
    NavItem(
      id: 2,
      icon: "assets/svg/games.svg",
      text: "Play",
      destination: PlayCaddie()
    ),
    NavItem(
      id: 3,
      icon: "assets/svg/star.svg",
      text: "My Ratings",
      destination: MyRatingsCaddie()
    ),
    NavItem(
      id: 4,
      icon: "assets/svg/date.svg",
      text: "Appointment",
      destination: MyAppointmentCaddie()
    ),
    NavItem(
      id: 5,
      icon: "assets/svg/profile.svg",
      text: "Profile",
      destination: ProfilePage(
        privateProfile: true,
      ),
    ),
  ];

}