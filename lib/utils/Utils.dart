import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/utils/CustomException.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:async';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class Utils{
  // static String url = "http://192.168.2.234/agile/public/";
  // static String url = "http://10.10.10.234/agile/public/";
  // static String url = "http://gif.natusi.co.id/public/"; //----- DEV TEST
  // static String url = "https://qa.golfisfun.id/"; //------------ QA TEST
  static String url = "https://khatibsulaiman.golfisfun.id/"; //--- LIVE PRODUCTION
  // static String url = "http://152312221850.ip-dynamic.com:8094/agile/public/";
  // static String url = "http://192.168.27.38/natusi/agyle.gif.prototype.backend/public/";
  static String url_API = url+"api/";

  static Future<dynamic> postAPI(data, apiUrl) async {
    print("Calling API: $url_API$apiUrl");
    print("Calling parameters: ${jsonEncode(data)}");

    var fullUrl = url_API + apiUrl;
    var responseJson;
    try {
      final response =  await http.post(
          fullUrl,
          body: jsonEncode(data),
          headers: _setHeaders(),
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    print("Calling respons: $responseJson");
    return responseJson;
  }

  static _setHeaders() => {
    'Content-type' : 'application/json',
    'Accept' : 'application/json',
  };

  static dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw UnauthorisedException(response.body.toString());
      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  static String formatSuffix(int mNumber) {
    if(mNumber >= 11 && mNumber <= 13) {
      return 'th';
    }
    switch(mNumber % 10) {
      case 1: return '${mNumber}st';
      case 2: return '${mNumber}nd';
      case 3: return '${mNumber}rd';
      default: return '${mNumber}th';
    }
  }

  static String getValidPhoneNumber(Iterable phoneNumbers) {
    if (phoneNumbers != null && phoneNumbers.toList().isNotEmpty) {
      // kango nampilno kabeh berupa list
      List phoneNumbersList = phoneNumbers.toList();
      // gae jopok nomor seng pertama
      return phoneNumbersList[0].value;
    }
    return null;
  }

  static List dropDownLineAndLongest(){
    List<int> dropDown = [];
    for(int i = 0; i<18;i++){
      dropDown.add(i+1);
    }
    return dropDown;
  }

  static Future<String > getToken(context) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      return prefs.getString("token");
    }
  }

  static void showToast(BuildContext mContext, String mStatus, String mMessage){
    Color mColorStatus;
    if (mStatus == "success"){
      mColorStatus = Colors.green;
    }else if (mStatus == "info"){
      mColorStatus = Colors.lightBlueAccent;
    }else if (mStatus == "error"){
      mColorStatus = Colors.black87;
    }else {
      mColorStatus = Colors.red;
    }
    Fluttertoast.showToast(
        msg: mMessage,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: mColorStatus,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  static dynamic boxDecorationGrey () {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0xFFF9F9F9),
        border: Border.all(
            color: Color(0xFFE5E5E5)
        )
    );
  }

  static void linkToGolferProfile(context, data){
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => GolferProfileScore(
          dataGolfer: data,
        )
    ));
  }

  static Future<Navigator> navLink(context, linkPage){
    return Navigator.push(context, MaterialPageRoute(builder: (context) => linkPage));
  }

  static void showSnackBar(BuildContext mContext, String mStatus, String mMessage) {
    Color mColorStatus;
    if (mStatus == "success"){
      mColorStatus = Colors.green;
    }else if (mStatus == "info"){
      mColorStatus = Colors.lightBlue;
    }else if (mStatus == "error"){
      mColorStatus = Colors.black54;
    }else {
      mColorStatus = Colors.red;
    }
    final snackBar = SnackBar(
      content: Text('$mMessage'),
      backgroundColor: mColorStatus,
    );

    // Find the ScaffoldMessenger in the widget tree
    // and use it to show a SnackBar.
    ScaffoldMessenger.of(mContext).showSnackBar(snackBar);
  }
}

