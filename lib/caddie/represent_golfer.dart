import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/caddie/play_game.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import '../main.dart';
import 'widget/button_rectangle_rounded_caddie.dart';
import 'widget/header_caddie_represent.dart';
import 'widget/text_head_represent_golfer.dart';

class RepresentGolfer extends StatefulWidget {
  const RepresentGolfer({Key key, this.role, this.dataGolferCaddie, this.fromSearch, this.fromHomeGolfer}) : super (key: key);
  final role, dataGolferCaddie, fromSearch, fromHomeGolfer;
  @override
  _RepresentGolferState createState() => _RepresentGolferState();
}

class _RepresentGolferState extends State<RepresentGolfer> {
  bool _isLoading = false;
  bool _isLoadingReject = false;
  SharedPreferences sharedPreferences;
  final snackbarKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    print("DATA CADDIE ${widget.dataGolferCaddie}");
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      if(widget.role == "Caddie"){
        getHomeRepresentForCountNotif();
      }
    }
  }

  void confirmRepresent({String action}) async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent' : widget.dataGolferCaddie['id_represent'],
      'id_notifikasi' : widget.dataGolferCaddie['notifikasi_id'],
      'aksi' : action
    };

    Utils.postAPI(data, "konfirmasi_represent").then((body) {
      if(body['status'] == 'success'){
        messageRepresent = body;
        if(widget.role == "Golfer"){
          if(action == "Yes"){
            setState(() {
              _isLoading = false;
            });
            alertDialogSuccessRepresent();
          }else{
            setState(() {
              _isLoading = false;
            });
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                    (Route<dynamic> route) => false
            );
          }
        }else {
          if (action == "Yes") {
            setState(() {
              _isLoading = false;
            });
            alertDialogSuccessRepresent();
          } else {
            setState(() {
              _isLoading = false;
            });
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => new HomeCaddie()),
                    (Route<dynamic> route) => false
            );
          }
          DeleteNotifCaddie.deleteCountNotification(context);
        }
      }else{
        var snackbar = SnackBar(
          content: Text('${body['message']}'),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  Future alertDialogSuccessRepresent({String type}) {
    return showDialog(
      // barrierDismissible: false,
      context: snackbarKey.currentContext,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () => widget.role == "Caddie" ? Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => new HomeCaddie()),
                  (Route<dynamic> route) => false) : Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                  (Route<dynamic> route) => false),
          child: AlertDialog(
            contentPadding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
            titlePadding: EdgeInsets.all(5),
            title: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text("${messageRepresent['message']=="You already made an appointment with this golfer"?"Ops...":"SUCCESS!"}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
            ),
            content: Container(
              width: double.maxFinite,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  widget.dataGolferCaddie['foto'] == null ? ClipRRect(
                    borderRadius: BorderRadius.circular(10000.0),
                    child: Image.asset(
                      "assets/images/userImage.png",
                      width: 60,
                      height: 60,
                    ),
                  ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                      CircularProgressIndicator(
                        value: progress.progress,
                      ), imageUrl: Utils.url + 'upload/user/' + widget.dataGolferCaddie['foto'], width: 100, height: 100, fit: BoxFit.cover,)),
                  widget.role == "Caddie" ? Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text("${messageRepresent['head_message']}\n${messageRepresent['message']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                  ) : Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text("${messageRepresent['message']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                  ),
                  ButtonRectangleRoundedCaddie(textButton: "Home", link: () => widget.role == "Caddie" ? Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (BuildContext context) => new HomeCaddie()),
                          (Route<dynamic> route) => false) : Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                          (Route<dynamic> route) => false),),
                ],
              ),
            ),
          ),
        );
      }
    );
  }

  var messageRepresent;
  sendRepresent() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'input_golfer' : widget.dataGolferCaddie['kode_user'],
      'id_turnamen': widget.dataGolferCaddie['cek_turnamen']=='ada'?widget.dataGolferCaddie['data_turnamen']['id_turnamen']:null,
      'tanggal_janji': widget.dataGolferCaddie['cek_turnamen']=='ada'?widget.dataGolferCaddie['data_turnamen']['tanggal_turnamen']:DateTime(_pickDateAppointment.year, _pickDateAppointment.month, _pickDateAppointment.day, _pickTimeAppointment.hour, _pickTimeAppointment.minute).toString(),
    };
    Utils.postAPI(data, "represent").then((body) {
      if(body['status'] == 'success'){
        print("MESSAGE ${body['message']}");
        setState(() {
          messageRepresent = body;
          if(widget.role == "Golfer"){
            alertDialogSuccessRepresent();
          }else {
            alertDialogSuccessRepresent(type: "sentRepresent");
          }
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text('${body['message']}'),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  DateTime _pickDateAppointment;
  TimeOfDay _pickTimeAppointment = TimeOfDay.now();

  Future<Null> selectDateTimeAppointment(BuildContext context) async {
    _pickDateAppointment = DateTime.now();
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _pickDateAppointment,
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );

    TimeOfDay t = await showTimePicker(context: context, initialTime: _pickTimeAppointment);

    if (picked != null && t != null) {
      setState(() {
        _pickDateAppointment = picked;
        _pickTimeAppointment = t;
      });
    }
  }

  var count;
  getHomeRepresentForCountNotif() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "home_represent").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
        });
        count = body['data'];
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: Container(
            margin: EdgeInsets.all(13),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                widget.role == "Golfer" ? HeaderCreate(title: "Book Caddie",) : HeaderCaddieRepresent(
                  countAppointment: count==null?null:count['count_appointment'], countRating: count==null?null:count['count_feedback'],
                ),
                TextHeadRepresentGolfer(golferText: widget.role == "Golfer" ? true : null, buttonBack: widget.role == "Golfer" ? null : true,),
                Expanded(
                  child: Column(
                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                   children: [
                     Column(
                       children: [
                         widget.dataGolferCaddie['foto'] == null ? ClipRRect(
                           borderRadius: BorderRadius.circular(10000.0),
                           child: Image.asset(
                             "assets/images/userImage.png",
                             width: 100,
                             height: 100,
                           ),
                         ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                             CircularProgressIndicator(
                               value: progress.progress,
                             ), imageUrl: Utils.url + 'upload/user/' + widget.dataGolferCaddie['foto'], width: 100, height: 100, fit: BoxFit.cover,)),
                         SizedBox(height: 5,),
                         Text("${widget.dataGolferCaddie['nama_lengkap']}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack),),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: [
                             Text("GIF ID : ", style: TextStyle(color: AppTheme.gFont),),
                             Text("${widget.dataGolferCaddie['kode_user']}", style: TextStyle(color: AppTheme.gFontBlack),)
                           ],
                         ),
                         SizedBox(height: 10,),
                         Text("Appointment Date", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                         SizedBox(height: 5,),
                         widget.dataGolferCaddie['cek_turnamen']=='ada'||widget.fromHomeGolfer==true?
                         Text("${widget.fromSearch==true?widget.dataGolferCaddie['data_turnamen']['tanggal_turnamen_janji']:widget.dataGolferCaddie['tanggal_janji']}", style: TextStyle(color: AppTheme.gButton, fontSize: 18, fontStyle: FontStyle.italic),) :
                         Column(
                           children: [
                             Text("${_pickDateAppointment==null?"No Appointment Date":DateFormat("EEE, MMM d yyyy").format(_pickDateAppointment)+", "+_pickTimeAppointment.hour.toString()+":"+"${_pickTimeAppointment.minute<10?"0"+_pickTimeAppointment.minute.toString():_pickTimeAppointment.minute}"}", style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gButton, fontSize: 18),),
                             ButtonRectangleRoundedCaddie(textButton: "Pick Date", link: (){
                               selectDateTimeAppointment(context);
                             },)
                           ],
                         )
                       ],
                     ),
                     Text(widget.role == "Golfer" ? "Are you sure\n you want to confirm this caddie?" : "Are you sure\nyou want to represent this golfer?", style: TextStyle(fontSize: 18, color: AppTheme.bogies), textAlign: TextAlign.center,),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                       children: [
                         ButtonRectangleRoundedCaddie(textButton: "No", link: (){
                           setState(() {
                             if(widget.fromSearch == true){
                               Navigator.pop(context);
                             }else {
                             _isLoading = true;
                               confirmRepresent(action: "No");
                             }
                           });
                         },),
                         ButtonRectangleRoundedCaddie(textButton: "Yes", link: (){
                           // alertDialogSuccessRepresent();
                           setState(() {
                             if(widget.fromSearch == true){
                               if(widget.dataGolferCaddie['cek_turnamen']!='ada'&&_pickDateAppointment==null){
                                 Utils.showToast(context, "warning", "Please pick the appointment date first.");
                               }else{
                                 _isLoading = true;
                                 sendRepresent();
                               }
                             }else {
                               _isLoading = true;
                               confirmRepresent(action: "Yes");
                             }
                           });
                           // Navigator.push(context, MaterialPageRoute(builder: (context) => PlayGameCaddie()));
                         },),
                       ],
                     )
                   ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
