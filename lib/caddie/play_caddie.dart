import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/represent_golfer.dart';
import 'package:gif/caddie/search_golfer.dart';
import 'package:gif/caddie/widget/text_title.dart';
import 'package:gif/caddie/widget/widget_golfer.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import '../header_home.dart';
import 'play_game.dart';
import 'widget/button_rectangle_rounded_caddie.dart';
import 'widget/header_caddie_represent.dart';
import 'widget/text_head_represent_golfer.dart';

class PlayCaddie extends StatefulWidget {
  @override
  _PlayCaddieState createState() => _PlayCaddieState();
}

class _PlayCaddieState extends State<PlayCaddie> {
  bool _isLoading = false;
  bool _isLoadingSubmit = false;
  SharedPreferences sharedPreferences;
  bool _switchTabManually = false;
  final snackbarKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getHomeRepresent();
    }
  }

  var dataRegularClient;
  var dataTodayAppointment;
  var count;

  getHomeRepresent() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "home_represent").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
        });
        dataRegularClient = body['data']['regular_client'];
        dataTodayAppointment = body['data']['today_appointment'];
        count = body['data'];
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  TextEditingController inputGolferManually = TextEditingController();

  String headMessage, message;

  var getGolferRepresent;
  void sendRepresent() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'lapangan_id' : "",
      "level":"Golfer",
      'username' : inputGolferManually.text
    };
    Utils.postAPI(data, "get_user_represent").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          getGolferRepresent = body['data'];
          headMessage = body['head_message'];
          message = body['message'];
          _switchTabManually = true;
          _isLoadingSubmit = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text('${body['message']}'),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoadingSubmit = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingSubmit = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getHomeRepresent();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    var itemList = MediaQuery.of(context).size.width;
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(page: 'caddie', playCaddie: true,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      // HeaderHome(
                      //   countNotifGolfer: count==null?null:count['count_appointment'],
                      //   countRunningNotif: count==null?null:count['count_feedback'],
                      //   count1: null,
                      //   context: context,
                      //   text: "Play Caddie",
                      //   page: "caddie",
                      //   forCaddie: true,
                      // ),
                      HeaderCaddieRepresent(countAppointment: count==null?null:count['count_appointment'], countRating: count==null?null:count['count_feedback'], disableBack: "yes",),
                      TextHeadRepresentGolfer(),
                      AnimatedCrossFade(
                        firstChild: Column(
                          children: [
                            SizedBox(height: 8,),
                            todayAppoinment(),
                            dataRegularClient==null||dataRegularClient.isEmpty ? Container() : regularClient(itemList),
                          ],
                        ),
                        secondChild: Container(),
                        duration: Duration(milliseconds: 500),
                        crossFadeState: _switchTabManually ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        sizeCurve: Curves.fastOutSlowIn,
                      ),
                      enterManually()
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column regularClient(double itemList) {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: textTitle(title: "Select from your regular client",),
        ),
        Container(
          height: 89,
          width: itemList,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Color(0xFFF9F9F9),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: ListView.builder(
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: dataRegularClient==null?0:dataRegularClient.length,
            itemBuilder: (context, index){
              return Container(
                width: itemList/4.6,
                child: InkWell(
                  splashColor: Colors.grey.withOpacity(0.2),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            RepresentGolfer(
                              role: "Caddie", dataGolferCaddie: dataRegularClient[index], fromSearch: true,)));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      dataRegularClient==null||dataRegularClient[index]['foto'] == null ? ClipRRect(
                        borderRadius: BorderRadius.circular(10000.0),
                        child: Image.asset(
                          "assets/images/userImage.png",
                          width: 50,
                          height: 50,
                        ),
                      ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                          CircularProgressIndicator(
                            value: progress.progress,
                          ), imageUrl: Utils.url + 'upload/user/' + dataRegularClient[index]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 1),
                        child: Tooltip(message: "${dataRegularClient==null?"":dataRegularClient[index]['nama_lengkap']}", child: Text("${dataRegularClient==null?"":dataRegularClient[index]['nama_pendek']==null?dataRegularClient[index]['nama_lengkap']:dataRegularClient[index]['nama_pendek']}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),)),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Column todayAppoinment() {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 5, bottom: 5),
          child: textTitle(title: "Select from your today appointment",),
        ),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataTodayAppointment==null?0:dataTodayAppointment.length,
          itemBuilder: (context, i){
            return WidgetGolfer(
              month: "${DateFormat("MMM").format(DateTime.parse(dataTodayAppointment[i]['tanggal_turnamen']))}",
              date: "${DateFormat("d").format(DateTime.parse(dataTodayAppointment[i]['tanggal_turnamen']))}",
              time: "${DateFormat("h:mm").format(DateTime.parse(dataTodayAppointment[i]['tanggal_turnamen']))}",
              pm: "${DateFormat("a").format(DateTime.parse(dataTodayAppointment[i]['tanggal_turnamen']))}",
              picture: dataTodayAppointment[i]['foto'],
              name: "${dataTodayAppointment[i]['nama_lengkap']}",
              tourName: "${dataTodayAppointment[i]['nama_turnamen']}",
              orga: "${dataTodayAppointment[i]['penyelenggara_turnamen']}",
              // icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
              link: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    PlayGameCaddie(
                      idRepresent: dataTodayAppointment[i]['id_represent'],
                      todayGame: dataTodayAppointment[i]['today'],
                    ))).then((value) => setState(() {
                  getHomeRepresent();
                  print("RELOAD");}));
                // Navigator.push(context, MaterialPageRoute(
                //     builder: (context) =>
                //         RepresentGolfer(
                //           role: "Caddie", dataGolferCaddie: dataTodayAppointment[i], fromSearch: true,)));
              },
            );
          },),
      ],
    );
  }

  Column enterManually() {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              textTitle(title: "Enter manually",),
              Text("Enter Golfer’s Name, GIF-ID or Phone number", style: TextStyle(color: AppTheme.gFont),)
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 40,
                child: TextFormField(
                  controller: inputGolferManually,
                  decoration: InputDecoration(
                      hintText: "Input Golfer Name, ID or Number"
                  ),
                ),
              ),
            ),
            SizedBox(width: 10,),
            ButtonRectangleRoundedCaddie(textButton: "Search", link: (){
              setState(() {
                FocusScope.of(context).unfocus();
                _isLoadingSubmit = true;
                sendRepresent();
              });
            }, loading: _isLoadingSubmit,),
          ],
        ),
        _switchTabManually ? Column(
          children: [
            SizedBox(height: 15,),
            InkWell(
              onTap: (){
                setState(() {
                  _switchTabManually = false;
                });
              },
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Transform.rotate(angle: 90 * pi/180,
                      child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton,),
                    ),
                  ),
                  Text("Close", style: TextStyle(color: AppTheme.gButton),),
                ],
              ),
            ),
            SizedBox(height: 8,),
            getGolferRepresent==null||getGolferRepresent.length < 1 ? Container(
              margin: EdgeInsets.only(top: 10),
              child: Text("No Golfer(s) Found.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
            ) : ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: getGolferRepresent==null?0:getGolferRepresent.length,
              itemBuilder: (context, i){
                return getGolferRepresent[i]['cek_turnamen'] == "ada" ? WidgetGolfer(
                  month: "${DateFormat("MMM").format(DateTime.parse(getGolferRepresent[i]['data_turnamen']['tanggal_turnamen']))}",
                  date: "${DateFormat("d").format(DateTime.parse(getGolferRepresent[i]['data_turnamen']['tanggal_turnamen']))}",
                  time: "${DateFormat("h:mm").format(DateTime.parse(getGolferRepresent[i]['data_turnamen']['tanggal_turnamen']))}",
                  pm: "${DateFormat("a").format(DateTime.parse(getGolferRepresent[i]['data_turnamen']['tanggal_turnamen']))}",
                  picture: getGolferRepresent[i]['foto'],
                  name: "${getGolferRepresent[i]['nama_lengkap']}",
                  tourName: "${getGolferRepresent[i]['data_turnamen']['nama_turnamen']}",
                  orga: "${getGolferRepresent[i]['data_turnamen']['penyelenggara_turnamen']}",
                  // icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                  link: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            RepresentGolfer(
                              role: "Caddie", dataGolferCaddie: getGolferRepresent[i], fromSearch: true,)));
                  },
                ) : WidgetGolfer(
                  hasTournament: getGolferRepresent[i]['cek_turnamen'] == true ? "ada" : "tidak ada",
                  picture: getGolferRepresent[i]['foto'],
                  name: getGolferRepresent[i]['nama_lengkap'],
                  course: getGolferRepresent[i]['nama_lapangan'],
                  city: getGolferRepresent[i]['kota'],
                  country: getGolferRepresent[i]['negara'],
                  // icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                  link: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            RepresentGolfer(
                              role: "Caddie", dataGolferCaddie: getGolferRepresent[i], fromSearch: true,)));
                  },
                );
              },),
          ],
        ) : Container(),
      ],
    );
  }
}



