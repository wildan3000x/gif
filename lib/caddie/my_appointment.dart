import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/widget/text_title.dart';
import 'package:gif/caddie/widget/widget_golfer.dart';
import 'package:gif/main.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import '../header_home.dart';
import 'play_game.dart';
import 'represent_golfer.dart';
import 'widget/header_menu_2.dart';

class MyAppointmentCaddie extends StatefulWidget {
  MyAppointmentCaddie({Key key, this.bottomNavBarActive}) : super (key: key);
  final bottomNavBarActive;
  @override
  _MyAppointmentCaddieState createState() => _MyAppointmentCaddieState();
}

class _MyAppointmentCaddieState extends State<MyAppointmentCaddie> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getAppointmentCaddie();
      deleteCountNotification();
    }
  }

  var dataAppoinment;

  getAppointmentCaddie() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "my_appointment").then((body) {
      if(body['status'] == 'success'){
        dataAppoinment = body['data'];
      }else{
        Utils.showToast(context, "err", "${body['message']}");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void deleteCountNotification() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'jenis' : 'Appointment'
    };
    Utils.postAPI(data, "update_notifikasi_caddie").then((body) {
      print("STATUS ${body['message']}");
    }, onError: (error) {
      print("Error == $error");
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getAppointmentCaddie();
    _refreshController.refreshCompleted();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBar(page: 'caddie', appointment: widget.bottomNavBarActive == true ? true : null,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderHome(
                        countNotifGolfer: dataAppoinment==null?0:dataAppoinment['count_appointment'],
                        countRunningNotif: dataAppoinment==null?0:dataAppoinment['count_feedback'],
                        count1: null,
                        context: context,
                        text: "Appointment",
                        page: "caddie",
                        forCaddie: true,
                      ),
                      SizedBox(height: 20,),
                      Text("My Appointment", style: TextStyle(color: Color(0xFF828282), fontWeight: FontWeight.bold, fontSize: 24),),
                      SizedBox(height: 10,),
                      /*_isLoading ? LoadingMenu(withoutMargin: true,) : */AnimatedCrossFade(
                        secondChild: Container(),
                        firstChild: dataAppoinment==null || dataAppoinment['appointment_turnamen'].isEmpty && dataAppoinment['appointment'].isEmpty
                            ? Container(margin: EdgeInsets.all(45), child: Text("You have no appointment at this moment.", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),)
                            : Column(
                          children: [
                            ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: dataAppoinment==null?0:dataAppoinment['appointment_turnamen'].length,
                              itemBuilder: (context, i){
                                // print(dataAppoinment['appointment_turnamen'].length);
                                return Column(children: [
                                  SizedBox(height: 6,),
                                  Align(alignment: Alignment.centerLeft,
                                      child: textTitle(title: "${/*DateFormat("MMM").format(DateTime.parse(*/dataAppoinment['appointment_turnamen'][i]['tanggal_turnamen']/*))*/}",)),
                                  SizedBox(height: 5,),
                                  ListView.builder(
                                    shrinkWrap: true,
                                    primary: false,
                                    itemCount: dataAppoinment['appointment_turnamen'][i]['data_appointment'].length,
                                    itemBuilder: (context, i2){
                                      return WidgetGolfer(
                                        month: "${DateFormat("MMM").format(DateTime.parse(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tanggal_turnamen']))}",
                                        date: "${DateFormat("d").format(DateTime.parse(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tanggal_turnamen']))}",
                                        time: "${DateFormat("h:mm").format(DateTime.parse(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tanggal_turnamen']))}",
                                        pm: "${DateFormat("a").format(DateTime.parse(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tanggal_turnamen']))}",
                                        picture: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['foto'],
                                        name: "${dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['nama_lengkap']}",
                                        tourName: "${dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['nama_turnamen']}",
                                        orga: "${dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['penyelenggara_turnamen']}",
                                        icon: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['status'] == "Represented" ? Image.asset("assets/images/arrow left.png", color: AppTheme.gFont,) : SvgPicture.asset("assets/svg/check.svg", color: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['status'] == 'Confirmed' ? AppTheme.gButton : AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                                        link: (){
                                          if(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['status'] == "Invited" && dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tipe'] == "Caddie"){
                                            Utils.showToast(context, "errr", "Confirmation from golfer required");
                                          }else if(dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['status'] == "Invited" && dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['tipe'] == "Golfer"){
                                            Navigator.push(context, MaterialPageRoute(
                                                builder: (context) =>
                                                    RepresentGolfer(
                                                      role: "Caddie", dataGolferCaddie: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2], fromHomeGolfer: true,)));
                                          }else {
                                            Navigator.push(
                                                context, MaterialPageRoute(
                                                builder: (context) =>
                                                    PlayGameCaddie(
                                                      idRepresent: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['id_represent'],
                                                      todayGame: dataAppoinment['appointment_turnamen'][i]['data_appointment'][i2]['today'],
                                                    ))).then((value) => setState(() {
                                                        getAppointmentCaddie();
                                                        print("RELOAD");}));
                                          }
                                        },
                                      );
                                    },),
                                ],);
                              },),
                            dataAppoinment==null||dataAppoinment['appointment'].isEmpty ? Container() : Padding(
                              padding: const EdgeInsets.only(bottom: 2.0, top: 8),
                              child: Text("all golfer that listed below are those who don't have any games yet.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontSize: 13), textAlign: TextAlign.center,),
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: dataAppoinment==null?0:dataAppoinment['appointment'].length,
                              itemBuilder: (context, i){
                                return WidgetGolfer(
                                  hasTournament: dataAppoinment['appointment'][i]['cek_turnamen'],
                                  picture: dataAppoinment['appointment'][i]['foto'],
                                  name: dataAppoinment['appointment'][i]['nama_lengkap'],
                                  course: dataAppoinment['appointment'][i]['nama_lapangan'],
                                  city: dataAppoinment['appointment'][i]['kota'],
                                  country: dataAppoinment['appointment'][i]['negara'],
                                  appointmentDate: dataAppoinment['appointment'][i]['tanggal_janji'],
                                  icon: dataAppoinment['appointment'][i]['status'] == "Represented" ? Image.asset("assets/images/arrow left.png", color: AppTheme.gFont,) : SvgPicture.asset("assets/svg/check.svg", color: dataAppoinment['appointment'][i]['status'] == 'Confirmed' ? AppTheme.gButton : AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                                  link: (){
                                    if(dataAppoinment['appointment'][i]['status'] == "Invited" && dataAppoinment['appointment'][i]['tipe'] == "Caddie"){
                                      Utils.showToast(context, "errr", "Confirmation from golfer required");
                                    }else if(dataAppoinment['appointment'][i]['status'] == "Invited" && dataAppoinment['appointment'][i]['tipe'] == "Golfer"){
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) =>
                                              RepresentGolfer(
                                                role: "Caddie", dataGolferCaddie: dataAppoinment['appointment'][i], fromHomeGolfer: true,)));
                                    }else{
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => PlayGameCaddie(idRepresent: dataAppoinment['appointment'][i]['id_represent'],))).then((value) => setState(() {
                                        getAppointmentCaddie();
                                        print("RELOAD");}));
                                    }
                                  },
                                );
                              },
                            ),
                            ConfirmedNotConfirmedText(withPencilIcon: false,),
                          ],
                        ),
                        duration: Duration(milliseconds: 500),
                        crossFadeState: _isLoading ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        sizeCurve: Curves.fastOutSlowIn,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class DeleteNotifCaddie {
  static dynamic deleteCountNotification(context) async {
    final mToken = await Utils.getToken(context);
    var data = {
      'token' : mToken,
      'jenis' : 'Appointment'
    };
    Utils.postAPI(data, "update_notifikasi_caddie").then((body) {
      print("STATUS ${body['message']}");
    }, onError: (error) {
      print("Error == $error");
    });
  }
}
