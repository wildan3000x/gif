import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/caddie/widget/text_title.dart';
import 'package:gif/create_games/create_games.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'widget/button_rectangle_rounded_caddie.dart';
import 'widget/header_play_game_caddie.dart';

class PlayGameCaddie extends StatefulWidget {
  const PlayGameCaddie({Key key, this.idRepresent, this.todayGame}) : super (key: key);
  final idRepresent, todayGame;

  @override
  _PlayGameCaddieState createState() => _PlayGameCaddieState();
}

class _PlayGameCaddieState extends State<PlayGameCaddie> {
  bool _formCancel = false;
  TextEditingController reason = TextEditingController();
  TextEditingController gamesId = TextEditingController();
  bool _validate = false;

  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  bool _isLoading = false;
  bool _isLoadingFindGame = false;

  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      playGame();
      // clickRepresent();
    }
  }

  clickRepresent() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent' : widget.idRepresent
    };
    Utils.postAPI(data, "tombol_represent").then((body) {
      if(body['status'] == 'success'){
        playGame();
        // if(dataPlayGame!=null) {
        //   // setState(() {
        //   //   _isLoading = false;
        //   // });
        // }
      }else{
        print("ERROR ${body['message']}");
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }

  var dataPlayGame;
  bool _dontHaveTournament = false;
  playGame() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent' : widget.idRepresent
    };
    Utils.postAPI(data, "play_game").then((body) {
      if(body['status'] == 'success'){
        dataPlayGame = body['data'];
        setState(() {
          if(dataPlayGame['data_turnamen']==""){
            _dontHaveTournament = true;
          }
        });
      }else{
        print("ERROR ${body['message']}");
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void sendCancelRepresent() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent': widget.idRepresent,
      'alasan' : reason.text
    };

    Utils.postAPI(data, "tombol_cancel_represent").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          // _isLoading = false;
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeCaddie()));
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          // _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }


  var dataTournament;
  var dataGolfer;
  void findGamesId() async {
    setState(() {
      _isLoadingFindGame = true;
    });
    var data = {
      'token' : dataPlayGame['token'],
      'kode_turnamen' : gamesId.text
    };
    Utils.postAPI(data, "join_game_caddie").then((body) {
      if(body['status'] == 'success'){
        dataGolfer = body['data'];
        dataTournament = body['data']['turnamen'];
        if(body['data']['type'] == "Open" || body['data']['type'] == "Invitation"){
          Navigator.push(context, MaterialPageRoute(builder: (context) => JoinGames(data: dataTournament, forCaddie: dataGolfer)));
        }else if(body['data']['type'] == "start"){
          Navigator.push(context, MaterialPageRoute(builder: (context) => StartGame(
            forCaddie: dataGolfer['golfer'], data: dataTournament, idRepresentCaddie: dataPlayGame['id_represent'],))).then((value) {
              setState(() {
                checkLoginStatus();
              });
          });
        }
      }else{
        print("${body['message']}");
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoadingFindGame = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingFindGame = false;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCaddiePlayGame(dataPlayGame: dataPlayGame),
                  Container(
                    margin: EdgeInsets.only(top: 15, bottom: 15),
                    alignment: Alignment.centerRight,
                    child: InkWell(onTap: (){
                      if(_dontHaveTournament){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => CreateGames(
                          roleCaddie: true,
                          tokenCaddie: sharedPreferences.getString("token"),
                          tokenGolfer: dataPlayGame['token'],
                          dataCaddie: dataPlayGame['data_caddie'],
                        )));
                      }
                    }, child: Text("+ Create\nNew Game", style: TextStyle(color: _dontHaveTournament ? AppTheme.bogies : Colors.grey), textAlign: TextAlign.right,)),
                  ),
                  Text("Play Game", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: AppTheme.gFontBlack),),
                  SizedBox(height: 10,),
                  _dontHaveTournament ? Container() : Column(
                    children: [
                      Align(alignment: Alignment.centerLeft, child: textTitle(title: "From ${dataPlayGame!=null?dataPlayGame['nama_lengkap']:""}’s Invitation")),
                      SizedBox(height: 2,),
                      ListContainerHome(
                        date: dataPlayGame!=null?dataPlayGame['data_turnamen']['tanggal_turnamen']:"2021-01-30 04:17:00",
                        nameTour: dataPlayGame!=null?dataPlayGame['data_turnamen']['nama_turnamen']:"",
                        courseTour: dataPlayGame!=null?dataPlayGame['data_turnamen']['nama_lapangan']:"",
                        cityTour: dataPlayGame!=null?dataPlayGame['data_turnamen']['kota_turnamen']:"",
                        orgaTour: dataPlayGame!=null?dataPlayGame['data_turnamen']['penyelenggara_turnamen']:"",
                        link: (){
                          if(widget.todayGame == true){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => StartGame(forCaddie: dataPlayGame, data: dataPlayGame['data_turnamen'],)));
                          }else{
                            Fluttertoast.showToast(
                                msg: "You cannot enter because the game has'nt started yet.",
                                toastLength: Toast.LENGTH_SHORT,
                                backgroundColor: Colors.black.withOpacity(0.7),
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1
                            );
                          }
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => SummaryGames(tourId: dataHistory[index]['id_turnamen'])));
                        },
                        widgetCorner: SvgPicture.asset("assets/svg/check.svg", width: 18, height: 18, color: AppTheme.gFont,),
                      )
                    ],
                  ),
                  SizedBox(height: 8,),
                  _dontHaveTournament ? enterManually() : Container(),
                  // SizedBox(height: 15,),
                  AnimatedCrossFade(
                    firstChild: SizedBox(height: 40,),
                    secondChild: Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Center(
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 12, left: 12, right: 12),
                              width: 250,
                              child: TextField(
                                maxLines: 6,
                                controller: reason,
                                onChanged: (_){
                                  setState(() {
                                    _validate = false;
                                  });
                                },
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Enter your reason.',
                                  errorBorder:  _validate ? OutlineInputBorder(borderSide: BorderSide(
                                      color: Colors.red
                                  )) : null,
                                  errorText: _validate ? 'Please fill the form' : null,
                                ),
                              ),
                            ),
                            Positioned(top: 0, left: 0, child: InkWell(
                              onTap: (){
                                setState(() {
                                  _formCancel = false;
                                });
                              },
                              child: Container(padding: EdgeInsets.all(2),
                                  margin: EdgeInsets.only(right: 8),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppTheme.gRed
                                  ),
                                  child: Icon(Icons.close_rounded, color: Colors.white,)),
                            ))
                          ],
                        ),
                      ),
                    ),
                    duration: Duration(milliseconds: 500),
                    crossFadeState: _formCancel ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                    sizeCurve: Curves.fastOutSlowIn,
                  ),
                  ButtonRectangleRoundedCaddie(textButton: "Cancel Represent", link: (){
                    if(dataPlayGame['status'] == 'Represented'){
                      Utils.showToast(context, 'eee', 'You cannot cancel represent with this golfer while the tournament is still running');
                    }else {
                      setState(() {
                        _formCancel = true;
                        if (reason.text.isNotEmpty) {
                          showDialog(
                              context: snackbarKey.currentContext,
                              builder: (BuildContext context) {
                                return AlertDialogCancelRepresent(
                                  role: "golfer", function: () {
                                  sendCancelRepresent();
                                },);
                              }
                          );
                        }
                      });
                    }
                  }, color: AppTheme.bogies,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column enterManually() {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 15, bottom: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              textTitle(title: "Or Enter Games ID",),
              Text("GIF Games ID", style: TextStyle(color: AppTheme.gFont),)
            ],
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 40,
                child: TextFormField(
                  controller: gamesId,
                  decoration: InputDecoration(
                      hintText: "Input Games ID"
                  ),
                ),
              ),
            ),
            SizedBox(width: 10,),
            ButtonRectangleRoundedCaddie(
              textButton: "Submit",
              link: (){
                if(!_isLoadingFindGame){
                  findGamesId();
                }},
              loading: _isLoadingFindGame,
            ),
          ],
        ),
      ],
    );
  }

}

class AlertDialogCancelRepresent extends StatelessWidget {
  AlertDialogCancelRepresent({@required this.function, @required this.role});
  final function, role;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      contentPadding: EdgeInsets.all(8),
      title: Column(
        children: [
          Text("Are you sure?", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18),),
          Text("You want to cancel represent with this $role?", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
        ],
      ),
      content: Container(
        height: 60,
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () { Navigator.pop(context);},
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    height: 40,
                    decoration: BoxDecoration(
                        color: AppTheme.bogies,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                        alignment: Alignment.center,
                        child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
              ),
            ),
            Container(
              child: RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: function,
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    height: 40,
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                        alignment: Alignment.center,
                        child: Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
              ),
            )
          ],
        ),
      ),
    );
  }
}


