import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/profile_caddie.dart';
import 'package:gif/caddie/represent_golfer.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/caddie/find_caddie.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'package:intl/intl.dart';
import 'widget/button_rectangle_rounded_caddie.dart';
import 'widget/header_caddie_represent.dart';
import 'widget/text_head_represent_golfer.dart';

class SearchRepresent extends StatefulWidget {
  SearchRepresent({Key key, @required this.findLevel, this.countNotifCaddie, this.dataTournament, this.forBookCaddie}) : super (key: key);
  final findLevel, countNotifCaddie, dataTournament, forBookCaddie;
  @override
  _SearchRepresentState createState() => _SearchRepresentState();
}

class _SearchRepresentState extends State<SearchRepresent> {
  bool _isCheckPickCourse = false;
  bool _isLoading = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    _isCheckPickCourse = true;
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      if(widget.forBookCaddie == null){
        getUserRepresent();
      }
    }
  }

  var dataRepresent;
  bool _switchToCaddieResult = false;
  getUserRepresent({forBookCaddieGetCourse}) async {
    setState(() {
      _isLoading = true;
    });
    var data;
    if(forBookCaddieGetCourse == null){
      data = {
        'token' : sharedPreferences.getString("token"),
        'lapangan_id' : widget.dataTournament['id_lapangan'],
        'id_turnamen' : widget.dataTournament['id_turnamen'],
        'level' : widget.findLevel
      };
    }else{
      data = {
        'token' : sharedPreferences.getString("token"),
        'lapangan_id' : forBookCaddieGetCourse,
        'level' : widget.findLevel
      };
    }

    setState(() {
      _member.clear();
      _allMember.clear();
      _uiMember.clear();
    });

    Utils.postAPI(data, "get_user_represent").then((body) {
      if(body['status'] == 'success'){
        dataRepresent = body['data'];
        print("DATA USER $dataRepresent");
        setState(() {
          for (int i = 0; i < dataRepresent.length; i++) {
            DefaultMembers def = DefaultMembers(
                id: dataRepresent[i]['id'],
                nama_lengkap: dataRepresent[i]["nama_lengkap"],
                nickName: dataRepresent[i]["nama_pendek"],
                kursus: dataRepresent[i]['nama_lapangan'],
                kota: dataRepresent[i]["kota"],
                negara: dataRepresent[i]["negara"],
                foto: dataRepresent[i]["foto"],
                rating: dataRepresent[i]["rating"].toString(),
                allData: dataRepresent[i],
                gif_id: dataRepresent[i]["kode_user"]);
            _member.add(def);
            _allMember.add(CustomMembers(showAll: def));
            _uiMember.add(CustomMembers(showAll: def));
            print("DATA ${_uiMember[i].showAll.nama_lengkap}");
          }
          _isLoading = false;
          if(forBookCaddieGetCourse != null){
            _switchToCaddieResult = true;
          }
        });
      }else{
        Utils.showToast(context, "eee", "There is no caddie in this course");
      }
      setState(() {
        _isLoading = false;
        _isCheckPickCourse = false;
        _switchToCaddieResult = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.gif_id.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.nickName.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.kursus.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();

  TextEditingController pickCourse = TextEditingController();

  bool _isLoadingSubmit = false;
  var course;
  void getCourse() async {
    dataRepresent = null;
    var data = {
      'nama_lapangan' : pickCourse.text,
    };

    Utils.postAPI(data, "cari_lapangan").then((body){
      if(body['status'] == 'success'){
        course = body['data'];
        print("COURSEEE $course");
      }else{
        Utils.showToast(context, "error", "${body['message']}");
        print(body['message']);
      }
      setState(() {
        _isLoadingSubmit = false;
        _switchToCaddieResult = true;
      });
      print(body);
    }, onError: (error) {
      Utils.showToast(context, "error", "$error");
      setState(() {
        print("Error == $error");
        _isLoadingSubmit = false;
      });
    });
  }

  String getCourseName = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    widget.findLevel == "Caddie" ? HeaderCreate(title: "Pick a Caddie", backSummary: true,) : HeaderCaddieRepresent(countRating: widget.countNotifCaddie['count_feedback'], countAppointment: widget.countNotifCaddie['count_appointment'],),
                    widget.forBookCaddie == true ? Container(
                      margin: EdgeInsets.only(top: 10, bottom: 0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset("assets/images/golf_caddy.png", width: 85, height: 85,),
                              SizedBox(width: 3,),
                              Column(
                                children: [
                                  titlePage("Find Your Caddie"),
                                  Text("First select golf course the\nsearch for your caddie", textAlign: TextAlign.center, style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),)
                                ],
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              searchCourse(
                                textButton: "Search & Select Golf Course",
                                action: () {
                                  setState(() {
                                    FocusScope.of(context).unfocus();
                                    _isLoadingSubmit = true;
                                    getCourse();
                                  });
                                },
                                controller: pickCourse,
                                loading: _isLoadingSubmit
                              ),
                              AnimatedCrossFade(
                                firstChild: Container(),
                                secondChild: _switchToCaddieResult ? course==null?Container():course.isEmpty ?
                                Center(
                                  heightFactor: 10,
                                  child: Text("Cannot find the course that you looking for.", style: TextStyle(color: AppTheme.gFontBlack),),
                                ) :
                                Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text("click the course", style: TextStyle(color: AppTheme.gFontBlack),),
                                    ),
                                    ListView.builder(
                                      shrinkWrap: true,
                                      primary: false,
                                      itemCount: course==null?0:course.length,
                                      itemBuilder: (context, i) {
                                        return Card(
                                          color: Color(0xFFF9F9F9),
                                          child: InkWell(
                                            borderRadius: BorderRadius.circular(4),
                                            splashColor: Colors.grey.withOpacity(0.3),
                                            onTap: (){
                                              // setState(() {
                                              //   getCourseName = course[i]['nama_lapangan'];
                                              // });
                                              getUserRepresent(forBookCaddieGetCourse: "${course[i]['id_lapangan']}");
                                            },
                                            child: Container(
                                              margin: EdgeInsets.all(8),
                                              child: Row(
                                                children: [
                                                  Image.asset("assets/images/flag_course.png"),
                                                  SizedBox(width: 5,),
                                                  Text("${course[i]['nama_lapangan']}",),
                                                ],
                                              ),),
                                          ),);
                                      },),
                                  ],
                                ) : Container(),
                                duration: Duration(milliseconds: 200),
                                crossFadeState: _switchToCaddieResult ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                                sizeCurve: Curves.easeInSine,
                              ),
                              SizedBox(height: 8,),
                              searchCourse(
                                textButton: "${widget.findLevel=="Caddie"?"Find Caddie":"Find Golfer ID/Name"}",
                                action: () {
                                    setState(() {
                                    FocusScope.of(context).unfocus();
                                    // _isLoadingSubmit = true;
                                    _searchMembers(controllerSearch3.text);
                                  });
                                },
                                controller: controllerSearch3,
                                onChanged: (val){
                                  print("DATA ${_uiMember.map((e) => print("e ${e.showAll.nama_lengkap}"))}");
                                  _searchMembers(val);
                                }
                              ),
                              // Container(
                              //   margin: EdgeInsets.only(top: 5),
                              //   width: 250,
                              //   height: 40,
                              //   child: TextFormField(
                              //     // controller: inputGolferManually,
                              //     decoration: InputDecoration(
                              //       hintText: "Input ${widget.findLevel=="Caddie"?"Caddie":"Golfer"} ID/Name",
                              //       isDense: true,
                              //       contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 3),
                              //       // icon: Icon(Icons.search_rounded),
                              //       suffixIcon: Icon(Icons.search_rounded),
                              //     ),
                              //     controller: controllerSearch3,
                              //     onChanged: (value) {
                              //       _searchMembers(value);
                              //     },
                              //   ),
                              // ),
                              listViewCaddieGolferSearchable()
                            ],
                          ),
                        ],
                      ),
                    ) : Column(
                      children: [
                        widget.findLevel == "Caddie" ? Container(
                          margin: EdgeInsets.only(top: 15, bottom: 0),
                          child: Column(
                            children: [
                              titlePage("Find a Caddie in"),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataTournament['nama_turnamen']} (ID ${widget.dataTournament['kode_turnamen']})",
                                style: TextStyle(
                                  color: AppTheme.gFont,
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  "${widget.dataTournament['nama_lapangan']}, ${DateFormat('d MMM yyyy').format(DateTime.parse(widget.dataTournament['tanggal_turnamen']))}",
                                  style: TextStyle(color: Color(0xFF4F4F4F)),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ) : TextHeadRepresentGolfer(buttonBack: true,),
                        SizedBox(height: 5,),
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          width: 250,
                          height: 40,
                          child: TextFormField(
                            // controller: inputGolferManually,
                            decoration: InputDecoration(
                              hintText: "Input ${widget.findLevel=="Caddie"?"Caddie":"Golfer"} ID/Name",
                              isDense: true,
                              contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 3),
                              // icon: Icon(Icons.search_rounded),
                              suffixIcon: Icon(Icons.search_rounded),
                            ),
                            controller: controllerSearch3,
                            onChanged: (value) {
                              _searchMembers(value);
                            },
                          ),
                        ),
                        SizedBox(height: 5,),
                        Text("Showing all available caddies in ${widget.dataTournament['nama_lapangan']}", style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                        listViewCaddieGolferSearchable()
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget searchCourse({textButton, action, controller, loading, onChanged}) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: Utils.boxDecorationGrey(),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
              child: Text("$textButton", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic),)),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 30,
                  child: TextFormField(
                    controller: controller,
                    onChanged: onChanged,
                    decoration: InputDecoration(
                      contentPadding: new EdgeInsets.only(bottom: 10),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 10,),
              ButtonSquareFindCaddie(
                textButton: "Search",
                link: action,
                isLoading: loading,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Padding listViewCaddieGolferSearchable() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: _isLoading ? Center(heightFactor: 10, child: Container(margin: EdgeInsets.only(top: 80), width: 20, height: 20, child: CircularProgressIndicator(),),) :
      dataRepresent==null||dataRepresent.isEmpty ? Container(margin: EdgeInsets.only(top: 80), child: Text(_isCheckPickCourse ? "Please search the course first." : "There is no caddie's in this course.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic))) : ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: _uiMember == null ? 0 : _uiMember.length/*_uiMember==null || _uiMember.length <= 0 ? 0 : _uiMember.length>=10?10:_uiMember.length*/,
        itemBuilder: (context, i){
          return _uiMember.length < 0 ?
          Container(margin: EdgeInsets.only(top: 80), child:
            Text("Cannot find caddie by your search.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic)))
              : Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(bottom: 4),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xFFF9F9F9),
                border: Border.all(
                    color: Color(0xFFE5E5E5)
                )
            ),
            child: FlatButton(
              padding: EdgeInsets.all(5),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              splashColor: Colors.grey.withOpacity(0.2),
              textTheme: ButtonTextTheme.normal,
              onPressed: () => Utils.navLink(context, CaddieProfile(dataCaddie: _uiMember[i].showAll.allData)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _uiMember[i].showAll.foto == null ? Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: Image.asset(
                        "assets/images/userImage.png",
                        width: 50,
                        height: 50,
                      ),
                    ),
                  ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                      CircularProgressIndicator(
                        value: progress.progress,
                      ), imageUrl: Utils.url + 'upload/user/' + _uiMember[i].showAll.foto, width: 50, height: 50, fit: BoxFit.cover,)),
                  SizedBox(width: 8,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("${_uiMember[i].showAll.nickName == null ? _uiMember[i].showAll.nama_lengkap : _uiMember[i].showAll.nickName+", "+_uiMember[i].showAll.nama_lengkap}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),),
                        Row(
                          children: [
                            Text("GIF ID : ${_uiMember[i].showAll.gif_id}", style: TextStyle(color: AppTheme.par, fontWeight: FontWeight.normal),),
                            Flexible(child: Text(", ${_uiMember[i].showAll.kursus}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal), overflow: TextOverflow.ellipsis,)),
                          ],
                        ),
                        Text("${_uiMember[i].showAll.kota}, ${_uiMember[i].showAll.negara}", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/svg/star.svg", color: Color(0xFFF2C94C), width: 20, height: 20,),
                          Text("${_uiMember[i].showAll.rating}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18),),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                        width: 50,
                        child: ButtonSquareFindCaddie(
                          textButton: "Book",
                          link: () => Utils.navLink(context,
                              RepresentGolfer(role: widget.findLevel == "Caddie" ? "Golfer" : "Caddie", dataGolferCaddie: _uiMember[i].showAll.allData, fromSearch: true,)),
                          isLoading: null,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        },),
    );
  }

  Center titlePage(String title) => Center(child: Text("$title", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: AppTheme.gFontBlack),));
}

class DefaultMembers{
  final int id;
  final String nama_lengkap;
  final String nickName;
  final String kursus;
  final String kota;
  final String negara;
  final String foto;
  final String rating;
  final String gif_id;
  final allData;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.nama_lengkap,
    this.nickName,
    this.kursus,
    this.kota,
    this.negara,
    this.foto,
    this.rating,
    this.allData,
    this.gif_id,
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
