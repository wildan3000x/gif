import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/main.dart';
import 'package:gif/my_games/caddie/find_caddie.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:gif/widget/profile_golfercaddie.dart';

import 'represent_golfer.dart';
import 'widget/widget_feedback.dart';

class CaddieProfile extends StatefulWidget {
  const CaddieProfile({Key key, @required this.dataCaddie, this.rating}) : super(key: key);

  final dataCaddie, rating;

  @override
  _CaddieProfileState createState() => _CaddieProfileState();
}

class _CaddieProfileState extends State<CaddieProfile> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  var dataCaddie;

  @override
  void initState() {
    dataCaddie = widget.dataCaddie;
    getCaddieFeedback();
    super.initState();
  }

  var dataFeedbacks;

  Future<void> getCaddieFeedback() async {
    final token = await Utils.getToken(context);
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : widget.dataCaddie['token'],
    };
    Utils.postAPI(data, "rating_feedback").then((body) {
      if(body['status'] == 'success'){
        dataFeedbacks = body['data'];
      }else{
        var snackbar = SnackBar(
          content: Text('Ops, something wrong, please try again later.'),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(13),
          child: Column(
            children: [
              HeaderCreate(title: "Caddie Profile", backSummary: true,),
              SizedBox(height: 10,),
              ProfilePicForCaddieGolfer(data: dataCaddie),
              SizedBox(
                width: double.maxFinite,
                height: 40,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 3,
                      child: Container(
                        width: 90,
                        padding: EdgeInsets.all(5),
                        child: Center(
                          child: RatingAndStar(
                            rating: widget.rating==null?dataCaddie['rating']:widget.rating,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 80,
                        child: Text("(from ${dataFeedbacks==null?"":dataFeedbacks.length} ratings)", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 13, color: AppTheme.gFontBlack),)),
                    Positioned(
                      right: 0,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: 100,
                          minHeight: 25
                        ),
                        child: ButtonSquareFindCaddie(
                          textButton: "Book Caddie",
                          link: () => Utils.navLink(context,
                              RepresentGolfer(role: "Golfer", dataGolferCaddie: dataCaddie, fromSearch: true,)),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              TitleListMenu(
                text: "Recent Feedbacks",
                color: AppTheme.gFontBlack,
              ),
              _isLoading ? Expanded(child: LoadingMenu(withoutMargin: true,)) :
              dataFeedbacks == null || dataFeedbacks.length <= 0
                  ? Expanded(
                    child: Center(child: Text("this caddie doesnt have any feedback yet",
                      style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack),)))
                  : Expanded(
                child: ListView.builder(
                  itemCount: dataFeedbacks==null?0:dataFeedbacks.length,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, i){
                    return WidgetFeedback(
                      picture: dataFeedbacks[i]['foto'],
                      name: "${dataFeedbacks[i]['nama_lengkap']}",
                      date: "${dataFeedbacks[i]['tanggal']}",
                      rating: "${dataFeedbacks[i]['bintang']}",
                      feedbackText: "${dataFeedbacks[i]['feedback']}",
                    );
                  },),
              )
            ],
          ),
        ),
      ),
    );
  }
}
