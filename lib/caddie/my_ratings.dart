import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/widget/header_menu_2.dart';
import 'package:gif/caddie/widget/widget_feedback.dart';
import 'package:gif/main.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import '../header_home.dart';

class MyRatingsCaddie extends StatefulWidget {
  MyRatingsCaddie({Key key, this.bottomNavBarActive}) : super (key: key);
  final bottomNavBarActive;
  @override
  _MyRatingsCaddieState createState() => _MyRatingsCaddieState();
}

class _MyRatingsCaddieState extends State<MyRatingsCaddie> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }


  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getHomeCaddie();
      deleteCountNotification();
    }
  }

  var dataRating;
  var average;
  var countAppointment;
  var countRating;

  getHomeCaddie() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "rating_feedback").then((body) {
      if(body['status'] == 'success'){
        dataRating = body['data'];
        average = body['rata_rata'];
        countAppointment = body['count_appointment'];
        countRating = body['count_feedback'];
      }else{
        Utils.showToast(context, "err", "${body['message']}");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void deleteCountNotification() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'jenis' : 'Feedback'
    };
    Utils.postAPI(data, "update_notifikasi_caddie").then((body) {
      print("STATUS ${body['message']}");
    }, onError: (error) {
      print("Error == $error");
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getHomeCaddie();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(page: 'caddie', rating: widget.bottomNavBarActive == true ? true : null,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderHome(
                        countNotifGolfer: countAppointment==null?null:countAppointment,
                        countRunningNotif: countRating==null?null:countRating,
                        count1: null,
                        context: context,
                        text: "Feedback",
                        page: "caddie",
                        forCaddie: true,
                      ),
                      SizedBox(height: 25,),
                      Text("My Ratings & Feedback", style: TextStyle(color: Color(0xFF828282), fontWeight: FontWeight.bold, fontSize: 24),),
                      Padding(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Average Ratings", style: TextStyle(fontSize: 18, color: Color(0xFF828282)),),
                            SizedBox(width: 8,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset("assets/svg/star.svg", color: Color(0xFFF2C94C), width: 20, height: 20,),
                                Text("${average==null?0:average}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18),),
                              ],
                            ),
                          ],
                        ),
                      ),
                      /*_isLoading ? LoadingMenu(withoutMargin: true,)
                          : */dataRating==null||dataRating.isEmpty ? Container(margin: EdgeInsets.only(top: 50), child: Text("You have no rating's or feedback's yet."),) : AnimatedCrossFade(
                        secondChild: Container(),
                        firstChild: ListView.builder(
                          itemCount: dataRating==null?0:dataRating.length,
                          shrinkWrap: true,
                          primary: false,
                          itemBuilder: (context, i){
                            return WidgetFeedback(
                              picture: dataRating==null?"":dataRating[i]['foto'],
                              name: "${dataRating==null?"":dataRating[i]['nama_lengkap']}",
                              date: "${dataRating==null?"":dataRating[i]['tanggal']}",
                              rating: "${dataRating==null?"":dataRating[i]['bintang']}",
                              feedbackText: "${dataRating==null?"":dataRating[i]['feedback']}",
                            );
                          },
                        ),
                        duration: Duration(milliseconds: 500),
                        crossFadeState: _isLoading ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        sizeCurve: Curves.fastOutSlowIn,),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
