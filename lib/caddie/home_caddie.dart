import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/play_game.dart';
import 'package:gif/caddie/represent_golfer.dart';
import 'package:gif/caddie/widget/header_menu_2.dart';
import 'package:gif/caddie/widget/text_title.dart';
import 'package:gif/header_home.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'widget/widget_feedback.dart';
import 'widget/widget_golfer.dart';

class HomeCaddie extends StatefulWidget {
  @override
  _HomeCaddieState createState() => _HomeCaddieState();
}

class _HomeCaddieState extends State<HomeCaddie> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getHomeCaddie();
    }
  }

  var dataCaddie;
  getHomeCaddie() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "home_caddie").then((body) {
      if(body['status'] == 'success'){
        dataCaddie = body['data'];
        setState(() {
          _isLoading = false;
        });
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getHomeCaddie();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(page: 'caddie', main: true,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderHome(
                        countNotifGolfer: dataCaddie==null?null:dataCaddie['count_appointment'],
                        countRunningNotif: dataCaddie==null?null:dataCaddie['count_feedback'],
                        count1: null,
                        context: context,
                        text: "Home Caddie",
                        page: "caddie",
                        forCaddie: true,
                      ),
                      SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top:15.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Hello,", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontSize: 18),),
                                    Text("${dataCaddie==null?"":dataCaddie['data_user']['nama_lengkap']}...", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 24),)
                                  ],
                                ),
                              ),
                            ),
                            Column(
                              children: [
                                dataCaddie==null || dataCaddie['data_user']['foto']==null?ClipRRect(
                                  borderRadius: BorderRadius.circular(10000.0),
                                  child: Image.asset(
                                    "assets/images/userImage.png",
                                    width: 80,
                                    height: 80,
                                  ),
                                ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                    CircularProgressIndicator(
                                      value: progress.progress,
                                    ), imageUrl: Utils.url + 'upload/user/' + dataCaddie['data_user']['foto'], width: 80, height: 80, fit: BoxFit.cover,)),
                                SizedBox(height: 3,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset("assets/svg/star.svg", width: 20, height: 20, color: Color(0xFFF2C94C),),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 3, right: 8.0),
                                      child: Text("${dataCaddie==null?"":dataCaddie['data_user']['rating']}", style: TextStyle(fontSize: 18, color: Color(0xFF828282)),),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      dataCaddie==null||dataCaddie['request_represent'].isEmpty ? Container() : Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                        child: Row(
                          children: [
                            Align(alignment: Alignment.centerLeft, child: textTitle(title: "Request Golfer",)),
                          ],
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataCaddie==null?0:dataCaddie['request_represent'].length,
                        itemBuilder: (context, i){
                          return dataCaddie['request_represent'][i]['cek_turnamen'] == 'ada' ? WidgetGolfer(
                            month: "${DateFormat("MMM").format(DateTime.parse(dataCaddie['request_represent'][i]['data_turnamen']['tanggal_turnamen']))}",
                            date: "${DateFormat("d").format(DateTime.parse(dataCaddie['request_represent'][i]['data_turnamen']['tanggal_turnamen']))}",
                            time: "${DateFormat("h:mm").format(DateTime.parse(dataCaddie['request_represent'][i]['data_turnamen']['tanggal_turnamen']))}",
                            pm: "${DateFormat("a").format(DateTime.parse(dataCaddie['request_represent'][i]['data_turnamen']['tanggal_turnamen']))}",
                            picture: dataCaddie['request_represent'][i]['foto'],
                            name: "${dataCaddie['request_represent'][i]['nama_lengkap']}",
                            tourName: "${dataCaddie['request_represent'][i]['data_turnamen']['nama_turnamen']}",
                            orga: "${dataCaddie['request_represent'][i]['data_turnamen']['penyelenggara_turnamen']}",
                            icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                            link: (){
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      RepresentGolfer(
                                        role: "Caddie", dataGolferCaddie: dataCaddie['request_represent'][i],)));
                            },
                          ) : WidgetGolfer(
                            hasTournament: dataCaddie['request_represent'][i]['cek_turnamen'],
                            picture: dataCaddie['request_represent'][i]['foto'],
                            name: dataCaddie['request_represent'][i]['nama_lengkap'],
                            course: dataCaddie['request_represent'][i]['nama_lapangan'],
                            city: dataCaddie['request_represent'][i]['kota_caddie'],
                            country: dataCaddie['request_represent'][i]['negara_caddie'],
                            appointmentDate: dataCaddie['request_represent'][i]['tanggal_janji'],
                            icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gRed, width: 18, height: 18, alignment: Alignment.topCenter,),
                            link: (){
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      RepresentGolfer(
                                        role: "Caddie", dataGolferCaddie: dataCaddie['request_represent'][i], fromHomeGolfer: true,)));
                            },
                          );
                        },),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                        child: Row(
                          children: [
                            Align(alignment: Alignment.centerLeft, child: textTitle(title: "My Appointment Today",)),
                          ],
                        ),
                      ),
                      dataCaddie==null||dataCaddie['appointment'].isEmpty ? Container(margin: EdgeInsets.all(35),
                        child: Text("You have no appointment yet. Go find a golfer to be pair with!", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),) : ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataCaddie==null?0:dataCaddie['appointment'].length,
                        itemBuilder: (context, i){
                          return WidgetGolfer(
                            month: "${DateFormat("MMM").format(DateTime.parse(dataCaddie['appointment'][i]['tanggal_turnamen']))}",
                            date: "${DateFormat("d").format(DateTime.parse(dataCaddie['appointment'][i]['tanggal_turnamen']))}",
                            time: "${DateFormat("h:mm").format(DateTime.parse(dataCaddie['appointment'][i]['tanggal_turnamen']))}",
                            pm: "${DateFormat("a").format(DateTime.parse(dataCaddie['appointment'][i]['tanggal_turnamen']))}",
                            picture: dataCaddie['appointment'][i]['foto'],
                            name: "${dataCaddie['appointment'][i]['nama_lengkap']}",
                            tourName: "${dataCaddie['appointment'][i]['nama_turnamen']}",
                            orga: "${dataCaddie['appointment'][i]['penyelenggara_turnamen']}",
                            icon: SvgPicture.asset("assets/svg/check.svg", color: AppTheme.gButton, width: 18, height: 18, alignment: Alignment.topCenter,),
                            link: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  PlayGameCaddie(
                                    idRepresent: dataCaddie['appointment'][i]['id_represent'],
                                    todayGame: dataCaddie['appointment'][i]['today'],
                                  ))).then((value) => setState(() {
                                getHomeCaddie();
                                print("RELOAD");}));
                            },
                          );
                      },),
                      SizedBox(height: 8,),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 4),
                        child: Row(
                          children: [
                            Align(alignment: Alignment.centerLeft, child: textTitle(title: "Recent Feedbacks",)),
                          ],
                        ),
                      ),
                      dataCaddie==null||dataCaddie['recent_feedback'].isEmpty ? Container(margin: EdgeInsets.all(35),
                        child: Text("No feedback yet. Ask to your golfers to give some!", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),) : ListView.builder(
                        itemCount: dataCaddie==null?0:dataCaddie['recent_feedback'].length>3?3:dataCaddie['recent_feedback'].length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (context, i){
                          return WidgetFeedback(
                            picture: dataCaddie==null?"":dataCaddie['recent_feedback'][i]['foto'],
                            name: "${dataCaddie==null?"":dataCaddie['recent_feedback'][i]['nama_lengkap']}",
                            date: "${dataCaddie==null?"":dataCaddie['recent_feedback'][i]['tanggal']}",
                            rating: "${dataCaddie==null?"":dataCaddie['recent_feedback'][i]['bintang']}",
                            feedbackText: "${dataCaddie==null?"":dataCaddie['recent_feedback'][i]['feedback']}",
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}




