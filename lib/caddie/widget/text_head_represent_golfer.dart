import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../app_theme.dart';

class TextHeadRepresentGolfer extends StatelessWidget {
  const TextHeadRepresentGolfer({
    Key key, this.golferText, this.buttonBack
  }) : super(key: key);

  final golferText, buttonBack;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 15, bottom: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buttonBack == true ? Padding(
                padding: const EdgeInsets.only(top: 2),
                child: InkWell(splashColor: Colors.grey.withOpacity(0.2), onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
              ) : Container(),
              Expanded(child: Center(child: Text(golferText == true ? "Caddie Request" : "Represent Golfer", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: AppTheme.gFontBlack),))),
            ],
          ),
        ),
        Text(golferText == true ? "Confirm this person to become your caddy?" : "Who are you representing today ?", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gButton), textAlign: TextAlign.center,),
      ],
    );
  }
}
