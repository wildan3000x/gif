import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/widget/custom_alert_dialog.dart';

import '../../app_theme.dart';
import '../home_caddie.dart';

class HeaderCaddiePlayGame extends StatelessWidget {
  const HeaderCaddiePlayGame({
    Key key,
    @required this.dataPlayGame,
  }) : super(key: key);

  final dataPlayGame;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFF1760EB),
            Color(0xFF1A478A),
          ],
        ),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, left: 5),
                  child: InkWell(onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
                ),
                // Container(
                //     alignment: Alignment.topLeft,
                //     width: 30,
                //     height: 30,
                //     margin: EdgeInsets.only(left: 8, top: 0),
                //     // padding: const EdgeInsets.only(left: 10.0, bottom: 10),
                //     child: Stack(
                //       children: [
                //         Positioned(top: 11, child: SvgPicture.asset("assets/svg/notif_2.svg", width: 20, height: 20, color: Colors.white,)),
                //         Positioned(left: 10, bottom: 5,
                //           child: Container(
                //             padding: EdgeInsets.all(6),
                //             decoration: BoxDecoration(
                //                 shape: BoxShape.circle,
                //                 color: Color(0xFFF70707)
                //             ),
                //             child: Text("7", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,),
                //           ),
                //         ),
                //       ],
                //     )
                // ),
              ],
            ),
          ),
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text("You are now representing", style: TextStyle(color: Colors.white),),
              Text("${dataPlayGame==null?"":dataPlayGame['nama_pendek']==null?dataPlayGame['nama_lengkap']:dataPlayGame['nama_pendek']+", "+dataPlayGame['nama_lengkap']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
              SizedBox(height: 5,),
            ],
          )),
          InkWell(
            onTap: (){
              return showDialog(
                context: context,
                builder: (context) => new CustomAlertDialog(
                  title: 'Are you sure?',
                  contentText: " want to quit to the main menu?",
                  noButton: () => Navigator.of(context).pop(false),
                  yesButton: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (BuildContext context) => HomeCaddie()),
                            (Route<dynamic> route) => false);
                  },
                )/*AlertDialog(
                  title: new Text('Are you sure want quit to main menu?', style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16),),actions: <Widget>[
                  RaisedButton(
                      child: Text('No'),
                      onPressed: () => Navigator.of(context).pop(false)),
                  RaisedButton(
                    color: AppTheme.gRed,
                    child: Text('Yes'),
                    onPressed: () => Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (BuildContext context) => HomeCaddie()),
                            (Route<dynamic> route) => false),
                  ),
                ],),*/
              );
            },
            child: Container(
              margin: EdgeInsets.only(right: 3),
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/gif_logo.png"),
                      fit: BoxFit.cover)),
            ),
          ),
        ],
      ),
    );
  }
}
