import 'package:flutter/material.dart';

import '../../app_theme.dart';

class ButtonRectangleRoundedCaddie extends StatelessWidget {
  const ButtonRectangleRoundedCaddie({
    Key key, this.textButton, this.link, this.loading, this.color
  }) : super(key: key);

  final textButton;
  final link;
  final loading;
  final color;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      splashColor: Colors.grey.withOpacity(0.5),
      onPressed: link,
      padding: const EdgeInsets.all(0.0),
      child: Ink(
          decoration: BoxDecoration(
              color: color != null ? color : AppTheme.gButton,
              borderRadius: BorderRadius.circular(5)
          ),
          child: Container(
              constraints: BoxConstraints(maxWidth: textButton == "Cancel Represent" || textButton == "Cancel Appointment" ? 160 : 92, minHeight: 36),
              alignment: Alignment.center,
              child: loading == true ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),)
              : Text("$textButton", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
    );
  }
}
