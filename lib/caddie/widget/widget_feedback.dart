import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';

import '../../app_theme.dart';

class WidgetFeedback extends StatelessWidget {
  const WidgetFeedback({
    Key key, this.picture, this.name, this.date, this.feedbackText, this.rating, this.link
  }) : super(key: key);

  final picture, name, date, feedbackText, rating;
  final link;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(
              color: Color(0xFFE5E5E5)
          )
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: link,
        child: Row(
          children: [
            picture == null ? ClipRRect(
              borderRadius: BorderRadius.circular(10000.0),
              child: Image.asset(
                "assets/images/userImage.png",
                width: 50,
                height: 50,
              ),
            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/user/' + picture, width: 50, height: 50, fit: BoxFit.cover,)),
            SizedBox(width: 5,),
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(child: Text("$name", style: TextStyle(color: Color(0xFF828282), fontSize: 18, fontWeight: FontWeight.bold),)),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("$date", style: TextStyle(color: AppTheme.gFontBlack),),
                          SizedBox(width: 10,),
                          RatingAndStar(rating: rating),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: 2,),
                Text("$feedbackText", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
              ],
            ))
          ],
        ),
      ),
    );
  }
}

class RatingAndStar extends StatelessWidget {
  const RatingAndStar({
    Key key,
    @required this.rating,
  }) : super(key: key);

  final rating;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset("assets/svg/star.svg", color: Color(0xFFF2C94C), width: 18, height: 18,),
        SizedBox(width: 1,),
        Text("$rating", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18),),
      ],
    );
  }
}