import 'package:flutter/material.dart';

import '../../app_theme.dart';

class textTitle extends StatelessWidget {
  const textTitle({
    Key key, this.title
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(title, style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.start,);
  }
}