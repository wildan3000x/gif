import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';

import '../../app_theme.dart';

class WidgetGolfer extends StatelessWidget {
  const WidgetGolfer({
    Key key, this.month, this.date, this.name, this.picture, this.icon, this.time, this.tourName,
    this.orga, this.pm, this.link, this.hasTournament, this.course, this.city, this.country, this.appointmentDate
  }) : super(key: key);

  final month, date, time, pm, picture, name, tourName, orga, icon;
  final link;
  final hasTournament, course, city, country;
  final appointmentDate;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(
              color: Color(0xFFE5E5E5)
          )
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: link,
        child: hasTournament == "tidak ada" ? Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            picture == null ? Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10000.0),
                child: Image.asset(
                  "assets/images/userImage.png",
                  width: 50,
                  height: 50,
                ),
              ),
            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/user/' + picture, width: 50, height: 50, fit: BoxFit.cover,)),
            SizedBox(width: 8,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("$name", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),),
                  Text("$course", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                  Text("$city, $country", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                  appointmentDate == null ? Container() : Text("Appointment date: ${DateFormat('EEEE, d MMM yyyy, h:mm a').format(DateTime.parse(appointmentDate))}", style: TextStyle(color: AppTheme.par, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),),
                ],
              ),
            ),
            Align(alignment: Alignment.topCenter, child: icon == null ? Container() : icon)
          ],
        ) : Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Container(
                  width: time.length == 5 ? 76 : 69,
                  height: 26,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                      color: AppTheme.gButton
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text("$month", style: TextStyle(fontSize: 16, color: Colors.white),),
                      Text("$date", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),),
                    ],
                  ),
                ),
                Container(
                  width: time.length == 5 ? 76 : 69,
                  height: 31,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                      color: Color(0xFFF9F9F9),
                      border: Border.all(color: AppTheme.gButton, width: 2)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text("$time", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),),
                      Text("$pm", style: TextStyle(fontSize: 12, color: AppTheme.gFontBlack),),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 4.0, left: 5, right: 5),
              child: Center(
                child: picture == null ? ClipRRect(
                  borderRadius: BorderRadius.circular(10000.0),
                  child: Image.asset(
                    "assets/images/userImage.png",
                    width: 50,
                    height: 50,
                  ),
                ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/user/' + picture, width: 50, height: 50, fit: BoxFit.cover,)),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("$name", style: TextStyle(color: Color(0xFF828282), fontSize: 18, fontWeight: FontWeight.bold),),
                  Text("$tourName", style: TextStyle(color: AppTheme.gFontBlack),),
                  Text("$orga", style: TextStyle(color: AppTheme.gFontBlack),),
                ],
              ),
            ),
            Align(alignment: Alignment.topCenter, child: icon == null ? Container() : icon)
          ],
        ),
      ),
    );
  }
}
