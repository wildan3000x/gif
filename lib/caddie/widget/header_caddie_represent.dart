import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/caddie/my_ratings.dart';

import '../../app_theme.dart';
import '../my_appointment.dart';

class HeaderCaddieRepresent extends StatelessWidget {
  const HeaderCaddieRepresent({
    Key key, this.countRating, this.countAppointment, this.disableBack
  }) : super(key: key);

  final countAppointment, countRating, disableBack;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 70,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppTheme.gBlue,
                AppTheme.gBlueOcean,
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => MyAppointmentCaddie(bottomNavBarActive: true,)));
                        },
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: Stack(
                            alignment: Alignment.bottomLeft,
                            children: [
                              SvgPicture.asset("assets/svg/appointment.svg", width: 20, height: 20, color: Colors.white,),
                              countAppointment == null || countAppointment == 0 ? Container() : Positioned(
                                right: 0,
                                top: 0,
                                child: CircleAvatar(
                                    radius: 10,
                                    backgroundColor: Color(0xFFF70707),
                                    child: Text("${countAppointment != null ? countAppointment : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 5,),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => MyRatingsCaddie(bottomNavBarActive: true,)));
                        },
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: Stack(
                            alignment: Alignment.bottomLeft,
                            children: [
                              SvgPicture.asset("assets/svg/star.svg", width: 20, height: 20, color: Colors.white,),
                              countRating == null || countRating == 0 ? Container() : Positioned(
                                right: 0,
                                top: 0,
                                child: CircleAvatar(
                                    radius: 10,
                                    backgroundColor: Color(0xFFF70707),
                                    child: Text("${countRating != null ? countRating : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              // Row(
              //   children: [
              //     Container(
              //         alignment: Alignment.topLeft,
              //         width: 30,
              //         height: 30,
              //         margin: EdgeInsets.only(left: 15, top: 3),
              //         // padding: const EdgeInsets.only(left: 10.0, bottom: 10),
              //         child: InkWell(
              //           onTap: () {
              //             Navigator.push(context, MaterialPageRoute(builder: (
              //                 context) => MyAppointmentCaddie(bottomNavBarActive: true,)));
              //           },
              //           child: Stack(
              //             children: [
              //               Positioned(top: 11, child: SvgPicture.asset("assets/svg/appointment.svg", width: 20, height: 20, color: Colors.white,)),
              //               Positioned(left: 10, bottom: countAppointment != null ? countAppointment <= 9 ? 5 : 9 : 5,
              //                 child: Container(
              //                   padding: countAppointment != null ? countAppointment <= 9 ? EdgeInsets.all(6) : EdgeInsets.all(2.5) : EdgeInsets.all(6),
              //                   decoration: BoxDecoration(
              //                       shape: BoxShape.circle,
              //                       color: Color(0xFFF70707)
              //                   ),
              //                   child: Text("${countAppointment != null ? countAppointment : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,),
              //                 ),
              //               ),
              //             ],
              //           ),
              //         )
              //     ),
              //     SizedBox(width: 5,),
              //     Container(
              //         alignment: Alignment.topLeft,
              //         width: 30,
              //         height: 30,
              //         margin: EdgeInsets.only(top: 3),
              //         // padding: const EdgeInsets.only(left: 10.0, bottom: 10),
              //         child: InkWell(
              //           onTap: (){
              //             Navigator.push(context, MaterialPageRoute(builder: (
              //                 context) => MyRatingsCaddie(bottomNavBarActive: true,)));
              //           },
              //           child: Stack(
              //             children: [
              //               Positioned(top: 11, child: SvgPicture.asset("assets/svg/star.svg", width: 20, height: 20, color: Colors.white,)),
              //               Positioned(left: 10, bottom: countRating != null ? countRating <= 9 ? 5 : 9 : 5,
              //                 child: Container(
              //                   padding: countRating != null ? countRating <= 9 ? EdgeInsets.all(6) : EdgeInsets.all(2.5) : EdgeInsets.all(6),
              //                   decoration: BoxDecoration(
              //                       shape: BoxShape.circle,
              //                       color: Color(0xFFF70707)
              //                   ),
              //                   child: Text("${countRating != null ? countRating : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,),
              //                 ),
              //               ),
              //             ],
              //           ),
              //         )
              //     ),
              //   ],
              // ),
              InkWell(
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeCaddie())),
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Image.asset("assets/images/gif_logo.png", width: 60, height: 60, isAntiAlias: true,),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 2,),
        disableBack == null ? Text("click the GIF icon to return to the main page.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),) : Container()
      ],
    );
  }
}
