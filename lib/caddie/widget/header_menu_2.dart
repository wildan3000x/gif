import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/caddie/play_caddie.dart';

import '../../app_theme.dart';

class HeaderMenuTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 90,
      left: 0.5,
      right: 0.5,
      bottom: 0.5,
      child: Container(
        height: 70,
        width: 325,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              AppTheme.gGrey,
              AppTheme.gWhite,
            ],
          ),
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 13.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MyAppointmentCaddie(bottomNavBarActive: true,)));
                  },
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/svg/cart.svg", width: 30, height: 30,),
                      Text("Running Games", style: TextStyle(fontSize: 12),),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PlayCaddie()));
                  },
                  child: Column(
                    children: [
                      SvgPicture.asset("assets/svg/represent.svg", width: 30, height: 30),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text("Represent Golfer", style: TextStyle(fontSize: 12),),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
