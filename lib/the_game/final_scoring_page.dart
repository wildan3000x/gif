import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/pairing/success_msg.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/main.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/alert_dialog.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/remark.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/share_sosmed.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'leaderboard/final_leaderboard/final_leaderboard.dart';
import 'scoring_page.dart';

class FinalScorePage extends StatefulWidget {
  const FinalScorePage({Key key, this.dataId, this.status, this.id, this.userId, this.dataSG, this.tokenFromScoreCard}) : super(key: key);
  final dataId, status, id, userId, dataSG, tokenFromScoreCard;
  @override
  _FinalScorePageState createState() => _FinalScorePageState();
}

class _FinalScorePageState extends State<FinalScorePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final snackbarKey = GlobalKey<ScaffoldState>();
  final GlobalKey _listOneKey = GlobalKey();
  Size _listOneSize = Size.zero;
  final GlobalKey _listTwoKey = GlobalKey();
  Size _listTwoSize = Size.zero;
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataPairing;
  var dataPairingID;
  var dataStart;
  var dataScore;
  var id;
  var idUser;
  var count;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading2 = false;
  bool _isLoading = false;
  bool _isLoadingApprove = false;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.dataId;
    dataPairing = widget.dataSG;
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  getSizeAndPosition() {
    RenderBox _cardBox1 = _listOneKey.currentContext.findRenderObject();
    _listOneSize = _cardBox1.size;
    RenderBox _cardBox2 = _listTwoKey.currentContext.findRenderObject();
    _listTwoSize = _cardBox2.size;
    setState(() {});
  }

  var idOnLocalStorage;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    idOnLocalStorage = sharedPreferences.getString("id");
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      getScoreEntry();
    }
  }

  //button Approval;
  List<String> count_pair1 = [];
  List<String> count_pair2 = [];
  bool approve = true;

  void getScoreEntry() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': widget.tokenFromScoreCard != null ? widget.tokenFromScoreCard : sharedPreferences.getString("token"),
      'id_turnamen': id==null?"0":id
    };
    Utils.postAPI(data, "get_PFR").then((body) {
      if (body['status'] == 'success') {
        dataScore = body['data'];
        idUser = dataScore['user']['id'];
        setState(() {
          dataPairing = body['data'];
          dataPairingID = body['data']['game_pairing'];
          _isLoading = false;
          if(body['button'] == 'sembunyikan'){
            approve = true;
          }else{
            approve = false;
          }
          WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
        });

        // print("GET JOIN GAME = ${dataScore}");
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getScoreEntry();
    _refreshController.refreshCompleted();
  }

  String _textMSG, _textSub, idSetPreference;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: ShareSosMed(
        screenShotController: screenshotController,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  // margin: EdgeInsets.all(13),
                  child: _isLoading2 ? Center(
                    heightFactor: 20,
                    child: Container(
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(),
                    ),
                  ) : Column(
                    children: [
                      Screenshot(
                        controller: screenshotController,
                        child: Container(
                          padding: EdgeInsets.only(left: 13, right: 13),
                          color: Colors.white,
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 13),
                                child: Column(
                                  children: [
                                    HeaderScoring(dataPairing: widget.dataSG, myFormat: myFormat, onClickable: true,),
                                    onlyTextInTop(),
                                  ],
                                ),
                              ),
                              pairingPicture(),
                              infoGrossHandicapNett(),
                              inputScore(),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 13, right: 13, bottom: 13),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 8),
                              child: Text("All score has been approved by golfers", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0, bottom: 10),
                              child: buttonToFinalLeaderBoard(),
                            ),
                            // ShareSosMed(
                            //   screenShotController: screenshotController,
                            // ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
          ],
        ),
      ),
    );
  }

  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;

  Container infoGrossHandicapNett() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Container(
      padding: EdgeInsets.only(top: 5),
        width: screenWidth,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          Container(
            width: screenWidth/6.6*2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Divider(
                  color: Color(0xFFBDBDBD),
                  height: 0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3.0),
                  child: Text("Gross", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xFF2D9CDB)),),
                ),
                Divider(
                  color: Color(0xFFBDBDBD),
                  height: 0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3.0),
                  child: Text("Handicap", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xFFF15411)),),
                ),
                Divider(
                  color: Color(0xFFBDBDBD),
                  height: 0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 3.0),
                  child: Text("Nett", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xFF1AC5A6)),),
                ),
                Divider(
                  color: Color(0xFFBDBDBD),
                  height: 0,
                ),
              ],
            ),
          ),
          Container(
            // padding: EdgeInsets.only(left: 15),
            width: screenWidth/6.6*4,
            height: 90,
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
              itemBuilder: (context, i) {
                return Container(
                    width: screenWidth/6.6*1,
                    // padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                        children: [
                          Divider(
                            color: Color(0xFFBDBDBD),
                            height: 0,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 3.0),
                            child: Text("${dataScore['turnamen_pairing'][i]['gross']}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: idOnLocalStorage == dataScore['turnamen_pairing'][i]['user_id'].toString() ? Colors.black : Color(0xFF828282)),),
                          ),
                          Divider(
                            color: Color(0xFFBDBDBD),
                            height: 0,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 3.0),
                            child: Center(child: Text("${dataScore['turnamen_pairing'][i]['handicap']}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: idOnLocalStorage == dataScore['turnamen_pairing'][i]['user_id'].toString() ? Colors.black : Color(0xFF828282)),)),
                          ),
                          Divider(
                            color: Color(0xFFBDBDBD),
                            height: 0,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 3.0),
                            child: Center(child: Text("${dataScore['turnamen_pairing'][i]['net']}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: idOnLocalStorage == dataScore['turnamen_pairing'][i]['user_id'].toString() ? Colors.black : Color(0xFF828282)),)),
                          ),
                          Divider(
                            color: Color(0xFFBDBDBD),
                            height: 0,
                          ),
                        ],
                      )
                );
              },
            ),
          )
        ]
      )
    );
  }

  Widget buttonToFinalLeaderBoard() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
          splashColor: Colors.grey.withOpacity(0.5),
          onPressed: () {
            Navigator.pop(context);
          },
          padding: const EdgeInsets.all(0.0),
          child: Ink(
              decoration: BoxDecoration(
                  color: AppTheme.gButton,
                  borderRadius: BorderRadius.circular(100)
              ),
              child: Container(
                  constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                  alignment: Alignment.center,
                  child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
        ),
        // SizedBox(width: 10,),
        dataScore==null?Container():dataScore['tombol'] == "aktif" ? RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
          splashColor: Colors.grey.withOpacity(0.5),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => FinalLeaderBoards(dataHeader: widget.dataSG,)));
          },
          padding: const EdgeInsets.all(0.0),
          child: Ink(
              decoration: BoxDecoration(
                  color: Color(0xFF2D9CDB),
                  borderRadius: BorderRadius.circular(100)
              ),
              child: Container(
                  constraints: const BoxConstraints(maxWidth: 166, minHeight: 51),
                  alignment: Alignment.center,
                  child: Text("Final Leader Boards", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
        ) : Column(
          children: [
            Text("Waiting for other golfers\nto finish their game", textAlign: TextAlign.center,),
            RefreshScoreButton(
              isLoading: _isLoading,
              onTap: () => getScoreEntry(),
            ),
          ],
        ),
      ],
    );
  }

  Column onlyTextInTop() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 5, bottom: 15),
          child: Text(
            "Click tournament title for leaderboard and other info",
            style:
            TextStyle(fontSize: 12, color: Color(0xFF828282)),
          ),
        ),
      ],
    );
  }

  Container inputScore() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Container(
      margin: EdgeInsets.only(top: 13),
      width: double.maxFinite,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: screenWidth/6.6*1,
                child: Text(
                  "Hole",
                  style: TextStyle(
                      color: AppTheme.gFont,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                width: screenWidth/6.6*1,
                // margin: EdgeInsets.only(right: screenWidth/6.6*0.2),
                child: Text(
                  "Par",
                  style: TextStyle(
                      color: Color(0xFF2D9CDB),
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: screenWidth/6.6*(dataScore==null?4:dataScore['turnamen_pairing'].length),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Stroke",
                    style: TextStyle(
                        color: Color(0xFF828282),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ],
          ),
          Container(
              width: screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    key: _listOneKey,
                    width: screenWidth/6.6*2,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: dataScore == null ? 0 : dataScore['lapangan1'].length,
                        itemBuilder: (context, index) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                width: screenWidth/6.6*1,
                                alignment: Alignment.centerLeft,
                                child: Theme(
                                  data: ThemeData(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                  ),
                                  child: InkWell(
                                    onTap: () {},
                                    child: Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(top: 7, bottom: 7),
                                        width: screenWidth/6.6*0.49,
                                        height: screenWidth/6.6*0.49,
                                        decoration: BoxDecoration(
                                          color: AppTheme.gFont,
                                          shape: BoxShape.circle,
                                        ),
                                        child: Text("${dataScore==null?"":dataScore['lapangan1'][index]['hole']}", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
                                        )),
                                  ),
                                ),
                              ),
                              Container(
                                  width: screenWidth/6.6*1,
                                  child: Text("${dataScore==null?"":dataScore['lapangan1'][index]['par']}", textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 20),)),
                            ],
                          );
                        }),
                  ),
                  Container(
                    width: screenWidth/6.6*4,
                    height: _listOneSize.height,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                        itemBuilder: (context, index2) {
                          return Container(
                            width: screenWidth/6.6*1,
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: dataScore==null?0:dataScore['turnamen_pairing'][index2]['pairing_score1'].length,
                              itemBuilder: (context, index3) {
                                return Container(
                                  decoration: BoxDecoration(
                                    color: idOnLocalStorage == dataScore['turnamen_pairing'][index2]['user_id'].toString() ? dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna'] == null ? Colors.white:Colors.grey.withOpacity(0.1) : Colors.white,
                                    // borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: Container(
                                      width: screenWidth/6.6*0.49,
                                      height: screenWidth/6.6*0.49,
                                      margin: EdgeInsets.only(top: 7, bottom: 7, left: screenWidth/6.6*0.25, right: screenWidth/6.6*0.25),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        borderRadius: dataScore['turnamen_pairing'][index2]['pairing_score1'][index3] == null ? BorderRadius.circular(0) :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Par'?BorderRadius.circular(30):BorderRadius.circular(8),
                                        color: dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score1'][index3] == null ? Colors.white :
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna'] != "" || dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='warna_lain'?
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Eagle'?AppTheme.eagle:
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Birdie'?AppTheme.birdie:
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Par'?AppTheme.par:
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Bogies'?AppTheme.bogies:
                                        Colors.white:Colors.white,
                                      ),
                                      child: Center(
                                        child: Text("${dataScore==null?"":dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]==null?"":dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['score']}",
                                          style: TextStyle(color:
                                          dataScore==null?Colors.white
                                              :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]==null?Colors.white
                                              :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']==""||dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=="warna_lain"? Color(0xFF4F4F4F):Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20), textAlign: TextAlign.center,
                                        ),
                                      )
                                  ),
                                );
                              },
                            ),
                          );
                        }),
                  ),
                ],
              )
          ),
          Container(
            width: screenWidth,
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.only(top: 6),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xFFBDBDBD)),
                    bottom: BorderSide(color: Color(0xFFBDBDBD)))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: screenWidth/6.6*1,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_hole1']}",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.center,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_par1']}",
                    style: TextStyle(
                        color: Color(0xFF2D9CDB),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  width: screenWidth/6.6*4,
                  height: 30,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: screenWidth/6.6*1,
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_score1']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['plus1']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 12),
                              )
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RemarkScore(),
          ),
          dataScore==null || (dataScore['lapangan2'].isEmpty || dataScore['lapangan2'] == null || dataScore['lapangan2'] == "null") ? Container() :
          Container(
              width: screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    key: _listTwoKey,
                    width: screenWidth/6.6*2,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: dataScore == null ? 0 : dataScore['lapangan2'].length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    width: screenWidth/6.6*1,
                                    alignment: Alignment.centerLeft,
                                    child: Theme(
                                      data: ThemeData(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                      ),
                                      child: InkWell(
                                        onTap: () async {},
                                        child: Container(
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(top: 7, bottom: 7),
                                            width: screenWidth/6.6*0.49,
                                            height: screenWidth/6.6*0.49,
                                            decoration: BoxDecoration(
                                              color: AppTheme.gFont,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Text("${dataScore==null?"":dataScore['lapangan2'][index]['hole']}", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
                                            )),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      width: screenWidth/6.6*1,
                                      alignment: Alignment.center,
                                      child: Text("${dataScore==null?"":dataScore['lapangan2'][index]['par']}", textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 20),)),
                                ],
                              ),
                            ],
                          );
                        }),
                  ),
                  Container(
                    width: screenWidth/6.6*4,
                    height: _listTwoSize.height,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                        itemBuilder: (context, index2) {
                          return Container(
                            width: screenWidth/6.6*1,
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: dataScore==null?0:dataScore['turnamen_pairing'][index2]['pairing_score2'].length,
                              itemBuilder: (context, index3) {
                                return Container(
                                  decoration: BoxDecoration(
                                    color: idOnLocalStorage == dataScore['turnamen_pairing'][index2]['user_id'].toString() ? dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna'] == null ? Colors.white:Colors.grey.withOpacity(0.1) : Colors.white,
                                    // borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: Container(
                                      width: screenWidth/6.6*0.49,
                                      height: screenWidth/6.6*0.49,
                                      margin: EdgeInsets.only(top: 7, bottom: 7, left: screenWidth/6.6*0.25, right: screenWidth/6.6*0.25),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        borderRadius: dataScore['turnamen_pairing'][index2]['pairing_score2'][index3] == null ? BorderRadius.circular(0) :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Par'?BorderRadius.circular(30):BorderRadius.circular(8),
                                        color: dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score2'][index3] == null ? Colors.white :
                                        dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna'] != "" || dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='warna_lain'?
                                        dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Eagle'?AppTheme.eagle:
                                        dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Birdie'?AppTheme.birdie:
                                        dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Par'?AppTheme.par:
                                        dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Bogies'?AppTheme.bogies:
                                        Colors.white:Colors.white,
                                      ),
                                      child: Center(
                                        child: Text("${dataScore==null?"":dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]==null?"":dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['score']}",
                                          style: TextStyle(color:
                                          dataScore==null?Colors.white
                                              :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]==null?Colors.white
                                              :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']==""||dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=="warna_lain"? Color(0xFF4F4F4F):Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20), textAlign: TextAlign.center,
                                        ),
                                      )
                                  ),
                                );
                              },
                            ),
                          );
                        }),
                  ),
                ],
              )),
          Container(
            width: screenWidth,
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.only(top: 6),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xFFBDBDBD)),
                    bottom: BorderSide(color: Color(0xFFBDBDBD)))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_hole2']}",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.center,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_par2']}",
                    style: TextStyle(
                        color: Color(0xFF2D9CDB),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*4,
                  height: 30,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: screenWidth/6.6*1,
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_score2']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['plus2']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 12),
                              )
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Container totalScore() {
    return Container(
      width: SizeConfig.screenWidth,
      margin: EdgeInsets.only(top: 13),
      padding: EdgeInsets.only(top: 6),
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color: Color(0xFFBDBDBD)),
              bottom: BorderSide(color: Color(0xFFBDBDBD)))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: SizeConfig.screenWidth/6.6*1,
            child: Text(
              "Total",
              style: TextStyle(
                  color: AppTheme.gFont,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: SizeConfig.screenWidth/6.6*1,
          ),
          Container(
            width: SizeConfig.screenWidth/6.6*4,
            height: 30,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                primary: false,
                itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                itemBuilder: (context, index) {
                  return Container(
                    width: SizeConfig.screenWidth/6.6*1,
                    padding: EdgeInsets.only(left: 0),
                    child: Wrap(
                      children: [
                        Padding(
                          // padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left :0.0) : EdgeInsets.only(left :8.0),
                          padding: EdgeInsets.only(left: 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_semua_score']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_semua_score_pluss']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget pairingPicture() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 8),
          child: Text(
            "Pairing Final Scorecard",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF4F4F4F)),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: screenWidth/6.6*1,
              child: Column(
                children: [
                  Text("Pairing"),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 2),
                    width: screenWidth/6.6*1,
                    padding: EdgeInsets.only(bottom: 8, top: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF15411),
                    ),
                    child: Text(
                      "${dataPairingID==null?"":dataPairingID['pairing_id']}",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text("Start #${dataScore==null?"":dataScore['starting_hole']}", style: TextStyle(fontSize: 13),)
                ],
              ),
            ),
            Container(
              width: screenWidth/6.6*1,
            ),
            Container(
              height: 80,
              width: screenWidth/6.6*4,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  primary: false,
                  itemCount: dataPairing==null?0:dataPairing['turnamen_pairing_group'].length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () async{
                        await showDialog(
                            context: _scaffoldKey.currentContext,
                            builder: (BuildContext context) => namePopUp(
                              name: "${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_pendek']!=null?dataPairing['turnamen_pairing_group'][index]['nama_pendek']+", ":""}${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_lengkap']}",
                              photo: dataPairing['turnamen_pairing_group'][index]['foto'] == null ?
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Image.asset("assets/images/userImage.png",
                                  height: 120, width: 120,
                                ),
                              ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) => CircularProgressIndicator(
                                value: progress.progress,),
                                imageUrl: Utils.url + 'upload/user/' + dataPairing['turnamen_pairing_group'][index]['foto'], height: 120, width: 120, fit: BoxFit.cover,)),
                            )
                        );
                      },
                      child: Container(
                        margin: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? EdgeInsets.symmetric(horizontal: 3) : EdgeInsets.zero,
                        padding: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? EdgeInsets.all(2) : EdgeInsets.all(0),
                        width: screenWidth/6.6*1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              // decoration: BoxDecoration(
                              //   shape: BoxShape.circle,
                              // ),
                              child:
                              dataPairing['turnamen_pairing_group'][index]['foto'] == null ?
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10000.0),
                                child: Image.asset(
                                  "assets/images/userImage.png",
                                  height: screenWidth/6.6*0.8, width: screenWidth/6.6*0.8, fit: BoxFit.cover,
                                ),
                              )
                                  : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                  CircularProgressIndicator(
                                    value: progress.progress,
                                  ), imageUrl: Utils.url + 'upload/user/' + dataPairing['turnamen_pairing_group'][index]['foto'], height: screenWidth/6.6*0.8, width: screenWidth/6.6*0.8, fit: BoxFit.cover,)),
                            ),
                            Center(
                              child: Text(
                                "${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_pendek']!=null?dataPairing['turnamen_pairing_group'][index]['nama_pendek']:dataPairing['turnamen_pairing_group'][index]['nama_lengkap']}",
                                // "Dustin J",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? Colors.black : Color(0xFF4F4F4F), fontWeight: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? FontWeight.bold : FontWeight.normal),
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ],
    );
  }


  Container buttonApprove() {
    return Container(
      padding: EdgeInsets.only(top: 25),
      child: Column(
        children: [
          Text("All score has been entered completely", style: TextStyle(color: Color(0xFF4F4F4F)),),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Theme(
              data: ThemeData(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
              ),
              child: InkWell(
                onTap: () {
                  setState(() {
                    _isLoadingApprove = true;
                    approveScore();
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(18),
                  width: 157,
                  decoration: BoxDecoration(
                      color: AppTheme.gButton,
                      borderRadius: BorderRadius.circular(30)),
                  child: _isLoadingApprove ? Center(
                    child: Container(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(),
                    ),
                  ) : Text(
                    "Approve Score",
                    style: TextStyle(
                        color: AppTheme.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget namePopUp({String name, Widget photo}){
    return AlertDialog(
      title: Center(child: Column(
        children: [
          photo,
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(name, style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
          ),
        ],
      )),
      backgroundColor: Colors.black.withOpacity(0.5),
    );
  }

  void approveScore() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': id==null?"0":id,
      'id_user':dataScore['user']['id']
    };
    Utils.postAPI(data, "AS").then((body) {
      setState(() {
        _isLoadingApprove = false;
      });
      if (body['status'] == 'success') {
        print("MSG = ${body['message']}");
        setState(() {
          getScoreEntry();
        });
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }
}



