import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/search_golfer.dart';
import 'package:gif/caddie/widget/header_play_game_caddie.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/scoring_page.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/tournament_detail.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StartGame extends StatefulWidget {
  final data, status, id, userId, fromPairingNext, forCaddie, idRepresentCaddie;

  const StartGame({Key key, this.data, this.status, this.id, this.userId,
    this.fromPairingNext, this.forCaddie, this.idRepresentCaddie}) : super(key: key);

  @override
  _StartGameState createState() => _StartGameState();
}

class _StartGameState extends State<StartGame> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataPairing;
  var dataStart;
  var id;
  var count;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');
  bool _isLoading = false;
  bool _isLoading2 = false;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.data;
    // print("DATA = ${dataTour}");
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    } else {
      getStartGames();
    }
  }

  getStartGames() async {
    setState(() {
      _isLoading2 = true;
    });
    var data = {
      'token': widget.forCaddie == null ? sharedPreferences.getString("token") : widget.forCaddie['token'],
      'token_caddie': widget.forCaddie==null?"":sharedPreferences.getString("token"),
      'id_turnamen': id == null ? "" : id['id_turnamen']
    };
    Utils.postAPI(data, "get_SG").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _isLoading2 = false;
        });
        dataTour = body['data']['turnamen'];
        dataPairing = body['data']['turnamen_pairing_group'];
        dataStart = body['data'];
      } else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getStartGames();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: _isLoading2 ? Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Center(
          child: Container(
            width: 30,
            height: 30,
            child: CircularProgressIndicator(),
          ),
        ),
      ) : SafeArea(
        child: Stack(
          children: [
            SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      widget.forCaddie==null?HeaderCreate(title: "Game Time",):HeaderCaddiePlayGame(dataPlayGame: widget.forCaddie),
                      Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                      dataTour==null?Container():TournamentDetailWidget(dataTournament: dataTour, additionalWidget: Container(), isTournamentInfo: "yes",),
                      Container(
                        padding: EdgeInsets.only(bottom: 15),
                        child: Text(
                          "My Pairing",
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF4F4F4F)),
                        ),
                      ),
                      textPairingGolfer(),
                      myPairing(),
                      goodLuckText()
                    ],
                  ),
                ),
              ),
            ),
            _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
          ],
        ),
      ),
    );
  }

  GlobalKey _toolTipKey = GlobalKey();
  Container goodLuckText() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Column(
        children: [
          dataTour == null || dataTour['status_turnamen'] == "sudah" ? Container() : InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => PairingGames(fromRequestPairingChangeInStartGame: true, data: widget.status, id: widget.id, userId: dataStart['user']['id'],)));
            },
            child: Text(
              "Request Pairing Change",
              style: TextStyle(color: Color(0xFFF15411)),
            ),
          ),
          dataStart==null||widget.forCaddie!=null||dataStart['represent']=="ada"/* || dataTour['status_turnamen'] == "sudah"*/? Container() : Padding(
            padding: EdgeInsets.only(top: 10),
            child: Stack(
              children: [
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SearchRepresent(findLevel: "Caddie", dataTournament: dataTour,)));
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.bogies,
                          borderRadius: BorderRadius.circular(5)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 236, minHeight: 36),
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.search_rounded, color: Colors.white,),
                              Text("Search Caddie", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,),
                            ],
                          ))),
                ),
                Positioned(top: 0, right: 0, child: GestureDetector(
                  onTap: (){
                    final dynamic tooltip = _toolTipKey.currentState;
                    tooltip.ensureTooltipVisible();
                  },
                  child: Tooltip(
                    key: _toolTipKey,
                    preferBelow: false,
                    message: "You have no caddie for this game yet.",
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: Icon(Icons.error_outlined, size: 27, color: AppTheme.gRed,),),
                  ),
                ))
              ],
            ),
          ),
          SizedBox(height: 10,),
          Text(
            "${dataTour==null?"":dataTour['status_turnamen']=="sudah"?"Goodluck ${dataStart == null ? "":dataStart['user']['nama_lengkap']}...\nand have a lot of fun...":"The tournament hasnt started yet"}",
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Theme(
              data: ThemeData(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
              ),
              child: RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  if(dataTour == null ? false : dataTour['status_turnamen'] == "sudah") {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                            ScorePage(
                              dataId: dataTour==null?0:dataTour['id_turnamen'],
                              dataSG: dataStart,
                              forCaddie: widget.forCaddie,
                              idRepresentCaddie: widget.idRepresentCaddie,
                            )));
                  }
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: dataTour==null?Colors.black:dataTour['status_turnamen']=="sudah"?AppTheme.gButton:AppTheme.gGrey,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: Text("Input Score", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container myPairing() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: 1,
          itemBuilder: (context, index) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 7.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: AppTheme.gFont),
                          child: Container(
                            width: 28,
                            height: 28,
                            padding: EdgeInsets.only(top:3),
                            child: Text(
                              "${dataStart == null?"":dataStart['turnamen_pairing']['pairing_id']}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text("Hole 1",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Color(0xFF4F4F4F)))),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    height: 80,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        primary: false,
                        itemCount: dataPairing == null ? 0 : dataPairing.length,
                        itemBuilder: (context, index2) {
                          return InkWell(
                            onTap: ()async{
                              await showDialog(
                                  context: snackbarKey.currentContext,
                                  builder: (BuildContext context) => NamePopUp(
                                    name: "${dataPairing==null?"":dataPairing[index2]['nama_pendek']!=null?dataPairing[index2]['nama_pendek']+", ":""}${dataPairing==null?"":dataPairing[index2]['nama_lengkap']}",
                                    photo: dataPairing[index2]['foto'] == null ?
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10.0),
                                      child: Image.asset("assets/images/userImage.png",
                                        height: 120, width: 120,
                                      ),
                                    ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) => CircularProgressIndicator(
                                      value: progress.progress,),
                                      imageUrl: Utils.url + 'upload/user/' + dataPairing[index2]['foto'], height: 120, width: 120, fit: BoxFit.cover,)),
                                  )
                              );
                            },
                            child: Container(
                              // width: 70,
                              width: SizeConfig.screenWidth/5.2,
                              // padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left: 18) : EdgeInsets.only(left: 10),
                              padding: EdgeInsets.only(left: 18),
                              child: Wrap(
                                children: [
                                  Padding(
                                    // padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left :0.0) : EdgeInsets.only(left :8.0),
                                    padding: EdgeInsets.only(left: 0.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                            padding: EdgeInsets.only(top: 5),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                            ),
                                            child:
                                                dataPairing[index2]['foto'] == null ?
                                                ClipRRect(
                                                  borderRadius: BorderRadius.circular(10000.0),
                                                  child: Image.asset(
                                              "assets/images/userImage.png",
                                              width: 50,
                                              height: 50,
                                            ),
                                                )
                                            : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                                      CircularProgressIndicator(
                                                        value: progress.progress,
                                                      ), imageUrl: Utils.url + 'upload/user/' + dataPairing[index2]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                                            ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 3.0),
                                          child: Center(
                                            child: Text(
                                              "${dataPairing[index2]['nama_pendek']!=null?dataPairing[index2]['nama_pendek']:dataPairing[index2]['nama_lengkap']}",
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyle(color: dataPairing[index2]['warna_nama'] == 'ada' ? AppTheme.bogies : Color(0xFF4F4F4F)),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                )
              ],
            );
          }),
    );
  }

  SafeArea infoTournament() {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(15)),
            child: Align(
                alignment: Alignment.center,
                child: _isLoading2
                    ? Padding(
                        padding: const EdgeInsets.all(45.0),
                        child: Center(
                            child: Container(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(),
                        )),
                      )
                    : Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              "${dataTour == null ? "" : dataTour['nama_turnamen']}",
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text(
                                "Organized by ${dataTour == null ? "" : dataTour['penyelenggara_turnamen']}",
                                style: TextStyle(color: AppTheme.gFont)),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 25, bottom: 5),
                            child: Text(
                              "${dataTour == null ? "" : dataTour['nama_lapangan']}, ${dataTour == null ? "" : dataTour['kota_turnamen']}",
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Text(
                            "${dataTour == null ? "" : myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))}",
                            style: TextStyle(color: AppTheme.gFont),
                            textAlign: TextAlign.center,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 25),
                            child: Text(
                              "${dataTour == null ? "" : dataTour['partisipan_turnamen']} - ${dataTour == null ? "" : dataTour['tipe_turnamen']}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Color(0xFF4F4F4F)),
                            ),
                          ),
                        ],
                      )),
          ),
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.symmetric(horizontal: 65),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: AppTheme.gFont,
            ),
            child: Text(
                    "Tournament Info",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
          ),
        ],
      ),
    );
  }

  Container textPairingGolfer() {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xFFDADADA)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              "Pairing ID/\nStart Hole",
              style: TextStyle(color: AppTheme.gFont),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              // padding: EdgeInsets.only(left: 20, right: 10),
              padding: EdgeInsets.only(right: SizeConfig.screenWidth/15, left: 10),
              // width: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Golfers",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "1",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "2",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "3",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "4",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

}

class NamePopUp extends StatelessWidget {
  NamePopUp({
    Key key, this.name, this.photo
  }) : super(key: key);

  String name;
  Widget photo;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Column(
        children: [
          photo,
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(name, style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
          ),
        ],
      )),
      backgroundColor: Colors.black.withOpacity(0.75),
    );
  }
}
