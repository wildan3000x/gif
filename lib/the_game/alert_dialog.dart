import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/the_game/scoring_page.dart';
import 'package:gif/utils/Utils.dart';

class EntryDialog extends StatefulWidget {
  final String hole, mToken, userID, tourID, mScore, tokenCaddie, idRepresentCaddie;
  EntryDialog({
    @required this.hole, this.mToken, this.userID, this.tourID, this.mScore, this.tokenCaddie, this.idRepresentCaddie
  });

  @override
  _EntryDialogState createState() => _EntryDialogState();
}

class _EntryDialogState extends State<EntryDialog> {
  bool _loading = false;

  @override
  void initState() {
    hole = int.parse(widget.hole);
    stroke = int.parse(widget.mScore);
    super.initState();
  }

  int hole;
  int stroke = 1;
  TextEditingController _holeCon = TextEditingController();
  TextEditingController _strokeCon = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(context) {
    return Container(
      width: double.maxFinite,
      height: 190,
      alignment: Alignment.center,
      // padding: EdgeInsets.only(top: 15, bottom: 15),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          color: Color(0xFF2554BC).withOpacity(0.7),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: Text(
              "ENTRY SCORE",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              textAlign: TextAlign.center,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top:15.0, right: 5),
                    child: InkWell(
                        onTap: (){
                          setState(() {
                            if (hole > 1 && hole <= 99) {
                              hole -= 1;
                            }
                            // hole -= 1;
                            _holeCon.text = hole.toString();
                          });
                        },
                        child: SvgPicture.asset("assets/svg/entry_arrow_left.svg")),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Hole #", style: TextStyle(color: Colors.white, ), textAlign: TextAlign.center,),
                      Container(
                        width: 60,
                        height: 50,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xFFF15411),
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: TextFormField(
                          controller: _holeCon,
                          onChanged: (value){
                            hole = int.parse(value);
                          },
                          inputFormatters: [LengthLimitingTextInputFormatter(2)],
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 9),
                            hintText: "$hole",
                            hintStyle: TextStyle(color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold)
                          ),
                          style: TextStyle(
                              color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(top:15.0, left: 5),
                    child: InkWell(
                        onTap: (){
                          setState(() {
                            if (hole >= 1 && hole < 99) {
                              hole += 1;
                            }
                            _holeCon.text = hole.toString();
                          });
                        },
                        child: SvgPicture.asset("assets/svg/entry_arrow_right.svg")),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top:15.0, right: 5),
                    child: InkWell(
                      onTap: (){
                        setState(() {
                          if (stroke > 1 && stroke <= 99) {
                            stroke -= 1;
                          }
                          _strokeCon.text = stroke.toString();
                        });
                      },
                        child: SvgPicture.asset("assets/svg/entry_arrow_left.svg")),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Stroke", style: TextStyle(color: Colors.white, ), textAlign: TextAlign.center,),
                      Container(
                        width: 60,
                        height: 50,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: TextFormField(
                          controller: _strokeCon,
                          onChanged: (value) {
                            stroke = int.parse(value);
                          },
                          inputFormatters: [LengthLimitingTextInputFormatter(2)],
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(bottom: 9),
                              hintText: "$stroke",
                              hintStyle: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.black)
                          ),
                          style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:15.0, left: 5),
                    child: InkWell(
                        onTap: (){
                          setState(() {
                            // stroke += 1;
                            if (stroke >= 1 && stroke < 99) {
                              stroke += 1;
                            }
                            _strokeCon.text = stroke.toString();
                          });
                        },
                        child: SvgPicture.asset("assets/svg/entry_arrow_right.svg")),
                  )
                ],
              )
            ],
          ),
          Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: InkWell(
              onTap: (){
                setState(() {
                  _loading = true;
                  postScore();
                });
              },
              child: Container(
                margin: EdgeInsets.only(top:20),
                  width: 116,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: AppTheme.gFont,
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: Colors.white, width: 2)),
                  child: _loading ? Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      ))
                      : Text(
                    "Save",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                    textAlign: TextAlign.center,
                  )),
            ),
          )
        ],
      ),
    );
  }

  postScore() async {
    var data = {
      'token' : widget.mToken,
      'id_turnamen' : widget.tourID,
      'id_user' : widget.userID,
      'hole' : hole,
      'score' : stroke,
      'token_caddie' : widget.tokenCaddie,
      'id_represent' : widget.idRepresentCaddie
    };

    Utils.postAPI(data, "ES").then((body) {
      setState(() {
        _loading = false;
      });
      if(body['code'] == '200'){
        Navigator.pop(context, body["button"]);
        // Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(
        //       builder: (BuildContext context) => ScorePage(dataId: widget.tourID, dataSG: widget.dataSG,)),
        //   ModalRoute.withName('/'),
        // );
      }else{
        print("ERROR $body");
        Navigator.pop(context, body["message"]);
      }
    }, onError: (error) {
      setState(() {
        Utils.showToast(context, "warning", "Ops, something wrong, please try again later.");
        print("Error == $error");
        _loading = false;
      });
    });
  }
}




