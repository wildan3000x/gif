import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/size_config.dart';

class RemarkScore extends StatelessWidget {
  const RemarkScore({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Container(
      padding: EdgeInsets.only(top: 2),
      width: screenWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              width: screenWidth/6.6*1,
              child: Text("Remark", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold),)
          ),
          Container(
            width: screenWidth/6.6*5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(right: 1),
                  padding: EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: Color(0xFFF2C94C),
                      borderRadius: BorderRadius.circular(3)
                  ),
                ),
                Text("Eagle"),
                Container(
                  margin: EdgeInsets.only(left: 3, right: 1),
                  padding: EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: AppTheme.gFont,
                      borderRadius: BorderRadius.circular(3)
                  ),
                ),
                Text("Birdie"),
                Container(
                  margin: EdgeInsets.only(left: 3, right: 1),
                  padding: EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: Color(0xFF2D9CDB),
                      borderRadius: BorderRadius.circular(15)
                  ),
                ),
                Text("Par"),
                Container(
                  margin: EdgeInsets.only(left: 3, right: 1),
                  padding: EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      color: Color(0xFFF15411),
                      borderRadius: BorderRadius.circular(3)
                  ),
                ),
                Flexible(child: Text("3 Bogies/Worse", overflow: TextOverflow.ellipsis,)),
              ],
            ),
          )
        ],
      ),
    );
  }
}