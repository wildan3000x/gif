import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/leaderboard/all.dart';
import 'package:gif/the_game/leaderboard/final_leaderboard/final_leaderboard.dart';
import 'package:gif/the_game/leaderboard/per_pairing.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'input_skill_award/input_skill_award.dart';

class RunningLeaderBoards extends StatefulWidget {
  const RunningLeaderBoards({Key key, this.dataHeader, this.forCaddie}) : super (key: key);
  final dataHeader, forCaddie;
  @override
  _RunningLeaderBoardsState createState() => _RunningLeaderBoardsState();
}

class _RunningLeaderBoardsState extends State<RunningLeaderBoards> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var dataPairing;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading = false;
  bool _switchTabOne = true;
  bool _switchTabTwo = false;
  bool _switchTabThree = false;
  final GlobalKey _listOneKey = GlobalKey();
  final GlobalKey _listTwoKey = GlobalKey();
  final GlobalKey _listThreeKey = GlobalKey();
  final GlobalKey _listFourKey = GlobalKey();
  Size _listOneSize = Size.zero;
  Size _listTwoSize = Size.zero;
  Size _listThreeSize = Size.zero;
  Size _listFourSize = Size.zero;

  bool _firstLoading = false;

  @override
  void initState() {
    dataPairing = widget.dataHeader;
    setState(() {
      _firstLoading = true;
    });
    checkLoginStatus();
    super.initState();
  }

  bool _isLevelCaddie = false;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      if(sharedPreferences.getString("level") == "Caddie"){
        setState(() {
          _isLevelCaddie = true;
        });
      }
      getRunningLeaderboard();
    }
  }

  var dataRunning;
  var countRunning;
  var dataValid;
  bool _buttonActive = false;
  bool _SkillAward = false;

  getRunningLeaderboard() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': dataPairing['turnamen']['id_turnamen'],
      'aksi': _switchTabOne ? 'All' : _switchTabThree ? 'Skill Awards' : ''
    };

    Utils.postAPI(data, "get_RL").then((body) {
      if (body['status'] == 'success') {
         dataRunning = body['data'];
         countRunning = body['user_count'];
         dataValid = body;
         setState(() {
           if(body['tombol'] == 'aktif'){
             _buttonActive = true;
           }else{
             _buttonActive = false;
           }
           if(body['status_hio']=='ada' && body['status_ntp']=='ada'){
             _SkillAward = true;
           }
           _isLoading = false;
           _firstLoading = false;
           if(!_switchTabThree){
             WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
           }
         });
      }else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  getSizeAndPosition() {
    RenderBox _cardBox1 = _listOneKey.currentContext.findRenderObject();
    _listOneSize = _cardBox1.size;
    RenderBox _cardBox2 = _listTwoKey.currentContext.findRenderObject();
    _listTwoSize = _cardBox2.size;
    RenderBox _cardBox3 = _listThreeKey.currentContext.findRenderObject();
    _listThreeSize = _cardBox3.size;
    RenderBox _cardBox4 = _listFourKey.currentContext.findRenderObject();
    _listFourSize = _cardBox4.size;
    setState(() {});
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getRunningLeaderboard();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getRunningLeaderboard();
    if(mounted)
      setState(() {
        getRunningLeaderboard();
      });
    _refreshController.loadComplete();
  }

  TextEditingController info = TextEditingController();
  bool _isLoadingAction = false;
  void pauseMatch(){
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_turnamen': dataPairing['turnamen']['id_turnamen'],
      'alasan': info.text
    };

    Utils.postAPI(data, "alasan_jeda").then((body) {
      if(body['status'] == 'success'){
        var snackbar = SnackBar(
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.blueAccent,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        setState(() {
          _isLoadingAction = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        setState(() {
          _isLoadingAction = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingAction = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading || _firstLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: /*_firstLoading
                    ? Center(
                    heightFactor: 25,
                    child: Container(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(),
                    )) : */Column(
                  children: [
                    HeaderScoring(dataPairing: dataPairing, myFormat: myFormat, isCaddie: _isLevelCaddie ? true : null,),
                    onlyTextInTop(),
                    Column(
                      children: [
                        buttonSwitch(),
                        Container(
                          width: double.maxFinite,
                          padding: EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              _switchTabThree ? Container() : Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(flex: 0, child: Text("#", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFont,), textAlign: TextAlign.center,)),
                                  Expanded( flex: 2, child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text("Name", key: _listOneKey,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFont,), textAlign: TextAlign.center,),
                                  )),
                                  Expanded(
                                    flex: 1,
                                    child: Center(child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: _switchTabTwo ? Color(0xFFF15411) : AppTheme.gFont,), textAlign: TextAlign.center,)),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    key: _listFourKey,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Text("Thru", key: _listTwoKey,
                                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFont,), textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Text("Stroke", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFont,), textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Expanded(key: _listThreeKey, flex: 0, child: Container(
                                      padding: EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.1),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(5),
                                              topRight: Radius.circular(5)
                                          )
                                      ),
                                      child: Text("Score", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Color(0xFFF15411),), textAlign: TextAlign.center,))),
                                ],
                              ),
                              _switchTabThree ? Container() : Divider(
                                color: Color(0xFF828282),
                                thickness: 1.5,
                                height: 2,
                              ),
                              _switchTabThree ? Container() : _switchTabOne ? _isLoading
                                  ? Container()/*Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 30),
                                    width: 20,
                                    height: 20,
                                    child: CircularProgressIndicator(),
                                  ))*/ : TabAllPairing(dataRunning: dataRunning, listOneSize: _listOneSize, listThreeSize: _listThreeSize, listTwoSize: _listTwoSize) : _isLoading
                                  ? /*Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 30),
                                    width: 20,
                                    height: 20,
                                    child: CircularProgressIndicator(),
                                  ))*/Container() : TabPerPairing(listOneSize: _listOneSize, listThreeSize: _listThreeSize, listFourSize: _listFourSize, dataRunning: dataRunning, count: countRunning),
                              _switchTabThree ? _isLoading
                                  ? Container()/*Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 30),
                                    width: 20,
                                    height: 20,
                                    child: CircularProgressIndicator(),
                                  ))*/ : Column(
                                children: [
                                  skillAwards('hio'),
                                  SizedBox(height: 15,),
                                  skillAwards('ntp'),
                                  dataRunning == null || dataRunning['ntl'].isEmpty ? Container() : Column(
                                    children: [
                                      SizedBox(height: 15,),
                                      skillAwards('ntl'),
                                    ],
                                  ),
                                  dataRunning == null || dataRunning['ld'].isEmpty ? Container() : Column(
                                    children: [
                                      SizedBox(height: 15,),
                                      skillAwards('ld'),
                                    ],
                                  ),
                                ],
                              ) : Container()
                            ],
                          ),
                        ),
                      ],
                    ),
                    Text("tap and hold golfers name to see their full name", style: TextStyle(color: Colors.grey, fontSize: 12, fontStyle: FontStyle.italic),),
                    dataValid == null || dataValid['status_turnamen'] == 'Selesai' || dataValid['tombol_jeda'] == 'tidak aktif'
                        ? Container()
                        : Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Column(
                      children: [
                          Stack(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(15)),
                                child: Column(
                                  children: [
                                    SizedBox(height: 5,),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.black
                                          ),
                                          child: Text("?", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),),
                                        ),
                                        Expanded(child: Padding(
                                          padding: const EdgeInsets.only(top:3.0),
                                          child: Text("If something happen during the tournament, you can inform and pause the match here.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
                                        )),
                                      ],
                                    ),
                                    SizedBox(height: 5,),
                                    Container(
                                      width: 250,
                                      child: TextField(
                                        maxLines: 5,
                                        controller: info,
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          hintText: 'Enter information.',
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 10,),
                                    RaisedButton(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                      splashColor: Colors.grey.withOpacity(0.5),
                                      onPressed: () {
                                        setState(() {
                                          _isLoadingAction = true;
                                        });
                                        pauseMatch();
                                        // widget.function;
                                        // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                                      },
                                      padding: const EdgeInsets.all(0.0),
                                      child: Ink(
                                          decoration: BoxDecoration(
                                              color: AppTheme.gButton,
                                              borderRadius: BorderRadius.circular(100)
                                          ),
                                          child: Container(
                                              constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                                              alignment: Alignment.center,
                                              child: _isLoadingAction ? Center(child: Container(width: 15, height: 15, child: CircularProgressIndicator(),),) : Text("Submit and Pause", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                    ),
                                    SizedBox(height: 8,),
                                    dataValid['tombol_input_score']=="aktif"?RaisedButton(
                                      splashColor: Colors.grey.withOpacity(0.3),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30)),
                                      color: AppTheme.par,
                                      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => InputSkillAward(dataPair: dataPairing,))),
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        width: 250,
                                        child: Text(
                                          "Input Scores for Skill Award",
                                          style: TextStyle(
                                              color: AppTheme.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ):Container(),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.symmetric(horizontal: 90),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Color(0xFFF15411),
                                ),
                                child: Text(
                                  "Organizer Tab",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8,),
                      ],
                    ),
                        ),
                    _buttonActive ? Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Column(
                        children: [
                          Text("The game has ended, you can view the final leaderboards here!", textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF4f4f4f), fontSize: 13),),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0, bottom: 20),
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                              splashColor: Colors.grey.withOpacity(0.5),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => FinalLeaderBoards(dataHeader: dataPairing,)));
                              },
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                  decoration: BoxDecoration(
                                      color: Color(0xFF2D9CDB),
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: Container(
                                      constraints: const BoxConstraints(maxWidth: 166, minHeight: 51),
                                      alignment: Alignment.center,
                                      child: Text("Final Leader Boards", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
                            ),
                          ),
                        ],
                      ),
                    ) : Container(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column skillAwards([String s]) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(5),
          width: 180,
          decoration: BoxDecoration(
              color: s=='hio'?Color(0xFFF15411):s=='ntp'?Color(0xFFF2C94C):s=='ntl'?Color(0xFF2D9CDB):AppTheme.gButton,
              borderRadius: BorderRadius.circular(15)
          ),
          child: Center(child: Text("${s=='hio'?"Hole In One":s=='ntp'?"Nearest To The Pin":s=='ntl'?"Nearest To The Line":"Longest Drive"}", style: TextStyle(color: Colors.white),),),
        ),
        Container(
          padding: EdgeInsets.only(top: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(flex: 1, child: Text("#Hole", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
              Expanded( flex: 2, child: Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text("Name", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
              )),
              Expanded(
                flex: 1,
                child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
              ),
              s == 'hio' ? Container() : Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text("Distance", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: Color(0xFF828282),
          thickness: 1,
          height: 2,
        ),
        SizedBox(height: 4,),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          physics: NeverScrollableScrollPhysics(),
          itemCount: dataRunning[s].length,
          itemBuilder: (context, i){
            return Padding(
              padding: const EdgeInsets.only(bottom: 3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(flex: 1, child: Text("${dataRunning[s][i]['hole']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)),
                  Expanded( flex: 2, child: Tooltip(
                    message: dataRunning[s][i]['nama']==""?"No winner":dataRunning[s][i]['nama'],
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Text("${dataRunning==null?"":dataRunning[s][i]['nama']==""?"No winner":dataRunning[s][i]['nama1']==null||dataRunning[s][i]['nama1']==""?dataRunning[s][i]['nama']:dataRunning[s][i]['nama1']+", "+dataRunning[s][i]['nama']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                    ),
                  )),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: Text("${dataRunning[s][i]['pair']==""?"0":dataRunning[s][i]['pair']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                    ),
                  ),
                  s == 'hio' ? Container() : Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Text("${dataRunning[s][i]['distance']==""?"0":dataRunning[s][i]['distance']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                    ),
                  ),
                ],
              ),
            );
          },)
      ],
    );
  }

  Container buttonSwitch() {
    return Container(
        width: _SkillAward ? 242 : 202,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xFFE5E5E5)),
          borderRadius: BorderRadius.circular(8)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  if(_switchTabOne == false){
                    _switchTabOne = true;
                    _switchTabTwo = false;
                    _switchTabThree = false;
                    getRunningLeaderboard();
                  }
                });
              },
              child: Container(
                padding: EdgeInsets.all(5),
                width: _SkillAward ? 80 : 100,
                decoration: BoxDecoration(
                    color: _switchTabOne ? AppTheme.gFont : Color(0xFFF9F9F9),
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8))
                ),
                child: Text("All", style: TextStyle(color: _switchTabOne ? Colors.white : AppTheme.gFont), textAlign: TextAlign.center,),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  if(_switchTabTwo == false){
                    _switchTabTwo = true;
                    _switchTabOne = false;
                    _switchTabThree = false;
                    getRunningLeaderboard();
                  }
                });
              },
              child: ClipRRect(
                borderRadius: BorderRadius.only(topRight: Radius.circular(_SkillAward ? 0 : 8), bottomRight: Radius.circular(_SkillAward ? 0 : 8)),
                child: Container(
                  padding: EdgeInsets.all(5),
                  width: _SkillAward ? 80 : 100,
                  decoration: BoxDecoration(
                      color: _switchTabTwo ? AppTheme.gFont : Color(0xFFF9F9F9),
                      // borderRadius: BorderRadius.only(topRight: Radius.circular(8), bottomRight: Radius.circular(8)),
                      border: Border(
                        right: BorderSide(color: Color(0xFFE5E5E5)),
                        left: BorderSide(color: Color(0xFFE5E5E5)),
                      )
                  ),
                  child: Text("Per Pairing", style: TextStyle(color: _switchTabTwo ? Colors.white : AppTheme.gFont), textAlign: TextAlign.center,),
                ),
              ),
            ),
            _SkillAward ? InkWell(
              onTap: () {
                setState(() {
                  if(_switchTabThree == false){
                    _switchTabTwo = false;
                    _switchTabOne = false;
                    _switchTabThree = true;
                    getRunningLeaderboard();
                  }
                });
              },
              child: Container(
                padding: EdgeInsets.all(5),
                width: 80,
                decoration: BoxDecoration(
                    color: _switchTabThree ? AppTheme.gFont : Color(0xFFF9F9F9),
                    borderRadius: BorderRadius.only(topRight: Radius.circular(8), bottomRight: Radius.circular(8))
                ),
                child: Text("Skill Award", style: TextStyle(color: _switchTabThree ? Colors.white : AppTheme.gFont), textAlign: TextAlign.center,),
              ),
            ) : Container(),
          ],
        ),
      );
  }

  Column onlyTextInTop() {
    return Column(
      children: [
        // Container(
        //   padding: EdgeInsets.only(top: 5, bottom: 15),
        //   child: Text(
        //     "Click tournament title for leaderboard and other info",
        //     style:
        //     TextStyle(fontSize: 12, color: Color(0xFF828282)),
        //   ),
        // ),
        Container(
          padding: EdgeInsets.only(bottom: 8, top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: InkWell(splashColor: Colors.grey.withOpacity(0.2), onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "Running Leaderboard",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF4F4F4F)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

}


