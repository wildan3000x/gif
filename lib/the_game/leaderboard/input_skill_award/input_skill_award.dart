import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/api/province.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InputSkillAward extends StatefulWidget {
  const InputSkillAward({Key key, this.dataPair}) : super(key: key);
  final dataPair;
  @override
  _InputSkillAwardState createState() => _InputSkillAwardState();
}

class _InputSkillAwardState extends State<InputSkillAward> {
  var myFormat = DateFormat('d MMM yyyy');
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  bool _isLoading = false;
  bool _isFirstLoading = false;
  bool _isLoadingPin = false;
  bool _isLoadingLine = false;
  bool _isLoadingDrive = false;
  bool _isLoadingHole = false;
  List<bool> _isArrLoading = [false, false, false];

  List<TextEditingController> names = List<TextEditingController>();
  var mapNameArrHoleOne = Map<String, String>();
  List<TextEditingController> pair = List<TextEditingController>();

  List<TextEditingController> nameArrToThePin = List<TextEditingController>();
  var mapNameArrToThePin = Map<String, String>();
  List<TextEditingController> pairArrToThePin = List<TextEditingController>();
  List<TextEditingController> distanceArrToThePin = List<TextEditingController>();

  List<TextEditingController> nameArrToTheLine = List<TextEditingController>();
  var mapNameArrToTheLine = Map<String, String>();
  List<TextEditingController> pairArrToTheLine = List<TextEditingController>();
  List<TextEditingController> distanceArrToTheLine = List<TextEditingController>();

  List<TextEditingController> nameArrLongestDrive = List<TextEditingController>();
  var mapNameArrLongestDrive = Map<String, String>();
  List<TextEditingController> pairArrLongestDrive = List<TextEditingController>();
  List<TextEditingController> distanceLongestDrive = List<TextEditingController>();

  List<int> arrHoleSelected = new List();
  List<String> arrNameSelected = new List();
  List<String> arrPairSelected = new List();
  List<String> arrDistanceSelected = new List();
  List<int> arrCertificateSelected = new List();
  List<int> arrTrophySelected = new List();
  List<String> arrPrizeSelected = new List();
  List<int> arrRankSelected = new List();


  // final GlobalKey _OneKey = GlobalKey();
  // final GlobalKey _TwoKey = GlobalKey();
  // Size _OneSize = Size.zero;
  // Size _TwoSize = Size.zero;
  //
  // getSizeAndPosition() {
  //   RenderBox _cardBox1 = _OneKey.currentContext.findRenderObject();
  //   _OneSize = _cardBox1.size;
  //   RenderBox _cardBox2 = _TwoKey.currentContext.findRenderObject();
  //   _TwoSize = _cardBox2.size;
  //   setState(() {});
  // }

  @override
  void initState() {
    setState(() {
      _isFirstLoading = true;
    });
    checkLoginStatus();
    // WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      getSkillAward();
    }
  }

  var dataAward;
  var dataUser;
  List<dynamic> _dataUser = List();
  final List<DropdownMenuItem> items = [];
  List<bool> checkValPin = [];
  List<bool> checkValLine = [];
  List<bool> checkValLong = [];
  List<bool> checkValHole = [];

  getSkillAward() async {
    // setState(() {
    //   _isLoading = true;
    // });
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataPair['turnamen']['id_turnamen']/*74*/,
    };

    Utils.postAPI(data, "get_SA").then((body) {
      if (body['status'] == 'success') {
        dataAward = body['data'];
        dataUser = body['data_user'];
        _dataUser = dataUser;
        for(int i = 0; i < dataUser.length; i++) {
          items.add(DropdownMenuItem(
            child: Text("${dataUser[i]['nama_lengkap']}"),
            value: dataUser[i]['nama_lengkap'],
          ));
        }
        setState(() {
          _isLoading = false;
          _isFirstLoading = false;
          for (int i = 0; i < dataAward['hole_in_one'].length; i++) {
            checkValHole.add(false);
            names.add(TextEditingController());
            pair.add(TextEditingController());
            names[i].text = dataAward['hole_in_one'][i]['name'];
            pair[i].text = dataAward['hole_in_one'][i]['pair'];
          }
          for (int i = 0; i < dataAward['nearest_to_the_pin'].length; i++) {
            checkValPin.add(false);
            nameArrToThePin.add(TextEditingController());
            pairArrToThePin.add(TextEditingController());
            distanceArrToThePin.add(TextEditingController());
            nameArrToThePin[i].text = dataAward['nearest_to_the_pin'][i]['name'];
            pairArrToThePin[i].text = dataAward['nearest_to_the_pin'][i]['pair'];
            distanceArrToThePin[i].text = dataAward['nearest_to_the_pin'][i]['distance'];
          }
          for (int i = 0; i < dataAward['nearest_to_the_line'].length; i++) {
            checkValLine.add(false);
            nameArrToTheLine.add(TextEditingController());
            pairArrToTheLine.add(TextEditingController());
            distanceArrToTheLine.add(TextEditingController());
            nameArrToTheLine[i].text = dataAward['nearest_to_the_line'][i]['name'];
            pairArrToTheLine[i].text = dataAward['nearest_to_the_line'][i]['pair'];
            distanceArrToTheLine[i].text = dataAward['nearest_to_the_line'][i]['distance'];
          }
          for (int i = 0; i < dataAward['longest_drive'].length; i++) {
            checkValLong.add(false);
            nameArrLongestDrive.add(TextEditingController());
            pairArrLongestDrive.add(TextEditingController());
            distanceLongestDrive.add(TextEditingController());
            nameArrLongestDrive[i].text = dataAward['longest_drive'][i]['name'];
            pairArrLongestDrive[i].text = dataAward['longest_drive'][i]['pair'];
            distanceLongestDrive[i].text = dataAward['longest_drive'][i]['distance'];
          }
        });
        try {
          List<dynamic> lPro = body["data_user"];
          for (int i = 0; i<lPro.length; i++) {
            mapNameArrHoleOne.putIfAbsent(lPro[i]["nama_lengkap"], () => lPro[i]["id"].toString());
            mapNameArrToThePin.putIfAbsent(lPro[i]["nama_lengkap"], () => lPro[i]["id"].toString());
            mapNameArrToTheLine.putIfAbsent(lPro[i]["nama_lengkap"], () => lPro[i]["id"].toString());
            mapNameArrLongestDrive.putIfAbsent(lPro[i]["nama_lengkap"], () => lPro[i]["id"].toString());
          }
          Golfer.showAll = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
          _isFirstLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isFirstLoading = false;
      });
    });
  }

  postSkillAward(String type) async {
    setState(() {
      _isLoading = true;
    });
    List<String> iDArrToThePin = List<String>();
    for (int i = 0; i<arrNameSelected.length; i++) {
      if (type == 'Nearest To The Pin') {
        // print("ARR ${nameArrToThePin[i].text}");
        // if (nameArrToThePin[i].text.isNotEmpty) { //Ketika TextField diisi
          if (mapNameArrToThePin.containsKey(nameArrToThePin[i].text)) {
            iDArrToThePin.add(mapNameArrToThePin[nameArrToThePin[i].text]);
          } else {
            iDArrToThePin.add("");
          }
        // }
      }else if(type == 'Nearest To The Line'){
        // if (nameArrToTheLine[i].text.isNotEmpty) { //Ketika TextField diisi
          if (mapNameArrToTheLine.containsKey(nameArrToTheLine[i].text)) {
            iDArrToThePin.add(mapNameArrToTheLine[nameArrToTheLine[i].text]);
          } else {
            iDArrToThePin.add("0");
          }
        // }
      }else if(type == 'Hole In One'){
        // if (names[i].text.isNotEmpty) { //Ketika TextField diisi
          if (mapNameArrHoleOne.containsKey(names[i].text)) {
            iDArrToThePin.add(mapNameArrHoleOne[names[i].text]);
          } else {
            iDArrToThePin.add("0");
          }
        // }
      }else{
        // if (nameArrLongestDrive[i].text.isNotEmpty) { //Ketika TextField diisi
          if (mapNameArrLongestDrive.containsKey(nameArrLongestDrive[i].text)) {
            iDArrToThePin.add(mapNameArrLongestDrive[nameArrLongestDrive[i].text]);
          } else {
            iDArrToThePin.add("0");
          }
        // }
      }
    }
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataPair['turnamen']['id_turnamen']/*74*/,
      'tipe': type,
      'hole': arrHoleSelected,
      'user_id': iDArrToThePin,
      'pair': arrPairSelected,
      'sertifikat': arrCertificateSelected,
      'tropi': arrTrophySelected,
      'prize': arrPrizeSelected,
      'distance': arrDistanceSelected,
      'rank': arrRankSelected
    };
    // print("DATAAAA $data");

    Utils.postAPI(data, "add_TW").then((body) {
      setState(() {
        clearVar();
      });
      if (body['status'] == 'success') {
        // dataAward = body['data'];
        // getSkillAward();
        setState(() {
          if(_isLoadingDrive){
            _isLoadingDrive = true;
          }else if(_isLoadingLine){
            _isLoadingLine = true;
          }else if(_isLoadingHole){
            _isLoadingHole = true;
          }else{
            _isLoadingPin = true;
          }
        });
        if(dataAward != null || dataAward['nearest_to_the_line'].isNotEmpty && dataAward['longest_drive'].isNotEmpty
            && type == "Longest Drive"){
          Utils.showToast(context, "error", "${body['message']}");
        }else if(type == "Nearest To The Pin"){
          Utils.showToast(context, "error", "${body['message']}");
        }
        // var snackbar = SnackBar(
        //   content: Text(body['message'], style: TextStyle(color: Colors.white),),
        //   backgroundColor: Colors.blueAccent,
        // );
        // _scaffoldKey.currentState.showSnackBar(snackbar);
        // new Timer(const Duration(seconds: 1), onCloseDialog);
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => InputSkillAward(dataPair: widget.dataPair,)));
      }else {
        setState(() {
          if(_isLoadingDrive){
            _isLoadingDrive = false;
          }else if(_isLoadingLine){
            _isLoadingLine = false;
          }else if(_isLoadingHole){
            _isLoadingHole = false;
          }else{
            _isLoadingPin = false;
          }
        });
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      setState(() {
        clearVar();
      });
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        if(_isLoadingDrive){
          _isLoadingDrive = false;
        }else if(_isLoadingLine){
          _isLoadingLine = false;
        }else if(_isLoadingHole){
          _isLoadingHole = false;
        }else{
          _isLoadingPin = false;
        }
      });
    });
  }

  void clearVar() {
    arrNameSelected.clear();
    arrHoleSelected.clear();
    arrDistanceSelected.clear();
    arrPairSelected.clear();
    arrCertificateSelected.clear();
    arrPrizeSelected.clear();
    arrTrophySelected.clear();
    arrRankSelected.clear();
  }

  bool _isCheckDisPin = false;
  bool _isCheckDisLine = false;
  bool _isCheckDisLong = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(10),
                child: _isFirstLoading
                    ? Center(
                  heightFactor: 20,
                  child: Container(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                ) : Column(
                  children: [
                    HeaderScoring(
                      dataPairing: widget.dataPair,
                      myFormat: myFormat,
                      onClickable: false,
                      backButton: true,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Text("Input Winners of Skill Award", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 22),),
                    ),
                    Text("please fill the distance, otherwise the score award wont saved", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
                    holeInOne(),
                    SizedBox(height: 10,),
                    skillAwardWithDistance(category: 'nearest_to_the_pin'),
                    dataAward == null || dataAward['nearest_to_the_line'].isEmpty ? Container() : Column(
                      children: [
                        SizedBox(height: 10,),
                        skillAwardWithDistance(category: 'nearest_to_the_line'),
                      ],
                    ),
                    dataAward == null || dataAward['longest_drive'].isEmpty ? Container() : Column(
                      children: [
                        SizedBox(height: 10,),
                        skillAwardWithDistance(category: 'longest_drive'),
                      ],
                    ),
                    SizedBox(height: 15,),
                    RaisedButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                      splashColor: Colors.grey.withOpacity(0.5),
                      onPressed: () {
                        for (int i = 0; i < dataAward['hole_in_one'].length; i++) {
                          // if (names[i].text.isNotEmpty && pair[i].text.isNotEmpty) {
                            arrNameSelected.add(names[i].text);
                            arrPairSelected.add(pair[i].text);
                            arrHoleSelected.add(dataAward==null?"":dataAward['hole_in_one'][i]['hole']);
                            arrCertificateSelected.add(dataAward==null?"":dataAward['hole_in_one'][i]['sertifikat']=='centang'?1:0);
                            arrTrophySelected.add(dataAward==null?"":dataAward['hole_in_one'][i]['tropi']=='centang'?1:0);
                            arrPrizeSelected.add(dataAward==null?"":dataAward['hole_in_one'][i]['prize']);
                          // }
                          arrRankSelected.add(i+1);
                        }
                        setState(() {
                          // _isLoadingHole = true;
                          postSkillAward('Hole In One');
                          clearVar();
                        });
                        for (int i = 0; i < dataAward['nearest_to_the_pin'].length; i++) {
                          arrNameSelected.add(nameArrToThePin[i].text);
                          // if(pairArrToThePin[i].text.isNotEmpty && distanceArrToThePin[i].text.isNotEmpty) {
                            arrPairSelected.add(pairArrToThePin[i].text);
                            arrDistanceSelected.add(distanceArrToThePin[i].text);
                            arrHoleSelected.add(dataAward==null?0:dataAward['nearest_to_the_pin'][i]['hole']);
                            arrCertificateSelected.add(dataAward==null?0:dataAward['nearest_to_the_pin'][i]['sertifikat']=='centang'?1:0);
                            arrTrophySelected.add(dataAward==null?0:dataAward['nearest_to_the_pin'][i]['tropi']=='centang'?1:0);
                            arrPrizeSelected.add(dataAward==null?0:dataAward['nearest_to_the_pin'][i]['prize']);
                          // }
                          arrRankSelected.add(i+1);
                        }
                        setState(() {
                          // _isLoadingPin = true;
                          postSkillAward('Nearest To The Pin');
                          clearVar();
                        });
                        for (int i = 0; i < dataAward['nearest_to_the_line'].length; i++) {
                          arrNameSelected.add(nameArrToTheLine[i].text);
                          // if(pairArrToTheLine[i].text.isNotEmpty && distanceArrToTheLine[i].text.isNotEmpty){
                            arrPairSelected.add(pairArrToTheLine[i].text);
                            arrDistanceSelected.add(distanceArrToTheLine[i].text);
                            arrHoleSelected.add(dataAward==null?0:dataAward['nearest_to_the_line'][i]['hole']);
                            arrCertificateSelected.add(dataAward==null?0:dataAward['nearest_to_the_line'][i]['sertifikat']=='centang'?1:0);
                            arrTrophySelected.add(dataAward==null?0:dataAward['nearest_to_the_line'][i]['tropi']=='centang'?1:0);
                            arrPrizeSelected.add(dataAward==null?0:dataAward['nearest_to_the_line'][i]['prize']);
                          // }
                          arrRankSelected.add(i+1);
                        }
                        setState(() {
                          // _isLoadingLine = true;
                          if(dataAward == null || dataAward['nearest_to_the_line'].isEmpty){

                          }else{
                            postSkillAward('Nearest To The Line');
                          }
                          clearVar();
                        });
                        for (int i = 0; i < dataAward['longest_drive'].length; i++) {
                          arrNameSelected.add(nameArrLongestDrive[i].text);
                          // if(pairArrLongestDrive[i].text.isNotEmpty && distanceLongestDrive[i].text.isNotEmpty){
                            arrPairSelected.add(pairArrLongestDrive[i].text);
                            arrDistanceSelected.add(distanceLongestDrive[i].text);
                            // setState(() {
                            //     if(nameArrLongestDrive[i].text.isNotEmpty && distanceLongestDrive[i].text.isEmpty){
                            //       _isCheckDisLong = true;
                            //       Utils.showToast(context, "error", "SET THE DISTANCE ON LONGEST DRIVE");
                            //     }else{
                            //       _isCheckDisLong = false;
                            //       arrDistanceSelected.add(distanceLongestDrive[i].text);
                            //     }
                            // });
                            arrHoleSelected.add(dataAward==null?0:dataAward['longest_drive'][i]['hole']);
                            arrCertificateSelected.add(dataAward==null?0:dataAward['longest_drive'][i]['sertifikat']=='centang'?1:0);
                            arrTrophySelected.add(dataAward==null?0:dataAward['longest_drive'][i]['tropi']=='centang'?1:0);
                            arrPrizeSelected.add(dataAward==null?0:dataAward['longest_drive'][i]['prize']);
                          // }
                          arrRankSelected.add(i+1);
                        }
                        setState(() {
                          // _isLoadingDrive = true;
                          if(dataAward == null || dataAward['longest_drive'].isEmpty){

                          }else{
                            postSkillAward('Longest Drive');
                          }
                          clearVar();
                        });
                        // Navigator.pop(context);
                      },
                      padding: const EdgeInsets.all(0.0),
                      child: Ink(
                          decoration: BoxDecoration(
                              color: AppTheme.gButton,
                              borderRadius: BorderRadius.circular(100)
                          ),
                          child: Container(
                              constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                              alignment: Alignment.center,
                              child: Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
                    ),
                    SizedBox(height: 20,)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container holeInOne() {
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: [
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        flex: 0,
                        child: Center(
                          child: Text(
                            "#Hole",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Text(
                            "Name",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                            "Pair",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  // height: (dataAward==null?0:dataAward['hole_in_one'].length).toDouble()*50,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: dataAward==null?0:dataAward['hole_in_one'].length,
                    itemBuilder: (context, i) {
                      names.add(TextEditingController());
                      pair.add(TextEditingController());
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(width: 30, child: Center(child: Text("# ${dataAward==null?"":dataAward['hole_in_one'][i]['hole']}"))),
                          Expanded(
                            flex: 2,
                            child: SearchableDropdown.single(
                              items: _dataUser.map((data) {
                                return (DropdownMenuItem(
                                  child: Text("${data['nama_lengkap']}"),
                                  value: data,
                                ));
                              }).toList(),
                              value: names[i].text,
                              hint: names[i].text.isEmpty ? "No winner" : names[i].text,
                              searchHint: null,
                              onChanged: (value) {
                                if(value != null){
                                  names[i].text = value['nama_lengkap'];
                                  pair[i].text = value['pairing_id'];
                                  setState(() {
                                    checkValHole[i] = true;
                                  });
                                }
                              },
                              dialogBox: true,
                              isExpanded: true,
                              displayClearIcon: checkValHole[i] == false ? false : true,
                              iconSize: 30,
                              clearIcon: Icon(Icons.clear, size: 20,),
                              underline: "",
                              onClear: () {
                                setState(() {
                                  names[i].text = "";
                                  pair[i].text = "";
                                });
                              },
                              menuBackgroundColor: Color(0xFFe8fcf9),
                              // menuConstraints: BoxConstraints.tight(Size.fromHeight(350)),
                            ),
                          ),
                          checkValHole[i] == false ? InkWell(
                            onTap: (){
                              setState(() {
                                names[i].text = "";
                                pair[i].text = "";
                              });
                            },
                            child: Icon(Icons.clear, size: 20,),
                          ) : Container(),
                          SizedBox(width: 5,),
                          Expanded(
                            flex: 1,
                            child: Container(height: 40, padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 10),
                              child: TextFormField(
                                controller: pair[i],
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFFDADADA))),
                                  enabled: false,
                                  // disabledBorder: InputBorder.none
                                ),
                                textAlign: TextAlign.center,
                              ),),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            margin: EdgeInsets.symmetric(horizontal: 90),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Color(0xFFF15411),
            ),
            child: Text(
              "Hole In One",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget skillAwardWithDistance({String category}){
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: [
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        flex: 0,
                        child: Center(
                          child: Text(
                            "#Hole",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Text(
                            "Name",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                            "Pair",
                            style: TextStyle(color: AppTheme.gFont),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(left:4.0),
                            child: Text(
                              "Distance",
                              style: TextStyle(color: AppTheme.gFont),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: dataAward==null?0:dataAward[category].length,
                  itemBuilder: (context, i) {
                    if(category == 'nearest_to_the_pin'){
                      nameArrToThePin.add(TextEditingController());
                      pairArrToThePin.add(TextEditingController());
                      distanceArrToThePin.add(TextEditingController());
                      // if(nameArrToThePin[i].text.isEmpty){
                      //   nameArrToThePin[i].text = "";
                      // }
                    }else if(category == 'nearest_to_the_line'){
                      nameArrToTheLine.add(TextEditingController());
                      pairArrToTheLine.add(TextEditingController());
                      distanceArrToTheLine.add(TextEditingController());
                      // if(nameArrToTheLine[i].text.isEmpty){
                      //   nameArrToTheLine[i].text = _dataUser[0]['nama_lengkap'];
                      // }
                    }else{
                      nameArrLongestDrive.add(TextEditingController());
                      pairArrLongestDrive.add(TextEditingController());
                      distanceLongestDrive.add(TextEditingController());
                      // if(nameArrLongestDrive[i].text.isEmpty){
                      //   nameArrLongestDrive[i].text = _dataUser[0]['nama_lengkap'];
                      // }
                    }
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(width: 30, child: Center(child: Text("# ${dataAward==null?"":dataAward[category][i]['hole']}"))),
                        Expanded(
                          flex: 2,
                          child:
                          SearchableDropdown.single(
                            items: _dataUser.map((data) {
                              return (DropdownMenuItem(
                                child: Text("${data['nama_lengkap']}"),
                                value: data,
                              ));
                            }).toList(),
                            value: category == "nearest_to_the_line" ?
                            nameArrToTheLine[i].text.isEmpty ? "No Winner" : nameArrToTheLine[i].text :
                            category == "nearest_to_the_pin" ?
                            nameArrToThePin[i].text.isEmpty ? "No Winner" : nameArrToThePin[i].text :
                            nameArrLongestDrive[i].text.isEmpty ? "No Winner" : nameArrLongestDrive[i].text,
                            hint:
                            category == "nearest_to_the_line" ?
                            nameArrToTheLine[i].text.isEmpty ? "No Winner" : nameArrToTheLine[i].text :
                            category == "nearest_to_the_pin" ?
                            nameArrToThePin[i].text.isEmpty ? "No Winner" : nameArrToThePin[i].text :
                            nameArrLongestDrive[i].text.isEmpty ? "No Winner" : nameArrLongestDrive[i].text,
                            onChanged: (value) {
                              if(value != null) {
                                if(category == "nearest_to_the_line"){
                                  pairArrToTheLine[i].text = value['pairing_id'];
                                  nameArrToTheLine[i].text = value['nama_lengkap'];
                                  setState(() {
                                    checkValLine[i] = true;
                                  });
                                }else if(category == "nearest_to_the_pin"){
                                  pairArrToThePin[i].text = value['pairing_id'];
                                  nameArrToThePin[i].text = value['nama_lengkap'];
                                  setState(() {
                                    checkValPin[i] = true;
                                  });
                                }else{
                                  pairArrLongestDrive[i].text = value['pairing_id'];
                                  nameArrLongestDrive[i].text = value['nama_lengkap'];
                                  setState(() {
                                    checkValLong[i] = true;
                                  });
                                }
                              };
                            },
                            dialogBox: true,
                            isExpanded: true,
                            displayClearIcon: category == "nearest_to_the_pin" && checkValPin[i] == false
                                || category == "nearest_to_the_line" && checkValLine[i] == false
                                || category == "longest_drive" && checkValLong[i] == false ? false : true,
                            iconSize: 30,
                            clearIcon: Icon(Icons.close, size: 20,),
                            underline: "",
                            onClear: () {
                              setState(() {
                                if(category == "nearest_to_the_line"){
                                  pairArrToTheLine[i].text = "";
                                  nameArrToTheLine[i].text = "";
                                  distanceArrToTheLine[i].text = "";
                                }else if(category == "nearest_to_the_pin"){
                                  pairArrToThePin[i].text = "";
                                  nameArrToThePin[i].text = "";
                                  distanceArrToThePin[i].text = "";
                                }else{
                                  pairArrLongestDrive[i].text = "";
                                  nameArrLongestDrive[i].text = "";
                                  distanceLongestDrive[i].text = "";
                                }
                              });
                            },
                            menuBackgroundColor: Color(0xFFe8fcf9),
                            // menuConstraints: BoxConstraints.tight(Size.fromHeight(350)),
                          ),
                        ),
                        category == "nearest_to_the_pin" && checkValPin[i] == false
                            || category == "nearest_to_the_line" && checkValLine[i] == false
                            || category == "longest_drive" && checkValLong[i] == false ? InkWell(
                            onTap: (){
                                setState(() {
                                  if(category == "nearest_to_the_line"){
                                    pairArrToTheLine[i].text = "";
                                    nameArrToTheLine[i].text = "";
                                    distanceArrToTheLine[i].text = "";
                                  }else if(category == "nearest_to_the_pin"){
                                    pairArrToThePin[i].text = "";
                                    nameArrToThePin[i].text = "";
                                    distanceArrToThePin[i].text = "";
                                  }else{
                                    pairArrLongestDrive[i].text = "";
                                    nameArrLongestDrive[i].text = "";
                                    distanceLongestDrive[i].text = "";
                                  }
                                });
                            },
                            child: Icon(Icons.clear, size: 20,),
                        ) : Container(),
                        SizedBox(width: 5,),
                        Expanded(
                          flex: 1,
                          child: Container(height: 40, padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 10),
                            child: TextFormField(
                              controller: category == 'nearest_to_the_pin' ? pairArrToThePin[i] : category == 'nearest_to_the_line' ? pairArrToTheLine[i] : pairArrLongestDrive[i],
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xFFDADADA))),
                                enabled: false,
                              ),
                              textAlign: TextAlign.center,
                            ),),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(height: 40, padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 10, left: 8), child: TextFormField(
                            controller: category == 'nearest_to_the_pin' ? distanceArrToThePin[i] : category == 'nearest_to_the_line' ? distanceArrToTheLine[i] : distanceLongestDrive[i],
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xFFDADADA))),
                                // helperText: "cm",
                                hintText: category == 'longest_drive' ? "m" : "cm"
                            ),
                            keyboardType: TextInputType.number,
                            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                            textAlign: TextAlign.center,
                          ),),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            margin: EdgeInsets.symmetric(horizontal: 70),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: category=='nearest_to_the_line'?Color(0xFFF2C94C):category=='longest_drive'?Color(0xFF2D9CDB):AppTheme.gFont,
            ),
            child: Text(
              "${category=='nearest_to_the_line'?"Nearest To The Line":category=='longest_drive'?"Longest Drive":"Nearest To The Pin"}",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

}
