import 'package:flutter/material.dart';
import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/size_config.dart';

class TabAllPairing extends StatelessWidget {
  const TabAllPairing({
    Key key,
    @required this.dataRunning,
    @required Size listOneSize,
    @required Size listThreeSize,
    @required Size listTwoSize,
  }) : _listOneSize = listOneSize, _listThreeSize = listThreeSize, _listTwoSize = listTwoSize, super(key: key);

  final dataRunning;
  final Size _listOneSize;
  final Size _listThreeSize;
  final Size _listTwoSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      // height: SizeConfig.screenHeight/1.2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          LimitedBox(
            maxHeight: (dataRunning!=null?dataRunning['rank'].length:0).toDouble()*25+5,
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, i) {
                return Container(
                  width: i == 0 ? 25 : i == 1 ? _listOneSize.width : i == 5 ? _listThreeSize.width : _listTwoSize.width,
                  margin: EdgeInsets.only(left: i == 3 || i == 4 ? 5 : i == 5 ? 8 : /*i == 5 ? _listTwoSize.width/2.15 :*/ 0),
                  // padding: EdgeInsets.only(right: i == 1 ? 0 : 0, left: i == 1 ? 0 : 0),
                  padding: EdgeInsets.only(top: 3),
                  decoration: BoxDecoration(
                      color: i == 5 ? Colors.grey.withOpacity(0.1) : Colors.white
                  ),
                  child: ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: dataRunning!=null?dataRunning['rank'].length:0,
                    itemExtent: 25,
                    itemBuilder: (context, i2) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GolferProfileScore(
                                dataGolfer:dataRunning==null?"":dataRunning['token'][i2],
                                fromLeaderboards: true,
                              )
                          ));
                        },
                        child: i == 0 ? SizedBox(
                          width: 10,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text("${dataRunning==null?"":dataRunning["rank"][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14, color: Color(0xFF4f4f4f)),),
                            )) : i == 1 ? Padding(
                          padding: const EdgeInsets.only(top: 3.0, left: 5),
                          child: Tooltip(message: dataRunning==null?"":dataRunning['nama'][i2], child: Text("${dataRunning==null?"":dataRunning['nama_pendek'][i2] == null ? dataRunning['nama'][i2] : dataRunning['nama_pendek'][i2] + ", " + dataRunning['nama'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),)),
                        )
                            : i == 2 ? Center(child: Text("${dataRunning==null?"":dataRunning['pair'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),))
                            : i == 3 ? Center(child: Text("${dataRunning==null?"":dataRunning['thru'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),))
                            : i == 4 ? Center(child: Text("${dataRunning==null?"":dataRunning['stroke'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),))
                            : Center(child: Text("${dataRunning==null?"":dataRunning['score_plus'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),)),
                      );
                      },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
