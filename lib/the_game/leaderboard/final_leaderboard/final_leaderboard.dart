import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/leaderboard/winner_tournament/winner_tournament.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/share_sosmed.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FinalLeaderBoards extends StatefulWidget {
  const FinalLeaderBoards({Key key, this.dataHeader}) : super (key: key);
  final dataHeader;
  @override
  _FinalLeaderBoardsState createState() => _FinalLeaderBoardsState();
}

class _FinalLeaderBoardsState extends State<FinalLeaderBoards> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var dataPairing;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading = false;
  bool _switchTabOne = true;
  bool _switchTabTwo = false;
  bool _switchTabThree = false;
  final GlobalKey _listOneKey = GlobalKey();
  final GlobalKey _listTwoKey = GlobalKey();
  final GlobalKey _listThreeKey = GlobalKey();
  final GlobalKey _listKey1 = GlobalKey();
  final GlobalKey _listKey2 = GlobalKey();
  final GlobalKey _listKey3 = GlobalKey();
  Size _listOneSize = Size.zero;
  Size _listTwoSize = Size.zero;
  Size _listThreeSize = Size.zero;
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    dataPairing = widget.dataHeader;
    checkLoginStatus();
    super.initState();
  }

  String _token = "";

  bool _isLevelCaddie = false;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    _token = sharedPreferences.getString("token");
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      if(sharedPreferences.getString("level") == "Caddie"){
        setState(() {
          _isLevelCaddie = true;
        });
      }
      getFinalLeaderboard();
    }
  }

  var dataFinal;
  var countRunning;
  var statusFlight;
  var dataValid;

  getFinalLeaderboard() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': dataPairing['turnamen']['id_turnamen'],
      'aksi': _switchTabOne ? 'Gross' : _switchTabTwo ? 'Nett' : 'Flight'
    };

    Utils.postAPI(data, "get_TF").then((body) {
      if (body['status'] == 'success') {
        dataFinal = body['data'];
        countRunning = body['user_count'];
        dataValid = body;
        if(!_switchTabThree) {
          statusFlight = body['data']['status_flight'];
        }
        setState(() {
          if(body['tombol_close']=='tidak aktif'){
            _closed = true;
          }
          _isLoading = false;
        });
        // print("CCCC $statusFlight");
        setState(() {
          _isLoading = false;
          WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
        });
      }else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  getSizeAndPosition() {
    RenderBox _cardBox1 = _listOneKey.currentContext.findRenderObject();
    _listOneSize = _cardBox1.size;
    RenderBox _cardBox2 = _listTwoKey.currentContext.findRenderObject();
    _listTwoSize = _cardBox2.size;
    RenderBox _cardBox3 = _listThreeKey.currentContext.findRenderObject();
    _listThreeSize = _cardBox3.size;
    setState(() {});
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getFinalLeaderboard();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getFinalLeaderboard();
    if(mounted)
      setState(() {
        getFinalLeaderboard();
      });
    _refreshController.loadComplete();
  }

  bool _closed = false;
  bool _isLoadingClose = false;
  closeTournament() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataHeader['turnamen']['id_turnamen'],
    };

    Utils.postAPI(data, "UTS").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _closed = true;
          _isLoadingClose = false;
        });
        print("INFO CLOSE ${body['message']}");
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: ShareSosMed(
        screenShotController: screenshotController,
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Screenshot(
                controller: screenshotController,
                child: Container(
                  color: Colors.white,
                  child: Container(
                    margin: EdgeInsets.all(13),
                    child: Column(
                      children: [
                        HeaderScoring(dataPairing: dataPairing, myFormat: myFormat, isCaddie: _isLevelCaddie ? true : null,),
                        onlyTextInTop(),
                        dataValid==null?Container():dataValid['tombol']=='aktif'?Container(
                          child: Stack(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(15)),
                                child: dataValid==null?Container():dataValid['tombol']=='aktif'?_closed?Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("The Tournament has Closed", style: TextStyle(color: AppTheme.gRed, fontStyle: FontStyle.italic),),
                                      SizedBox(width: 3,),
                                      Icon(Icons.check, size: 20,)
                                    ],
                                  ),
                                ) : Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.black
                                          ),
                                          child: Text("?", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),),
                                        ),
                                        Expanded(child: Padding(
                                          padding: const EdgeInsets.only(top:3.0),
                                          child: Text("As an organizer, you can close the tournament by clicking the close button or the tournament will be automatically closed in 4 hours after the final results announced.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
                                        )),
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    RaisedButton(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                      splashColor: Colors.grey.withOpacity(0.5),
                                      onPressed: () {
                                        setState(() {
                                          _isLoadingClose = true;
                                        });
                                        closeTournament();
                                        // widget.function;
                                        // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                                      },
                                      padding: const EdgeInsets.all(0.0),
                                      child: Ink(
                                          decoration: BoxDecoration(
                                              color: AppTheme.gRed,
                                              borderRadius: BorderRadius.circular(100)
                                          ),
                                          child: Container(
                                              constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                                              alignment: Alignment.center,
                                              child: _isLoadingClose ? Center(child: Container(width: 15, height: 15, child: CircularProgressIndicator(),),) : Text("Close the Match", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                    ),
                                  ],
                                ) : Container(),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                margin: EdgeInsets.symmetric(horizontal: 90),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Color(0xFFF15411),
                                ),
                                child: Text(
                                  "Organizer Tab",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ) : Container(),
                        buttonSwitch(),
                        _switchTabThree ? dataFinal[0]==null ? Container() : Padding(
                          padding: const EdgeInsets.only(top: 10.0, left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.all(5),
                                width: 100,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: AppTheme.gFont
                                ),
                                child: Center(child: Text("Flight A", style: TextStyle(color: Colors.white),)),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Text("${dataFinal==null?"":dataFinal[0]['hc_flight']}", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18),),
                              )
                            ],
                          ),
                        ) : Container(),
                        Container(
                          width: double.maxFinite,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(flex: 0, child: Text("#", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
                                  Expanded( flex: 2, child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text("Name", key: _listOneKey, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                                  )),
                                  Expanded(
                                    flex: 1,
                                    child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(8),
                                              topLeft: Radius.circular(8),
                                            ),
                                            color: _switchTabOne ? Colors.grey.withOpacity(0.1) : Colors.white,
                                          ),
                                          child: Text("Gross", key: _listTwoKey, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Text("HC", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Expanded(key: _listThreeKey, flex: 0, child: Container(
                                      padding: EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(8),
                                            topLeft: Radius.circular(8)
                                        ),
                                        color: _switchTabTwo ? Colors.grey.withOpacity(0.1) : Colors.white,
                                      ),
                                      child: Text("Nett",  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center,))),
                                ],
                              ),
                              Divider(
                                color: Color(0xFF828282),
                                thickness: 1,
                                height: 2,
                              ),
                              _isLoading ? Container() : !_switchTabThree ? listFinalLeaderboards() :
                              Column(
                                children: [
                                  widgetFlight(0),
                                  dataFinal[1]['rank'].isNotEmpty ? flightHC(1) : noWinnerInThisFlight(1),
                                  dataFinal[1]['rank'].isNotEmpty ? widgetFlight(1) : Container(),
                                  dataFinal.length>2?Column(
                                    children: [
                                      dataFinal[2]['rank'].isNotEmpty ? flightHC(2) : noWinnerInThisFlight(2),
                                      dataFinal[2]['rank'].isNotEmpty ? widgetFlight(2) : Container()
                                    ],
                                  ):Container()
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8),
                                child: Text("tap and hold golfers name to see their full name", style: TextStyle(color: Colors.grey, fontSize: 12, fontStyle: FontStyle.italic),),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                splashColor: Colors.grey.withOpacity(0.5),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => TournamentWinners(dataHeader: dataPairing,)));
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                                },
                                padding: const EdgeInsets.all(0.0),
                                child: Ink(
                                    decoration: BoxDecoration(
                                        color: AppTheme.gButton,
                                        borderRadius: BorderRadius.circular(100)
                                    ),
                                    child: Container(
                                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                        alignment: Alignment.center,
                                        child: Text("Winners", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                              ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container noWinnerInThisFlight([int i]) {
    return Container(
      child: Column(
        children: [
          flightHC(i),
          Container(
            width: double.maxFinite,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                listColumn(),
                Divider(
                  color: Color(0xFF828282),
                  thickness: 1,
                  height: 2,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0/*, bottom: 8.0*/),
                  child: Text("no leaderboards in this flight"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Padding flightHC(int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, left: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(5),
            width: 100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: i == 0 ? AppTheme.gFont : i == 1 ? Color(0xFF2D9CDB) : Color(0xFFF3BF37)
            ),
            child: Center(child: Text("${i==0?"Flight A":i==1?"Flight B":"Flight C"}", style: TextStyle(color: Colors.white),)),
          ),
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Text("${dataFinal==null?"":dataFinal[i]['hc_flight']}", style: TextStyle(color: i == 0 ? AppTheme.gFont : i == 1 ? Color(0xFF2D9CDB) : Color(0xFFF3BF37), fontWeight: FontWeight.bold, fontSize: 18),),
          )
        ],
      ),
    );
  }

  Container widgetFlight(int i) {
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(vertical: i == 0 ? 0 : 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          i == 0 ? Container() : listColumn(),
          i == 0 ? Container() : Divider(
            color: Color(0xFF828282),
            thickness: 1,
            height: 2,
          ),
          _isLoading ? Center(
              child: Container(
                margin: EdgeInsets.only(top: 30),
                width: 20,
                height: 20,
                child: CircularProgressIndicator(),
              )) : listFlightFinalLeaderboards(i)
        ],
      ),
    );
  }

  Row listColumn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(flex: 0, child: Text("#", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
        Expanded( flex: 2, child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Text("Name", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
        )),
        Expanded(
          flex: 1,
          child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8),
                    topLeft: Radius.circular(8),
                  ),
                  color: _switchTabOne ? Colors.grey.withOpacity(0.1) : Colors.white,
                ),
                child: Text("Gross", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
          ),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Text("HC", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
          ),
        ),
        Expanded(flex: 0, child: Container(
            padding: EdgeInsets.all(1),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  topLeft: Radius.circular(8)
              ),
              color: _switchTabTwo ? Colors.grey.withOpacity(0.1) : Colors.white,
            ),
            child: Text("Nett",  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center,))),
      ],
    );
  }

  Widget listFinalLeaderboards() {
    return Container(
      width: SizeConfig.screenWidth,
      // height: SizeConfig.screenHeight/1.2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          LimitedBox(
            maxHeight: (dataFinal==null?0:dataFinal['rank'].length).toDouble()*25,
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, i) {
                return Container(
                  width: i == 0 ? 28 : i == 1 ? _listOneSize.width : i == 5 ? _listThreeSize.width : _listTwoSize.width,
                  margin: EdgeInsets.only(left: i == 3 || i == 4 ? 3 : i == 5 ? 10 : 0),
                  decoration: BoxDecoration(
                    color: _switchTabOne ? i == 3 ? Colors.grey.withOpacity(0.1) : Colors.white : _switchTabTwo ? i == 5 ? Colors.grey.withOpacity(0.1) : Colors.white : Colors.white,
                  ),
                  child: ListView.builder(
                    primary: false,
                    itemCount: dataFinal!=null?dataFinal['rank'].length:0,
                    itemExtent: 25,
                    itemBuilder: (context, i2) {
                      return InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GolferProfileScore(
                                dataGolfer: dataFinal['token'][i2],
                                fromLeaderboards: true,
                              )
                          ));
                        },
                        child: i == 0 ? Padding(
                          padding: const EdgeInsets.only(right: 3),
                          child: Center(child: Text("${dataFinal==null?"":dataFinal['rank'][i2]}", style: TextStyle(color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack), overflow: TextOverflow.ellipsis,)),
                        ) : i == 1 ? Padding(
                          padding: const EdgeInsets.only(top: 2.5, left: 8.0),
                          child: Tooltip(message: dataFinal==null?"":dataFinal['nama'][i2],
                              child: Text("${dataFinal==null?"":dataFinal['nama'][i2]}", style: TextStyle(fontSize: 16, color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack), overflow: TextOverflow.ellipsis,)),
                        )
                            : i == 2 ? Center(child: Text("${dataFinal==null?"":dataFinal['pair'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack)))
                            : i == 3 ? Center(child: Text("${dataFinal==null?"":dataFinal['gross'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack)))
                            : i == 4 ? Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Center(child: Text("${dataFinal==null?"":dataFinal['hc'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack))),
                            )
                            : Center(child: Text("${dataFinal==null?"":dataFinal['nett'][i2]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: _token == dataFinal['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack))),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget listFlightFinalLeaderboards(int mLength) {
    return Container(
      width: SizeConfig.screenWidth,
      // height: SizeConfig.screenHeight/1.2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          LimitedBox(
            maxHeight:
                (dataFinal == null ? 0 : dataFinal[mLength]['rank'].length)
                        .toDouble() *
                    25,
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, i) {
                return Container(
                  width: i == 0
                      ? 25
                      : i == 1
                          ? _listOneSize.width
                          : i == 5
                              ? _listThreeSize.width
                              : _listTwoSize.width,
                  margin: EdgeInsets.only(
                      left: i == 3 || i == 4
                          ? 3
                          : i == 5
                              ? 12
                              : 0),
                  decoration: BoxDecoration(
                    color: _switchTabOne
                        ? i == 3
                            ? Colors.grey.withOpacity(0.1)
                            : Colors.white
                        : _switchTabTwo
                            ? i == 5
                                ? Colors.grey.withOpacity(0.1)
                                : Colors.white
                            : Colors.white,
                  ),
                  child: ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: dataFinal != null
                        ? dataFinal[mLength]['rank'].length
                        : 0,
                    itemExtent: 25,
                    itemBuilder: (context, i2) {
                      return InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GolferProfileScore(
                                dataGolfer:dataFinal[mLength]['token'][i2],
                                fromLeaderboards: true,
                              )
                          ));
                        },
                        child: i == 0
                            ? Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Center(
                                    child: Text(
                                  "${dataFinal == null ? "" : dataFinal[mLength]['rank'][i2]}",
                                  style: TextStyle(color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack),
                                  overflow: TextOverflow.ellipsis,
                                )),
                              )
                            : i == 1
                                ? Padding(
                                    padding: const EdgeInsets.only(
                                        top: 2.5, left: 5.0),
                                    child: Tooltip(
                                      message: dataFinal == null ? "" : dataFinal[mLength]['nama'][i2],
                                      child: Text(
                                          "${dataFinal == null ? "" : dataFinal[mLength]['nama'][i2]}",
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack,
                                              fontSize: 16,)),
                                    ),
                                  )
                                : i == 2
                                    ? Center(
                                        child: Text(
                                            "${dataFinal == null ? "" : dataFinal[mLength]['pair'][i2]}",
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack)))
                                    : i == 3
                                        ? Center(
                                            child: Text(
                                                "${dataFinal == null ? "" : dataFinal[mLength]['gross'][i2]}",
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack)))
                                        : i == 4
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 5.0),
                                                child: Center(
                                                    child: Text(
                                                        "${dataFinal == null ? "" : dataFinal[mLength]['hc'][i2]}",
                                                        overflow:
                                                            TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack))),
                                              )
                                            : Center(
                                                child: Text(
                                                    "${dataFinal == null ? "" : dataFinal[mLength]['nett'][i2]}",
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: _token == dataFinal[mLength]['token'][i2] ? AppTheme.bogies : AppTheme.gFontBlack))),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container buttonSwitch() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Stack(
        children: [
          Center(
            child: Container(
              height: 30,
              width: 240,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color(0xFFE5E5E5),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    if(_switchTabOne == false){
                      _switchTabOne = true;
                      _switchTabTwo = false;
                      _switchTabThree = false;
                      getFinalLeaderboard();
                    }
                  });
                },
                child: Container(
                  height: 30,
                  width: 90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: _switchTabOne ? AppTheme.gFont : Color(0xFFE5E5E5),
                  ),
                  child: Center(child: Text("Gross", style: TextStyle(color: _switchTabOne ? Colors.white : AppTheme.gFontBlack),)),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    if(_switchTabTwo == false){
                      _switchTabTwo = true;
                      _switchTabOne = false;
                      _switchTabThree = false;
                      getFinalLeaderboard();
                    }
                  });
                },
                child: Container(
                  height: 30,
                  width: 90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: _switchTabTwo ? AppTheme.gFont : Color(0xFFE5E5E5),
                  ),
                  child: Center(child: Text("Nett", style: TextStyle(color: _switchTabTwo ? Colors.white : AppTheme.gFontBlack),)),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    if(statusFlight != 'tidak ada') {
                      if (_switchTabThree == false) {
                        _switchTabThree = true;
                        _switchTabOne = false;
                        _switchTabTwo = false;
                        getFinalLeaderboard();
                      }
                    }else{
                      var snackbar = SnackBar(
                        content: Text('No Scores in Flight',),
                        backgroundColor: Colors.black.withOpacity(0.6),
                      );
                      _scaffoldKey.currentState.showSnackBar(snackbar);
                    }
                  });
                },
                child: Container(
                  height: 30,
                  width: 90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: _switchTabThree ? AppTheme.gFont : Color(0xFFE5E5E5),
                  ),
                  child: Center(child: Text("Flight", style: TextStyle(color: _switchTabThree ? Colors.white : AppTheme.gFontBlack),)),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Column onlyTextInTop() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 8, top: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                splashColor: Colors.grey.withOpacity(0.2),
                  onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
              Expanded(
                child: Center(
                  child: Text(
                    "Tournament Final Leaderboard",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF4F4F4F),),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

}
