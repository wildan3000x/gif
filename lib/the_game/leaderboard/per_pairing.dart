import 'package:flutter/material.dart';
import 'package:gif/size_config.dart';

class TabPerPairing extends StatelessWidget {
  TabPerPairing({
    Key key,
    @required Size listOneSize,
    @required Size listThreeSize,
    @required Size listFourSize,
    @required this.dataRunning, this.count,
  }) : _listOneSize = listOneSize, _listThreeSize = listThreeSize, _listFourSize = listFourSize, super(key: key);

  final Size _listOneSize;
  final Size _listThreeSize;
  final Size _listFourSize;
  final dataRunning;
  final count;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      // height: SizeConfig.screenHeight/1.2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          LimitedBox(
            maxHeight: count.toDouble()*30,
            child: ListView.builder(
              primary: false,
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, i) {
                return Container(
                  width: i == 0 ? 25 : i == 1 ? _listOneSize.width : i == 5 ? _listThreeSize.width : _listFourSize.width/1.037,
                  decoration: BoxDecoration(
                      color: i == 5 ? Colors.grey.withOpacity(0.1) : Colors.white
                  ),
                  child: ListView.builder(
                    primary: false,
                    itemCount: dataRunning!=null?dataRunning.length:0,
                    itemBuilder: (context, i2) {
                      return Container(
                        height: dataRunning[i2]['rank'].length == 4 ? 108 : dataRunning[i2]['rank'].length == 3 ? 83 : dataRunning[i2]['rank'].length == 2 ? 58 : 35,
                        margin: EdgeInsets.only(top: i2 == 0 ? 0 : 3),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(color: Colors.grey, width: 1.5)
                            )
                        ),
                        child: ListView.builder(
                          primary: false,
                          itemCount: dataRunning!=null?dataRunning[i2]['rank'].length:0,
                          itemExtent: 25,
                          itemBuilder: (context, i3) {
                            return Padding(
                              padding: EdgeInsets.only(top: 2),
                              child: i == 0 ? Padding(
                                padding: const EdgeInsets.only(right: 8),
                                child: Center(child: Text("${dataRunning==null?"":dataRunning[i2]["rank"][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14, color: Color(0xFF4f4f4f)),)),
                              ) : i == 1 ? Padding(
                                padding: const EdgeInsets.only(top: 2.0, left: 5),
                                child: Tooltip(message: dataRunning==null?"":dataRunning[i2]['nama'][i3], child: Text("${dataRunning==null?"":dataRunning[i2]['nama_pendek'][i3] == null?dataRunning[i2]['nama'][i3]:dataRunning[i2]['nama_pendek'][i3]+", "+dataRunning[i2]['nama'][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),)),
                              )
                                  : i == 2 ? Center(child: Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: Text("${dataRunning==null?"":dataRunning[i2]['pair'][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),),
                              ))
                                  : i == 3 ? Center(child: Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: Text("${dataRunning==null?"":dataRunning[i2]['thru'][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),),
                              ))
                                  : i == 4 ? Center(child: Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: Text("${dataRunning==null?"":dataRunning[i2]['stroke'][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),),
                              ))
                                  : Center(child: Text("${dataRunning==null?"":dataRunning[i2]['score_plus'][i3]}", overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 16, color: Color(0xFF4f4f4f)),)),
                            );
                          },
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
