import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/my_games/golfer_profile_score.dart';

class ListWinnerSkillAward extends StatelessWidget {
  const ListWinnerSkillAward({
    Key key,
    @required this.dataWinners,
    @required String type,
  }) : type = type, super(key: key);

  final dataWinners;
  final String type;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            width: 180,
            decoration: BoxDecoration(
                color: type=='hio'?Color(0xFFF15411):type=='ntp'?AppTheme.gButton:type=='ntl'?Color(0xFFF2C94C):Color(0xFFF15411),
                borderRadius: BorderRadius.circular(15)
            ),
            child: Center(child: Text("${type=='hio'?"Hole In One":type=='ntp'?"Nearest To The Pin":type=='ntl'?"Nearest To The Line":"Longest Drive"}", style: TextStyle(color: Colors.white),),),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(flex: 1, child: Text("#Hole", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
                Expanded( flex: 2, child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text("Name", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                )),
                Expanded(
                  flex: 1,
                  child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(type=='hio'?"Prize":"Distance", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Color(0xFF828282),
            thickness: 1,
            height: 2,
          ),
          Container(
            padding: EdgeInsets.only(top: 2),
            height: (dataWinners==null?0:dataWinners[type].length).toDouble()*25,
            child: ListView.builder(
                itemExtent: 25,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: dataWinners==null?0:dataWinners[type].length,
                primary: false,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(flex: 1, child: Text("${dataWinners==null?"":dataWinners[type][index]['hole']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)),
                      Expanded( flex: 2, child: Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Tooltip(
                            message: "${dataWinners[type][index]['nama']}",
                            child: InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => GolferProfileScore(
                                        dataGolfer: dataWinners[type][index]['token'],
                                        fromLeaderboards: true,
                                      )
                                  ));
                                },
                                child: Text("${dataWinners==null?"":dataWinners[type][index]['nama1']==null||dataWinners[type][index]['nama1']==""?dataWinners[type][index]['nama']:dataWinners[type][index]['nama1']+", "+dataWinners[type][index]['nama']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,))),
                      )),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 5.0),
                          child: Text("${dataWinners==null?"":dataWinners[type][index]['pair']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Text("${dataWinners==null?"":type=='hio'?dataWinners[type][index]['prize']==null?"-":dataWinners[type][index]['prize']:dataWinners[type][index]['distance']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                    ],
                  );
                }),
          )
        ],
      ),
    );
  }
}
