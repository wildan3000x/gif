import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/my_games/golfer_profile_score.dart';

class ListWinnerFlight extends StatelessWidget {
  const ListWinnerFlight({
    Key key,
    @required this.dataWinners,
  }) : super(key: key);

  final dataWinners;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 500,
      child: ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: dataWinners==null?0:dataWinners['best_flight'].length,
        itemBuilder: (context, index) {
          return dataWinners['best_flight'][index].isNotEmpty ? Padding(
            padding: EdgeInsets.only(top: 8),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.only(top: 10),
                  width: 180,
                  decoration: BoxDecoration(
                      color: index == 0 ? Color(0xFFF2C94C) : index == 1 ? Color(0xFF2D9CDB) : AppTheme.gButton,
                      borderRadius: BorderRadius.circular(15)
                  ),
                  child: Center(child: Text("${index==0?"Best Nett Flight A":index==1?"Best Nett Flight B":"Best Nett Flight C"}", style: TextStyle(color: Colors.white),),),
                ),
                Container(
                  padding: EdgeInsets.only(top: 3),
                  child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][0]['status_hc']}", style: TextStyle(color: AppTheme.gFontBlack),),
                ),
                Container(
                  padding: EdgeInsets.only(top: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(flex: 0, child: Text("#", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
                      Expanded( flex: 2, child: Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text("Name", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                      )),
                      Expanded(
                        flex: 1,
                        child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Text("Gross", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Text("HC", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                        ),
                      ),
                      Expanded(flex: 0, child: Container(
                          padding: EdgeInsets.all(1),
                          child: Text("Nett",  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center,))),
                    ],
                  ),
                ),
                Divider(
                  color: Color(0xFF828282),
                  thickness: 1,
                  height: 2,
                ),
                Container(
                  padding: EdgeInsets.only(top: 2),
                  height: (dataWinners==null?0:dataWinners['best_flight'][index].length).toDouble()*25,
                  child: ListView.builder(
                      itemExtent: 25,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: dataWinners!=null?dataWinners['best_flight'][index].length:0,
                      primary: false,
                      shrinkWrap: true,
                      itemBuilder: (context, index2) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(flex: 0, child: Text("${index2+1}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)),
                            Expanded( flex: 2, child: Tooltip(
                              message: "${dataWinners['best_flight'][index][index2]['nama']}",
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => GolferProfileScore(
                                        dataGolfer: dataWinners['best_flight'][index][index2]['token'],
                                        fromLeaderboards: true,
                                      )
                                  ));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][index2]['nama1']==null?dataWinners['best_flight'][index][index2]['nama']:dataWinners['best_flight'][index][index2]['nama1']+", "+dataWinners['best_flight'][index][index2]['nama']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                                ),
                              ),
                            )),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][index2]['pair']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 20.0),
                                child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][index2]['gross']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 18.0),
                                child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][index2]['hc']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                              ),
                            ),
                            Expanded(flex: 0, child: Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text("${dataWinners==null?"":dataWinners['best_flight'][index][index2]['nett']}",  style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                            )),
                          ],
                        );
                      }),
                )
              ],
            ),
          ) : Container();
        },),
    );
  }
}
