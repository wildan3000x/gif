import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/my_games/golfer_profile_score.dart';

class ListWinner extends StatelessWidget {
  const ListWinner({
    Key key,
    @required this.dataWinners,
    @required String type,
  }) : type = type, super(key: key);

  final dataWinners;
  final String type;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.zero,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            width: 180,
            decoration: BoxDecoration(
                color: type=='best_gross'?Color(0xFF2D9CDB):AppTheme.gButton,
                borderRadius: BorderRadius.circular(15)
            ),
            child: Center(child: Text("${type=='best_gross'?"Best Gross":"Best Nett Overall"}", style: TextStyle(color: Colors.white),),),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(flex: 0, child: Text("#", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)),
                Expanded( flex: 2, child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text("Name", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                )),
                Expanded(
                  flex: 1,
                  child: Text("Pair", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("Gross", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("HC", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
                  ),
                ),
                Expanded(flex: 0, child: Container(
                    padding: EdgeInsets.all(1),
                    child: Text("Nett",  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center,))),
              ],
            ),
          ),
          Divider(
            color: Color(0xFF828282),
            thickness: 1,
            height: 2,
          ),
          Container(
            padding: EdgeInsets.only(top: 2),
            height: (dataWinners==null?0:dataWinners[type].length).toDouble()*25,
            child: ListView.builder(
                itemExtent: 25,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: dataWinners==null?0:dataWinners[type].length,
                primary: false,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(flex: 0, child: Text("${index+1}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)),
                      Expanded( flex: 2, child: InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GolferProfileScore(
                                dataGolfer: dataWinners[type][index]['token'],
                                fromLeaderboards: true,
                              )
                          ));
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Tooltip(
                            message: "${dataWinners[type][index]['nama']}",
                              child: Text("${dataWinners==null?"":dataWinners[type][index]['nama1']==null?dataWinners[type][index]['nama']:dataWinners[type][index]['nama1']+", "+dataWinners[type][index]['nama']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)),
                        ),
                      )),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Text("${dataWinners==null?"":dataWinners[type][index]['pair']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: Text("${dataWinners==null?"":dataWinners[type][index]['gross']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 18.0),
                          child: Text("${dataWinners==null?"":dataWinners[type][index]['hc']}", style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack,), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                      Expanded(flex: 0, child: Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Text("${dataWinners==null?"":dataWinners[type][index]['nett']}",  style: TextStyle(fontSize: 16, color: AppTheme.gFontBlack), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                      )),
                    ],
                  );
                }),
          )
        ],
      ),
    );
  }
}
