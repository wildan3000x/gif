import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/leaderboard/winner_tournament/list_prizes.dart';
import 'package:gif/the_game/leaderboard/winner_tournament/list_winner_flight.dart';
import 'package:gif/the_game/leaderboard/winner_tournament/list_winner_skill_award.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/share_sosmed.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../main.dart';
import 'list_winner.dart';

class TournamentWinners extends StatefulWidget {
  const TournamentWinners({Key key, this.dataHeader}) : super (key: key);
  final dataHeader;
  @override
  _TournamentWinnersState createState() => _TournamentWinnersState();
}

class _TournamentWinnersState extends State<TournamentWinners> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading = false;

  @override
  void initState() {
    checkLoginStatus();
    super.initState();
  }

  bool _isLevelCaddie = false;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      if(sharedPreferences.getString("level") == "Caddie"){
        setState(() {
          _isLevelCaddie = true;
        });
      }
      getFinalLeaderboard();
    }
  }

  var dataWinners;

  getFinalLeaderboard() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataHeader['turnamen']['id_turnamen'],
    };

    Utils.postAPI(data, "get_TW").then((body) {
      if (body['status'] == 'success') {
        saveTournamentWinner();
        dataWinners = body['data'];
        // print("LLL ${dataWinners['best_flight'][1][0]['nama']}");
        setState(() {
          if(dataWinners['tombol_close']=='tidak aktif'){
            _closed = true;
          }
          _isLoading = false;
        });
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  saveTournamentWinner() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataHeader['turnamen']['id_turnamen'],
    };

    Utils.postAPI(data, "STW").then((body) {
      if (body['status'] == 'success') {
        print("INFO ${body['message']}");
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  bool _closed = false;
  bool _isLoadingClose = false;
  closeTournament() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataHeader['turnamen']['id_turnamen'],
    };

    Utils.postAPI(data, "UTS").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _closed = true;
          _isLoadingClose = false;
        });
        print("INFO CLOSE ${body['message']}");
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getFinalLeaderboard();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getFinalLeaderboard();
    if(mounted)
      setState(() {
        getFinalLeaderboard();
      });
    _refreshController.loadComplete();
  }

  ScreenshotController screenshotController = ScreenshotController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: ShareSosMed(
        screenShotController: screenshotController,
      ),
      key: _scaffoldKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Screenshot(
                controller: screenshotController,
                child: Container(
                  color: Colors.white,
                  child: Container(
                    margin: EdgeInsets.all(13),
                    child: Column(
                      children: [
                        HeaderScoring(dataPairing: widget.dataHeader, myFormat: myFormat, isCaddie: _isLevelCaddie ? true : null,),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Column(
                            children: [
                              SizedBox(height: 10,),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 2),
                                    child: InkWell(
                                      splashColor: Colors.grey.withOpacity(0.2),
                                        onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
                                  ),
                                  Expanded(child: Center(child: Text("Tournament Winners", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 24), textAlign: TextAlign.center,))),
                                ],
                              ),
                              SizedBox(height: 3,),
                              // Image.asset('assets/images/trophy.png', width: 60, height: 40, fit: BoxFit.fill,),
                              SizedBox(height: 5,)
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            dataWinners==null||dataWinners['status_bg']!='ada' || dataWinners['best_gross'].isEmpty ? Container() : Column(
                              children: [
                                ListWinner(dataWinners: dataWinners, type: 'best_gross'),
                                SizedBox(height: 15,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_bn']!='ada' || dataWinners['best_nett'].isEmpty ? Container() : Column(
                              children: [
                                ListWinner(dataWinners: dataWinners, type: 'best_nett'),
                                SizedBox(height: 10,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_fl']!='ada' || dataWinners['best_flight'].isEmpty ? Container() : Column(
                              children: [
                                ListWinnerFlight(dataWinners: dataWinners),
                                SizedBox(height: 15,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_hio']!='ada' || dataWinners['hio'].isEmpty ?Container():Column(
                              children: [
                                ListWinnerSkillAward(dataWinners: dataWinners, type: 'hio'),
                                SizedBox(height: 15,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_ntp']!='ada' || dataWinners['ntp'].isEmpty ?Container():Column(
                              children: [
                                ListWinnerSkillAward(dataWinners: dataWinners, type: 'ntp'),
                                SizedBox(height: 15,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_ntl']!='ada' || dataWinners['ntl'].isEmpty ? Container():Column(
                              children: [
                                ListWinnerSkillAward(dataWinners: dataWinners, type: 'ntl'),
                                SizedBox(height: 15,),
                              ],
                            ),
                            dataWinners==null||dataWinners['status_ld']!='ada'||dataWinners['ld'].isEmpty?Container():ListWinnerSkillAward(dataWinners: dataWinners, type: 'ld')
                          ],
                        ),
                        _isLoading ? Container() : Container(
                          padding: EdgeInsets.only(top: 15, bottom: 20),
                          child: Column(
                            children: [
                              Text("Congrats for the winners!", style: TextStyle(color: AppTheme.eagle, fontSize: 18, fontStyle: FontStyle.italic),),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 5),
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                  splashColor: Colors.grey.withOpacity(0.5),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => ListPrizes(dataHeader: widget.dataHeader,)));
                                    // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                                  },
                                  padding: const EdgeInsets.all(0.0),
                                  child: Ink(
                                      decoration: BoxDecoration(
                                          color: AppTheme.gButton,
                                          borderRadius: BorderRadius.circular(100)
                                      ),
                                      child: Container(
                                          constraints: const BoxConstraints(maxWidth: 146, minHeight: 51),
                                          alignment: Alignment.center,
                                          child: Text("Prizes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                ),
                              ),
                              SizedBox(height: 3,),
                              _isLevelCaddie ? Container() : BackToHomeButton(
                                onPressed: () {
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(builder: (BuildContext context) => HomePage(navbar: true,)),
                                          (Route<dynamic> route) => false);
                                },
                                textButton: "Back to Home",
                              )
                              // dataWinners==null?Container():dataWinners['tombol']=='aktif'?_closed?
                              // Container(padding: EdgeInsets.only(top: 25),
                              //   child: Row(
                              //     mainAxisAlignment: MainAxisAlignment.center,
                              //     children: [
                              //       Text("Tournament Closed", style: TextStyle(color: AppTheme.gRed, fontStyle: FontStyle.italic),),
                              //       SizedBox(width: 3,),
                              //       Icon(Icons.check, size: 20,)
                              //     ],
                              // ),):
                              // Stack(
                              //   children: [
                              //     InkWell(
                              //       onTap: () {
                              //         setState(() {
                              //           _isLoadingClose = true;
                              //           closeTournament();
                              //         });
                              //       },
                              //       child: Container(
                              //         margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                              //         padding: EdgeInsets.all(10),
                              //         width: 116,
                              //         height: 51,
                              //         decoration: BoxDecoration(
                              //             color: AppTheme.gRed,
                              //             borderRadius: BorderRadius.circular(30)),
                              //         child: Center(
                              //           child: _isLoadingClose?Container(
                              //             width: 15,
                              //             height: 15,
                              //             child: CircularProgressIndicator(),
                              //           ) : Text(
                              //             "Close",
                              //             style: TextStyle(
                              //                 color: AppTheme.white,
                              //                 fontSize: 16,
                              //                 fontWeight: FontWeight.bold),
                              //             textAlign: TextAlign.center,
                              //           ),
                              //         ),
                              //       ),
                              //     ),
                              //     Positioned(
                              //       right: 0,
                              //       top: 0,
                              //       child: InkWell(
                              //         onTap: (){
                              //           showDialog(
                              //             context: context,
                              //           builder: (context){
                              //               return Dialog(
                              //                 backgroundColor: Colors.black.withOpacity(0.7),
                              //                 child: Container(
                              //                   padding: EdgeInsets.all(8),
                              //                   decoration: BoxDecoration(
                              //                     // color: Colors.black.withOpacity(0.7),
                              //                   ),
                              //                   child: Text("As an organizer, you can close the tournament by clicking the close button or the tournament will be automatically closed in 4 hours after the final results announced.", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                              //                 ),
                              //               );
                              //           });
                              //         },
                              //         child: Container(
                              //           padding: EdgeInsets.all(7),
                              //           decoration: BoxDecoration(
                              //             color: Colors.black.withOpacity(0.8),
                              //             shape: BoxShape.circle
                              //           ),
                              //           child: Text("?", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                              //         ),
                              //       ),
                              //     )
                              //   ],
                              // ) : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

