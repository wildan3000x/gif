import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/share_sosmed.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListPrizes extends StatefulWidget {
  const ListPrizes({Key key, this.dataHeader}) : super (key: key);
  final dataHeader;
  @override
  _ListPrizesState createState() => _ListPrizesState();
}

class _ListPrizesState extends State<ListPrizes> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading = false;

  @override
  void initState() {
    checkLoginStatus();
    super.initState();
  }

  bool _isLevelCaddie = false;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      if(sharedPreferences.getString("level") == "Caddie"){
        setState(() {
          _isLevelCaddie = true;
        });
      }
      getFinalLeaderboard();
    }
  }

  var dataWinners;

  getFinalLeaderboard() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': widget.dataHeader['turnamen']['id_turnamen'],
    };

    Utils.postAPI(data, "get_TW").then((body) {
      if (body['status'] == 'success') {
        dataWinners = body['data'];
        // print("LLL ${dataWinners['best_flight'][1][0]['nama']}");
        setState(() {
          _isLoading = false;
        });
      }else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  ScreenshotController screenshotController = ScreenshotController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: ShareSosMed(
        screenShotController: screenshotController,
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Container(
                color: Colors.white,
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderCreate(
                        title: "Tournament Winners",
                        disable: _isLevelCaddie ? true : false,
                        disableBack: _isLevelCaddie ? true : false,
                        backSummary: _isLevelCaddie ? true : null,
                      ),
                      titleAward(),
                      listWinner('best_gross'),
                      listWinnerFlight(),
                      listWinner('best_nett'),
                      listWinner('hio'),
                      listWinner('ntp'),
                      listWinner('ntl'),
                      listWinner('ld'),
                      SizedBox(height: 5,),
                      RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                        splashColor: Colors.grey.withOpacity(0.5),
                        onPressed: () {
                          Navigator.pop(context);
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                        },
                        padding: const EdgeInsets.all(0.0),
                        child: Ink(
                            decoration: BoxDecoration(
                                color: AppTheme.gButton,
                                borderRadius: BorderRadius.circular(100)
                            ),
                            child: Container(
                                constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                alignment: Alignment.center,
                                child: Text("Back", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                      ),
                      SizedBox(height: 10,),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget listWinnerFlight() {
    return dataWinners==null||dataWinners['best_flight'] == 'tidak ada' ? Container() : ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: dataWinners['best_flight'].length,
      itemBuilder: (context, i){
        return dataWinners['best_flight'][i].isEmpty ? Container() : Padding(
          padding: const EdgeInsets.only(bottom: 18.0),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(5),
                width: 250,
                decoration: BoxDecoration(
                    color: i == 0 ? Color(0xFFF15411) : i == 1 ? Color(0xFFF3BF37) : Color(0xFF2D9CDB),
                    borderRadius: BorderRadius.circular(15)
                ),
                child: Center(child: Text("Winners of Flight ${i==0?"A":i==1?"B":"C"}!", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),),
              ),
              // Text("Winner ${setText()}!", style: TextStyle(color: setColor(), fontSize: 20, fontWeight: FontWeight.bold),),
              SizedBox(height: 8,),
              Container(
                // padding: EdgeInsets.symmetric(horizontal: 4),
                child: ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: dataWinners['best_flight'][i].length,
                  itemBuilder: (context, i2){
                    return Tooltip(
                      message: dataWinners['best_flight'][i][i2]['nama'],
                      child: InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) => GolferProfileScore(
                                dataGolfer: dataWinners['best_flight'][i][i2]['token'],
                                fromLeaderboards: true,
                              )
                          ));
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 4),
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Color(0xFFE5E5E5)),
                              color: Color(0xFFF9F9F9)
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset("assets/images/medal${i2==1?"#2":i2==2?"#3":""}.png", width: 40, height: 40, isAntiAlias: true,),
                                  // Text("#${i2+1}", style: TextStyle(color: i2 == 0 ? AppTheme.bogies : Color(0xFF2D9CDB), fontSize: /*i == 0 ? 26 : */24, fontWeight: FontWeight.bold,),),
                                  // SizedBox(width: 3,),
                                  dataWinners['best_flight'][i][i2]['foto']==null?ClipRRect(
                                    borderRadius: BorderRadius.circular(10000.0),
                                    child: Image.asset(
                                      "assets/images/userImage.png",
                                      width: /*i == 0 ? 40 : */35,
                                      height: /*i == 0 ? 40 : */35,
                                    ),
                                  ):ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                      CircularProgressIndicator(
                                        value: progress.progress,
                                      ), imageUrl: Utils.url + 'upload/user/' + dataWinners['best_flight'][i][i2]['foto'], width: /*i == 0 ? 40 : */35, height: /*i == 0 ? 40 : */35, fit: BoxFit.cover,)),
                                  SizedBox(width: 8,),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      // mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text("${dataWinners['best_flight'][i][i2]['nama1']==null?dataWinners['best_flight'][i][i2]['nama']:dataWinners['best_flight'][i][i2]['nama1']+", "+dataWinners['best_flight'][i][i2]['nama']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: /*i == 0 ? 20:*/16, fontWeight: FontWeight.bold), overflow: TextOverflow.clip,),
                                        Text(dataWinners['best_flight'][i][i2]['tropi']==0&&dataWinners['best_flight'][i][i2]['sertifikat']==0?" -":"${dataWinners['best_flight'][i][i2]['tropi']==1?"Trophy":""}${dataWinners['best_flight'][i][i2]['tropi']==1&&dataWinners['best_flight'][i][i2]['sertifikat']==1?" + ":""}${dataWinners['best_flight'][i][i2]['sertifikat']==1?"Certificate":""}", style: TextStyle(color: AppTheme.gButton, fontSize: 12),),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 5),
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(color: Colors.grey.shade200)
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    // Text("gets : ", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic, fontSize: 11),),
                                    Text("${dataWinners['best_flight'][i][i2]['prize']=="null"||dataWinners['best_flight'][i][i2]['prize']==null?"no prize":dataWinners['best_flight'][i][i2]['prize']}", style: TextStyle(color: AppTheme.par, fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.center,),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },),
              )
            ],
          ),
        );
    },);
  }

  Widget listWinner(String s) {
    Color hexToColor(String code) {
      return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
    }

    Color setColor() {
      if (s == "best_gross") {
        return hexToColor("#1ac5a6");
      } else if (s == "best_nett") {
        return hexToColor("#E31212");
      } else if (s == "hio") {
        return hexToColor("#F15411");
      } else if (s == "ntp") {
        return hexToColor("#F2C94C");
      } else if (s == "ntl") {
        return hexToColor("#2D9CDB");
      } else {
        return hexToColor("#1AC5A6");
      }
    }

    String setText() {
      if (s == "best_gross") {
        return "Best Gross";
      } else if (s == "best_nett") {
        return "Best Nett Overall";
      } else if (s == "hio") {
        return "Hole In One";
      } else if (s == "ntp") {
        return "Nearest to the Pin";
      } else if (s == "ntl") {
        return "Nearest to the Line";
      } else {
        return "Longest Drive";
      }
    }

    return dataWinners==null||dataWinners[s].isEmpty ? Container() : Padding(
      padding: const EdgeInsets.only(bottom: 18.0),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            width: 250,
            decoration: BoxDecoration(
                color: setColor(),
                borderRadius: BorderRadius.circular(15)
            ),
            child: Center(child: Text("Winners of ${setText()}!", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold,),),),
          ),
          // Text("Winner ${setText()}!", style: TextStyle(color: setColor(), fontSize: 20, fontWeight: FontWeight.bold),),
          SizedBox(height: 8,),
          Container(
            // padding: EdgeInsets.symmetric(horizontal: 4),
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: dataWinners[s].length,
              itemBuilder: (context, i){
                return Tooltip(
                  message: dataWinners[s][i]['nama'],
                  child: InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => GolferProfileScore(
                            dataGolfer: dataWinners[s][i]['token'],
                            fromLeaderboards: true,
                          )
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 4),
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Color(0xFFE5E5E5)),
                        color: Color(0xFFF9F9F9)
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.asset(s=='best_gross' || s=='best_nett' ? "assets/images/medal${i==1?"#2":i==2?"#3":""}.png" : "assets/images/trophy_without_rank.png", width: s=='best_gross' || s=='best_nett' ? 40 : 30, height: s=='best_gross' || s=='best_nett' ? 40 : 30, isAntiAlias: true,),
                              // Text("#${i+1}", style: TextStyle(color: i == 0 ? AppTheme.bogies : Color(0xFF2D9CDB), fontSize: /*i == 0 ? 26 : */24, fontWeight: FontWeight.bold),),
                              s=='best_gross' || s=='best_nett' ? Container() : SizedBox(width: 8,),
                              dataWinners[s][i]['foto']==null?ClipRRect(
                                borderRadius: BorderRadius.circular(10000.0),
                                child: Image.asset(
                                  "assets/images/userImage.png",
                                  width: /*i == 0 ? 40 : */35,
                                  height: /*i == 0 ? 40 : */35,
                                ),
                              ):ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                  CircularProgressIndicator(
                                    value: progress.progress,
                                  ), imageUrl: Utils.url + 'upload/user/' + dataWinners[s][i]['foto'], width: /*i == 0 ? 40 : */35, height: /*i == 0 ? 40 : */35, fit: BoxFit.cover,)),
                              SizedBox(width: 8,),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("${dataWinners[s][i]['nama1']==null?dataWinners[s][i]['nama']:dataWinners[s][i]['nama1']+", "+dataWinners[s][i]['nama']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: /*i == 0 ? 20:*/16, fontWeight: FontWeight.bold), overflow: TextOverflow.clip,),
                                    Text(dataWinners[s][i]['tropi']==0&&dataWinners[s][i]['sertifikat']==0?" -":"${dataWinners[s][i]['tropi']==1?"Trophy":""}${dataWinners[s][i]['tropi']==1&&dataWinners[s][i]['sertifikat']==1?" + ":""}${dataWinners[s][i]['sertifikat']==1?"Certificate":""}", style: TextStyle(color: AppTheme.gButton, fontSize: 12),),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            padding: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.grey.shade200)
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                // Text("gets : ", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic, fontSize: 11),),
                                Text("${dataWinners[s][i]['prize']=="null"||dataWinners[s][i]['prize']==null?"no prize":dataWinners[s][i]['prize']}", style: TextStyle(color: AppTheme.par, fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.center,),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },),
          )
        ],
      ),
    );
  }

  Container titleAward() {
    return Container(
      padding: EdgeInsets.only(top: 18, bottom: 2),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Image.asset('assets/images/trophy.png', width: 60, height: 40, fit: BoxFit.fill,),
            Text(
              "${widget.dataHeader['turnamen']['nama_turnamen']}",
              style: TextStyle(
                color: AppTheme.gFont,
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Text("${widget.dataHeader['turnamen']['nama_lapangan']}, ${myFormat.format(DateTime.parse(widget.dataHeader['turnamen']['tanggal_turnamen']))}"
                /*widget.data == null ? "${dataTour==null?"":dataTour['lapangan_turnamen']}, ${dataTour==null?"":myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))}"
                    :
                "${widget.data["turnamen"]['lapangan_turnamen']}, ${myFormat.format(DateTime.parse(widget.data["turnamen"]['tanggal_turnamen']))}"*/,
                style: TextStyle(color: Color(0xFF4F4F4F)),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              width: 200,
              child: Divider(
                thickness: 2,
              ),
            )
          ],
        ),
      ),
    );
  }
}
