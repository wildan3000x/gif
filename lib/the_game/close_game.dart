import 'package:flutter/material.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/my_games.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../app_theme.dart';

class CloseGame extends StatefulWidget {
  CloseGame({this.dataTournament});
  final dataTournament;
  @override
  _CloseGameState createState() => _CloseGameState();
}

class _CloseGameState extends State<CloseGame> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _closed = false;
  bool _isLoadingClose = false;

  closeTournament() async {
    final mToken = await Utils.getToken(context);
    var data = {
      'token': mToken,
      'id_turnamen': widget.dataTournament['id_turnamen'],
    };

    Utils.postAPI(data, "UTS").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _closed = true;
          _isLoadingClose = false;
        });
        print("INFO CLOSE ${body['message']}");
        Utils.showToast(context, "error", "Game has successfully ended, closing the screen...");
        Future.delayed(Duration(seconds: 3), () {
          // 3s over, navigate to a new page
          Navigator.pushAndRemoveUntil(
              context, MaterialPageRoute(builder: (BuildContext context) => new MyGames()),
                  (Route<dynamic> route) => false);
        });
      }else {
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoadingClose,
        child: SafeArea(
          child: Center(
            child: Container(
              padding: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCreate(title: "Close Game",),
                  Container(
                    margin: EdgeInsets.only(top: 15, bottom: 0),
                    child: Column(
                      children: [
                        // Center(child: Text("Close The Game", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: AppTheme.gFontBlack),)),
                        SizedBox(height: 5,),
                        Text(
                          "${widget.dataTournament['nama_turnamen']} (ID ${widget.dataTournament['kode_turnamen']})",
                          style: TextStyle(
                            color: AppTheme.gFont,
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            "${widget.dataTournament['nama_lapangan']}, ${DateFormat('d MMM yyyy').format(DateTime.parse(widget.dataTournament['tanggal_turnamen']))}",
                            style: TextStyle(color: Color(0xFF4F4F4F)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15,),
                  Container(
                    child: Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(15)),
                          child: /*dataValid==null?Container():dataValid['tombol']=='aktif'?*/
                          _closed?Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("The Tournament has Closed", style: TextStyle(color: AppTheme.gRed, fontStyle: FontStyle.italic),),
                                SizedBox(width: 3,),
                                Icon(Icons.check, size: 20,)
                              ],
                            ),
                          ) : Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.black
                                    ),
                                    child: Text("?", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),),
                                  ),
                                  Expanded(child: Padding(
                                    padding: const EdgeInsets.only(top:3.0, left: 0.1),
                                    child: Text("As an organizer, you can close the game by pressing the close button or the game will be automatically closed in 4 hours after the final results announced.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
                                  )),
                                ],
                              ),
                              SizedBox(height: 10,),
                              RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                splashColor: Colors.grey.withOpacity(0.5),
                                onPressed: () {
                                  setState(() {
                                    _isLoadingClose = true;
                                  });
                                  closeTournament();
                                  // widget.function;
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                                },
                                padding: const EdgeInsets.all(0.0),
                                child: Ink(
                                    decoration: BoxDecoration(
                                        color: AppTheme.gRed,
                                        borderRadius: BorderRadius.circular(100)
                                    ),
                                    child: Container(
                                        constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                                        alignment: Alignment.center,
                                        child: Text("Close the Game", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                              ),
                            ],
                          )/* : Container()*/,
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.symmetric(horizontal: 90),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Color(0xFFF15411),
                          ),
                          child: Text(
                            "Organizer Tab",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
