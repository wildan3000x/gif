import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/the_game/leaderboard/running_leaderboard.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
// import 'package:intl/intl.dart';
import 'dart:ui' as ui;

import '../app_theme.dart';
import '../main.dart';

// ignore: must_be_immutable
class HeaderScoring extends StatelessWidget {
  HeaderScoring({
    Key key,
    @required this.dataPairing,
    @required this.myFormat,
    @required this.onClickable, this.txtSize, this.keySize, this.size, this.backButton, this.isCaddie
  }) : super(key: key);

  final dataPairing;
  final myFormat;
  final bool onClickable;
  final bool backButton;
  Size txtSize;

  final keySize;
  final Size size;
  final isCaddie;

  Size _textSize(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style), maxLines: 1, textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  Widget build(BuildContext context) {
    final String text = "${dataPairing==null?"":dataPairing['turnamen']['nama_turnamen']}";
    final TextStyle textStyle = TextStyle(
        fontSize: 18,
        color: Colors.white,
        fontWeight: FontWeight.bold);
    txtSize = _textSize(text, textStyle);
    return Container(
      height: 70,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFF1760EB),
            Color(0xFF1A478A),
          ],
        ),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(left: backButton == true ? 5 : 10.0, bottom: 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    backButton == true ? InkWell(onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg")) : Container(),
                    backButton == true ? SizedBox(width: 4,) : Container(),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              if(onClickable == true){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => RunningLeaderBoards(dataHeader: dataPairing,)));
                              }
                            },
                            child: Container(
                              // width: double.minPositive,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Flexible(
                                    child: Container(
                                      // width: txtSize.width>230?230:txtSize.width,
                                      padding: const EdgeInsets.only(bottom: 4.0),
                                      child: Text(
                                        "${dataPairing==null?"":dataPairing['turnamen']['nama_turnamen']}",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                  onClickable == true ? SizedBox(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 3),
                                      child: SvgPicture.asset(
                                        "assets/svg/arrow_scoring_header.svg",
                                        width: 20,
                                        height: 20,
                                      ),
                                    ),
                                  ) : Container()
                                ],
                              ),
                            ),
                          ),
                          Text(
                            "${dataPairing==null?"":dataPairing['turnamen']['nama_lapangan']}, ${dataPairing==null?"":myFormat.format(DateTime.parse(dataPairing['turnamen']['tanggal_turnamen']))}",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          InkWell(
            onTap: (){
              return showDialog(
                context: context,
                builder: (context) => CustomAlertDialog(
                  title: 'Are you sure?',
                  contentText: " want to quit to the main menu?",
                  noButton: () => Navigator.of(context).pop(false),
                  yesButton: () {
                    if(isCaddie == true){
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (BuildContext context) => new HomeCaddie()),
                              (Route<dynamic> route) => false);
                    }else {
                      Navigator.pushAndRemoveUntil(context,
                          MaterialPageRoute(builder: (BuildContext context) =>
                              HomePage(navbar: true,)),
                              (Route<dynamic> route) => false);
                    }
                  },
                )
                // new AlertDialog(
                //   title: new Text('Are you sure want quit to main menu?', style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16),),actions: <Widget>[
                //   RaisedButton(
                //       child: Text('No'),
                //       onPressed: () => Navigator.of(context).pop(false)),
                //   RaisedButton(
                //       color: AppTheme.gRed,
                //       child: Text('Yes'),
                //       onPressed: () => Navigator.pushAndRemoveUntil(
                //           context,
                //           MaterialPageRoute(builder: (BuildContext context) => HomePage(navbar: true,)),
                //               (Route<dynamic> route) => false),
                //   ),
                // ],),
              );
            },
            child: Container(
              margin: EdgeInsets.only(right: 8),
              child: Image.asset("assets/images/gif_logo.png", width: 60, height: 60,),
            ),
          ),
        ],
      ),
    );
  }
}