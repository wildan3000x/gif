import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io' as Io;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/caddie/widget/button_rectangle_rounded_caddie.dart';
import 'package:gif/caddie/widget/header_play_game_caddie.dart';
import 'package:gif/create_games/pairing/success_msg.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/main.dart';
import 'package:gif/my_games/my_games.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/alert_dialog.dart';
import 'package:gif/the_game/close_game.dart';
import 'package:gif/the_game/final_scoring_page.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/leaderboard/running_leaderboard.dart';
import 'package:gif/the_game/remark.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
import 'package:gif/widget/share_sosmed.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:screenshot/screenshot.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_share/social_share.dart';

class ScorePage extends StatefulWidget {
  const ScorePage({Key key, this.dataId, this.status, this.id,
    this.userId, this.dataSG, this.forCaddie, this.idRepresentCaddie}) : super(key: key);
  final dataId, status, id, userId, dataSG, forCaddie, idRepresentCaddie;
  @override
  _ScorePageState createState() => _ScorePageState();
}

class _ScorePageState extends State<ScorePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final snackbarKey = GlobalKey<ScaffoldState>();
  final GlobalKey _listOneKey = GlobalKey();
  Size _listOneSize = Size.zero;
  final GlobalKey _listTwoKey = GlobalKey();
  Size _listTwoSize = Size.zero;
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataPairing;
  var dataPairingID;
  var dataStart;
  var dataScore;
  var id;
  var idUser;
  var count;
  var myFormat = DateFormat('d MMM yyyy');
  bool _isLoading2 = false;
  bool _isLoading = false;
  bool _isLoadingApprove = false;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.dataId;
    dataPairing = widget.dataSG;
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  getSizeAndPosition() {
    RenderBox _cardBox1 = _listOneKey.currentContext.findRenderObject();
    _listOneSize = _cardBox1.size;
    RenderBox _cardBox2 = _listTwoKey.currentContext.findRenderObject();
    _listTwoSize = _cardBox2.size;
    setState(() {});
  }

  var idOnLocalStorage;
  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    idOnLocalStorage = sharedPreferences.getString("id");
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    } else {
      getScoreEntry();
    }
  }

  //button Approval;
  List<String> count_pair1 = [];
  List<String> count_pair2 = [];
  bool approve = true;
  bool closeGameForOrga = false;
  bool approveForExpired = false;

  getScoreEntry() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token': widget.forCaddie==null?sharedPreferences.getString("token"):widget.forCaddie['token'],
      'token_caddie': widget.forCaddie==null?"":sharedPreferences.getString("token"),
      'id_turnamen': id==null?"0":id
    };
    Utils.postAPI(data, "get_SEV").then((body) {
      if (body['status'] == 'success') {
        dataScore = body['data'];
        idUser = dataScore['user']['id'];
        setState(() {
          dataPairing = body['data'];
          dataPairingID = body['data']['game_pairing'];
          closeGameForOrga = body['data']['tombol_end'];
          approveForExpired = body['data']['expired'];
          _isLoading = false;
          if(body['button'] == 'sembunyikan'){
            approve = true;
          }else{
            approve = false;
          }
          WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
        });

        // print("GET JOIN GAME = ${dataScore}");
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getScoreEntry();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    // getDataNotif();
    if(mounted)
      setState(() {
        // getDataNotif();
      });
    _refreshController.loadComplete();
  }

  String _textMSG, _textSub, idSetPreference;
  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.storage.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.storage].request();
      return permissionStatus[Permission.storage] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }

  Future<void> captureScreen() async {
    PermissionStatus permissionStatus = await _getPermission();
    if(permissionStatus.isGranted) {
      _imageFile = null;
      screenshotController.capture(
          pixelRatio: 1.5, delay: Duration(milliseconds: 10)).then((
          File image) async {
        //print("Capture Done");
        setState(() {
          _imageFile = image;
        });
        final result = await ImageGallerySaver.saveImage(image
            .readAsBytesSync()); // Save image to gallery,  Needs plugin  https://pub.dev/packages/image_gallery_saver
        var snackbar = SnackBar(
          content: Text(
            'Image saved to gallery!', style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.black.withOpacity(0.8),
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }).catchError((onError) {
        print(onError);
      });
    }else {
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text('Permissions Not Granted'),
            content: Text('Please enable storage access permission in system app settings'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ));
    }
  }

  final ScrollController _homeController = ScrollController();

  @override
  Widget build(BuildContext context) {
    if(dataScore!=null?dataScore['jumlah_approve'] == dataScore['jumlah_golfer']:false){
      setState(() {
        approve = false;
      });
    }
    // MediaQueryData queryData;
    // queryData = MediaQuery.of(context);
    return approve ? WillPopScope(
      onWillPop: () async {
        return widget.forCaddie != null ? true : showDialog(
          context: context,
          builder: (context) => alertForWarning(context),
        ) ?? false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        floatingActionButton: ShareSosMed(
          screenShotController: screenshotController,
        ),
        body: SafeArea(
          child: Stack(
            children: [
              SmartRefresher(
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: SingleChildScrollView(
                  controller: _homeController,
                  scrollDirection: Axis.vertical,
                  child: Screenshot(
                    controller: screenshotController,
                    child: Container(
                      color: Colors.white,
                      child: Container(
                        margin: EdgeInsets.all(13),
                        child: _isLoading2 ? Center(
                          heightFactor: 20,
                          child: Container(
                            width: 30,
                            height: 30,
                            child: CircularProgressIndicator(),
                          ),
                        ) : Column(
                          children: [
                            widget.forCaddie==null?
                            HeaderScoring(dataPairing: widget.dataSG, myFormat: myFormat, onClickable: true,):
                            HeaderCaddiePlayGame(dataPlayGame: widget.forCaddie),
                            onlyTextInTop(),
                            pairingPicture(),
                            totalScore(),
                            inputScore(),
                            SubmittedText(msg: _textMSG==null?null:_textMSG,),
                            Container(
                              padding: EdgeInsets.only(top: 8),
                              child: Text("Total golfers that already approve (${dataScore==null?"":dataScore['jumlah_approve']}/${dataScore==null?"":dataScore['jumlah_golfer']})"),
                            ),
                            RefreshScoreButton(
                              isLoading: _isLoading,
                              onTap: () => getScoreEntry(),
                            ),
                            dataScore==null?Container():dataScore['jumlah_approve'] == dataScore['jumlah_golfer'] ? buttoNext() :
                            dataScore==null?Container():approve ? Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              child: Column(
                                children: [
                                  Text("If all golfer scores have been filled, an approve button will appear. And if you have pressed the approve button, you have to wait until all the golfers have approving their scores", style: TextStyle(color: Color(0xFF4f4f4f)),),
                                  SizedBox(height: 8,),
                                  buttonBackWithWarning(context),
                                ],
                              ),
                            ) : buttonApprove(),
                            // ShareSosMed(
                            //   screenShotController: screenshotController,
                            // )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    ) : Scaffold(
      key: _scaffoldKey,
      floatingActionButton: ShareSosMed(
        screenShotController: screenshotController,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            SmartRefresher(
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: SingleChildScrollView(
                controller: _homeController,
                scrollDirection: Axis.vertical,
                child: Screenshot(
                  controller: screenshotController,
                  child: Container(
                    color: Colors.white,
                    child: Container(
                      margin: EdgeInsets.all(13),
                      child: _isLoading2 ? Center(
                        heightFactor: 20,
                        child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      ) : Column(
                        children: [
                          widget.forCaddie == null ?
                          HeaderScoring(dataPairing: widget.dataSG,
                            myFormat: myFormat,
                            onClickable: true,) : HeaderCaddiePlayGame(
                              dataPlayGame: widget.forCaddie),
                          onlyTextInTop(),
                          pairingPicture(),
                          totalScore(),
                          inputScore(),
                          SubmittedText(msg: _textMSG==null?null:_textMSG,),
                          Container(
                            padding: EdgeInsets.only(top: 8),
                            child: Text("Total golfers that already approve (${dataScore['jumlah_approve']}/${dataScore['jumlah_golfer']})"),
                          ),
                          RefreshScoreButton(
                            isLoading: _isLoading,
                            onTap: () => getScoreEntry(),
                          ),
                          dataScore['jumlah_approve'] == dataScore['jumlah_golfer'] ? buttoNext() : approve ? Container(
                            padding: EdgeInsets.symmetric(vertical: 15),
                            child: Text("If all golfer scores have been filled, an approve button will appear. And if you have pressed the approve button, you have to wait until all the golfers have approving their scores", style: TextStyle(color: Color(0xFF4f4f4f)),),
                          ) : buttonApprove(),
                          // ShareSosMed(
                          //   screenShotController: screenshotController,
                          // )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  RaisedButton buttonBackWithWarning(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
      splashColor: Colors.grey.withOpacity(0.5),
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) => alertForWarning(context),
        );
      },
      padding: const EdgeInsets.all(0.0),
      child: Ink(
          decoration: BoxDecoration(
              color: AppTheme.gButton,
              borderRadius: BorderRadius.circular(100)
          ),
          child: Container(
              constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
              alignment: Alignment.center,
              child: Text("Back", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
    );
  }

  Widget alertForWarning(BuildContext context) {
    return CustomAlertDialog(
      title: "Warning",
      contentText: "Scoring still not finished, sure want to leave?",
      noButton: () => Navigator.of(context).pop(false),
      yesButton: () {
        if(widget.forCaddie==null){
          Navigator.pushAndRemoveUntil(
              context, MaterialPageRoute(builder: (BuildContext context) => new MyGames()),
                  (Route<dynamic> route) => false);
        }else{
          Navigator.pushAndRemoveUntil(
              context, MaterialPageRoute(builder: (BuildContext context) => new MyAppointmentCaddie(bottomNavBarActive: true,)),
                  (Route<dynamic> route) => false);
        }
      },
    );
      // AlertDialog(
      //     title: new Text('Warning', style: TextStyle(color: AppTheme.bogies),),
      //     content: new Text("Scoring still not finished, sure want to leave?"),
      //     actions: <Widget>[
      //       RaisedButton(
      //           child: Text('No'),
      //           onPressed: () => Navigator.of(context).pop(false)),
      //       RaisedButton(
      //         color: AppTheme.gRed,
      //         child: Text('Yes'),
      //         onPressed: () {
      //           if(widget.forCaddie==null){
      //             Navigator.pushAndRemoveUntil(
      //                 context, MaterialPageRoute(builder: (BuildContext context) => new MyGames()),
      //                     (Route<dynamic> route) => false);
      //           }else{
      //             Navigator.pushAndRemoveUntil(
      //                 context, MaterialPageRoute(builder: (BuildContext context) => new MyAppointmentCaddie(bottomNavBarActive: true,)),
      //                     (Route<dynamic> route) => false);
      //           }
      //         },
      //       ),
      //     ],
      //   );
  }

  Container buttoNext() {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 5,),
          Text("All golfers have approved their scores, you can ${widget.forCaddie==null?"proceed to the next stage.":"tell your golfer to check his/her score"}", style: TextStyle(color: Color(0xFF4f4f4f)), textAlign: TextAlign.center),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text("Back", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    if(widget.forCaddie==null){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FinalScorePage(dataId: widget.dataId, dataSG: widget.dataSG,)));
                    }else{
                      Navigator.push(context, MaterialPageRoute(builder: (context) => MyAppointmentCaddie(bottomNavBarActive: true,)));
                    }
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text(widget.forCaddie==null?"Next":"Complete!", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Column onlyTextInTop() {
    return Column(
      children: [
        widget.forCaddie==null?Container(
          padding: EdgeInsets.only(top: 5, bottom: 15),
          child: Text(
            "Click tournament title for leaderboard and other info",
            style:
                TextStyle(fontSize: 12, color: Color(0xFF828282)),
          ),
        ):Container(
          padding: EdgeInsets.only(top: 5, bottom: 15),
          alignment: Alignment.centerRight,
          child: InkWell(
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) =>
                RunningLeaderBoards(dataHeader: widget.forCaddie,))),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text("${widget.forCaddie['data_turnamen']['nama_turnamen']}", style: TextStyle(color: Color(0xFF1857B7), fontWeight: FontWeight.bold, fontSize: 18),),
                    Text("${widget.forCaddie['data_turnamen']['nama_lapangan']}, ${DateFormat('d MMM yyyy').format(DateTime.parse(widget.forCaddie['data_turnamen']['tanggal_turnamen']))}", style: TextStyle(color: Color(0xFF1857B7)),),
                    Text("click the arrow to view golfer leaderboards", style: TextStyle(fontSize: 12))
                  ],
                ),
                SvgPicture.asset("assets/svg/entry_arrow_right.svg", color: AppTheme.gButton,)
              ],
            ),
          ),
        ),
        closeGameForOrga ? Column(
          children: [
            Text("The game has expired, as an Organizer, please close the game by pressing this button", style: TextStyle(color: AppTheme.bogies, fontWeight: FontWeight.bold, fontSize: 18, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
            ButtonRectangleRoundedCaddie(color: AppTheme.bogies, textButton: "Close", link: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => CloseGame(dataTournament: widget.dataSG['turnamen'],))),)
          ],
        ) : Container(),
        !closeGameForOrga && approveForExpired
            ? Text(
          "The game has expired, you have to press the approve button in the bottom in order to end the game.",
          style: TextStyle(color: AppTheme.bogies,
              fontWeight: FontWeight.bold,
              fontSize: 18,
              fontStyle: FontStyle.italic), textAlign: TextAlign.center,)
            : Container(),
      ],
    );
  }

  Container inputScore() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Container(
      margin: EdgeInsets.only(top: 13),
      width: double.maxFinite,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: screenWidth/6.6*1,
                child: Text(
                  "Hole",
                  style: TextStyle(
                      color: AppTheme.gFont,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                width: screenWidth/6.6*1,
                margin: EdgeInsets.only(right: screenWidth/6.6*0.2),
                child: Text(
                  "Par",
                  style: TextStyle(
                      color: Color(0xFF2D9CDB),
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              Flexible(
                flex: 1,
                child: Text(
                  "Click hole number to entry score",
                  style: TextStyle(color: Color(0xFF4F4F4F)),
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
          Container(
              width: screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  key: _listOneKey,
                  width: screenWidth/6.6*2,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                    itemCount: dataScore == null ? 0 : dataScore['lapangan1'].length,
                    itemBuilder: (context, index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            width: screenWidth/6.6*1,
                            alignment: Alignment.centerLeft,
                            child: Theme(
                              data: ThemeData(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                              ),
                              child: InkWell(
                                onTap: () => handleInputScoreCourse1(index),
                                child: Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.only(top: 7, bottom: 7),
                                    width: screenWidth/6.6*0.49,
                                    height: screenWidth/6.6*0.49,
                                    decoration: BoxDecoration(
                                      color: AppTheme.gFont,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Text("${dataScore==null?"":dataScore['lapangan1'][index]['hole']}", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
                                    )),
                              ),
                            ),
                          ),
                          Container(
                              width: screenWidth/6.6*1,
                              child: Text("${dataScore==null?"":dataScore['lapangan1'][index]['par']}", textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 20),)),
                        ],
                      );
                    }),
                ),
                Container(
                  width: screenWidth/6.6*4,
                  height: _listOneSize.height,
                  child: ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                      itemBuilder: (context, index2) {
                        return Container(
                          width: screenWidth/6.6*1,
                          child: ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: dataScore==null?0:dataScore['turnamen_pairing'][index2]['pairing_score1'].length,
                            itemBuilder: (context, index3) {
                              return InkWell(
                                onTap: () => handleInputScoreCourse1(index3),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: idOnLocalStorage == dataScore['turnamen_pairing'][index2]['user_id'].toString() ? dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna'] == null ? Colors.white:Colors.grey.withOpacity(0.1) : Colors.white,
                                      // borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: Container(
                                      width: screenWidth/6.6*0.49,
                                      height: screenWidth/6.6*0.49,
                                      margin: EdgeInsets.only(top: 7, bottom: 7, left: screenWidth/6.6*0.25, right: screenWidth/6.6*0.25),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        borderRadius: dataScore['turnamen_pairing'][index2]['pairing_score1'][index3] == null ? BorderRadius.circular(0) :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Par'?BorderRadius.circular(30):BorderRadius.circular(8),
                                        color: dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score1'][index3] == null ? Colors.white :
                                        dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna'] != "" || dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='warna_lain'?
                                          dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Eagle'?AppTheme.eagle:
                                          dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Birdie'?AppTheme.birdie:
                                          dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Par'?AppTheme.par:
                                          dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=='Bogies'?AppTheme.bogies:
                                          Colors.white:Colors.white,
                                      ),
                                      child: Center(
                                        child: Text("${dataScore==null?"":dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]==null?"":dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['score']}",
                                          style: TextStyle(color:
                                            dataScore==null?Colors.white
                                                :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]==null?Colors.white
                                                :dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']==""||dataScore['turnamen_pairing'][index2]['pairing_score1'][index3]['warna']=="warna_lain"? Color(0xFF4F4F4F):Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20), textAlign: TextAlign.center,
                                        ),
                                      )
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      }),
                ),
              ],
            )
          ),
          Container(
            width: screenWidth,
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.only(top: 6),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xFFBDBDBD)),
                    bottom: BorderSide(color: Color(0xFFBDBDBD)))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: screenWidth/6.6*1,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_hole1']}",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.center,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_par1']}",
                    style: TextStyle(
                        color: Color(0xFF2D9CDB),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  width: screenWidth/6.6*4,
                  height: 30,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: screenWidth/6.6*1,
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_score1']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['plus1']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 12),
                              )
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RemarkScore(),
          ),
          dataScore==null || (dataScore['lapangan2'].isEmpty || dataScore['lapangan2'] == null || dataScore['lapangan2'] == "null") ? Container() :
          Container(
              width: screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    key: _listTwoKey,
                    width: screenWidth/6.6*2,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: dataScore == null ? 0 : dataScore['lapangan2'].length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    width: screenWidth/6.6*1,
                                    alignment: Alignment.centerLeft,
                                    child: Theme(
                                      data: ThemeData(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                      ),
                                      child: InkWell(
                                        onTap: () => handleInputScoreCourse2(index),
                                        child: Container(
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(top: 7, bottom: 7),
                                            width: screenWidth/6.6*0.49,
                                            height: screenWidth/6.6*0.49,
                                            decoration: BoxDecoration(
                                              color: AppTheme.gFont,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Text("${dataScore==null?"":dataScore['lapangan2'][index]['hole']}", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
                                            )),
                                      ),
                                    ),
                                  ),
                                  Container(
                                      width: screenWidth/6.6*1,
                                      alignment: Alignment.center,
                                      child: Text("${dataScore==null?"":dataScore['lapangan2'][index]['par']}", textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 20),)),
                                ],
                              ),
                            ],
                          );
                        }),
                  ),
                  Container(
                    width: screenWidth/6.6*4,
                    height: _listTwoSize.height,
                    child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                        itemBuilder: (context, index2) {
                          return Container(
                            width: screenWidth/6.6*1,
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: dataScore==null?0:dataScore['turnamen_pairing'][index2]['pairing_score2'].length,
                              itemBuilder: (context, index3) {
                                return InkWell(
                                  onTap: () => handleInputScoreCourse2(index3),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: idOnLocalStorage == dataScore['turnamen_pairing'][index2]['user_id'].toString() ? dataScore==null/*?Colors.white:*/||dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna'] == null ? Colors.white:Colors.grey.withOpacity(0.1) : Colors.white,
                                      // borderRadius: BorderRadius.circular(8)
                                    ),
                                    child: Container(
                                        width: screenWidth/6.6*0.49,
                                        height: screenWidth/6.6*0.49,
                                        margin: EdgeInsets.only(top: 7, bottom: 7, left: screenWidth/6.6*0.25, right: screenWidth/6.6*0.25),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          borderRadius: dataScore['turnamen_pairing'][index2]['pairing_score2'][index3] == null ? BorderRadius.circular(0) :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Par'?BorderRadius.circular(30):BorderRadius.circular(8),
                                          color: dataScore==null?Colors.white:dataScore['turnamen_pairing'][index2]['pairing_score2'][index3] == null ? Colors.white :
                                          dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna'] != "" || dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='warna_lain'?
                                          dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Eagle'?AppTheme.eagle:
                                          dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Birdie'?AppTheme.birdie:
                                          dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Par'?AppTheme.par:
                                          dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=='Bogies'?AppTheme.bogies:
                                          Colors.white:Colors.white,
                                        ),
                                        child: Center(
                                          child: Text("${dataScore==null?"":dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]==null?"":dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['score']}",
                                            style: TextStyle(color:
                                            dataScore==null?Colors.white
                                                :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]==null?Colors.white
                                                :dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']==""||dataScore['turnamen_pairing'][index2]['pairing_score2'][index3]['warna']=="warna_lain"? Color(0xFF4F4F4F):Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20), textAlign: TextAlign.center,
                                          ),
                                        )
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                        }),
                  ),
                ],
              )),
          Container(
            width: screenWidth,
            margin: EdgeInsets.only(top: 5, bottom: 5),
            padding: EdgeInsets.only(top: 6),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color(0xFFBDBDBD)),
                    bottom: BorderSide(color: Color(0xFFBDBDBD)))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_hole2']}",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*1,
                  alignment: Alignment.center,
                  child: Text(
                    "${dataScore==null?"":dataScore['total_par2']}",
                    style: TextStyle(
                        color: Color(0xFF2D9CDB),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: screenWidth/6.6*4,
                  height: 30,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: screenWidth/6.6*1,
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_score2']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['plus2']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 12),
                              )
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Container totalScore() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Container(
      width: screenWidth,
      margin: EdgeInsets.only(top: 13),
      padding: EdgeInsets.only(top: 6),
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color: Color(0xFFBDBDBD)),
              bottom: BorderSide(color: Color(0xFFBDBDBD)))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: screenWidth/6.6*1,
            child: Text(
              "Total",
              style: TextStyle(
                  color: AppTheme.gFont,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: screenWidth/6.6*1,
          ),
          Container(
            width: screenWidth/6.6*4,
            height: 30,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                primary: false,
                itemCount: dataScore==null?0:dataScore['turnamen_pairing'].length,
                itemBuilder: (context, index) {
                  return Container(
                    width: screenWidth/6.6*1,
                    padding: EdgeInsets.only(left: 0),
                    child: Wrap(
                      children: [
                        Padding(
                          // padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left :0.0) : EdgeInsets.only(left :8.0),
                          padding: EdgeInsets.only(left: 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_semua_score']}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${dataScore==null?"":dataScore['turnamen_pairing'][index]['total_semua_score_pluss']}",
                                style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 11),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget pairingPicture() {
    MediaQueryData _mediaQueryData;
    double screenWidth;
    double screenHeight;
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 8),
          child: Text(
            "Pairing Scorecard",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF4F4F4F)),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: screenWidth/6.6*1,
              child: Column(
                children: [
                  Text("Pairing"),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 2),
                    width: screenWidth/6.6*1,
                    padding: EdgeInsets.only(bottom: 8, top: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF15411),
                    ),
                    child: Text(
                      "${dataPairingID==null?"":dataPairingID['pairing_id']}",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text("Start #${dataScore==null?"":dataScore['starting_hole']}", style: TextStyle(fontSize: 13),)
                ],
              ),
            ),
            Container(
              width: screenWidth/6.6*1,
            ),
            Container(
              height: 80,
              width: screenWidth/6.6*4,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  primary: false,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: dataPairing==null?0:dataPairing['turnamen_pairing_group'].length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () async{
                        await showDialog(
                            context: _scaffoldKey.currentContext,
                          builder: (BuildContext context) => namePopUp(
                            name: "${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_pendek']!=null?dataPairing['turnamen_pairing_group'][index]['nama_pendek']+", ":""}${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_lengkap']}",
                            photo: dataPairing['turnamen_pairing_group'][index]['foto'] == null ?
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Image.asset("assets/images/userImage.png",
                                height: 120, width: 120,
                              ),
                            ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) => CircularProgressIndicator(
                                  value: progress.progress,),
                              imageUrl: Utils.url + 'upload/user/' + dataPairing['turnamen_pairing_group'][index]['foto'], height: 120, width: 120, fit: BoxFit.cover,)),
                          )
                        );
                      },
                      child: Container(
                        margin: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? EdgeInsets.symmetric(horizontal: 3) : EdgeInsets.zero,
                        padding: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? EdgeInsets.all(2) : EdgeInsets.all(0),
                        width: screenWidth/6.6*1,
                        // decoration: BoxDecoration(
                        //   color: sharedPreferences.getString("id") == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? Colors.grey.withOpacity(0.15) : Colors.white,
                        //   borderRadius: BorderRadius.circular(8)
                        // ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                padding: EdgeInsets.only(top: 5),
                                // decoration: BoxDecoration(
                                //   shape: BoxShape.circle,
                                // ),
                                child:
                                    dataPairing['turnamen_pairing_group'][index]['foto'] == null ?
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10000.0),
                                      child: Image.asset(
                                  "assets/images/userImage.png",
                                  height: screenWidth/6.6*0.8, width: screenWidth/6.6*0.8, fit: BoxFit.cover,
                                ),
                                    )
                                : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                          CircularProgressIndicator(
                                            value: progress.progress,
                                          ), imageUrl: Utils.url + 'upload/user/' + dataPairing['turnamen_pairing_group'][index]['foto'], height: screenWidth/6.6*0.8, width: screenWidth/6.6*0.8, fit: BoxFit.cover,)),
                                ),
                            Center(
                              child: Text(
                                "${dataPairing==null?"":dataPairing['turnamen_pairing_group'][index]['nama_pendek']!=null?dataPairing['turnamen_pairing_group'][index]['nama_pendek']:dataPairing['turnamen_pairing_group'][index]['nama_lengkap']}",
                                // "Dustin J",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() || dataPairing['turnamen_pairing_group'][index]['warna_nama'] == 'ada' ? AppTheme.bogies : Color(0xFF4F4F4F), fontWeight: idOnLocalStorage == dataPairing['turnamen_pairing_group'][index]['user_id'].toString() ? FontWeight.bold : FontWeight.normal),
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ],
    );
  }

  Container buttonApprove() {
    return Container(
      padding: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          Text("All score has been entered completely", style: TextStyle(color: Color(0xFF4F4F4F)),),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buttonBackWithWarning(context),
            RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
              splashColor: Colors.grey.withOpacity(0.5),
              onPressed: () {
                setState(() {
                  _isLoadingApprove = true;
                  approveScore();
                });
              },
              padding: const EdgeInsets.all(0.0),
              child: Ink(
                  decoration: BoxDecoration(
                      color: AppTheme.gButton,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: Container(
                      constraints: const BoxConstraints(maxWidth: 136, minHeight: 51),
                      alignment: Alignment.center,
                      child: _isLoadingApprove ? Center(
                        child: Container(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(),
                        ),
                      ) : Text(
                        "Approve Score", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,)))),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget namePopUp({String name, Widget photo}){
    return AlertDialog(
      title: Center(child: Column(
        children: [
          photo,
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(name, style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
          ),
        ],
      )),
      backgroundColor: Colors.black.withOpacity(0.5),
    );
  }

  void approveScore() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': id==null?"0":id,
      'id_user':dataScore['user']['id']
    };
    Utils.postAPI(data, "AS").then((body) {
      setState(() {
        _isLoadingApprove = false;
      });
      if (body['status'] == 'success') {
        print("MSG = ${body['message']}");
        setState(() {
          getScoreEntry();
        });
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void handleInputScoreCourse1(index) async {
    for (int a = 0; a<dataScore['turnamen_pairing'].length; a++){
      if (dataScore['turnamen_pairing'][a]['user_id'] == idUser){
        for (int i = 0; i<dataScore['turnamen_pairing'][a]['pairing_score1'].length; i++){
          if (dataScore['turnamen_pairing'][a]['pairing_score1'][i]['hole'] == dataScore['lapangan1'][index]['hole']){
            await showDialog(
              context: _scaffoldKey.currentContext,
              builder: (BuildContext context) => EntryDialog(
                mToken: widget.forCaddie==null?sharedPreferences.getString("token"):widget.forCaddie['token'],
                hole: "${dataScore['lapangan1'][index]['hole']}",
                tourID: "$id",
                userID: "${dataScore['user']['id']}",
                mScore: dataScore['turnamen_pairing'][a]['pairing_score1'][i]['score'] == null ? dataScore['lapangan1'][index]['par'].toString() : "${dataScore['turnamen_pairing'][a]['pairing_score1'][i]['score']}",
                tokenCaddie: widget.forCaddie==null?"":sharedPreferences.getString("token"),
                idRepresentCaddie: widget.idRepresentCaddie.toString(),
              ),
            ).then((value){
              if(value != null) {
                setState(() {
                  print("PPPPP $value");
                  if (value == 'sembunyikan') {
                    approve = true;
                  } else if(value == 'tampilkan'){
                    approve = false;
                    _textMSG = null;
                  }else if(value == 'Tidak dapat mengubah score, sudah melakukan approve'){
                    Utils.showToast(context, 'erraaaor', 'Cannot change score, already approved');
                    // _textMSG = 'Cannot change score, already approved';
                  }else{
                    Utils.showToast(context, 'aaa', '$value');
                    // _textMSG = value;
                  }
                  getScoreEntry();
                });
              }
            });
          }
        }
      }
    }
  }

  void handleInputScoreCourse2(index) async {
    for (int a = 0; a<dataScore['turnamen_pairing'].length; a++){
      if (dataScore['turnamen_pairing'][a]['user_id'] == idUser){
        for (int i = 0; i<dataScore['turnamen_pairing'][a]['pairing_score2'].length; i++){
          if (dataScore['turnamen_pairing'][a]['pairing_score2'][i]['hole'] == dataScore['lapangan2'][index]['hole']){
            await showDialog(
              context: _scaffoldKey.currentContext,
              builder: (BuildContext context) => EntryDialog(
                  mToken: widget.forCaddie==null?sharedPreferences.getString("token"):widget.forCaddie['token'],
                  hole: "${dataScore['lapangan2'][index]['hole']}",
                  tourID: "$id",
                  userID: "${dataScore['user']['id']}",
                  mScore: dataScore['turnamen_pairing'][a]['pairing_score2'][i]['score'] == null ? dataScore['lapangan2'][index]['par'].toString() : "${dataScore['turnamen_pairing'][a]['pairing_score2'][i]['score']}",
                  tokenCaddie: widget.forCaddie==null?"":sharedPreferences.getString("token")
              ),
            ).then((value){
              if(value != null) {
                setState(() {
                  print("PPPPP $value");
                  if (value == 'sembunyikan') {
                    approve = true;
                  } else if(value == 'tampilkan'){
                    _textMSG = null;
                    approve = false;
                  }else if(value == 'Tidak dapat mengubah score, sudah melakukan approve'){
                    Utils.showToast(context, 'eaaa', 'Cannot change score, already approved');
                    // _textMSG = 'Cannot change score, already approved';
                  }else{
                    Utils.showToast(context, 'aaa', '$value');
                    // _textMSG = value;
                  }
                  getScoreEntry();
                });
              }
            });
          }
        }
      }
    }
  }
}

class RefreshScoreButton extends StatelessWidget {
  const RefreshScoreButton({
    Key key,
    @required bool isLoading,
    @required this.onTap,
  }) : _isLoading = isLoading, super(key: key);

  final bool _isLoading;
  final onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: InkWell(
        onTap: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("refresh the scores", style: TextStyle(color: AppTheme.gButton)),
            _isLoading ? Container(margin: EdgeInsets.only(left: 8), width: 10, height: 10, child:
            CircularProgressIndicator(strokeWidth: 2,)) : Container()
          ],
        ),
      ),
    );
  }
}





