import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/main.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchJoinGames extends StatefulWidget {
  @override
  _SearchJoinGamesState createState() => _SearchJoinGamesState();
}

class _SearchJoinGamesState extends State<SearchJoinGames> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('d MMM yyyy');


  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getAllGames();
    }
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();

  var dataAllGames;
  
  void getAllGames() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    setState(() {
      _member.clear();
      _allMember.clear();
      _uiMember.clear();
    });

    Utils.postAPI(data, "get_turnamen_open").then((body) {
      if(body['status'] == 'success'){
        dataAllGames = body['data'];
        setState(() {
          for (int i = 0; i < dataAllGames.length; i++) {
            DefaultMembers def = DefaultMembers(id: dataAllGames[i]['id'],
                kode_tur: dataAllGames[i]["kode_turnamen"],
                nama_tur: dataAllGames[i]["nama_turnamen"],
                penyelenggara: dataAllGames[i]['penyelenggara_turnamen'],
                tipe: dataAllGames[i]["tipe_turnamen"],
                kota: dataAllGames[i]["kota_turnamen"],
                negara: dataAllGames[i]["negara_turnamen"],
                lapangan: dataAllGames[i]["nama_lapangan"],
                data: dataAllGames[i],
                tgl: dataAllGames[i]["tanggal_turnamen"]);
            _member.add(def);
            _allMember.add(CustomMembers(showAll: def));
            _uiMember.add(CustomMembers(showAll: def));
            // print("DATA ${_uiMember[i].showAll.nama_lengkap}");
          }
        });
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.kode_tur.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.nama_tur.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.tipe.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.lapangan.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.penyelenggara.toLowerCase().contains(mQuery.toLowerCase())
          || DateFormat("MMMM").format(DateTime.parse(item.showAll.tgl)).toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.tgl.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void updateInformation(String information) {
    setState(() {
      if(information == 'reload'){
        getAllGames();
      }
    });
  }

  void moveToOtherPage({data}) async {
    final information = await Navigator.push(context, MaterialPageRoute(builder: (context) => data)).then((value) => setState(() {}));
    // updateInformation(information);
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getAllGames();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getAllGames();
    if(mounted)
      setState(() {
        getAllGames();
      });
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return SmartRefresher(
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: constraints.maxHeight),
                    child: SizedBox(
                      height: _isLoading || _uiMember==null || _uiMember.length <= 0 ? constraints.maxHeight : null,
                      child: Container(
                        margin: EdgeInsets.all(13),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            HeaderCreate(title: "Join Games", backSummary: true,),
                            SizedBox(height: 13,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(child: TitleCenter(
                                  textTitle: "All Available Games in GIF!",
                                )),
                              ],
                            ),
                            Container(
                              width: 250,
                              height: 45,
                              padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 4, top: 5),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  labelText: "Search Games",
                                  hintText: "Search",
                                  suffixIcon: Icon(Icons.search),
                                  contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                                ),
                                controller: controllerSearch3,
                                onChanged: (value) {
                                  _searchMembers(value);
                                },
                              ),
                            ),
                            SizedBox(height: 8,),
                            _isLoading ? Expanded(
                              child: Center(
                                child: LoadingMenu(
                                  withoutMargin: false,
                                ),
                              ),
                            ) :
                            _uiMember==null || _uiMember.length <= 0
                                ? Expanded(
                              child: Center(
                                child: Text("There's no new games at this moment.",
                                  style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                              ),)
                                : Column(
                              children: [
                                Text("showing ${dataAllGames==null?0:_uiMember.length>=10?10:_uiMember.length}"
                                    " out of ${dataAllGames==null?0:dataAllGames.length} from the latest open games",
                                  style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontSize: 13),),
                                SizedBox(height: 3,),
                                ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: _uiMember==null || _uiMember.length <= 0 ? 0 : _uiMember.length>=10?10:_uiMember.length,
                                  itemBuilder: (context, index){
                                    return Container(
                                      width: double.maxFinite,
                                      margin: EdgeInsets.only(bottom: 4),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                          color: Color(0xFFF9F9F9),
                                          border: Border.all(
                                              color: Color(0xFFE5E5E5)
                                          )
                                      ),
                                      child: FlatButton(
                                        padding: EdgeInsets.all(5),
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                        splashColor: Colors.grey.withOpacity(0.2),
                                        textTheme: ButtonTextTheme.normal,
                                        onPressed: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => JoinGames(data: _uiMember[index].showAll.data))).then((value) => setState(() {
                                            getAllGames();
                                          }));
                                          // moveToOtherPage(data: JoinGames(data: _uiMember[index].showAll.data));
                                        },
                                        child: Stack(
                                          children: [
                                            Row(
                                              // mainAxisSize: MainAxisSize.max,
                                              // crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      width: 48,
                                                      height: 25,
                                                      padding: EdgeInsets.all(3),
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                                                          color: DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Aug" ||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Sep" ||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Oct"||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Nov"||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton
                                                      ),
                                                      child: Center(child: Text("${DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl))}", style: TextStyle(fontSize: 16, color: Colors.white),)),
                                                    ),
                                                    Container(
                                                      width: 48,
                                                      height: 32,
                                                      padding: EdgeInsets.all(3),
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                                                          color: Color(0xFFF9F9F9),
                                                          border: Border.all(color: DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Aug" ||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Sep" ||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Oct"||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Nov"||
                                                              DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton, width: 2)
                                                      ),
                                                      child: Center(child: Text("${DateFormat("d").format(DateTime.parse(_uiMember[index].showAll.tgl))}", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),)),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(width: 6,),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Expanded(child:
                                                          RichText(
                                                            text: TextSpan(
                                                                children: [
                                                                  TextSpan(text: "${_uiMember[index].showAll.nama_tur},", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16, fontWeight: FontWeight.bold),),
                                                                  TextSpan(text: " ${_uiMember[index].showAll.kode_tur}", style: TextStyle(color: AppTheme.bogies, fontSize: 16, fontWeight: FontWeight.bold),),
                                                                ]
                                                            ),
                                                          )),
                                                          SizedBox(width: 20,)
                                                        ],
                                                      ),
                                                      Text("${_uiMember[index].showAll.lapangan}, ${_uiMember[index].showAll.kota}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                                                      Row(
                                                        children: [
                                                          Text("${_uiMember[index].showAll.tipe}, ", style: TextStyle(color: AppTheme.par, fontWeight: FontWeight.bold),),
                                                          Expanded(
                                                            child: Text("${_uiMember[index].showAll.penyelenggara}", style: TextStyle(
                                                                color: DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Aug" ||
                                                                    DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Sep" ||
                                                                    DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Oct"||
                                                                    DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Nov"||
                                                                    DateFormat("MMM").format(DateTime.parse(_uiMember[index].showAll.tgl)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                // Align(alignment: Alignment.topRight, child: SvgPicture.asset("assets/svg/check.svg", width: 20, height: 20, color: AppTheme.gFont,))
                                              ],
                                            ),
                                            Positioned(
                                              top: 0,
                                              right: 0,
                                              child: SvgPicture.asset("assets/svg/join.svg", color: Color(0xFFF15411), width: 20, height: 20,),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}

class TitleCenter extends StatelessWidget {
  const TitleCenter({
    Key key, @required this.textTitle, this.color
  }) : super(key: key);

  final textTitle, color;

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("$textTitle", style: TextStyle(color: color == null ? AppTheme.gButton : color, fontWeight: FontWeight.bold, fontSize: 24),));
  }
}

class DefaultMembers{
  final int id;
  final String kode_tur;
  final String nama_tur;
  final String penyelenggara;
  final String tipe;
  final String negara;
  final String kota;
  final String lapangan;
  final String tgl;
  var data;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.kode_tur,
    this.nama_tur,
    this.penyelenggara,
    this.tipe,
    this.kota,
    this.negara,
    this.lapangan,
    this.tgl,
    this.data,
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
