import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/caddie/my_ratings.dart';
import 'package:gif/caddie/search_golfer.dart';
import 'package:gif/create_games/create_games.dart';
import 'package:gif/join_games/join_games.dart';
import 'package:gif/my_games/my_games.dart';
import 'package:gif/my_games/trophy_room.dart';
import 'package:gif/notification/notif_golfer.dart';
import 'package:gif/notification/notif_running.dart';
import 'package:gif/socials/home_groups.dart';
import 'package:gif/the_game/start_game.dart';

import 'caddie/play_caddie.dart';
import 'socials/buddys/add_buddy.dart';
import 'socials/create_group.dart';

class HeaderHome extends StatelessWidget {
  HeaderHome({
    Key key,
    @required this.countNotifGolfer,
    @required this.countRunningNotif,
    @required this.page,
    @required this.count1, this.context,
    this.text,
    this.forCaddie,
    this.otherOnTapFunction,
    this.checkDataTrophy,
    this.dataTrophy,
    this.forMyGamesCaddie,
  }) : super(key: key);

  final int countNotifGolfer;
  final int countRunningNotif;
  final int count1;
  final BuildContext context;
  final text, dataTrophy, checkDataTrophy;
  final forCaddie, otherOnTapFunction;
  final page, forMyGamesCaddie;

  List<String> titleMenu(){
    if(page == "home"){
      return ["Running Games", "Join Games", "Create Games"];
    } else if(page == "my games"){
      return [forMyGamesCaddie==true?"Running Games":"Caddie", "Join Games", "Trophy Room"];
    } else if(page == "socials"){
      return ["Add Buddy", "Join Clubs", "Create New Club"];
    } else{
      return ["Running Games", "Represent Golfer"];
    }
  }

  List<String> iconMenu(){
    if(page == "home"){
      return ["cart", "join", "create"];
    } else if(page == "my games"){
      return [forMyGamesCaddie==true?"cart":"golf_caddy", "join", "trophy"];
    } else if(page == "socials"){
      return ["add_friend", "represent", "add_group"];
    } else{
      return ["cart", "represent"];
    }
  }

  List<dynamic> linkMenu(){
    if(page == "home"){
      return [MyGames(), SearchJoinGames(), CreateGames()];
    } else if(page == "my games"){
      return [SearchRepresent(findLevel: "Caddie", forBookCaddie: true,), SearchJoinGames(), TrophyRoom(dataTrophy: dataTrophy, checkData: checkDataTrophy,)];
    } else if(page == "socials"){
      return [AddBuddy(buddyScore: false,), HomeGroups(), CreateGroup()];
    } else{
      return [MyAppointmentCaddie(bottomNavBarActive: true,), PlayCaddie()];
    }
  }

  List<Color> colorMenu = [AppTheme.gcart, AppTheme.gjoin, AppTheme.gcreate];

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 160.4,
          padding: EdgeInsets.all(8),
          width: double.maxFinite,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [ AppTheme.gBlue, AppTheme.gBlueOcean, ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        if(forCaddie == true){
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => MyAppointmentCaddie(bottomNavBarActive: true,)));
                        }else {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => NotifGolfer()));
                        }
                      },
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: Stack(
                          alignment: Alignment.bottomLeft,
                          children: [
                            SvgPicture.asset(forCaddie != null ? "assets/svg/appointment.svg" : "assets/svg/ball.svg", width: 20, height: 20, color: Colors.white,),
                            countNotifGolfer == null || countNotifGolfer == 0 ? Container() : Positioned(
                              right: 0,
                              top: 0,
                              child: CircleAvatar(
                                radius: 10,
                                  backgroundColor: Color(0xFFF70707),
                                  child: Text("${countNotifGolfer != null ? countNotifGolfer : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 5,),
                    GestureDetector(
                      onTap: () {
                        if(forCaddie == true){
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => MyRatingsCaddie(bottomNavBarActive: true,)));
                        }else {
                          Navigator.push(context, MaterialPageRoute(builder: (
                              context) => RunningNotif()));
                        }
                      },
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: Stack(
                          alignment: Alignment.bottomLeft,
                          children: [
                            SvgPicture.asset(forCaddie != null ? "assets/svg/star.svg" : "assets/svg/flag.svg", width: 20, height: 20, color: Colors.white,),
                            countRunningNotif == null || countRunningNotif == 0 ? Container() : Positioned(
                              right: 0,
                              top: 0,
                              child: CircleAvatar(
                                radius: 10,
                                  backgroundColor: Color(0xFFF70707),
                                  child: Text("${countRunningNotif != null ? countRunningNotif : "0"}", style: TextStyle(fontSize: 12, color: Colors.white), textAlign: TextAlign.center,)),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Image.asset("assets/images/gif_logo.png", width: 60, height: 60, )
            ],
          ),
        ),
        Positioned(
          top: 60.5,
          left: 0.9,
          right: 0.9,
          bottom: 0.8,
          child: Column(
            children: [
              Text(
                text,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5,),
              Container(
                height: 71,
                padding: EdgeInsets.only(top: page == "socials" ? 18 : 15),
                width: double.maxFinite,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      AppTheme.gGrey,
                      AppTheme.gWhite,
                    ],
                  ),
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(titleMenu().length, (index) => Expanded(
                    child: InkWell(
                      onTap: otherOnTapFunction == null ? () => Navigator.push(context, MaterialPageRoute(builder: (context) => linkMenu()[index])) : otherOnTapFunction[index],
                      child: Column(
                        children: [
                          SvgPicture.asset("assets/svg/${iconMenu()[index]}.svg", width: page == "socials" ? 24 : page == "caddie" ? 22 : 27, height: page == "socials" ? 24 : page == "caddie" ? 22 : 27, color: colorMenu[index],),
                          SizedBox(height: 3,),
                          Text("${titleMenu()[index]}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 13), textAlign: TextAlign.center,)
                        ],
                      ),
                    ),
                  )),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
