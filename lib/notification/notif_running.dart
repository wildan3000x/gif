import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../header_home.dart';

class RunningNotif extends StatefulWidget {
  @override
  _RunningNotifState createState() => _RunningNotifState();
}

class _RunningNotifState extends State<RunningNotif> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getDataRunningNotif();
      getDataNotifCount();
      getMyRecentScoreCards();
    }
  }

  var dataRunningNotif;
  bool _isLoading = false;

  getDataRunningNotif() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      // 'token' : 'XDuHVPNVExZfeMJ05d5f0d0c9f2d1fAC'
    };

    Utils.postAPI(data, "rg_notifikasi").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
          dataRunningNotif = body['data'];
        });
      }else{
        print("ERROR ${body['message']}");
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var countNotifGolfer;
  getDataNotifCount() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      // 'token' : 'XDuHVPNVExZfeMJ05d5f0d0c9f2d1fAC'
    };
    setState(() {
      _isLoading = true;
    });
    Utils.postAPI(data, "test_notifikasi_tournament").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
          countNotifGolfer = body['notifikasi_count'];
        });
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataRecentScore;
  var dataTrophyRoom;
  bool _isLoading3 = false;
  getMyRecentScoreCards() async {
    setState(() {
      _isLoading3 = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "my_recent_scorecards").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading3 = false;
        });
        dataRecentScore = body['data'];
        dataTrophyRoom = body;
        print("DATA TROPHY $dataTrophyRoom");
      }else{
        print("ERROR ${body['message']}");
        setState(() {
          _isLoading3 = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading3 = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getDataRunningNotif();
    getDataNotifCount();
    getMyRecentScoreCards();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getDataRunningNotif();
    getDataNotifCount();
    getMyRecentScoreCards();
    if(mounted)
      setState(() {
        getDataRunningNotif();
        getDataNotifCount();
        getMyRecentScoreCards();
      });
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(clickable: true, main: true,),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onLoading: _onLoading,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderHome(
                      countNotifGolfer: countNotifGolfer,
                      countRunningNotif: dataRunningNotif==null?0:dataRunningNotif['notikasi_rg_count'],
                      context: context,
                      text: "Notifications",
                      page: "my games",
                      checkDataTrophy: dataTrophyRoom==null?"":dataTrophyRoom['trophy_room'],
                      dataTrophy: dataTrophyRoom==null?"":dataTrophyRoom['trophy_room'],
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top:10),
                        padding: EdgeInsets.all(5),
                        child: Text("Running Games Notifications", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,)
                    ),
                    dataRunningNotif == null ? Container() : dataRunningNotif['alasan_jeda'] == "" && dataRunningNotif['turnamen_winner'].isEmpty ? Container(margin: EdgeInsets.all(40), child: Center(child: Text("no new notifications."))) : Column(
                      children: [
                        dataRunningNotif['alasan_jeda'] == "" ? Container() : Container(
                          margin: EdgeInsets.only(bottom: 5),
                          width: double.maxFinite,
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xFFE5E5E5)),
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xFFF9F9F9)
                          ),
                          child: Row(
                            children: [
                              Expanded(child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${dataRunningNotif['alasan_jeda']['nama_turnamen']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18),),
                                  Text("${dataRunningNotif['alasan_jeda']['alasan_jeda']}", style: TextStyle(color: AppTheme.gFontBlack),),
                                ],
                              )),
                              Image.asset("assets/images/warning_sign.png", width: 40, height: 40, isAntiAlias: true,),
                            ],
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: dataRunningNotif == null ? 0 : dataRunningNotif['turnamen_winner'].length,
                          itemBuilder: (context, index){
                            return Container(
                              margin: EdgeInsets.only(bottom: 5),
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0xFFE5E5E5)),
                                borderRadius: BorderRadius.circular(10),
                                color: Color(0xFFF9F9F9)
                              ),
                              child: Row(
                                children: [
                                  dataRunningNotif['turnamen_winner'][index]['foto'] == null ? ClipRRect(
                                    borderRadius: BorderRadius.circular(10000.0),
                                    child: Image.asset(
                                      "assets/images/userImage.png",
                                      width: 40,
                                      height: 40,
                                    ),
                                  ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                      CircularProgressIndicator(
                                        value: progress.progress,
                                      ), imageUrl: Utils.url + 'upload/user/' + dataRunningNotif['turnamen_winner'][index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                                  SizedBox(width: 10,),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text("${dataRunningNotif['turnamen_winner'][index]['message']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18),),
                                        // Text("on Hole #${dataRunningNotif['turnamen_winner'][index]['hole']}", style: TextStyle(color: AppTheme.gFontBlack),)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          },),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
