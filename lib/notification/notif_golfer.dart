import 'dart:async';
import 'dart:io';

import 'dart:io' show Platform;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/represent_golfer.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/invitation/confirmation_page.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/pairing/swap_pairing.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/socials/club/home_club.dart';
import 'package:gif/socials/club/join_request.dart';
import 'package:gif/socials/club/profile_club.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../header_home.dart';


class NotifGolfer extends StatefulWidget {
  @override
  _NotifGolferState createState() => _NotifGolferState();
}

class _NotifGolferState extends State<NotifGolfer> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  bool _isLoading = true;
  var notif;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getDataNotif();
      getSumRunningNotif();
      getMyRecentScoreCards();
    }
  }

  var dataRecentScore;
  var dataTrophyRoom;
  bool _isLoading3 = false;
  getMyRecentScoreCards() async {
    setState(() {
      _isLoading3 = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "my_recent_scorecards").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading3 = false;
        });
        dataRecentScore = body['data'];
        dataTrophyRoom = body;
        print("DATA TROPHY $dataTrophyRoom");
      }else{
        print("ERROR ${body['message']}");
        setState(() {
          _isLoading3 = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading3 = false;
      });
    });
  }

  var countNotifGolfer;
  getDataNotif() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      // 'token' : 'XDuHVPNVExZfeMJ05d5f0d0c9f2d1fAC'
    };
    setState(() {
      _isLoading = true;
    });
    Utils.postAPI(data, "test_notifikasi_tournament").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
          countNotifGolfer = body['notifikasi_count'];
        });
        // dataTransaksi = body["data"];
        notif = body;
        print("COUNT : ${notif}");
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var countRunningNotif;
  getSumRunningNotif() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "rg_notifikasi").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          countRunningNotif = body['data']['notikasi_rg_count'];
        });
        print("CCC ${countRunningNotif}");
      }else{
        print("CCC ${countRunningNotif}");
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getDataNotif();
    getSumRunningNotif();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getDataNotif();
    getSumRunningNotif();
    if(mounted)
      setState(() {
        getDataNotif();
        getSumRunningNotif();
      });
    _refreshController.loadComplete();
  }

  forDeleteNotif({idNotif, dataNotif, @required type}) async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_notifikasi' : idNotif
    };

    Utils.postAPI(data, "update_notifikasi").then((body) {
      if(body['status'] == 'success'){
        if(type == "join_game_canceled") {
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => JoinGames(data: dataNotif))).then((value) =>
              setState(() {
                getDataNotif();
                getSumRunningNotif();
                print("RELOAD");
              }));
        }else if(type == "closed_game"){
          Utils.showToast(context, "error", "Invalid, notification has expired or the game has been closed/removed");
          setState(() {
            getDataNotif();
            getSumRunningNotif();
            print("RELOAD");
            showOldNotif = true;
            // Scrollable.ensureVisible(scrollToOldNotif.currentContext);
          });
        }else{
          setState(() {
            getDataNotif();
            getSumRunningNotif();
            print("RELOAD");
            showOldNotif = true;
            // Scrollable.ensureVisible(scrollToOldNotif.currentContext);
          });
        }
        print("SUCCESS DELETE NOTIF");
      }else{
        print("FAILED DELETE NOTIF");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      Utils.showToast(context, "wee", "Ops, something went wrong, please try again later.");
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  bool showOldNotif = false;
  final scrollToOldNotif = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        key: snackbarKey,
        bottomNavigationBar: BottomNavBar(clickable: true, main: true,),
        // bottomNavigationBar: BottomNavBar(),
        body: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: Stack(
              children: [
                SmartRefresher(
                  controller: _refreshController,
                  onLoading: _onLoading,
                  onRefresh: _onRefresh,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Container(
                      margin: EdgeInsets.all(13),
                      child: Column(
                        children: [
                          HeaderHome(
                            countNotifGolfer: countNotifGolfer,
                            countRunningNotif: countRunningNotif,
                            count1: countNotifGolfer,
                            context: context,
                            text: "Notifications",
                            page: "home",
                            checkDataTrophy: dataTrophyRoom==null?"":dataTrophyRoom['trophy_room'],
                            dataTrophy: dataTrophyRoom==null?"":dataTrophyRoom['trophy_room'],
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                              margin: EdgeInsets.only(top:10),
                              padding: EdgeInsets.all(5),
                              child: Text("Request Status", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,)
                          ),
                          notif==null||notif['notifikasi_aktif'].length <= 0 ? Center(
                            child: Container(
                              margin: EdgeInsets.all(30),
                              child: Text("No New Notification."),
                            ),
                          ) : ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: notif==null?0:notif['notifikasi_aktif'].length,
                            itemBuilder: (context, index){
                              return containerNotif(
                                link: () {
                                  if(notif['notifikasi_aktif'][index]['expired'] == true){
                                    forDeleteNotif(
                                        idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],
                                        type: "closed_game"
                                    );
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Join Game Confirmed" && notif['notifikasi_aktif'][index]['tipe_turnamen'] == "Tournament"
                                  || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Request"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmPage(id: notif['notifikasi_aktif'][index], idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Request"||notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Request"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => SwapPairing(idTour: notif['notifikasi_aktif'][index]['turnamen_id'], idUserConfirm: notif['notifikasi_aktif'][index]['user_id'], idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Join Game Approved"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Confirmed" && notif['notifikasi_aktif'][index]['tipe_turnamen'] != "Tournament"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Invited Game"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Confirmed"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Confirmed"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Rejected"
                                      || notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Rejected"
                                      ||notif['notifikasi_aktif'][index]['head_message'] == "Change Pairing Rejected"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => PairingGames(
                                        id: notif['notifikasi_aktif'][index]['turnamen_id'],
                                        msg: notif['notifikasi_aktif'][index]['head_message'],
                                        sub_msg: notif['notifikasi_aktif'][index]['body_message'],
                                        idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],
                                        userId: notif['notifikasi_aktif'][index]['user_id_penerima'],
                                        data: /*notif['notifikasi_aktif'][index]['head_message'] == "Tournament Pairing Confirmed" ? */"Confirmed"/* : ""*/,))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Join Game Rejected"){
                                    forDeleteNotif(
                                      idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],
                                      dataNotif: notif['notifikasi_aktif'][index],
                                      type: "join_game_canceled"
                                    );
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Request Join Club"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => JoinRequest(idClub: notif['notifikasi_aktif'][index]['club_golf_id'], idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Confirmed Join Club"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeClub(idClub: notif['notifikasi_aktif'][index]['club_golf_id'], idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Rejected Join Club"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileClub(dataClub: notif['notifikasi_aktif'][index]['club_golf_id'], idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'], fromSearch: true,))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Represent Invite"){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => RepresentGolfer(dataGolferCaddie: notif['notifikasi_aktif'][index], role: "Golfer", fromHomeGolfer: true,))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  }else if(notif['notifikasi_aktif'][index]['head_message'] == "Organizer Invite"){
                                    forDeleteNotif(
                                        idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],
                                        type: "others"
                                    );
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => SummaryGames(
                                      tourId: notif['notifikasi_aktif'][index]['turnamen_id'],))).then((value) => setState(() {
                                      getDataNotif();
                                      getSumRunningNotif();
                                      print("RELOAD");
                                    }));
                                  } else{
                                    forDeleteNotif(
                                        idNotif: notif['notifikasi_aktif'][index]['id_notifikasi'],
                                        type: "others"
                                    );
                                  }
                                },
                                headMsg: notif['notifikasi_aktif'][index]['head_message'],
                                bodyMsg: notif['notifikasi_aktif'][index]['body_message'],
                                footMsg: notif['notifikasi_aktif'][index]['head_message'] == "Represent Invite"
                                    ? "Appointment : " + notif['notifikasi_aktif'][index]['tanggal_janji'] : notif['notifikasi_aktif'][index]['foot_message'],
                                icon: notif['notifikasi_aktif'][index]['head_message'] == "Rejected Join Club"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Rejected"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Rejected"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Request Join Club"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Request"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Request"
                                    // || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Confirmed"
                                    || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Rejected"
                                    ? SvgPicture.asset("assets/svg/check_bold.svg", color: Color(0xFFF15411), width: 18, height: 18,) :
                                      notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Confirmed"
                                          || notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Confirmed"
                                          || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Confirmed"
                                          || notif['notifikasi_aktif'][index]['head_message'] == "Invited Game"
                                          || notif['notifikasi_aktif'][index]['head_message'] == "Join Game Approved"
                                          ? SvgPicture.asset("assets/svg/check_bold.svg", color: AppTheme.gButton, width: 18, height: 18,) :
                                      Icon(Icons.circle, size: 18, color: Color(0xFF4f4f4f)),
                                iconType: notif['notifikasi_aktif'][index]['head_message'] == "Rejected Join Club" || notif['notifikasi_aktif'][index]['head_message'] == "Confirmed Join Club" || notif['notifikasi_aktif'][index]['head_message'] == "Request Join Club"
                                    ? SvgPicture.asset("assets/svg/groups.svg") :
                                      notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Confirmed"||notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Confirmed"||notif['notifikasi_aktif'][index]['head_message'] == "Change Pairing Rejected"
                                          ? SvgPicture.asset("assets/svg/join.svg", color: AppTheme.gButton,) :
                                      notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Request"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Request"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Same Pairing Rejected"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Swap Pairing Rejected"
                                          ? SvgPicture.asset("assets/svg/join.svg") :
                                      notif['notifikasi_aktif'][index]['head_message'] == "Join Game Confirmed" ||notif['notifikasi_aktif'][index]['head_message'] == "Join Game Request"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Join Game Rejected"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Tournament Confirm"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Join Game Approved"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Invited Game"
                                          ||notif['notifikasi_aktif'][index]['head_message'] == "Organizer Invite"
                                          ? SvgPicture.asset("assets/svg/games.svg", color: AppTheme.gButton,) :
                                      notif['notifikasi_aktif'][index]['head_message'] == "Represent Invite" ? notif['notifikasi_aktif'][index]['foto'] == null ? Center(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(10000.0),
                                          child: Image.asset(
                                            "assets/images/userImage.png",
                                            width: 50,
                                            height: 50,
                                          ),
                                        ),
                                      ) : ClipRRect(borderRadius: BorderRadius.circular(1000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) => CircularProgressIndicator(
                                        value: progress.progress,),
                                        imageUrl: Utils.url + 'upload/user/' + notif['notifikasi_aktif'][index]['foto'], height: 50, width: 50, fit: BoxFit.cover,)) : Container(),
                              );},
                          ),
                          // listViewGolferNotif(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                    margin: EdgeInsets.only(top:20),
                                    padding: EdgeInsets.all(5),
                                    child: Row(
                                      children: [
                                        Text("${notif!=null?notif['notifikasi_tidak_aktif'].isEmpty?"":notif['notifikasi_tidak_aktif'].length:""} ", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 14, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
                                        Text("${notif!=null?notif['notifikasi_tidak_aktif'].isEmpty?"":"Old Notifications":""}", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 14, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
                                      ],
                                    )
                                ),
                              ),
                              notif==null ? Container():notif['notifikasi_tidak_aktif'].isEmpty ? Container():Container(
                                  padding: EdgeInsets.only(right: 5),
                                  child: InkWell(
                                      onTap: (){
                                        setState(() {
                                          showOldNotif = !showOldNotif;
                                        });
                                      },
                                      child: Text(showOldNotif?"Hide":"See All", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 14, fontWeight: FontWeight.bold),
                                      ),
                                  ),
                              )
                            ],
                          ),
                          Container(
                            key: scrollToOldNotif,
                            padding: EdgeInsets.only(bottom: 30),
                            child: showOldNotif?listViewOrgaApply():Container(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container containerNotif({link, icon, iconType, headMsg, bodyMsg, footMsg}) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(
              color: Color(0xFFE5E5E5)
          )
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: link,
        child: IntrinsicHeight(
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              iconType,
              SizedBox(width: 8,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("$headMsg", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),),
                    Text("$bodyMsg", style: TextStyle(color: AppTheme.gFontBlack),),
                    Text("$footMsg", style: TextStyle(color: AppTheme.par),),
                  ],
                ),
              ),
              Align(alignment: Alignment.topCenter, child: icon)
            ],
          ),
        ),
      ),
    );
  }

  Widget listViewOrgaApply() {
    return notif!=null?notif['notifikasi_tidak_aktif'].length > 0 ? ListView.builder(
      // scrollDirection: Axis.vertical,
        shrinkWrap: true,
        primary: false,
        itemCount: notif!=null?notif['notifikasi_tidak_aktif'].length:0,
        itemBuilder: (context, index) => Column(
          children: [
            InkWell(
              onTap: () {
                // Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmPage(id: notif['data'][index])));
              },
              child: Container(
                margin: EdgeInsets.only(top: 8),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xFFF9F9F9),
                    border: Border.all(
                        color: Color(0xFFE5E5E5)
                    )
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Text("${dataTransaksi[index]["nama_turnamen"]}", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold, fontSize: 14), textAlign: TextAlign.start,),
                            Text("${notif!=null?notif['notifikasi_tidak_aktif'][index]['head_message']:""}", style: TextStyle(color: Color(0xFF4f4f4f), fontWeight: FontWeight.bold, fontSize: 14), textAlign: TextAlign.start,),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              // child: Text("${dataTransaksi[index]["lapangan_turnamen"]}, ${dataTransaksi[index]["kota_turnamen"]}", style: TextStyle(color: Color(0xFF4F4F4F)),),
                              child: Text("${notif!=null?notif['notifikasi_tidak_aktif'][index]['body_message']:""}", style: TextStyle(color: Color(0xFF4F4F4F))),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              // child: Text("${dataTransaksi[index]["penyelenggara_turnamen"]}", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic),),
                              child: Text("${notif!=null?notif['notifikasi_tidak_aktif'][index]['foot_message']:""}", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
                            )
                          ],
                        ),
                      ),
                    ),
                    Icon(Icons.access_time, color: Color(0xFF4f4f4f),)
                    // Image.asset('assets/images/arrow left.png', color: AppTheme.gFont)
                  ],
                ),
              ),
            ),
          ],
        )
    ): Container(margin: EdgeInsets.all(40), child: Text("no new notifications.")) : Container();
  }

  Container headerHome() {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            AppTheme.gBlue,
            AppTheme.gBlueOcean,
          ],
        ),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Notification",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 15),
            width: 70,
            height: 70,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        "assets/images/GIF for Colored BG.PNG"),
                    fit: BoxFit.cover)),
          ),
        ],
      ),
    );
  }

}

