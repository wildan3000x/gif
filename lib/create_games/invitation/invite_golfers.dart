import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/invitation/type/menu_invite.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InviteGolfer extends StatefulWidget {
  const InviteGolfer({Key key, this.success, this.data, this.msg, this.skip, this.backSummary}) : super(key: key);
  final success;
  final msg;
  final data;
  final skip;
  final backSummary;

  @override
  _InviteGolferState createState() => _InviteGolferState();
}

class _InviteGolferState extends State<InviteGolfer> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _loading = false;
  String _valTypeInv = "My Buddies";
  var myFormat = DateFormat('d MMM yyyy');
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataWinner;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      _getSummary();
    }
  }

  bool _isLoadingGroup = false;

  void getOtherClub() async {
    setState(() {
      _isLoadingGroup = true;
    });
    var data = {
      'tipe_invite' : 'Members of My Golf Club (Group)'
    };

    Utils.postAPI(data, "get_GC").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _isLoadingGroup = false;
        });
      } else {
        print(body['message']);
        setState(() {
          _isLoadingGroup = false;
        });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
      });
    });
  }

  void _getSummary() async {
    setState(() {
      _loading = true;
    });
    var data = {
      "id_turnamen":widget.data,
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_ts").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
          dataWinner = body;
        });
        print("BODY = $dataWinner");
      }else{
        print("ERROR");
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: SafeArea(
            child: Stack(
              children: [
                _loading ? Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      )),
                ) : SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.all(13),
                          child: Column(
                            children: [
                              HeaderCreate(title: "Invite Screen",),
                              titleInfo(),
                              widget.success == 'success'
                                  ? successScreen(context)
                                  : SelectMenuInvite(),
                              widget.success == 'success' ? Container() : MenuInvGolfer(
                                type: _valTypeInv,
                                data: dataWinner!=null?dataWinner:"",
                                id: widget.data,
                                snackKey: snackbarKey,
                                button: widget.skip,
                                backSummary: widget.backSummary,
                              ),
                              // Button()
                            ],
                          ),
                        ),
                      ],
                    ),
                ),
                _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
              ],
            ),
          ),
        ));
  }

  Widget titleInfo() {
    return Container(
      padding: EdgeInsets.only(top: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 3, bottom: 5),
                    child: Text(
                      "${dataWinner != null ? dataWinner['turnamen']['nama_turnamen']:""}",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: AppTheme.gFont,
                      ),
                      overflow: TextOverflow.ellipsis,
                    )),
                Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 3),
                    child: Text("${dataWinner != null ? dataWinner['turnamen']['nama_lapangan']:""}\n${dataWinner != null ? myFormat.format(DateTime.parse(dataWinner['turnamen']['tanggal_turnamen'])):""}")),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                    padding: EdgeInsets.only(right: 3, bottom: 5),
                    child: Text("Current Participant",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: AppTheme.gFont))),
                Container(
                    padding: EdgeInsets.only(right: 3),
                    child: Text(
                      "${dataWinner != null ? dataWinner['invited']:"0"} invited\n${dataWinner != null ? dataWinner['applying']:"0"} Applying\n${dataWinner != null ? dataWinner['confirmed']:"0"} Confirmed",
                      textAlign: TextAlign.end,
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container successScreen(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Column(
        children: [
          Text("Invitation Were Sent", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Color(0Xff2D9CDB)),),
          Align(alignment: Alignment.center, child: Padding(padding: EdgeInsets.symmetric(vertical: 20), child: Text("${widget.msg}", textAlign: TextAlign.center,),)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25.0),
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
              splashColor: Colors.grey.withOpacity(0.5),
              onPressed: () {
                if(widget.skip == true){
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) => SummaryGames(tourId: widget.data,)),
                    ModalRoute.withName('/'),
                  );
                  // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SummaryGames()));
                }else if(widget.backSummary == true){
                  // Navigator.pop(context);
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) => SummaryGames(tourId: widget.data,)),
                    ModalRoute.withName('/'),
                  );
                }else{
                  Navigator.pop(context);
                }
              },
              padding: const EdgeInsets.all(0.0),
              child: Ink(
                  decoration: BoxDecoration(
                      color: AppTheme.gButton,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: Container(
                      constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                      alignment: Alignment.center,
                      child: Text(widget.skip == true ? "Next" : "Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
            ),
          ),
        ],
      ),
    );
  }

  Column SelectMenuInvite() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 22),
          child: Text(
            "Invite Golfers",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF4F4F4F)),
          ),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select from",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: DropdownButton(
            isExpanded: true,
            underline: SizedBox(),
            items: <String>[
              'Members of My Golf Club (Group)',
              'My Buddies',
              'My Contacts',
              'Add Manually'
            ].map((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                _valTypeInv = value;
              });
            },
            value: _valTypeInv,
            hint: Text(
              "Members of My Golf Club (Group)",
              textAlign: TextAlign.center,
            ),
            // value: _valWinners,
          ),
        ),
      ],
    );
  }
}

