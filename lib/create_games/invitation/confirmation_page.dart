import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/notification/notif_golfer.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfirmPage extends StatefulWidget {
  const ConfirmPage({Key key, this.id, this.idNotif}) : super(key: key);
  final id, idNotif;
  @override
  _ConfirmPageState createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');

  bool _isLoading = false;
  bool _isLoading2 = false;
  var dataUser;
  var dataTour;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getDataUser();
    }
  }

  bool _isLoadingPage = false;

  getDataUser() async {
    setState(() {
      _isLoadingPage = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_user_confirm' : widget.id['user_id'],
      'id_turnamen' : widget.id['turnamen_id']
    };

    Utils.postAPI(data, "get_CG").then((body) {
      if(body['status'] == 'success'){
        // dataTransaksi = body["data"];
        dataUser = body['data'];
        dataTour = body['data_turnamen'];
        print("COUNT : ${dataUser}");
        print("DATATOUR : ${dataTour}");
        setState(() {
          _isLoadingPage = false;
        });
      }else{
        setState(() {
          _isLoadingPage = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoadingPage = false;
      });
    });
  }

  confirmUser(String confirm) async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_notifikasi' : widget.idNotif,
      'id_turnamen' : widget.id['turnamen_id'],
      'id_user' : widget.id['user_id'],
      'status' : confirm,
    };

    Utils.postAPI(data, "test_confirm_game").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          if(confirm == "Confirmed"){
            _isLoading = false;
            // Navigator.pushAndRemoveUntil(
            //   context,
            //   MaterialPageRoute(builder: (BuildContext context) => PairingGames(id: dataTour['id_turnamen'],)),
            //   ModalRoute.withName('/'),
            // );

            Navigator.pop(context/*, "reload"*/);
            Utils.showToast(context, "success", "Confirmed");
            // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(id: dataTour['id_turnamen'], navigation: "confirm",)));
          }else{
            _isLoading = false;

            Navigator.pop(context/*, "reload"*/);
            Utils.showToast(context, "errorss", "Rejected");
            // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(id: dataTour['id_turnamen'], navigation: "confirm", msg: "Refused", sub_msg: "you have refused ${dataUser == null ? "" : dataUser['nama_lengkap']} to join this game",)));
          }
        });

        // dataTransaksi = body["data"];
        print("COUNT : ${body}");
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  margin: EdgeInsets.all(15),
                  child: Column(
                    children: [
                      HeaderCreate(title: "Confirm\nGame Pairing",),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                      ),
                      SafeArea(
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 20, bottom: 10),
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Align(
                                  alignment: Alignment.center,
                                  child: _isLoadingPage ? Center(
                                    heightFactor: 10,
                                    child: Container(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(),
                                    ),
                                  ) : Column(
                                    children: [
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      dataUser != null ? dataUser['foto'] == null ? ClipRRect(
                                        borderRadius: BorderRadius.circular(10000.0),
                                        child: Image.asset(
                                          "assets/images/userImage.png",
                                          width: 70,
                                          height: 70,
                                        ),
                                      ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(imageUrl: Utils.url + 'upload/user/' + dataUser['foto'], width: 70, height: 70, fit: BoxFit.cover,)) : Container(),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Text(
                                          "${dataUser == null ? "" : dataUser['nama_lengkap']}",
                                          style: TextStyle(fontSize: 18),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                            "GIF ID : ${dataUser == null ? "" : dataUser['kode_user']}",
                                            style: TextStyle(color: AppTheme.gFont)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                            "PHONE : ${dataUser == null ? "" : dataUser['no_telp']}",
                                            style: TextStyle(color: AppTheme.gFont)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 30.0, bottom: 5),
                                        child: Text(
                                            "ask for approval on",
                                            style: TextStyle( color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "${dataTour == null ? "" : dataTour['nama_turnamen']}",
                                          style: TextStyle(fontSize: 18,),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                            "${dataTour==null?"":myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))}",
                                            style: TextStyle(color: AppTheme.gFont)),
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(vertical: 25),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            buttonConfirmReject(
                                              link: () {
                                                setState(() {
                                                  _isLoading = true;
                                                  confirmUser(
                                                      "Not Confirmed");
                                                });},
                                              textButton: "Reject",
                                              colorButton: AppTheme.gRed,
                                            ),
                                            buttonConfirmReject(
                                              link: () {
                                                setState(() {
                                                  _isLoading = true;
                                                  confirmUser(
                                                      "Confirmed");
                                                });
                                              },
                                              textButton: "Confirm",
                                              colorButton: AppTheme.gButton
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  )),
                            ),
                            Container(
                              padding: EdgeInsets.all(8),
                              margin: EdgeInsets.symmetric(horizontal: 70),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: AppTheme.gFont,
                              ),
                              child: Text(
                                "Confirmation",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  RaisedButton buttonConfirmReject({@required dynamic link, @required String textButton, Color colorButton}) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
      splashColor: Colors.grey.withOpacity(0.5),
      onPressed: link,
      padding: const EdgeInsets.all(0.0),
      child: Ink(
        decoration: BoxDecoration(
            color: colorButton, borderRadius: BorderRadius.circular(100)),
        child: Container(
          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
          alignment: Alignment.center,
          child: Text(
            "$textButton",
            style: TextStyle(
                color: AppTheme.white,
                fontSize: 16,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
