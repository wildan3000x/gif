import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/create_games/invitation/invite_golfers.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/utils/Utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:collection/collection.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuInvGolfer extends StatefulWidget {
  const MenuInvGolfer({Key key, this.type, this.data, this.snackKey, this.button, this.backSummary, this.id}) : super(key: key);
  final type;
  final data;
  final snackKey;
  final button;
  final backSummary;
  final id;

  @override
  _MenuInvGolferState createState() => _MenuInvGolferState();
}

class _MenuInvGolferState extends State<MenuInvGolfer> {
  // final snackbarKey = GlobalKey<ScaffoldState>();
  bool _loading = false;
  bool _tabSelectMem = false;
  bool _checkMember = false;
  bool _checkMember2 = false;
  bool _checkMember3 = false;
  bool _successScreen = false;
  bool _isLoading = false;

  String _tipeInv = "1";
  String _valSearchCM;
  int _valGroup;

  double sizeH;
  int addLine;

  TextEditingController searchCM = TextEditingController();

  List<Widget> arrAddText = List<Widget>();
  List<TextEditingController> names = List<TextEditingController>();
  List<TextEditingController> numbs = List<TextEditingController>();

  List<DefaultGroups> _group = new List<DefaultGroups>();
  List<CustomGroup> _uiGroup = List<CustomGroup>();
  List<CustomGroup> _allGroup = List<CustomGroup>();

  List<DefaultBuddies> _buddies = new List<DefaultBuddies>();
  List<CustomBuddies> _uiBuddies = List<CustomBuddies>();
  List<CustomBuddies> _allBuddies = List<CustomBuddies>();

  List<Contact> _contacts = new List<Contact>();
  List<CustomContact> _uiCustomContacts = List<CustomContact>();
  List<CustomContact> _allContacts = List<CustomContact>();
  List<bool> arrSelected2;
  List<bool> arrSelected3;
  List<String> arrNameSelected = new List();
  List<String> arrIdSelected = new List();
  List<String> arrNumberSelected = new List();
  TextEditingController controllerSearch = TextEditingController();
  TextEditingController controllerSearch2 = TextEditingController();
  TextEditingController controllerSearch3 = TextEditingController();


  @override
  void initState() {
    getPermission();
    getInvitation();
    super.initState();
  }

  SharedPreferences sharedPreferences;

  void getInvitation() async {
    sharedPreferences = await SharedPreferences.getInstance();
    getBuddies();
    getOtherClub();
    setArrText(0);
  }

  void getPermission() async {
    sharedPreferences = await SharedPreferences.getInstance();
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus.isGranted){
      // getPermission();
        refreshContacts();
        // getGroup();
        getContact();
    }else {
        showDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text('Permissions Not Granted'),
              content: Text('Please enable contacts access permission in system app settings'),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text('OK'),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            ));
    }
  }

  var dataOtherClub;
  List<dynamic> _dataClub = List();
  bool _isLoadingGroup = false;
  bool _isEmptyClub = false;

  void getOtherClub() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _isLoadingGroup = true;
    });
    var data = {
      'token': sharedPreferences.getString("token"),
      'tipe_invite' : 'Members of My Golf Club (Group)'
    };

    Utils.postAPI(data, "get_GC").then((body) {
      if (body['status'] == 'success') {
        dataOtherClub = body["data"];
        setState(() {
          getGroup(dataOtherClub[0]['id_club_golf']);
          _dataClub  = dataOtherClub;
          _isEmptyClub = false;
          _isLoadingGroup = false;
        });
        print("DDDDDDD $_dataClub");
      } else {
        print(body['message']);
        setState(() {
          _isEmptyClub = true;
          _isLoadingGroup = false;
        });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
      });
    });
  }

  var contacts2;

  void getContact() async {
    var data = {
      'tipe_invite' : 'My Contacts',
      'id_turnamen' : widget.id,
    };

    Utils.postAPI(data, "get_Contact").then((body) {
      if (body['status'] == 'success') {
        contacts2 = body["sudah_invite"];
      } else {
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var getIdGroup;
  var getLengthMemGroup;
  bool emptyClub = false;


  void getGroup(int id) async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'id_turnamen' : widget.id,
      'id_golf_club_lain' : id,
    };

    Utils.postAPI(data, "get_UGC").then((body) {
      setState(() {
        _isLoading = false;
      });
      getLengthMemGroup = body['data'];
      print("LLLLL ${getLengthMemGroup.length}");
      if (body['status'] == 'success') {
        var l = body["data"];
        // print("DATA GOLFER $l");
        setState(() {
          _group.clear();
          _allGroup.clear();
          _uiGroup.clear();
          getIdGroup = body['id_club_golf'];
          emptyClub = false;
          for (int i = 0; i < l.length; i++) {
            DefaultGroups def = DefaultGroups(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l.contains(l[i]['status_invite']) ? "null" : l[i]['status_invite']);
            _group.add(def);
            _allGroup.add(CustomGroup(showAll: def, isChecked: false));
            _uiGroup.add(CustomGroup(showAll: def, isChecked: false));
          }
        });
      } else {
        setState(() {
          getIdGroup = body['id_club_golf'];
          _group.clear();
          _allGroup.clear();
          _uiGroup.clear();
          emptyClub = true;
        });
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }


  bool emptyBuddy = false;
  void getBuddies() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var data = {
      'token': sharedPreferences.getString("token"),
      'tipe_invite' : 'My Buddies',
      'id_turnamen' : widget.id,
    };

    Utils.postAPI(data, "get_Buddies").then((body) {
      if (body['status'] == 'success') {
        var l = body["data"];
        // print("DATA GOLFER $l");
        setState(() {
          for (int i = 0; i < l.length; i++) {
            DefaultBuddies def = DefaultBuddies(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l.contains(l[i]['status_invite']) ? "null" : l[i]['status_invite']);
            // DefaultBuddies def = DefaultBuddies(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l[i]['status_invite']);
            _buddies.add(def);
            _allBuddies.add(CustomBuddies(showAll: def, isChecked: false));
            _uiBuddies.add(CustomBuddies(showAll: def, isChecked: false));
          }
          emptyBuddy = false;
        });
      } else {
        setState(() {
          emptyBuddy = true;
        });
        print("BUDDIES "+body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var validGroup;

  _invite() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'turnamen_id' : widget.data!=null?widget.data['turnamen']['id_turnamen']:"",
      'tipe_invite' : widget.type,
      'golf_club' : _valGroup,
      'tipe_im' : _tipeInv,
      'user_id' : arrIdSelected,
      'nama' : arrNameSelected,
      'no_telp' : arrNumberSelected
    };

    Utils.postAPI(data, "invite_golfer").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
        });
        // print("INVITE = ${body}");
        Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, animation1, animation2) => InviteGolfer(success: 'success', msg: body['body_message'], data: widget.data!=null?widget.data['turnamen']['id_turnamen']:"", skip: widget.button == true ? true : false, backSummary: widget.backSummary,)));
        // // dataTransaksi = body["data"];
        // countNotifGolfer = body["count"];
        // print("COUNT : ${countNotifGolfer}");
      }else{
        // arrIdSelected.clear();
        // arrNameSelected.clear();
        // arrNumberSelected.clear();
        print(body['body_message']);
        setState(() {
          _loading = false;
        });
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        widget.snackKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        arrIdSelected.clear();
        arrNameSelected.clear();
        arrNumberSelected.clear();
      });
    }, onError: (error) {
      arrIdSelected.clear();
      arrNameSelected.clear();
      arrNumberSelected.clear();
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      widget.snackKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var _checkTypeInv;
  void _handleInv(String value) {
    setState(() {
      _tipeInv = value;
      _checkTypeInv = widget.type;
    });

    if (value == '2') {
      setState(() {
        _tabSelectMem = true;
      });
    } else {
      setState(() {
        _tabSelectMem = false;
      });
    }
  }

  void setArrText (int index) {
    names.add(TextEditingController());
    numbs.add(TextEditingController());
    arrAddText.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: TextFormField(controller: names[index],)),
        Padding(padding: EdgeInsets.only(left: 15)),
        Expanded(child: TextFormField(controller: numbs[index],)),
      ],
    ));
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }

  @override
  Widget build(BuildContext context) {
    if(_checkTypeInv != widget.type){
      setState(() {
        _tabSelectMem = false;
        _handleInv("1");
      });
    }
    // print("ID = ${widget.data['turnamen']['id_turnamen']}");
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = size.width/12;
    final double item2Height = size.width/6;
    final double itemWidth = size.width / 2;
    return Container(
      child: Form(
        child: Column(
          children: [
            widget.type == 'Members of My Golf Club (Group)' ? _isEmptyClub ? Container(
              padding: EdgeInsets.only(top: 25),
              child: Text("You haven't joined any club yet.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
            ) : dropdownGroupType() : Container(),
            widget.type == 'Members of My Golf Club (Group)' || widget.type == 'My Buddies'
                ? widget.type == 'Members of My Golf Club (Group)' && _isEmptyClub
                ? Container() : widget.type == 'My Buddies' && emptyBuddy
                ? Container(margin: EdgeInsets.only(top: 25), child: Text(emptyBuddy?"You have no buddies.":"", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),)
                : radioButtonMenu()
                : Container(),
            widget.type == 'My Contacts' || widget.type == 'Add Manually' ? Padding(padding: EdgeInsets.only(top: 25)) : Container(),
            //radio select members
            widget.type == "My Contacts" ? searchMember(itemWidth, itemHeight) : Container(),
            widget.type == "My Contacts" ? Container() :
            widget.type == "Add Manually" ?
                formAddManually(itemWidth, item2Height)
            : _tabSelectMem
                ? searchMember(itemWidth, itemHeight)
                : Container(),
            Button()
          ],
        ),
      ),
    );
  }

  Container formAddManually(double itemWidth, double item2Height) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 8)),
              Text("Name", style: TextStyle(color: AppTheme.gFont),),
              Padding(padding: EdgeInsets.only(left: 20)),
              Text("Mobile Phone Number", style: TextStyle(color: AppTheme.gFont),)
            ],
          ),
          Container(
            // height: 200,
            child: ListView.builder(
              itemCount: arrAddText.length<=10?arrAddText.length:10,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (context, index){
                return arrAddText[index];
              }
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                setArrText(arrAddText.length);
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/svg/+.svg"),
                  Padding(padding: EdgeInsets.only(left: 8)),
                  Text("Add More Line", style: TextStyle(color: Color(0xFFF15411)),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Container searchMember(double itemWidth, double itemHeight) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Text(
            widget.type == 'My Contacts' ? "Search Contact" : "Search Club Member",
            style: TextStyle(color: AppTheme.gFont),
          ),
          Container(
            width: 230,
            child: widget.type == 'My Contacts' ?
            //Conatct
            TextFormField(
              decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                suffixIcon: Icon(Icons.search),
              ),
              controller: controllerSearch,
              onChanged: (value) {
                _searchContacts(value);
              },
            )
                : widget.type == "My Buddies" ?
            TextFormField(
              decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                suffixIcon: Icon(Icons.search),
              ),
              controller: controllerSearch3,
              onChanged: (value) {
                _searchBuddies(value);
              },
            ) :TextFormField(
              decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                suffixIcon: Icon(Icons.search),
              ),
              controller: controllerSearch2,
              onChanged: (value) {
                _searchGroup(value);
              },
            )
          ),
          // widget.type == 'My Contacts' ? FlatButton(onPressed: (){refreshContacts();}, child: Text("Refresh Contact", style: TextStyle(color: AppTheme.gFont),)) : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    if (widget.type == "My Contacts"){
                      setState(() {
                        _allContacts.forEach((element) { element.isChecked = true; });
                        _uiCustomContacts = _allContacts;
                      });
                    }else if(widget.type == "Members of My Golf Club (Group)"){
                      setState(() {
                        _allGroup.forEach((element) { element.isChecked = true; });
                        _uiGroup = _allGroup;
                      });
                    }else if(widget.type == "My Buddies"){
                      setState(() {
                        _allBuddies.forEach((element) { element.isChecked = true; });
                        _uiBuddies = _allBuddies;
                      });
                    }else {
                      setState(() {
                        arrSelected2.fillRange(0, arrSelected2.length, true);
                        arrSelected3.fillRange(0, arrSelected3.length, true);
                      });
                    }
                  },
                  child: Text(
                    "Select All",
                    style: TextStyle(
                        color: AppTheme.gFont, fontSize: 18),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      if (widget.type == "My Contacts"){
                        setState(() {
                          _allContacts.forEach((element) { element.isChecked = false; });
                          _uiCustomContacts = _allContacts;
                      });
                      }else if(widget.type == "Members of My Golf Club (Group)"){
                        setState(() {
                          _allGroup.forEach((element) { element.isChecked = false; });
                          _uiGroup = _allGroup;
                        });
                      }else if(widget.type == "My Buddies"){
                        setState(() {
                          _allBuddies.forEach((element) { element.isChecked = false; });
                          _uiBuddies = _allBuddies;
                        });
                      }else {
                        arrSelected2.fillRange(0, arrSelected2.length, false);
                        arrSelected3.fillRange(0, arrSelected3.length, false);
                      }
                    });
                  },
                  child: Text(
                    "Deselect All",
                    style: TextStyle(
                        color: AppTheme.gFont, fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
          _uiCustomContacts.isEmpty && widget.type == "My Contacts" || _isLoading && widget.type == "Members of My Golf Club (Group)" ? Padding(
            padding: const EdgeInsets.all(25.0),
            child: Center(
                child: Container(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(),
                )),
          ) : Padding(
            padding: EdgeInsets.only(top: 8),
            child: Container(
              height: 130,
              child: widget.type == "Members of My Golf Club (Group)" && emptyClub ? Container(margin: EdgeInsets.all(50), child: Text("No Member in this group.", style: TextStyle(color: AppTheme.gFontBlack),),) :
              widget.type == "My Buddies" && emptyBuddy ? Container(margin: EdgeInsets.all(50), child: Text("You have no buddy.", style: TextStyle(color: AppTheme.gFontBlack),),) :
              GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.only(right: 25),
                crossAxisSpacing: 20,
                mainAxisSpacing: 10,
                childAspectRatio: (itemWidth / itemHeight),
                children: widget.type == "My Contacts" ? List.generate(_uiCustomContacts==null || _uiCustomContacts.length <= 0 ? 0 : _uiCustomContacts.length, (index) {
                  // var _phonesList = _contact.contact.phones.toList();
                  return _buildListTile(_uiCustomContacts[index]/*, _phonesList*/);
                }) : widget.type == "My Buddies" ? List.generate(_uiBuddies==null || _uiBuddies.length <= 0 ? 0 : _uiBuddies.length, (index) {
                  CustomBuddies budie = _uiBuddies?.elementAt(index);
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                          padding:
                          EdgeInsets.only(top: 5, right: 10)),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (budie.isChecked == false) {
                              budie.isChecked = true;
                            } else {
                              budie.isChecked = false;
                            }
                          });
                        },
                        child: _uiBuddies[index].showAll.invited == "sudah" ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : budie.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
                      ),
                      Flexible(
                        child: Container(
                          padding:
                          const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "${_uiBuddies[index].showAll.nama_lengkap}",
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                                color: _checkMember2
                                    ? AppTheme.gGrey
                                    : Color(0xFF828282)),
                          ),
                        ),
                      )
                    ],
                  );
                }) : List.generate(_uiGroup==null || _uiGroup.length < 0 ? 0 : _uiGroup.length, (index) {
                  CustomGroup group = _uiGroup?.elementAt(index);
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                          padding:
                          EdgeInsets.only(top: 5, right: 10)),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (group.isChecked == false) {
                              group.isChecked = true;
                            } else {
                              group.isChecked = false;
                            }
                          });
                        },
                        child: _uiGroup[index].showAll.invited == "sudah" ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : group.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
                        // child: _uiGroup[index].showAll.invited == "sudah" ? group.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,),
                      ),
                      Flexible(
                        child: Container(
                          padding:
                          const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "${_uiGroup[index].showAll.nama_lengkap}",
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                                color: _checkMember3
                                    ? AppTheme.gGrey
                                    : Color(0xFF828282)),
                          ),
                        ),
                      )
                    ],
                  );
                }),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/svg/check.svg',
                  color: AppTheme.gGrey,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "Already Invited",
                    style: TextStyle(
                      color: AppTheme.gGrey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ));
  }

  Row radioButtonMenu() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          width: 15,
          child: Radio(
            value: "1",
            groupValue: _tipeInv,
            onChanged: _handleInv,
          ),
        ),
        Text(
          widget.type == 'My Buddies' ? "Invite All Buddies" : "Invite All Member",
          style: TextStyle(color: Color(0xFF837B7B)),
        ),
        Padding(padding: EdgeInsets.only(right: 25)),
        SizedBox(
          width: 15,
          child: Radio(
            value: "2",
            groupValue: _tipeInv,
            onChanged: _handleInv,
          ),
        ),
        Text(
            widget.type == 'My Buddies' ? "Select Buddies" : "Select Members",
            style: TextStyle(color: Color(0xFF837B7B)))
      ],
    );
  }

  Widget dropdownGroupType() {
    return _isLoadingGroup ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),) : Column(
      children: [
        Container(
            padding: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              "My Golf Club",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: DropdownButton(
            isExpanded: true,
            underline: SizedBox(),
            items: _dataClub.map((value) {
              setState(() {
                if(_valGroup == null){
                  _valGroup = _dataClub[0]['id_club_golf'];
                }
              });
              return DropdownMenuItem(
                value: value['id_club_golf'],
                child: Text(value['nama_club']),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                _valGroup = value;
                if(value != getIdGroup) {
                  getGroup(value);
                }
              });
            },
            value: _valGroup,
            hint: Text(
              "${_dataClub[0]['nama_club']}",
              textAlign: TextAlign.center,
            ),
            // value: _valWinners,
          ),
        ),
      ],
    );
  }

  Padding Button() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
            splashColor: Colors.grey.withOpacity(0.5),
            onPressed: () {
              if(widget.button == true){
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) => SummaryGames(tourId: widget.id,)),
                  ModalRoute.withName('/'),
                );
              }else{
                Navigator.pop(context);
              }
            },
            padding: const EdgeInsets.all(0.0),
            child: Ink(
                decoration: BoxDecoration(
                    color: AppTheme.gButton,
                    borderRadius: BorderRadius.circular(100)
                ),
                child: Container(
                    constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                    alignment: Alignment.center,
                    child: Text(widget.button == true ? "Skip" : "Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
          ),
          RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
            splashColor: Colors.grey.withOpacity(0.5),
            onPressed: widget.type == "Members of My Golf Club (Group)" && _isEmptyClub || widget.type == "My Buddies" && emptyBuddy ? null : () {
              if(!_loading){
                if (widget.type == 'My Contacts') {
                  int mCounter = _allContacts.where((contact) => contact.isChecked == true).toList().length;
                  if (mCounter == 0){
                    var snackbar = SnackBar(
                      content: Text("Please invite at least one person"),
                      backgroundColor: Colors.red,
                    );
                    widget.snackKey.currentState.showSnackBar(snackbar);
                  }else {
                    for (int i = 0; i < _allContacts.length; i++) {
                      if (_allContacts[i].isChecked){
                        arrNameSelected.add(_allContacts[i].contact.displayName);
                        arrNumberSelected.add(Utils.getValidPhoneNumber(_allContacts[i].contact.phones));
                      }
                    }
                    setState(() {
                      _loading = true;
                      _invite();
                    });
                  }
                }else if(widget.type == "My Buddies"){
                  int mCounter = _uiBuddies
                      .where((buddies) => buddies.isChecked == true)
                      .toList()
                      .length;
                  if (_tipeInv == "1") {
                    // _uiBuddies.forEach((element) {
                    //   element.isChecked = true;
                    // });
                    _allBuddies = _uiBuddies;
                    for (int i = 0; i < _uiBuddies.length; i++) {
                      if (_uiBuddies[i].showAll.invited != "sudah") {
                        arrIdSelected.add(_uiBuddies[i].showAll.id);
                        arrNameSelected.add(_uiBuddies[i].showAll.nama_lengkap);
                        arrNumberSelected.add(_uiBuddies[i].showAll.no_telp);
                      }
                    }
                    if(arrIdSelected.isEmpty){
                      var snackbar = SnackBar(
                        content: Text("All of your buddy has been invited."),
                        backgroundColor: Colors.red,
                      );
                      widget.snackKey.currentState.showSnackBar(snackbar);
                    }else {
                      setState(() {
                        _loading = true;
                        _invite();
                      });
                    }
                  } else {
                    if (mCounter == 0) {
                      var snackbar = SnackBar(
                        content: Text("Please invite at least one person"),
                        backgroundColor: Colors.red,
                      );
                      widget.snackKey.currentState.showSnackBar(snackbar);
                    } else {
                      for (int i = 0; i < _uiBuddies.length; i++) {
                        if (_uiBuddies[i].isChecked) {
                          arrIdSelected.add(_uiBuddies[i].showAll.id);
                          arrNameSelected
                              .add(_uiBuddies[i].showAll.nama_lengkap);
                          arrNumberSelected.add(_uiBuddies[i].showAll.no_telp);
                        }
                      }
                      setState(() {
                        _loading = true;
                        _invite();
                      });
                    }
                  }
                }else if(widget.type == "Members of My Golf Club (Group)"){
                  int mCounter = _uiGroup.where((group) => group.isChecked == true).toList().length;
                  if (_tipeInv=="1"){
                    // _uiGroup.forEach((element) {
                    //   element.isChecked = true;
                    // });
                    _allGroup = _uiGroup;
                    for (int i = 0; i < _uiGroup.length; i++) {
                      if (_uiGroup[i].showAll.invited != "sudah") {
                        arrIdSelected.add(_uiGroup[i].showAll.id);
                        arrNameSelected.add(_uiGroup[i].showAll.nama_lengkap);
                        arrNumberSelected.add(_uiGroup[i].showAll.no_telp);
                      }
                    }
                    if(arrIdSelected.isEmpty){
                      var snackbar = SnackBar(
                        content: Text("All golfers in this club has been invited."),
                        backgroundColor: Colors.red,
                      );
                      widget.snackKey.currentState.showSnackBar(snackbar);
                    }else {
                      setState(() {
                        _loading = true;
                        _invite();
                      });
                    }
                  }else {
                    if (mCounter == 0) {
                      var snackbar = SnackBar(
                        content: Text("Please invite at least one person"),
                        backgroundColor: Colors.red,
                      );
                      widget.snackKey.currentState.showSnackBar(snackbar);
                    } else {
                      for (int i = 0; i < _uiGroup.length; i++) {
                        if (_uiGroup[i].isChecked) {
                          arrIdSelected.add(_uiGroup[i].showAll.id);
                          arrNameSelected.add(_uiGroup[i].showAll.nama_lengkap);
                          arrNumberSelected.add(_uiGroup[i].showAll.no_telp);
                        }
                      }
                      setState(() {
                        _loading = true;
                        _invite();
                      });
                    }
                  }
                }else{
                  if(widget.type == "Add Manually"){
                    for (int i = 0; i < arrAddText.length; i++) {
                      if (names[i].text.isNotEmpty && numbs[i].text.isNotEmpty) {
                        arrNameSelected.add(names[i].text);
                        arrNumberSelected.add(numbs[i].text);
                      }
                    }
                  }
                  setState(() {
                    if ((arrNameSelected.length<=0 || arrNumberSelected.length<=0) && _tipeInv == null){
                      var snackbar = SnackBar(
                        content: Text("Please invite at least one person"),
                        backgroundColor: Colors.red,
                      );
                      widget.snackKey.currentState.showSnackBar(snackbar);
                    }else {
                      _loading = true;
                      _invite();
                    }
                  });
                }
              }
            },
            padding: const EdgeInsets.all(0.0),
            child: Ink(
                decoration: BoxDecoration(
                    color: widget.type == "Members of My Golf Club (Group)" && _isEmptyClub || widget.type == "My Buddies" && emptyBuddy ? AppTheme.gBlack : AppTheme.gButton,
                    borderRadius: BorderRadius.circular(100)
                ),
                child: Container(
                    constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                    alignment: Alignment.center,
                    child: _loading
                        ? Center(
                        child: Container(
                          width: 15,
                          height: 15,
                          child: CircularProgressIndicator(),
                        ))
                        : Text(
                      "Invite",
                      style: TextStyle(
                          color: AppTheme.white,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    ),)),
          ),
        ],
      ),
    );
  }

  Row _buildListTile(CustomContact c/*, List<Item> list*/) {
    var l;
    var _name = c.contact.phones.toList();
    List<bool> check = List<bool>();
    Function eq = const DeepCollectionEquality.unordered().equals;
    if(contacts2 != null){
      for(int a=0;a<contacts2.length;a++) {
        l = contacts2[a]['cek_no_telp'];
        c.contact.phones.forEach((element) {
          check.add(eq(element.value, l));
        });
        // check.add(eq(c.contact.displayName, l));
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
            padding:
            EdgeInsets.only(top: 5, right: 10)),
        InkWell(
          onTap: () {
            setState(() {
              if (c.isChecked) {
                c.isChecked = false;
              } else {
                c.isChecked = true;
              }
            });
          },
          child: contacts2 == null ? Container() : check.contains(true) ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : c.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
          // child: c.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
        ),
        Flexible(
          child: Container(
            padding:
            const EdgeInsets.only(left: 8.0),
            child: Text(
              /*kanggo nampilno or ngambil nomer*/
              // Utils.getValidPhoneNumber(contact.phones) ?? '',
              /*kanggo nampilno or ngambil nomer*/
              "${c.contact.displayName}",
              overflow: TextOverflow.fade,
              maxLines: 1,
              softWrap: false,
              style: TextStyle(
                  color: _checkMember
                      ? AppTheme.gGrey
                      : Color(0xFF828282)),
            ),
          ),
        )
      ],
    );
  }

  refreshContacts() async {
    setState(() {
      _isLoading = true;
    });
    var contacts = await ContactsService.getContacts();
    _populateContacts(contacts);
  }

  void _populateContacts(Iterable<Contact> contacts) {
    _contacts = contacts.where((item) => item.displayName != null).toList();
    _contacts.sort((a, b) => a.displayName.compareTo(b.displayName));
    _allContacts = _contacts.map((contact) => CustomContact(contact: contact)).toList();
    print("CONTACTS ALL ${_allContacts}");
    setState(() {
      _uiCustomContacts = _allContacts;
      _isLoading = false;
    });
  }

  void _searchContacts(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiCustomContacts = _allContacts.where((item) => item.contact.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void _searchGroup(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiGroup = _allGroup.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void _searchBuddies(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiBuddies = _allBuddies.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }
}

class CustomContact {
  final Contact contact;
  bool isChecked;

  CustomContact({
    this.contact,
    this.isChecked = false,
  });
}

class DefaultGroups{
  final String nama_lengkap;
  final String no_telp;
  final String id;
  final String invited;
  final String showAll;
  DefaultGroups({
    this.showAll,
    this.nama_lengkap,
    this.no_telp,
    this.invited,
    this.id,
  });
}

class CustomGroup {
  final DefaultGroups showAll;
  bool isChecked;
  CustomGroup({
    this.showAll,
    this.isChecked = false,
  });
}

class DefaultBuddies{
  final String nama_lengkap;
  final String no_telp;
  final String id;
  final String invited;
  final String showAll;
  DefaultBuddies({
    this.showAll,
    this.nama_lengkap,
    this.no_telp,
    this.id,
    this.invited,
  });
}

class CustomBuddies {
  final DefaultBuddies showAll;
  bool isChecked;
  CustomBuddies({
    this.showAll,
    this.isChecked = false,
  });
}