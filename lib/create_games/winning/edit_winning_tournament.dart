import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/set_winning.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/create_games/winning/set_winning_tournament.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class EditWinningTournament extends StatefulWidget {
  const EditWinningTournament({Key key, this.isEdit, this.template, this.disable, this.data}) : super(key: key);
  final isEdit;
  final template;
  final disable;
  final data;

  @override
  _EditWinningTournamentState createState() => _EditWinningTournamentState();
}

class _EditWinningTournamentState extends State<EditWinningTournament> {
  bool _loading = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  var myFormat = DateFormat('d MMM yyyy');

  String _valTemplate;
  String _valWinners = "1";
  int _valWinners10;
  int _valWinners20;
  int _valWinnersHigh;
  String _valWinnersGross;
  String _valFlight = "1";
  String _valOverAll = "1";
  String _valNearsToTheLin = "3";
  String _valLongesDrive = "3";
  List<String> _locations = ['1', '2', '3'];
  //hole in one (hole, prize)
  List<String> _arrValHoleInOne = List<String>();
  List<TextEditingController> _arrTextHoleInOne = List<TextEditingController>();
  //nearest to the line (hole, prize)
  List<String> _arrValNearsToThePin = List<String>();
  List<TextEditingController> _arrTextNearPin = List<TextEditingController>();

  var hc = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36];

  var _arrValWinners = List<String>();
  var _arrMinRange = [1, 10, ""];
  var _arrMaxRange = ["", 20, 21];
  List<TextEditingController> _arrTextBestMettOverall = [TextEditingController(), TextEditingController(), TextEditingController()];
  List<TextEditingController> _arrTextBestGros = [TextEditingController(), TextEditingController(), TextEditingController()];
  var _arrTittleFlight = ["Flight A", "Flight B", "Flight C"];
  var _arrColorFlight = [Color(0xFF2D9CDB), AppTheme.gFont, Color(0xFFF3BF37)];

  //checkbox hole
  List<bool> valCheckHoleOneCer = List<bool>();
  List<bool> valCheckHoleOneTro = List<bool>();
  //checkbox near pin
  List<bool> valChecksNearPinCer = List<bool>();
  List<bool> valChecksNearPinTro = List<bool>();

  Map<String, dynamic> mapBestNetCer = Map<String, dynamic>();
  Map<String, dynamic> mapBestNetTro = Map<String, dynamic>();
  Map<String, dynamic> mapBestNetPri = Map<String, dynamic>();
  var valChecksOverAllCer = [false, false, false], valChecksOverAllTro = [false, false, false];
  var valChecksGrossCer = [false, false, false], valChecksGrossTro = [false, false, false];
  var valChecksNearLineCer = false, valChecksNearLineTro = false;
  var valChecksLongestCer = false, valChecksLongestTro = false;

  bool _dropBestNett = false;
  bool _dropBestGross = false;
  bool _dropSkillAward = false;

  TextEditingController nlcon = TextEditingController();
  TextEditingController ldcon = TextEditingController();
  List<TextEditingController> priceFlight;
  List<bool> arrValChecksBNC;
  List<bool> arrValChecksBNT;

  List dataPar;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    _valTemplate = widget.data['turnamen']['template_turnamen'];
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    _getDataTour();
    _valFlight = widget.data["rule_bn"]["flight"].toString();
    if(widget.data["rule_fl"].isNotEmpty) {
      for (int i = 0; i < widget.data["rule_bn"]["flight"]; i++) {
        int no = 1;
        _arrValWinners.add(widget.data["rule_fl"][i]["winner"].toString());
        if (i == 0) {
          _arrMinRange[i] = widget.data["rule_fl"][i]["min_range"].toString();
          _valWinners = widget.data["rule_fl"][i]["min_range"].toString();
        } else if (i == 1) {
          _arrMinRange[i] = widget.data["rule_fl"][i]["min_range"].toString();
          _arrMaxRange[i] = widget.data["rule_fl"][i]["max_range"].toString();
          _valWinners10 = widget.data["rule_fl"][i]["min_range"];
          _valWinners20 = widget.data["rule_fl"][i]["max_range"];
        } else if (i == 2) {
          _arrMaxRange[i] = widget.data["rule_fl"][i]["max_range"];
          _valWinnersHigh = widget.data["rule_fl"][i]["max_range"];
        }

        arrValChecksBNC = List<bool>();
        arrValChecksBNT = List<bool>();
        priceFlight = List<TextEditingController>();
        mapBestNetCer.putIfAbsent((i + no).toString(), () => arrValChecksBNC);
        mapBestNetTro.putIfAbsent((i + no).toString(), () => arrValChecksBNT);
        mapBestNetPri.putIfAbsent((i + no).toString(), () => priceFlight);

        for (int a = 0; a<widget.data["rule_fl"][i]["winner"]; a++){
          arrValChecksBNC.add(widget.data["rule_fl"][i]["price_fl"][a]["sertifikat"] == 1 ? true : false);
          arrValChecksBNT.add(widget.data["rule_fl"][i]["price_fl"][a]["tropi"] == 1 ? true : false);
          priceFlight.add(TextEditingController(text: widget.data["rule_fl"][i]["price_fl"][a]["price"]));
          mapBestNetCer.update((i + no).toString(), (value) => arrValChecksBNC = value);
          mapBestNetTro.update((i + no).toString(), (value) => arrValChecksBNT = value);
          mapBestNetPri.update((i + no).toString(), (value) => priceFlight = value);
        }
      }
    }else{

    }

    _valOverAll = widget.data["rule_bn"]["winner"].toString();
    for (int i = 0; i<widget.data["rule_bn"]["winner"]; i++){
      valChecksOverAllCer[i] = widget.data["price_bn"][i]["sertifikat"] == 1 ? true : false;
      valChecksOverAllTro[i] = widget.data["price_bn"][i]["tropi"] == 1 ? true : false;
      _arrTextBestMettOverall[i].text = widget.data["price_bn"][i]["price"] == "null" ? "" : widget.data["price_bn"][i]["price"];
    }

    if(widget.data['rule_bg'] != null) {
      _valWinnersGross = widget.data["rule_bg"]["winner"].toString();
      for (int i = 0; i < widget.data["rule_bg"]['winner']; i++) {
        valChecksGrossCer[i] = widget.data["price_bg"][i]["sertifikat"] == 1 ? true : false;
        valChecksGrossTro[i] = widget.data["price_bg"][i]["tropi"] == 1 ? true : false;
        _arrTextBestGros[i].text = widget.data["price_bg"][i]["price"] == "null" ? "" : widget.data["price_bg"][i]["price"];
      }
    }


    if(widget.data != null ? widget.data['price_ho'].isNotEmpty && widget.data['price_np'].isNotEmpty : "") {
      getCourse();
      if(widget.data['price_ld'].isNotEmpty && widget.data['price_nl'].isNotEmpty){
        _valNearsToTheLin = widget.data["price_nl"][0]["hole"].toString();
        valChecksNearLineCer = widget.data["price_nl"][0]["sertifikat"] == 1 ? true : false;
        valChecksNearLineTro = widget.data["price_nl"][0]["tropi"] == 1 ? true : false;
        nlcon.text = widget.data["price_nl"][0]["price"];

        _valLongesDrive = widget.data["price_ld"][0]["hole"].toString();
        valChecksLongestCer = widget.data["price_ld"][0]["sertifikat"] == 1 ? true : false;
        valChecksLongestTro = widget.data["price_ld"][0]["tropi"] == 1 ? true : false;
        ldcon.text = widget.data["price_ld"][0]["price"];
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }


  var token;
  void _getDataTour() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tokenJson = localStorage.getString('usertoken');
    var datatoken = json.decode(tokenJson);
    setState(() {
      token = datatoken;
    });
  }

  bool _isLoading = false;
  List<dynamic> _dataDropDownHoleAndPar = List();
  List<dynamic> _dataDropDownHoleAndParFirst = List();
  bool _isDoesntHavePar5 = false;
  void getCourse() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'nama_kota' : widget.data["turnamen"]["kota_turnamen"],
      'lapangan_id' : widget.data["turnamen"]['id_lapangan'],
    };

    Utils.postAPI(data, "get_par").then((body){

      if(body['status'] == 'success'){
        // dataPar = body['data'];
        setState(() {
          dataPar = body['data'];
          for (int i = 0; i<dataPar.length; i++) {
            _dataDropDownHoleAndParFirst.add(dataPar[i]['hole'].toString());
            if (_arrValHoleInOne.length < dataPar.length) {
              _arrValHoleInOne.add(dataPar[i]["hole"].toString());
              valCheckHoleOneCer.add(false);
              valCheckHoleOneTro.add(false);
              _arrTextHoleInOne.add(TextEditingController());
            }

            if (_arrValNearsToThePin.length<dataPar.length) {
              _arrValNearsToThePin.add(dataPar[i]["hole"].toString());
              valChecksNearPinCer.add(false);
              valChecksNearPinTro.add(false);
              _arrTextNearPin.add(TextEditingController());
            }
          }
          _dataDropDownHoleAndPar = [...{..._dataDropDownHoleAndParFirst}];
          if(widget.data['price_ho'].isNotEmpty && widget.data['price_np'].isNotEmpty) {
            print("ARR VAL LENGTH ${_arrValHoleInOne.length}");
            for (int i = 0; i<widget.data['price_ho'].length; i++){
              _dataDropDownHoleAndParFirst.add(widget.data['price_ho'][i]["hole"].toString());
              _arrValHoleInOne[i] = widget.data["price_ho"][i]["hole"].toString();
              valCheckHoleOneCer[i] = widget.data["price_ho"][i]["sertifikat"] == 1 ? true : false;
              valCheckHoleOneTro[i] = widget.data["price_ho"][i]["tropi"] == 1 ? true : false;
              _arrTextHoleInOne[i].text = widget.data["price_ho"][i]["price"];
            }

            for (int i = 0; i<widget.data['price_np'].length; i++){
              _dataDropDownHoleAndParFirst.add(widget.data['price_np'][i]["hole"].toString());
              _arrValNearsToThePin[i] = widget.data["price_np"][i]["hole"].toString();
              valChecksNearPinCer[i] = widget.data["price_np"][i]["sertifikat"] == 1 ? true : false;
              valChecksNearPinTro[i] = widget.data["price_np"][i]["tropi"] == 1 ? true : false;
              _arrTextNearPin[i].text = widget.data["price_np"][i]["price"];
            }

            _dataDropDownHoleAndPar = [...{..._dataDropDownHoleAndParFirst}];
            print("DROPDOWN HOLE ONE AND PAR = ${_dataDropDownHoleAndPar}");
          }
          if(body['par_5'] == null){
            _isDoesntHavePar5 = true;
          }else{
            _valNearsToTheLin = body['par_5']['hole'].toString();
            _valLongesDrive = body['par_5']['hole'].toString();
          }
        });
      }else{
        print(body['message']);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  bool showBestGross, showAwardSkill = false;

  @override
  Widget build(BuildContext context) {
    print("VAL NTL ${_valNearsToTheLin}");
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: Stack(
            children: [
              ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        HeaderCreate(title: "Edit Game Rules",),
                        titleAward(), winningRules()],
                    ),
                  ),
                ],
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  Container winningRules() {
    return Container(
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              "Winning Rules & Prizes",
              style: TextStyle(
                color: AppTheme.gBlack,
                fontSize: 24,
                fontWeight: FontWeight.w900,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Text(
                    "Select Award Template",
                    style: TextStyle(color: AppTheme.gOrange),
                  ),
                  Container(
                    width: 200,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                            BorderSide(width: 1, color: AppTheme.gGrey))),
                    child: DropdownButton(
                      isExpanded: true,
                      underline: SizedBox(),
                      hint: Text(
                        "${widget.data["turnamen"]["template_turnamen"]}",
                        textAlign: TextAlign.center,
                      ),
                      value: _valTemplate,
                      items: <String>['Match', 'Mini Tournament', 'Tournament'].map((value) {
                        return DropdownMenuItem(
                          child: Text(value),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _valTemplate = value;
                          _dropBestNett = false;
                          _dropBestGross = false;
                          _dropSkillAward = false;
                          if(value=="Tournament"){
                            if(widget.data == null || widget.data['rule_bg'] == null){
                              _valWinnersGross = "1";
                            }
                            getCourse();
                          }else if(value!="Match" && widget.data == null || widget.data['rule_bg'] == null){
                            _valWinnersGross = "1";
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10),
              child: Column(
                children: [
                  Text(
                    "Winners Rules & Awards Configuration",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (_dropBestNett == false) {
                          _dropBestNett = true;
                        } else if (_dropBestNett == true) {
                          _dropBestNett = false;
                        }
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 10, bottom: 3),
                      padding: EdgeInsets.all(15),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: AppTheme.gGrey),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                SvgPicture.asset('assets/svg/check.svg'),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Best Nett",
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    if (_dropBestNett == false) {
                                      _dropBestNett = true;
                                    } else if (_dropBestNett == true) {
                                      _dropBestNett = false;
                                    }
                                  });
                                },
                                child: _dropBestNett == true
                                    ? Image.asset(
                                    'assets/images/arrow down.png')
                                    : Image.asset(
                                    'assets/images/arrow left.png')),
                          ]),
                    ),
                  ),
                  AnimatedCrossFade(
                    firstChild: Container(),
                    secondChild: dropDownBestNett(),
                    duration: Duration(milliseconds: 500),
                    crossFadeState: _dropBestNett ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                    sizeCurve: Curves.fastOutSlowIn,
                  ),
                  // _dropBestNett == true ? dropDownBestNett() : emptyCon(),
                  _valTemplate == null ? widget.data == null || widget.data['rule_bg'] == null ? Container() : Column(
                    children: [
                      buttonDropDownBestGross(),
                      AnimatedCrossFade(
                        firstChild: Container(),
                        secondChild: dropDownBestGross(),
                        duration: Duration(milliseconds: 500),
                        crossFadeState: _dropBestGross ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        sizeCurve: Curves.fastOutSlowIn,
                      ),
                    ],
                  ) : _valTemplate == "Match" ? Container() : Column(
                    children: [
                      buttonDropDownBestGross(),
                      AnimatedCrossFade(
                        firstChild: Container(),
                        secondChild: dropDownBestGross(),
                        duration: Duration(milliseconds: 500),
                        crossFadeState: _dropBestGross ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        sizeCurve: Curves.fastOutSlowIn,
                      ),
                    ],
                  ),
                  // widget.data == null || widget.data['rule_bg'] == null ? Container() : AnimatedCrossFade(
                  //   firstChild: Container(),
                  //   secondChild: dropDownBestGross(),
                  //   duration: Duration(milliseconds: 500),
                  //   crossFadeState: _dropBestGross ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                  //   sizeCurve: Curves.fastOutSlowIn,
                  // ),
                  // _dropBestGross == true ? dropDownBestGross() : emptyCon(),
                 _valTemplate == null ?
                 widget.data['price_ho'].isNotEmpty && widget.data['price_ld'].isNotEmpty && widget.data['price_nl'].isNotEmpty && widget.data['price_np'].isNotEmpty ?
                 buttonDropSkillAward() : Container() : _valTemplate == "Tournament" ? buttonDropSkillAward() : Container(),
                  AnimatedCrossFade(
                    firstChild: Container(),
                    secondChild: dropDownSkillAward(),
                    duration: Duration(milliseconds: 500),
                    crossFadeState: _dropSkillAward ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                    sizeCurve: Curves.fastOutSlowIn,
                  ),
                  // _dropSkillAward == true ? dropDownSkillAward() : emptyCon(),
                  buttonBackSave()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  InkWell buttonDropDownBestGross() {
    return InkWell(
      onTap: () {
        setState(() {
          if (_dropBestGross == false) {
            _dropBestGross = true;
          } else if (_dropBestGross == true) {
            _dropBestGross = false;
          }
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 3),
        padding: EdgeInsets.all(15),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppTheme.gGrey),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SvgPicture.asset('assets/svg/check.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "Best Gross",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
              _dropBestGross ? Image.asset('assets/images/arrow down.png') : Image.asset('assets/images/arrow left.png'),
            ]),
      ),
    );
  }

  InkWell buttonDropSkillAward() {
    return InkWell(
      onTap: () {
        setState(() {
          if (_dropSkillAward == false) {
            _dropSkillAward = true;
          } else if (_dropSkillAward == true) {
            _dropSkillAward = false;
          }
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 3),
        padding: EdgeInsets.all(15),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppTheme.gGrey),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SvgPicture.asset('assets/svg/check.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "Skill Awards",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
              _dropSkillAward == true
                  ? Image.asset('assets/images/arrow down.png')
                  : Image.asset('assets/images/arrow left.png'),
            ]),
      ),
    );
  }

  Container emptyCon() {
    return Container();
  }

  Container buttonBackSave() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    if(!_loading){
                      setState(() {
                        _loading = true;
                      });
                      if(_valTemplate==null){
                        if(widget.data['rule_bg'] != null && widget.data['price_ho'].isNotEmpty && widget.data['price_ld'].isNotEmpty && widget.data['price_nl'].isNotEmpty && widget.data['price_np'].isNotEmpty){
                          _save();
                        }else if(widget.data['rule_bg'] != null){
                          _saveMini();
                        }else{
                          _saveMatch();
                        }
                      }else{
                        if(_valTemplate == "Tournament"){
                          _save();
                        }else if(_valTemplate == 'Mini Tournament'){
                          _saveMini();
                        }else{
                          _saveMatch();
                        }
                      }
                    }
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                          alignment: Alignment.center,
                          child: _loading
                              ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),))
                              : Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container titleAward() {
    return Container(
      padding: EdgeInsets.only(top: 25, bottom: 15),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              "${widget.data["turnamen"]['nama_turnamen']} (ID ${widget.data["turnamen"]['kode_turnamen']})",
              style: TextStyle(
                color: AppTheme.gFont,
                fontSize: 18,
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                "${widget.data["turnamen"]['nama_lapangan']}, ${myFormat.format(DateTime.parse(widget.data["turnamen"]['tanggal_turnamen']))}",
                style: TextStyle(color: Color(0xFF4F4F4F)),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  //dropdownmenu
  Column dropDownBestNett() {
    int no = 1;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 5),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Text("HC System", style: TextStyle(color: AppTheme.gFont)),
                Container(
                  width: 150,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    hint: Text(
                      "System 36",
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "# Flights",
                  style: TextStyle(color: AppTheme.gFont),
                ),
                Container(
                  width: 100,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    hint: Text(
                      widget.data == null ? "1" : widget.data["rule_bn"]["flight"].toString(),
                      textAlign: TextAlign.center,
                    ),
                    value: _valFlight,
                    items: <String>['1', '2', '3'].map((value) {
                      return DropdownMenuItem(
                        child: Text(value),
                        value: value,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _valFlight = value;
                        if(value == "3" && _valWinners20 == 36){
                          _valFlight = "2";
                          Utils.showToast(context, "error", "max hc range reached on flight b (36)");
                        }
                      });
                    },
                  ),
                ),
              ],
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 5),
        ),
        int.parse(_valFlight)==1 ? emptyCon() : ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: int.parse(_valFlight)  ,
            itemBuilder: (BuildContext ctxt, int index) {
              _arrValWinners.length = int.parse(_valFlight);
              if (_arrValWinners.length<int.parse(_valFlight)){
                _arrValWinners.add("1");
              }
              // ignore: unnecessary_statements
              _arrValWinners[index]==null?_arrValWinners[index]="1":_arrValWinners[index];
              return Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 10),
                      padding: EdgeInsets.all(25),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(15)),
                      child: Align(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "HC Range",
                                            style: TextStyle(color: AppTheme.gFont),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        Container(
                                          width: 140,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1, color: AppTheme.gGrey))),
                                          child: index + no == 1
                                              ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("HC", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SvgPicture.asset("assets/svg/is-less-than-or-equal-to-mathematical-symbol.svg", width:11, height: 11,),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: <String>['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15',
                                                    '16', '17', '18'].map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners = value;
                                                      _arrMinRange[0] = value;
                                                      _valWinners10 = int.parse(value) + 1;
                                                      _arrMinRange[1] = _valWinners10;
                                                    });
                                                  },
                                                  hint: Text(
                                                    widget.data == null ? "1" : _arrMinRange[0].toString(),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners,
                                                ),
                                              ),
                                            ],
                                          )
                                              : index + no == 2
                                              ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: hc.map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text("$value"),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners10 = value;
                                                      _arrMinRange[1] = value;
                                                    });
                                                  },
                                                  hint: Text(
                                                    "${_valWinners==null?"10":_valWinners}",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners10,
                                                ),
                                              ),
                                              Text("To", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: hc.map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text("$value"),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners20 = value;
                                                      _arrMaxRange[1] = value;
                                                      if(value==36){
                                                        _valFlight = "2";
                                                      }else{
                                                        _valWinnersHigh = value+1;
                                                        _arrMaxRange[2] = _valWinnersHigh;
                                                      }
                                                    });
                                                  },
                                                  hint: Text(
                                                    "20",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners20,
                                                ),
                                              ),
                                            ],
                                          ) : Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("HC", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SvgPicture.asset("assets/svg/is-equal-to-or-greater-than-symbol.svg", width: 11, height: 11,),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: hc.map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text("$value"),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      if(value > _valWinners20){
                                                        _valWinnersHigh = value;
                                                        _arrMaxRange[2] = value;
                                                      }else{
                                                        Utils.showToast(context, "error", "can't less than flight b hc");
                                                      }
                                                    });
                                                  },
                                                  hint: Text(
                                                    "${_valWinners20==null?"21":_valWinners20}",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinnersHigh,
                                                ),
                                              ),

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "# Winners",
                                        style: TextStyle(color: AppTheme.gFont),
                                      ),
                                      Container(
                                        width: 80,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 1, color: AppTheme.gGrey))),
                                        child: DropdownButton(
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          items: _locations.map((value) {
                                            return DropdownMenuItem(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              _arrValWinners[index] = value;
                                            });
                                          },
                                          hint: Text(
                                            "1",
                                            textAlign: TextAlign.center,
                                          ),
                                          value: _arrValWinners[index],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 15),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Text(
                                      "Rank",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Text(
                                      "Certificate",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 20.0),
                                    child: Text(
                                      "Trophy",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.symmetric(horizontal: 20.0),
                                    child: Text(
                                      "Prize",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: setCB(index, _arrValWinners[index]!=null?int.parse(_arrValWinners[index]):1),
                              )
                            ],
                          )
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.symmetric(horizontal: 95),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: _arrColorFlight[index],
                      ),
                      child: Text(
                        _arrTittleFlight[index],
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              );
            }
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                          "# Winners",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Container(
                          width: 80,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 1, color: AppTheme.gGrey))),
                          child: DropdownButton(
                            isExpanded: true,
                            underline: SizedBox(),
                            hint: Text(
                              widget.data == null ? "1" : widget.data["rule_bn"]["winner"].toString(),
                              textAlign: TextAlign.center,
                            ),
                            value: _valOverAll,
                            items: <String>['1', '2', '3'].map((value) {
                              return DropdownMenuItem(
                                child: Text(value),
                                value: value,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valOverAll = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Rank",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: int.parse(_valOverAll),
                            itemBuilder: (context, index) =>
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 3.0),
                                      child: Text(
                                        Utils.formatSuffix(index + no),
                                        style: TextStyle(color: AppTheme.gGrey),
                                      ),
                                    ),
                                    Padding(
                                      padding: index + no == 2
                                          ? EdgeInsets.only(left: 21)
                                          : EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksOverAllCer[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksOverAllCer[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksOverAllTro[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksOverAllTro[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.only(left: 20),
                                        width: 100,
                                        child: TextFormField(
                                          controller: _arrTextBestMettOverall[index],
                                        ))
                                  ],
                                )
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 70),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFFE31212),
                ),
                child: Text(
                  int.parse(_valFlight)==1 ? "Best Nett" : "Best Nett Overall",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> setCB(int mPosition, int mLenght){
    int no = 1;
    var commentWidgets = List<Widget>();
    arrValChecksBNC = List<bool>();
    arrValChecksBNT = List<bool>();
    priceFlight = List<TextEditingController>();
    mapBestNetCer.putIfAbsent((mPosition + no).toString(), () => arrValChecksBNC);
    mapBestNetTro.putIfAbsent((mPosition + no).toString(), () => arrValChecksBNT);
    mapBestNetPri.putIfAbsent((mPosition + no).toString(), () => priceFlight);

    // if(widget.data['rule_fl'].isNotEmpty) {
      for (int a = 0; a < mLenght; a++) {
        if (widget.data["rule_fl"].isNotEmpty) {
          if (int.parse(_valFlight) <= mPosition){
            dynamic tot = widget.data["rule_fl"][mPosition]["price_fl"];
            if (tot.length == mLenght && arrValChecksBNC.length < mLenght && arrValChecksBNT.length < mLenght) {
              arrValChecksBNC.add(widget.data["rule_fl"][mPosition]["price_fl"][a]["sertifikat"] == 1 ? true : false);
              arrValChecksBNT.add(widget.data["rule_fl"][mPosition]["price_fl"][a]["tropi"] == 1 ? true : false);
              priceFlight.add(TextEditingController(text: widget.data["rule_fl"][mPosition]["price_fl"][a]["price"]));
            } else {
              if (arrValChecksBNC.length < mLenght && arrValChecksBNT.length < mLenght) {
                arrValChecksBNC.add(false);
                arrValChecksBNT.add(false);
                priceFlight.add(TextEditingController());
              }
            }
          }else {
            arrValChecksBNC.add(false);
            arrValChecksBNT.add(false);
            priceFlight.add(TextEditingController());
          }
        }else {
          if (arrValChecksBNC.length < mLenght && arrValChecksBNT.length < mLenght) {
            arrValChecksBNC.add(false);
            arrValChecksBNT.add(false);
            priceFlight.add(TextEditingController());
          }
        }

        if (mapBestNetCer.containsKey("${mPosition + no}")) {
          mapBestNetCer.update((mPosition + no).toString(), (value) => arrValChecksBNC = value);
          mapBestNetTro.update((mPosition + no).toString(), (value) => arrValChecksBNT = value);
          mapBestNetPri.update((mPosition + no).toString(), (value) => priceFlight = value);
        }

        commentWidgets.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 3.0),
                child: Text(
                  Utils.formatSuffix(a + no),
                  style: TextStyle(color: AppTheme.gGrey),
                ),
              ),
              Padding(
                padding: a + no == 2
                    ? EdgeInsets.only(left: 21)
                    : EdgeInsets.only(left: 25.0),
                child: Checkbox(
                  value: mapBestNetCer[(mPosition + no).toString()][a],
                  onChanged: (valueCer) {
                    setState(() {
                      mapBestNetCer[(mPosition + no).toString()][a] = valueCer;
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: Checkbox(
                  value: mapBestNetTro[(mPosition + no).toString()][a],
                  onChanged: (valueTro) {
                    setState(() {
                      mapBestNetTro[(mPosition + no).toString()][a] = valueTro;
                    });
                  },
                ),
              ),
              Container(
                  padding: EdgeInsets.only(left: 20),
                  width: 100,
                  child: TextFormField(
                    controller: priceFlight[a],
                  )
              )
            ],
          ),
        );
      }
    // }

    return commentWidgets;
  }

  Column dropDownBestGross() {
    int no = 1;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5.0, bottom: 10),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                          "# Winners",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Container(
                          width: 80,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 1, color: AppTheme.gGrey))),
                          child: DropdownButton(
                            isExpanded: true,
                            underline: SizedBox(),
                            hint: Text(
                              "1",
                              textAlign: TextAlign.center,
                            ),
                            value: _valWinnersGross,
                            items: <String>['1', '2', '3'].map((value) {
                              return DropdownMenuItem(
                                child: Text(value),
                                value: value,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valWinnersGross = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Rank",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: int.parse(_valWinnersGross),
                            itemBuilder: (context, index) =>
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 3.0),
                                      child: Text(
                                        Utils.formatSuffix(index + no),
                                        style: TextStyle(color: AppTheme.gGrey),
                                      ),
                                    ),
                                    Padding(
                                      padding: index + no == 2
                                          ? EdgeInsets.only(left: 21)
                                          : EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksGrossCer[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksGrossCer[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksGrossTro[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksGrossTro[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.only(left: 20),
                                        width: 100,
                                        child:
                                        TextFormField(
                                          controller: _arrTextBestGros[index],
                                        )
                                    )
                                  ],
                                )
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 90),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: AppTheme.gFont,
                ),
                child: Text(
                  "Best Gross",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Column dropDownSkillAward() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "#Hole",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: dataPar==null?0:dataPar.length>=4?4:dataPar.length,
                          itemBuilder: (context, index) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 0.0),
                                  child: Container(
                                    width: 42,
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 1, color: Colors.grey))),
                                    child: DropdownButton(
                                      isExpanded: true,
                                      underline: SizedBox(),
                                      hint: Text(
                                        "3",
                                        textAlign: TextAlign.center,
                                      ),
                                      value: _arrValHoleInOne[index],
                                      items: _dataDropDownHoleAndPar.map((value) {
                                        return DropdownMenuItem(
                                          child: Text("${value}"),
                                          value: value.toString(),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          _arrValHoleInOne[index] = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 18.0),
                                  child: Checkbox(
                                    value: valCheckHoleOneCer[index],
                                    onChanged: (value) {
                                      setState(() {
                                        valCheckHoleOneCer[index] = value;
                                      });
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 22.0),
                                  child: Checkbox(
                                    value: valCheckHoleOneTro[index],
                                    onChanged: (value) {
                                      setState(() {
                                        valCheckHoleOneTro[index] = value;
                                      });
                                    },
                                  ),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 20),
                                    width: 100,
                                    child: TextFormField(
                                      controller: _arrTextHoleInOne[index],
                                    ))
                              ],
                            );
                          },
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 90),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: AppTheme.gOrange,
                ),
                child: Text(
                  "Hole in One",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "#Hole",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: dataPar==null?0:dataPar.length>=4?4:dataPar.length,
                          itemBuilder: (context, index) {
                            // if (_arrValNearsToThePin.length<dataPar.length) {
                            //   _arrValNearsToThePin.insert(index, dataPar[index]["hole"].toString());
                            //   valChecksNearPinCer.insert(index, false);
                            //   valChecksNearPinTro.insert(index, false);
                            //   _arrTextNearPin.insert(index, "");
                            // }
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 0.0),
                                  child: Container(
                                    width: 42,
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 1, color: Colors.grey))),
                                    child: DropdownButton(
                                      isExpanded: true,
                                      underline: SizedBox(),
                                      hint: Text(
                                        "3",
                                        textAlign: TextAlign.center,
                                      ),
                                      value: _arrValNearsToThePin[index],
                                      items: _dataDropDownHoleAndPar.map((value) {
                                        return DropdownMenuItem(
                                          child: Text("${value}"),
                                          value: value.toString(),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          _arrValNearsToThePin[index] = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 18.0),
                                  child: Checkbox(
                                    value: valChecksNearPinCer[index],
                                    onChanged: (value) {
                                      setState(() {
                                        valChecksNearPinCer[index] = value;
                                      });
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 22.0),
                                  child: Checkbox(
                                    value: valChecksNearPinTro[index],
                                    onChanged: (value) {
                                      setState(() {
                                        valChecksNearPinTro[index] = value;
                                      });
                                    },
                                  ),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 20),
                                    width: 100,
                                    child: TextFormField(
                                      controller: _arrTextNearPin[index],
                                    ))
                              ],
                            );
                          },
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 70),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFFF2C94C),
                ),
                child: Text(
                  "Nearest to The Pin",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        _isDoesntHavePar5 ? Container() : Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [

                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Hole",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: 1,
                          itemBuilder: (context, index) =>
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: Container(
                                      width: 42,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 1, color: Colors.grey))),
                                      child: DropdownButton(
                                        isExpanded: true,
                                        underline: SizedBox(),
                                        hint: Text(
                                          "17",
                                          textAlign: TextAlign.center,
                                        ),
                                        value: _valNearsToTheLin,
                                        items: Utils.dropDownLineAndLongest().map((value) {
                                          return DropdownMenuItem(
                                            child: Text(value.toString()),
                                            value: value.toString(),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          setState(() {
                                            _valNearsToTheLin = value;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 22.0),
                                    child: Checkbox(
                                      value: valChecksNearLineCer,
                                      onChanged: (value) {
                                        setState(() {
                                          valChecksNearLineCer = value;
                                        });
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 18.0),
                                    child: Checkbox(
                                      value: valChecksNearLineTro,
                                      onChanged: (value) {
                                        setState(() {
                                          valChecksNearLineTro = value;
                                        });
                                      },
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(left: 20),
                                      width: 100,
                                      child: TextFormField(
                                        controller: nlcon,
                                      ))
                                ],
                              ),
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 70),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF2D9CDB),
                ),
                child: Text(
                  "Nearest to The Line",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        _isDoesntHavePar5 ? Container() : Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Hole",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: 1,
                          itemBuilder: (context, index) => Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 0.0),
                                child: Container(
                                  width: 42,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              width: 1, color: Colors.grey))),
                                  child: DropdownButton(
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    hint: Text(
                                      "3",
                                      textAlign: TextAlign.center,
                                    ),
                                    value: _valLongesDrive,
                                    items: Utils.dropDownLineAndLongest().map((value) {
                                      return DropdownMenuItem(
                                        child: Text(value.toString()),
                                        value: value.toString(),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        _valLongesDrive = value;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 22.0),
                                child: Checkbox(
                                  value: valChecksLongestCer,
                                  onChanged: (value) {
                                    setState(() {
                                      valChecksLongestCer = value;
                                    });
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 18.0),
                                child: Checkbox(
                                  value: valChecksLongestTro,
                                  onChanged: (value) {
                                    setState(() {
                                      valChecksLongestTro = value;
                                    });
                                  },
                                ),
                              ),
                              Container(
                                  padding: EdgeInsets.only(left: 20),
                                  width: 100,
                                  child: TextFormField(
                                    controller: ldcon,
                                  ))
                            ],
                          ),
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 70),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: AppTheme.gFont,
                ),
                child: Text(
                  "Longest Drive",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _save() async {
    print("HOLE LINE ${_valLongesDrive}");
    if (_arrValWinners.isEmpty){
      _arrValWinners.add("1");
      mapBestNetCer.putIfAbsent("1", () => [false]);
      mapBestNetTro.putIfAbsent("1", () => [false]);
      mapBestNetPri.putIfAbsent("1", () => [""]);
    }
    var data;
    var arrBestNettOverall = List<String>();
    arrBestNettOverall.addAll(_arrTextBestMettOverall.map((e) => e.text));
    var arrBestGros = List<String>();
    arrBestGros.addAll(_arrTextBestGros.map((e) => e.text));
    var arrHoleInOne = List<String>();
    arrHoleInOne.addAll(_arrTextHoleInOne.map((e) => e.text));
    var arrNearPin = List<String>();
    arrNearPin.addAll(_arrTextNearPin.map((e) => e.text));
    if(_valFlight == "1"){
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        "winner_bg":int.parse(_valWinnersGross),
        "certificate_bg":valChecksGrossCer.getRange(0, int.parse(_valWinnersGross)).toList(),
        "trophy_bg":valChecksGrossTro.getRange(0, int.parse(_valWinnersGross)).toList(),
        "prize_bg":arrBestGros.getRange(0, int.parse(_valWinnersGross)).toList(),
        "hole_ho":_arrValHoleInOne,
        "certificate_ho":valCheckHoleOneCer,
        "trophy_ho":valCheckHoleOneTro,
        "prize_ho":arrHoleInOne,
        "hole_np":_arrValNearsToThePin,
        "certificate_np":valChecksNearPinCer,
        "trophy_np":valChecksNearPinTro,
        "prize_np":arrNearPin,
        "hole_nl":_valNearsToTheLin,
        "certificate_nl":valChecksNearLineCer,
        "trophy_nl":valChecksNearLineTro,
        "prize_nl": nlcon.text,
        "hole_ld":int.parse(_valLongesDrive),
        "certificate_ld":valChecksLongestCer,
        "trophy_ld":valChecksLongestTro,
        "prize_ld":ldcon.text,
        'template_turnamen': _valTemplate,
        "token" : token
      };
    }else{
      if (int.parse(_valFlight) == 2){
        mapBestNetCer.remove("3");
        mapBestNetTro.remove("3");
        mapBestNetPri.remove("3");
      }
      var mapBestNetPrize = Map<String, dynamic>();
      for (int index = 0; index < int.parse(_valFlight); index++){
        var arrTextPriceFlight = List<String>();
        mapBestNetPrize.putIfAbsent("${index+1}", () => arrTextPriceFlight);
        int lenght = int.parse(_arrValWinners[index]);
        for (int a = 0; a < lenght; a++){
          arrTextPriceFlight.add(mapBestNetPri["${index+1}"][a].text);
          mapBestNetPrize.update("${index+1}", (value) => arrTextPriceFlight = value);
        }
      }
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "nama_flight":_arrTittleFlight.getRange(0, int.parse(_valFlight)).toList(),
        "winner_fl":_arrValWinners,
        "min_range":_arrMinRange,
        "max_range":_arrMaxRange,
        "certificate_fl":mapBestNetCer,
        "trophy_fl":mapBestNetTro,
        "prize_fl":mapBestNetPrize,
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        "winner_bg":int.parse(_valWinnersGross),
        "certificate_bg":valChecksGrossCer.getRange(0, int.parse(_valWinnersGross)).toList(),
        "trophy_bg":valChecksGrossTro.getRange(0, int.parse(_valWinnersGross)).toList(),
        "prize_bg":arrBestGros.getRange(0, int.parse(_valWinnersGross)).toList(),
        "hole_ho":_arrValHoleInOne.take(4).toList(),
        "certificate_ho":valCheckHoleOneCer.take(4).toList(),
        "trophy_ho":valCheckHoleOneTro.take(4).toList(),
        "prize_ho":arrHoleInOne.take(4).toList(),
        "hole_np":_arrValNearsToThePin.take(4).toList(),
        "certificate_np":valChecksNearPinCer.take(4).toList(),
        "trophy_np":valChecksNearPinTro.take(4).toList(),
        "prize_np":arrNearPin.take(4).toList(),
        "hole_nl":_isDoesntHavePar5 ? null : _valNearsToTheLin,
        "certificate_nl":valChecksNearLineCer,
        "trophy_nl":valChecksNearLineTro,
        "prize_nl": nlcon.text,
        "hole_ld":_isDoesntHavePar5 ? null : int.parse(_valLongesDrive),
        "certificate_ld":valChecksLongestCer,
        "trophy_ld":valChecksLongestTro,
        "prize_ld":ldcon.text,
        'template_turnamen': _valTemplate,
        "token" : token
      };
    }
    print("TOURNAMENT $data");
    Utils.postAPI(data, "create_wrpt").then((body) async {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SummaryGames(tourId: widget.data["turnamen"]['id_turnamen'])),
            ModalRoute.withName('/'),
          );
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something went wrong. Please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  Future<void> _saveMini() async{
    if (_arrValWinners.isEmpty){
      _arrValWinners.add("1");
      mapBestNetCer.putIfAbsent("1", () => [false]);
      mapBestNetTro.putIfAbsent("1", () => [false]);
      mapBestNetPri.putIfAbsent("1", () => [""]);
    }
    var data;
    var arrBestNettOverall = List<String>();
    arrBestNettOverall.addAll(_arrTextBestMettOverall.map((e) => e.text));
    var arrBestGros = List<String>();
    arrBestGros.addAll(_arrTextBestGros.map((e) => e.text));
    if(_valFlight == "1"){
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        "winner_bg":int.parse(_valWinnersGross),
        "certificate_bg":valChecksGrossCer.getRange(0, int.parse(_valWinnersGross)).toList(),
        "trophy_bg":valChecksGrossTro.getRange(0, int.parse(_valWinnersGross)).toList(),
        "prize_bg":arrBestGros.getRange(0, int.parse(_valWinnersGross)).toList(),
        'template_turnamen': _valTemplate,
        "token" : token
      };
    }else{
      if (int.parse(_valFlight) == 2){
        mapBestNetCer.remove("3");
        mapBestNetTro.remove("3");
        mapBestNetPri.remove("3");
      }
      var mapBestNetPrize = Map<String, dynamic>();
      for (int index = 0; index < int.parse(_valFlight); index++){
        var arrTextPriceFlight = List<String>();
        mapBestNetPrize.putIfAbsent("${index+1}", () => arrTextPriceFlight);
        int lenght = int.parse(_arrValWinners[index]);
        for (int a = 0; a < lenght; a++){
          arrTextPriceFlight.add(mapBestNetPri["${index+1}"][a].text);
          mapBestNetPrize.update("${index+1}", (value) => arrTextPriceFlight = value);
        }
      }
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "nama_flight":_arrTittleFlight.getRange(0, int.parse(_valFlight)).toList(),
        "winner_fl":_arrValWinners,
        "min_range":_arrMinRange,
        "max_range":_arrMaxRange,
        "certificate_fl":mapBestNetCer,
        "trophy_fl":mapBestNetTro,
        "prize_fl":mapBestNetPrize,
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        "winner_bg":int.parse(_valWinnersGross),
        "certificate_bg":valChecksGrossCer.getRange(0, int.parse(_valWinnersGross)).toList(),
        "trophy_bg":valChecksGrossTro.getRange(0, int.parse(_valWinnersGross)).toList(),
        "prize_bg":arrBestGros.getRange(0, int.parse(_valWinnersGross)).toList(),
        'template_turnamen': _valTemplate,
        "token" : token
      };
    }
    print("MINI TOURNAMENT $data");

    Utils.postAPI(data, "create_wrpmt").then((body) async {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SummaryGames(tourId: widget.data["turnamen"]['id_turnamen'])),
            ModalRoute.withName('/'),
          );
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something went wrong. Please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  Future<void> _saveMatch() async {
    if (_arrValWinners.isEmpty){
      _arrValWinners.add("1");
      mapBestNetCer.putIfAbsent("1", () => [false]);
      mapBestNetTro.putIfAbsent("1", () => [false]);
      mapBestNetPri.putIfAbsent("1", () => [""]);
    }

    var data;
    var arrBestNettOverall = List<String>();
    arrBestNettOverall.addAll(_arrTextBestMettOverall.map((e) => e.text));

    if(_valFlight == "1"){
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        'template_turnamen': _valTemplate,
        'token' : token
      };
    }else{
      if (int.parse(_valFlight) == 2){
        mapBestNetCer.remove("3");
        mapBestNetTro.remove("3");
        mapBestNetPri.remove("3");
      }
      var mapBestNetPrize = Map<String, dynamic>();
      for (int index = 0; index < int.parse(_valFlight); index++){
        var arrTextPriceFlight = List<String>();
        mapBestNetPrize.putIfAbsent("${index+1}", () => arrTextPriceFlight);
        int lenght = int.parse(_arrValWinners[index]);
        for (int a = 0; a < lenght; a++){
          arrTextPriceFlight.add(mapBestNetPri["${index+1}"][a].text);
          mapBestNetPrize.update("${index+1}", (value) => arrTextPriceFlight = value);
        }
      }
      data = {
        "id_turnamen":widget.data["turnamen"]['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "nama_flight":_arrTittleFlight.getRange(0, int.parse(_valFlight)).toList(),
        "winner_fl":_arrValWinners,
        "min_range":_arrMinRange,
        "max_range":_arrMaxRange,
        "certificate_fl":mapBestNetCer,
        "trophy_fl":mapBestNetTro,
        "prize_fl":mapBestNetPrize,
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":arrBestNettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        'template_turnamen': _valTemplate,
        'token' : token
      };
    }
    print("MATCH $data");

    Utils.postAPI(data, "create_wrpm").then((body) async {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SummaryGames(tourId: widget.data["turnamen"]['id_turnamen'],)),
            ModalRoute.withName('/'),
          );
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something went wrong. Please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }
}
