import 'package:flutter/material.dart';

class SubmittedText extends StatefulWidget {
  const SubmittedText({
    Key key, this.submit, this.msg, this.sub_msg
  }) : super(key: key);

  final submit;
  final msg;
  final sub_msg;

  @override
  _SubmittedTextState createState() => _SubmittedTextState();
}

class _SubmittedTextState extends State<SubmittedText> {
  @override
  void initState() {
    widget.submit;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.msg!=null?Container(
      padding: widget.msg == "Cannot change score, already approved" ? EdgeInsets.zero : EdgeInsets.only(bottom: 18),
      child: Column(
        children: [
          Text("${widget.msg!=null?widget.msg:""}", style: TextStyle(color: Color(0xFFF15411), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text("${widget.sub_msg!=null?widget.sub_msg:""}", style: TextStyle(color: Color(0xFF4F4F4F)), textAlign: TextAlign.center,),
          )
        ],
      ),
    ):Container();
  }
}