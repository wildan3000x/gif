import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/utils/Utils.dart';

class ChangePairing extends StatefulWidget {
  final String description, mToken, userID, tourID, pairingID, column;

  ChangePairing({
    @required this.description, this.mToken, this.userID, this.tourID, this.pairingID, this.column
  });
  @override
  _ChangePairingState createState() => _ChangePairingState();
}

class _ChangePairingState extends State<ChangePairing> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController fromSwap = TextEditingController();
  TextEditingController toSwap = TextEditingController();
  bool _isLoading = false;

  FocusNode focusNode = FocusNode();
  FocusNode focusNode2 = FocusNode();
  String hintText = '1A.1';
  String hintText2;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    hintText2 = "${widget.pairingID}.${widget.column}";
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        hintText = '';
      } else {
        hintText = '1A.1';
      }
      setState(() {});
    });
    focusNode2.addListener(() {
      if (focusNode2.hasFocus) {
        hintText2 = '';
      } else {
        hintText2 = "${widget.pairingID}.${widget.column}";
      }
      setState(() {});
    });
  }

  changePairing() async {
    var data = {
      'token' : widget.mToken,
      'id_turnamen' : int.parse(widget.tourID),
      'pairing_swap' : fromSwap.text,
      'request_pairing_swap' : toSwap.text==""?"1A.1":toSwap.text,
      'pairing_request' : widget.description
    };

    Utils.postAPI(data, "test_SPO").then((body) {
      if(body['code'] == '200'){
        setState(() {
          _isLoading = false;
        });
        print("DATA PAIR = $body");
        Navigator.pop(context, body["message"]);
      }else{
        Navigator.pop(context, body["message"]);
        // setState(() {
        //   _isLoading = false;
        // });
      }
    }, onError: (error) {
      Fluttertoast.showToast(
          msg: "Please enter a valid pairing",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: Colors.red,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1
      );

      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      key: _scaffoldKey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(context) {
    print("KEY $_scaffoldKey");
    fromSwap.text = "${widget.pairingID}.${widget.column}";
    return Container(
      width: double.maxFinite,
      height: 190,
      alignment: Alignment.center,
      // padding: EdgeInsets.only(top: 15, bottom: 15),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          color: Color(0xFF2554BC).withOpacity(0.7),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: Text(
              "CHANGE PAIRING",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              textAlign: TextAlign.center,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: 70,
                child: TextFormField(
                  controller: fromSwap,
                  // inputFormatters: [LengthLimitingTextInputFormatter(2)],
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 2,color: Colors.white)
                      ),
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(bottom: 9),
                      hintText: hintText2,
                      hintStyle: TextStyle(color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold)
                  ),
                  style: TextStyle(
                      color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                  focusNode: focusNode2,
                ),
              ),
              Container(
                child: Text("Swap With", style: TextStyle(color: Colors.white, fontSize: 18),),
              ),
              Container(
                width: 70,
                child: TextFormField(
                  controller: toSwap,
                  // inputFormatters: [LengthLimitingTextInputFormatter(2)],
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 2,color: Colors.white)
                      ),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(width: 2,color: Colors.white)
                      ),
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(bottom: 9),
                      // hintText: hintText,
                      hintStyle: TextStyle(color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold)
                  ),
                  style: TextStyle(color: Colors.white, fontSize: 32, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                  // focusNode: focusNode,
                ),
              )
            ],
          ),
          InkWell(
              onTap: () {
                setState(() {
                  if(fromSwap.text.isEmpty || toSwap.text.isEmpty){
                    Fluttertoast.showToast(
                        msg: "Please enter a valid pairing",
                        toastLength: Toast.LENGTH_SHORT,
                        backgroundColor: Colors.red,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1
                    );
                  }else {
                    _isLoading = true;
                    changePairing();
                  }
                });
              },
              child: Container(
                  margin: EdgeInsets.only(top:20),
                  width: 116,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: AppTheme.gFont,
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: Colors.white, width: 2)),
                  child: _isLoading ? Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      ))
                      : Text(
                    "Submit",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                    textAlign: TextAlign.center,
                  )))
        ],
      ),
    );
  }
}
