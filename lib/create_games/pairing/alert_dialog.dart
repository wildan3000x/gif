import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/pairing/success_msg.dart';
import 'package:gif/main.dart';

class CustomDialog extends StatefulWidget {
  final String description, mToken, userID, tourID;

  CustomDialog({
    @required this.description, this.mToken, this.userID, this.tourID,
  });

  @override
  _CustomDialogState createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  String _pairRadio = "Same Pairing";
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  void _handlePair(String value) {
    setState(() {
      _pairRadio = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(context) {
    return Container(
      width: double.maxFinite,
      height: 190,
      alignment: Alignment.center,
        // padding: EdgeInsets.only(top: 15, bottom: 15),
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Color(0xFF2554BC).withOpacity(0.7),
            borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Text(
              "REQUEST PAIRING CHANGE",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              textAlign: TextAlign.center,
            ),
            Wrap(
              direction: Axis.vertical,
            children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SizedBox(
                      width: 26,
                      child: CustomRadioWidget(
                        value: "Swap Pairing",
                        groupValue: _pairRadio,
                        onChanged: _handlePair,
                      ),
                    ),
                    Text("Swap Pairing",
                        style: TextStyle(color: Colors.white, fontSize: 15)),
                    Padding(padding: EdgeInsets.only(left: 8)),
                    SizedBox(
                      width: 26,
                      child: CustomRadioWidget(
                        value: "Same Pairing",
                        groupValue: _pairRadio,
                        onChanged: _handlePair,
                      ),
                      /*Radio(
                        activeColor: Colors.white,
                        focusColor: Colors.white,
                        hoverColor: Colors.white,
                        value: "Same Pairing",
                        groupValue: _pairRadio,
                        onChanged: _handlePair,
                      ),*/
                    ),
                    Text("Same Pairing",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ],
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("With ",
                      style: TextStyle(color: Colors.white, fontSize: 18)),
                  Text(widget.description,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18))
                ],
              ),
            ),
            InkWell(
                onTap: () {
                  setState(() {
                    _isLoading = true;
                    requestPairing();
                  });
                },
                child: Container(
                  width: 116,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: AppTheme.gFont,
                      borderRadius: BorderRadius.circular(30),
                      border: Border.all(color: Colors.white, width: 2)),
                  child: _isLoading ? Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      ))
                      : Text(
                    "Submit",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                    textAlign: TextAlign.center,
                  )
                )
            )
          ],
        ),
    );
  }

  requestPairing() async {
    var data = {
      'token' : widget.mToken,
      'turnamen_id' : widget.tourID,
      'request_user_id' : widget.userID,
      'pairing_status' : _pairRadio
    };

    Utils.postAPI(data, "test_request_pairing").then((body) {
      if(body['code'] == '200'){
        setState(() {
          _isLoading = false;
        });
        print("DATA PAIR = $body");
        Navigator.pop(context, body["message"]);
      }else{
        setState(() {
          _isLoading = false;
        });
        Navigator.pop(context, body["message"]);
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }
}

class CustomRadioWidget<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final ValueChanged<T> onChanged;
  final double width;
  final double height;

  CustomRadioWidget({this.value, this.groupValue, this.onChanged, this.width = 32, this.height = 32});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: GestureDetector(
        onTap: () {
          onChanged(this.value);
        },
        child: Container(
          padding: EdgeInsets.all(3),
          height: this.height,
          width: this.width,
          decoration: ShapeDecoration(
            shape: CircleBorder(),
            color: Colors.white
          ),
          child: Center(
            child: Container(
              height: this.height - 8,
              width: this.width - 8,
              decoration: ShapeDecoration(
                shape: CircleBorder(),
                color: value == groupValue ? Colors.black : Theme.of(context).scaffoldBackgroundColor,
                // gradient: LinearGradient(
                //   colors: value == groupValue ? [
                //     Color(0xFFE13684),
                //     Color(0xFFFF6EEC),
                //   ] : [
                //     Theme.of(context).scaffoldBackgroundColor,
                //     Theme.of(context).scaffoldBackgroundColor,
                //   ],
                // ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}






