import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/auth/profile/profile_page.dart';
import 'package:gif/caddie/search_golfer.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/invitation/invite_golfers.dart';
import 'package:gif/create_games/pairing/alert_change_pairing.dart';
import 'package:gif/create_games/pairing/alert_dialog.dart';
import 'package:gif/create_games/pairing/success_msg.dart';
import 'package:gif/create_games/pairing/view_profile.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/main.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/notification/notif_golfer.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/leaderboard/input_skill_award/input_skill_award.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gif/the_game/start_game.dart';


class PairingGames extends StatefulWidget {
  PairingGames({Key key, this.submit, this.data, this.id, this.msg, this.sub_msg, this.userId, this.navigation, this.viewFromSummary, this.idNotif, this.organizer, this.fromRequestPairingChangeInStartGame}) : super(key: key);
  final submit, id, userId, sub_msg, navigation, viewFromSummary, idNotif, organizer, fromRequestPairingChangeInStartGame;
  var msg, data;

  @override
  _PairingGamesState createState() => _PairingGamesState();
}

class _PairingGamesState extends State<PairingGames> {
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataPairing;
  var confirmed;
  var myFormat = DateFormat('d MMM yyyy');
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isLoading = false;
  bool _isLoading2 = false;

  String _textMSG, _textSub, idSetPreference;
  int id;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  var _currentItemSelected = List<String>();
  List<String> _hole = ['1', '2', '3', '4', '5'];

  @override
  void initState() {
    print("MESSAGE FROM NOTIF ${widget.msg}");
    print("ID USER ${widget.userId}");
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.id;
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else{
      getTournamentPairing();
    }
  }

  List<String> idOrganizer = [];
  List<String> idGolferJoin = [];
  var dataLain;
  bool checkOrga = false;
  bool checkStatusGolferJoin = false;

  getTournamentPairing() async {
    setState(() {
      _isLoading = true;
    });
    idSetPreference = sharedPreferences.getString("id");
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_turnamen' : id == null ? "" : id,
      'id_notifikasi' : widget.idNotif
    };

    Utils.postAPI(data, "get_tp").then((body) {
      if(body['code'] == '200'){
        dataPairing = body["data"];
        confirmed = body["status"];
        dataTour = body['data_turnamen'];
        print("ID USER ${dataTour['user_id']}");
        setState(() {
          dataLain = body;
        });
        print("MASUK PAIRING ${dataLain['masuk_pairing']} //widget.data for if golfer has joined the pairing/game or not (masuk pairing)");
        if(dataLain['masuk_pairing'] == 'Ya'){
          setState(() {
            widget.data = "Confirmed";
            _textMSG = null;
          });
        }else{
          widget.data = "Out";
        }
        setState(() {
          _isLoading = false;
          if(dataLain['data_organizer'].isNotEmpty){
            for(int a=0;a<dataLain['data_organizer'].length;a++){
              idOrganizer.add(dataLain['data_organizer'][a]['user_id'].toString());
            }
          }
          checkOrga = idOrganizer.contains(idSetPreference);
          print("ORGANIZER $checkOrga");

          for(int i=0;i<dataPairing.length;i++){
            for(int a=0;a<dataPairing[i]['colom'].length;a++){
              idGolferJoin.add(dataPairing[i]['colom'][a]['user_id'].toString());
            }
          }
          checkStatusGolferJoin = idGolferJoin.contains(idSetPreference);
          // print("ID GOLFER $idGolferJoin");
          // print("STATUS $checkStatusGolferJoin");
        });
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  List<TextEditingController> cPairCon = [TextEditingController(), TextEditingController()];

  changePairingID(String id_turnamen_pairing, String pairing_id_baru) async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_turnamen_pairing' : id_turnamen_pairing,
      'pairing_id_baru' : pairing_id_baru
    };

    Utils.postAPI(data, "CPID").then((body) {
      if(body['code'] == '200'){
        getTournamentPairing();
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(data: widget.data, id: widget.id, userId: widget.userId,)));
        // setState(() {
        //   cPairCon.text = body['data'];
        // });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        // _loading = false;
      });
    });
  }

  samePairing(String turnamen_id, String user_id, String no_kolom, String p_pairing_id, String user_id_request, String no_kolom_request, String p_pairing_id_request, String pairing_id) async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'turnamen_id' : turnamen_id,
      'user_id' : user_id,
      'no_kolom' : no_kolom,
      'p_pairing_id' : p_pairing_id,
      'user_id_request' : user_id_request,
      'no_kolom_request' : no_kolom_request,
      'p_pairing_id_request' : p_pairing_id_request,
      'pairing_id_request' : pairing_id
    };

    Utils.postAPI(data, "same_pairing").then((body) {
      if(body['code'] == '200' || body['code'] == 200){
        setState(() {
          _isLoading = false;
        });
        getTournamentPairing();
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(data: widget.data, id: widget.id, userId: widget.userId,)));
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
        // Navigator.pop(context, body["message"]);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getTournamentPairing();
    setState(() {
      if(dataLain['masuk_pairing'] == 'Ya'){
        widget.msg = null;
        widget.data = "Confirmed";
      }else{
        widget.data = "Out";
      }
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getTournamentPairing();
    if(mounted)
      setState(() {
        getTournamentPairing();
      });
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if(widget.navigation == "confirm"){
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
        } else{
          Navigator.pop(context, "reload");
        }
        return true;
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: Stack(
              children: [
                SmartRefresher(
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    margin: EdgeInsets.all(13),
                    child: Column(
                      children: [
                        HeaderCreate(title: "Pairing Screen",),
                        titleInfo(),
                        textPairing(),
                        SubmittedText(msg: _textMSG==null?widget.msg:_textMSG, sub_msg: _textSub==null?widget.sub_msg:_textSub,),
                        textPairingGolfer(),
                        pairingGolfer(),
                        _isLoading ? Container() : infoFooterButton()
                      ],
                    ),
                  ),
                ),),
                _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container titleInfo() {
    return Container(
      padding: EdgeInsets.only(top: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.only(left: 3, bottom: 5),
                    child: Text(
                      "${dataTour == null ? "" : dataTour['nama_turnamen']}",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: AppTheme.gFont),
                    overflow: TextOverflow.ellipsis,)),
                Container(
                    padding: EdgeInsets.only(left: 3),
                    child: Text("${dataTour == null ? "" : dataTour['nama_lapangan']}\n${dataTour==null?"":myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))}")),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                    padding: EdgeInsets.only(right: 3, bottom: 5),
                    child: Text("Current Participant",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: AppTheme.gFont))),
                Container(
                    padding: EdgeInsets.only(right: 3),
                    child: Text(
                      "${confirmed == null?"0":confirmed['confirmed']} Confirmed",
                      textAlign: TextAlign.end,
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container pairingGolfer() {
    int no = 1;
    return dataPairing!=null?dataPairing.isEmpty ?
    Container(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 7.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppTheme.gFont),
                  child: Text(
                    "1A",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: Text(
                    "Hole 1",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color(0xFF4F4F4F)),
                  ),
                )
              ],
            ),
          ),
          Container(
            // alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: 80),
            child: Text("No one's here yet."),
          )
        ],
      ),
    ) :
    Container(
      padding: EdgeInsets.only(top: 10),
      child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataPairing!=null?dataPairing.length:0,
          itemBuilder: (context, index) {
            if (_currentItemSelected.length<dataPairing.length){
              _currentItemSelected.add("1");
            }
            cPairCon.add(TextEditingController());
            return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 7.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top:3),
                            padding: EdgeInsets.all(12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: AppTheme.gFont),
                            child: Container(
                              width: 27,
                              height: 27,
                              child: dataTour['user_id'] == widget.userId || checkOrga == true ?
                              Center(
                                child: TextFormField(
                                  controller: cPairCon[index],
                                  onFieldSubmitted: (value){
                                    setState(() {
                                      if(value!=dataPairing[index]['pairing_id']) {
                                        changePairingID(dataPairing[index]['colom'][0]['id_turnamen_pairing'].toString(), cPairCon[index].text);
                                      }else{
                                        return false;
                                      }
                                    });
                                  },
                                  inputFormatters: [
                                    new LengthLimitingTextInputFormatter(2),
                                  ],
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    hintText: "${dataPairing[index]['pairing_id']}",
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    contentPadding: EdgeInsets.only(bottom: 10),
                                    hintStyle: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),
                                  ),
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),
                                ),
                              ) : Center(child: Text("${dataPairing[index]['pairing_id']}", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),)),
                            )
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 3.0),
                            child: Text(_currentItemSelected[index] == '1' ? "Hole ${index + no}" : "Hole ${_currentItemSelected[index]}",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Color(0xFF4F4F4F))
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(
                      // width: 300,
                      // alignment: Alignment.centerLeft,
                      height: 80,
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          primary: false,
                          itemCount: dataPairing[index]['colom'].length,
                          itemBuilder: (context, index2) {
                            return Container(
                              width: SizeConfig.screenWidth/5.2,
                              padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left: 18) : EdgeInsets.only(left: 10),
                              child: Wrap(
                                children: [
                                  Stack(
                                    // crossAxisAlignment: WrapCrossAlignment.start,
                                    // alignment: WrapAlignment.start,
                                    children: [
                                      checkOrga == true || dataTour['user_id'] == widget.userId ?
                                      dataPairing[index]['colom'][index2]['user_id'] == null ? Container() : Draggable<dynamic>(
                                        data: dataPairing[index]['colom'][index2],
                                        childWhenDragging: Container(),
                                        feedback: Padding(
                                          padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left :0.0) : EdgeInsets.only(left :8.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(top: 5),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                ),
                                                child: dataPairing[index]['colom'][index2]['foto'] == null ? ClipRRect(
                                                  borderRadius: BorderRadius.circular(10000.0),
                                                  child: Image.asset(
                                                    "assets/images/userImage.png",
                                                    width: 50,
                                                    height: 50,
                                                  ),
                                                ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                                    CircularProgressIndicator(
                                                      value: progress.progress,
                                                    ), imageUrl: Utils.url + 'upload/user/' + dataPairing[index]['colom'][index2]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        child: InkWell(
                                          onTap: () async {
                                              await showDialog(
                                                context: _scaffoldKey.currentContext,
                                                builder: (BuildContext context) =>
                                                    ChangePairing(
                                                      description: "Swap Pairing",
                                                      mToken: sharedPreferences.getString("token"),
                                                      tourID: dataTour["id_turnamen"].toString(),
                                                      userID: dataPairing[index]['colom'][index2]['user_id'].toString(),
                                                      pairingID: dataPairing[index]['colom'][index2]['pairing_id'],
                                                      column: dataPairing[index]['colom'][index2]['no_kolom'].toString(),
                                                    )
                                            ).then((value) {
                                              setState(() {
                                                if(value != null) {
                                                  getTournamentPairing();
                                                  _textMSG = value;
                                                  _textSub = "";
                                                }
                                              });
                                            });
                                          },
                                          child: pictureGolferDragDrop(index, index2),
                                        ),
                                      )
                                          : dataPairing[index]['colom'][index2]['user_id'] == null ? Container() : InkWell(
                                        onTap: () async {
                                          print("JOIN ${dataLain['masuk_pairing']}");
                                          print("ID 1 ${dataPairing[index]['colom'][index2]['user_id']} ID 2 ${widget.userId}");
                                          widget.data != null ? widget.data != "Confirmed" || widget.userId.toString() == dataPairing[index]['colom'][index2]['user_id'].toString() ? Container() :
                                          await showDialog(
                                            context: _scaffoldKey.currentContext,
                                            builder: (
                                                BuildContext context) =>
                                                CustomDialog(
                                                  description: "${dataPairing[index]['colom'][index2]['nama_lengkap']}",
                                                  mToken: sharedPreferences.getString("token"),
                                                  tourID: dataTour["id_turnamen"].toString(),
                                                  userID: dataPairing[index]['colom'][index2]['user_id'].toString(),
                                                ),
                                          ).then((value) {
                                            setState(() {
                                              _textMSG = value;
                                              if (_textMSG ==
                                                  "Pairing Change Request was Submitted") {
                                                _textSub =
                                                "You are requesting to swap pairing with ${dataPairing[index]['colom'][index2]['nama_lengkap']} Organizer is notified";
                                              } else {
                                                _textSub = "";
                                              }
                                            });
                                          }) : Container();
                                        },
                                        child: pictureGolferDragDrop(index, index2),
                                      ),
                                      DragTarget<dynamic>(
                                        builder: (context, List<dynamic> candidateData, rejectedData) {
                                          return Container(
                                              padding: EdgeInsets.only(top: 22, left: 18, right: 0, bottom: 0),
                                              // color: Colors.yellow,
                                              height: 60,
                                              width: 70 ,
                                              child: Stack(
                                                  children: [
                                                    Container(),
                                                  ],
                                                )
                                          );
                                        },
                                        onWillAccept: (data) {
                                          return true;
                                        },
                                        onAccept: (data) {
                                          dynamic dataReq = dataPairing[index]['colom'][index2];
                                          if(data['user_id'].toString() != dataReq['user_id'].toString()) {
                                            setState(() {
                                              _isLoading = true;
                                            });
                                            samePairing(
                                                data['turnamen_id'].toString(),
                                                data['user_id'].toString(),
                                                data['no_kolom'].toString(),
                                                data['p_pairing_id'].toString(),
                                                dataReq['user_id'].toString(),
                                                dataReq['no_kolom'].toString(),
                                                dataReq['p_pairing_id'].toString(),
                                                dataReq['pairing_id'].toString());
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          }),
                    ),
                  ),
                ]);
          }),
    ): Container();
  }

  Padding pictureGolferDragDrop(int index, int index2) {
    return Padding(
      padding: dataPairing[index]['colom'].length <= 3 ? EdgeInsets.only(left :0.0) : EdgeInsets.only(left :8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            child: dataPairing[index]['colom'][index2]['user_id'] == null ? Container(width: 50, height: 50,) : dataPairing[index]['colom'][index2]['foto'] == null ? ClipRRect(
              borderRadius: BorderRadius.circular(10000.0),
              child: Image.asset(
                "assets/images/userImage.png",
                width: 50,
                height: 50,
              ),
            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/user/' + dataPairing[index]['colom'][index2]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
          ),
          InkWell(
            onTap: () => Utils.linkToGolferProfile(context, dataPairing[index]['colom'][index2]),
                // Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userId: dataPairing[index]['colom'][index2]['user_id'],))),
            child: Padding(
              padding: const EdgeInsets.only(top: 3.0),
              child: Center(
                child: dataPairing[index]['colom'][index2]['user_id'] == null ? Container() : Text(
                  "${dataPairing[index]['colom'][index2]['nama_pendek']!=null?dataPairing[index]['colom'][index2]['nama_pendek']:dataPairing[index]['colom'][index2]['nama_lengkap']}",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Color(0xFF4F4F4F)),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container textPairingGolfer() {
    SizeConfig().init(context);
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xFFDADADA)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              "Pairing ID/\nStart Hole",
              style: TextStyle(color: AppTheme.gFont),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.only(right: SizeConfig.screenWidth/15, left: 10),
              // width: 300,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Golfers",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "1",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "2",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "3",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Text(
                          "4",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Container infoFooterButton() {
    // print("SSSS ${widget.userId}");
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 5, left: 15, right: 15),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Text(
              "${dataTour==null?"":dataTour['user_id'] == widget.userId || checkOrga == true ?"You Are Organizer":"Pairing Change Request"}",
              style: TextStyle(
                  color: AppTheme.gFont,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            "${dataTour==null?"":dataTour['user_id'] == widget.userId || checkOrga == true ?"As an Organizer, you can swap golfers pairing by dragging or by clicking the golfers picture and manually enter the pairing.":"To request pairing change, click golfer image\nand select the option. You can request to swap pairing\nwith or be in the same pairing with the golfer you choose."} "
                "\nTap on golfers name to see their profile.",
            style: TextStyle(color: Color(0xFF4F4F4F)),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                /*widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true) ? Container() : */
                RaisedButton(
                  onPressed: (){
                    if(widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true)){
                      Navigator.pop(context, "reload");
                    }else {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SummaryGames(tourId: id, onlyView: true, viewFromPairing: true)));
                    }
                  },
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true) ? AppTheme.gButton : AppTheme.par,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: BoxConstraints(maxWidth: widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true) ? 116 : 166, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text( widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true) ? "Back" : "View Tournament Summary", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: widget.viewFromSummary == true || dataTour==null || (dataTour['user_id'] == widget.userId || checkOrga == true) ? 16 : 14), textAlign: TextAlign.center,))
                  ),
                ),
                checkStatusGolferJoin ? Row(
                  children: [
                    SizedBox(width: 15,),
                    RaisedButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                      splashColor: Colors.grey.withOpacity(0.5),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => StartGame(data: dataTour, fromPairingNext: true, id: dataTour['id_turnamen'], userId: widget.userId,)));
                      },
                      padding: const EdgeInsets.all(0.0),
                      child: Ink(
                          decoration: BoxDecoration(
                              color: AppTheme.gButton,
                              borderRadius: BorderRadius.circular(100)
                          ),
                          child: Container(
                              constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                              alignment: Alignment.center,
                              child: Text("Next", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                    ),
                  ],
                ) : Container(),
              ],
            ),
          ),
          // backToHomeButton()
        ],
      ),
    );
  }

  Container textPairing() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "Tournament Pairing",
          style: TextStyle(
            color: Color(0xFF4F4F4F),
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

}



//PAIRING (BACKUP)