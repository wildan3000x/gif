import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/tournament_detail.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JoinGames extends StatefulWidget {
  const JoinGames({Key key, this.data, this.forCaddie, this.notInvited}) : super(key: key);
  final data, forCaddie, notInvited;

  @override
  _JoinGamesState createState() => _JoinGamesState();
}

class _JoinGamesState extends State<JoinGames> {
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataWinner;
  var id;
  var count;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');
  bool _isLoading = false;
  bool _isLoading2 = false;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.data;
    // print("DATA = ${id}");
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getJoinGames();
      _getSummary();
    }
  }

  final snackbarKey = GlobalKey<ScaffoldState>();

  joinGames() async {
    var data = {
      'token' : widget.forCaddie == null ? sharedPreferences.getString("token") : widget.forCaddie['golfer']['token'],
      'token_caddie': widget.forCaddie == null ? "" : sharedPreferences.getString("token"),
      'id_turnamen' : id == null ? "" : id['id_turnamen']
    };
    Utils.postAPI(data, "test_join_game").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading2 = false;
        });
        print("JOIN GAME = ${body}");
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>
            PairingGames(
              data: body['data']['status'],
              id: widget.data['id_turnamen'],
              msg: body['head_message'],
              sub_msg: body["sub_message"],
              userId: sharedPreferences.getString("id"),
            )));
        // // dataTransaksi = body["data"];
        // countNotifGolfer = body["count"];
        // print("COUNT : ${countNotifGolfer}");
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading2 = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading2 = false;
      });
    });
  }

  getJoinGames() async {
    setState(() {
      _isLoading2 = true;
    });
    var data = {
      'token' : widget.forCaddie == null ? sharedPreferences.getString("token") : widget.forCaddie['golfer']['token'],
      'id_turnamen' : id == null ? "" : id['id_turnamen']
    };
    Utils.postAPI(data, "get_JG").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading2 = false;
        });
        dataTour = body['data'];
        count = body['count_confirm'];
        print("GET JOIN GAME = ${dataTour['user_id']}");
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(data: body['data']['status'], id: widget.data['id_turnamen'], msg: "Joining Request was Submitted", sub_msg: "You are requesting to join in ${dataTour == null ? "" : dataTour['nama_turnamen']},\nOrganizer is notified",)));
        // // dataTransaksi = body["data"];
        // countNotifGolfer = body["count"];
        // print("COUNT : ${countNotifGolfer}");
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void _getSummary() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'id_turnamen' : id == null ? "" : id['id_turnamen'],
      'token' : widget.forCaddie == null ? sharedPreferences.getString("token") : widget.forCaddie['golfer']['token'],
    };

    Utils.postAPI(data, "get_ts").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          dataWinner = body;
          _isLoading = false;
        });
      }else{
        setState(() {
          _isLoading = false;
        });
        print("ERROR");
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading2,
        child: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderCreate(title: "Join Tournament",),
                      SizedBox(height: 15,),
                      dataTour == null ? Container() : TournamentDetailWidget(dataTournament: dataTour, additionalWidget: Container()),
                      tournamentRules(context),
                      SizedBox(height: 8,),
                      widget.notInvited != null ? Container() : FlatButton(
                        padding: EdgeInsets.zero,
                        splashColor: Colors.grey.withOpacity(0.3),
                        color: AppTheme.gLightGrey,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15),),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>
                              PairingGames(
                                id: dataWinner == null ? "0" : dataTour == null ? id : dataTour['id_turnamen'],
                                viewFromSummary: true,
                              )));
                        },
                        child: Container(
                          // margin: EdgeInsets.only(bottom: 3, top: 10),
                          padding: EdgeInsets.all(12),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                              border: Border.all(color: Color(0xFFBDBDBD)),),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Text(
                                        "Currently ${count==null?"0":count} Golfers Confirmed",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                    ),
                                  ],
                                ),
                                Image.asset('assets/images/arrow left.png')
                              ]),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Column(
                          children: [
                            RaisedButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                              splashColor: Colors.grey.withOpacity(0.5),
                              onPressed: () {
                                print("STATUS ${widget.notInvited}");
                                if(dataTour['status_turnamen'] != 'sudah' || widget.notInvited == null) {
                                  setState(() {
                                    _isLoading2 = true;
                                    joinGames();
                                  });
                                }else{
                                  print("STATUS ${widget.notInvited}");
                                }
                              },
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                  decoration: BoxDecoration(
                                      color: dataTour==null?Colors.grey:dataTour['status_turnamen'] == 'sudah' || widget.notInvited != null ? Colors.grey : AppTheme.gButton,
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: Container(
                                      constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                      alignment: Alignment.center,
                                      child: Text("Join", style: TextStyle(
                                          color: AppTheme.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,))),
                            ),
                            SizedBox(height: 12,),
                          ],
                        ),
                      ),
                      dataTour==null?Container():dataTour['status_turnamen'] != 'sudah' ? Container() : Text("This tournament has already been started. You cannot join further.", textAlign: TextAlign.center,),
                      widget.notInvited == null ? Container() : Text("Sorry, you are not invited in this game. Contact the organizer for further information.", textAlign: TextAlign.center,)
                    ],
                  ),
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  Stack tournamentRules(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.fromLTRB(13, 13, 13, 8),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15)),
          child: Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 13.0, bottom: 8),
                    child: Text(
                      "${dataTour == null ? "" : dataTour['tipe_turnamen']}",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Text(
                    "Top ${dataWinner==null?"":dataWinner['rule_bn']==null?"0":dataWinner['rule_bn']['winner']} Best Nett, HC ${dataWinner==null?"":dataWinner['rule_bn']==null?"0":dataWinner['rule_bn']['sistem']}, ${dataWinner==null?"0":dataWinner['price_bn'].isEmpty?"0":dataWinner['price_bn'][0]['price']==null||dataWinner['price_bn'][0]['price'].isNotEmpty?"With Prizes":"No Prizes"}",
                    style: TextStyle(color: AppTheme.gFont),
                    textAlign: TextAlign.center,
                  ),
                  dataTour==null?Container():dataTour['status_turnamen'] == 'sudah' ? Container() : Container(
                    padding: EdgeInsets.only(top: 5),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.zero),
                          backgroundColor: MaterialStateProperty.all(AppTheme.gLightGrey,),
                          side: MaterialStateProperty.all(BorderSide(color: Colors.grey.withOpacity(0.3))),
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0))),
                          overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.2))
                      ),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SummaryGames(tourId: id['id_turnamen'], onlyView: true,)));
                      },
                      child: Container(
                        width: 150,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: Text("view game detail", style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold),),
                            ),
                            Image.asset("assets/images/arrow right.png", width: 15, height: 15,)
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.symmetric(horizontal: 55),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Color(0xFFF15411),
          ),
          child: Text(
            "Winning Rules & Prizes",
            style: TextStyle(color: Colors.white, fontSize: 18),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
