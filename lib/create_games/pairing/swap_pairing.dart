import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SwapPairing extends StatefulWidget {
  const SwapPairing({Key key, this.idTour, this.idUserConfirm, this.idNotif}) : super(key: key);
  final idTour, idUserConfirm, idNotif;
  @override
  _SwapPairingState createState() => _SwapPairingState();
}

class _SwapPairingState extends State<SwapPairing> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  var dataSwap;
  bool _isLoading = false;
  bool _isLoading2 = false;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getDataUser();
    }
  }

  getDataUser() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      // 'token' : 'XDuHVPNVExZfeMJ05d5f0d0c9f2d1fAC',
      'id_user_confirm' : widget.idUserConfirm,
      'id_turnamen' : widget.idTour
      // 'id_user_confirm' : 32,
      // 'id_turnamen' : 1
    };

    Utils.postAPI(data, "get_CP").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
        });
        // dataTransaksi = body["data"];
        dataSwap = body;
        if(dataSwap['data1']==null){
          confirmUser("Confirmed");
        }
        print("COUNT : ${dataSwap['data1']}");
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  confirmUser(String confirm) async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_notifikasi' : widget.idNotif,
      'id_turnamen' : widget.idTour,
      'id_user' : dataSwap['user_pairing']['user_id'],
      'id_user_request' : dataSwap['user_pairing1']!=null?dataSwap['user_pairing1']['user_id']:null,
      'pairing_request' : dataSwap['user_pairing']['pairing_request'],
      'status' : confirm,
    };

    Utils.postAPI(data, "test_confirm_pairing").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          if(confirm == "Confirmed"){
            _isLoading = false;
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(id: dataSwap['data_turnamen']['id_turnamen'], navigation: "confirm",)));
          }
          else{
            _isLoading2 = false;
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PairingGames(id: dataSwap['data_turnamen']['id_turnamen'], navigation: "reject", msg: "Refused", sub_msg: "Refused to Swap",)));
          }
        });

        // dataTransaksi = body["data"];
        print("COUNT : ${body}");
      }else {
        if (dataSwap['data1'] != null) {
          var snackbar = SnackBar(
            content: Text(body['message']),
            backgroundColor: Colors.red,
          );
          snackbarKey.currentState.showSnackBar(snackbar);
          setState(() {
            _isLoading = false;
            _isLoading2 = false;
          });
        }else{
          Utils.showToast(context, "erere", "${body['message']}");
          Navigator.pop(context);
          print("DATA1 NULL");
        }
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
        _isLoading2 = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Stack(
          children: [
            ModalProgressHUD(
              inAsyncCall: _isLoading,
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderCreate(title: "${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['pairing_request'] == 'Swap Pairing'?"Swap":"Same"} Confirmation",),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['pairing_request'] == 'Swap Pairing'?"Swap":"Same"} Pairing", style: TextStyle(color: Color(0xFF4f4f4f), fontSize: 24, fontWeight: FontWeight.bold),),
                        ),
                        Text("Accept ${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['pairing_request'] == 'Swap Pairing'?"swap":"same"} pairing for golfer:", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFont),),
                        Container(
                          margin: EdgeInsets.only(top: 40),
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    dataSwap==null?Container():dataSwap['data1']==""||dataSwap['data1']==null?Container():dataSwap['data']['foto'] == null ? ClipRRect(
                                      borderRadius: BorderRadius.circular(10000.0),
                                      child: Image.asset(
                                        "assets/images/userImage.png",
                                        width: 75,
                                        height: 75,
                                      ),
                                    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                        CircularProgressIndicator(
                                          value: progress.progress,
                                        ), imageUrl: Utils.url + 'upload/user/' + dataSwap['data']['foto'], width: 75, height: 75, fit: BoxFit.cover,)),
                                    Padding(
                                      padding: const EdgeInsets.only(top:8.0),
                                      child: Column(
                                        children: [
                                          Text("${dataSwap==null?"":dataSwap['data']['nama_lengkap']}", style: TextStyle(color: Color(0xFF4f4f4f), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                          Padding(
                                            padding: const EdgeInsets.only(top:5.0),
                                            child: Text("${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['pairing_id']}.${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['no_kolom']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 24, fontWeight: FontWeight.bold),),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                                child: Text("With", style: TextStyle(color: Color(0xFF4f4f4f)),textAlign: TextAlign.center,),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    dataSwap==null?Container():dataSwap['data1']==""||dataSwap['data1']==null?Container():dataSwap['data1']['foto'] == null ? ClipRRect(
                                      borderRadius: BorderRadius.circular(10000.0),
                                      child: Image.asset(
                                        "assets/images/userImage.png",
                                        width: 75,
                                        height: 75,
                                      ),
                                    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                        CircularProgressIndicator(
                                          value: progress.progress,
                                        ), imageUrl: Utils.url + 'upload/user/' + dataSwap['data1']['foto'], width: 75, height: 75, fit: BoxFit.cover,)),
                                    Padding(
                                      padding: const EdgeInsets.only(top:8.0),
                                      child: Column(
                                        children: [
                                          Text("${dataSwap==null?"":dataSwap['data1']==""||dataSwap['data1']==null?"":dataSwap['data1']['nama_lengkap']}", style: TextStyle(color: Color(0xFF4f4f4f), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                          Padding(
                                            padding: const EdgeInsets.only(top:5.0),
                                            child: Text("${dataSwap==null?"":dataSwap['data1']==""||dataSwap['data1']==null?"":dataSwap['user_pairing1']['pairing_id']}.${dataSwap==null?"":dataSwap['user_pairing1']==""||dataSwap['data1']==null?"":dataSwap['user_pairing1']['no_kolom']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 24, fontWeight: FontWeight.bold),),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 40),
                          child: Column(
                            children: [
                              Text("Confirm this ${dataSwap==null?"":dataSwap['user_pairing']==null?"":dataSwap['user_pairing']['pairing_request'] == 'Swap Pairing'?"swap":"same"} pair?", style: TextStyle(fontSize: 18, color: Color(0xFFF15411)),),
                              InkWell(
                                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => PairingGames(id: dataSwap['data_turnamen']['id_turnamen']))),
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 20.0, top: 5),
                                  child: Text("view pairing", style: TextStyle(color: Color(0xFF4f4f4f)),),
                                ),
                              ),
                              dataSwap==null || dataSwap['user_pairing']==null?Container():dataSwap['user_pairing']['pairing_request'] == 'Swap Pairing'?
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        _isLoading = true;
                                        confirmUser("Not Confirmed");
                                      });
                                      // Navigator.push(context, MaterialPageRoute(builder: (context) => CreatedGames()));
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(12),
                                      width: 98,
                                      decoration: BoxDecoration(
                                          color: AppTheme.gRed,
                                          borderRadius: BorderRadius.circular(15)
                                      ),
                                      child: Text("Reject", style: TextStyle(color: AppTheme.white, fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        _isLoading = true;
                                        confirmUser("Confirmed");
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(12),
                                      width: 98,
                                      decoration: BoxDecoration(
                                          color: AppTheme.gButton,
                                          borderRadius: BorderRadius.circular(15)
                                      ),
                                      child: Text("Confirm", style: TextStyle(color: AppTheme.white, fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                    ),
                                  ),
                                ],
                              ) : InkWell(
                                onTap: () {
                                  setState(() {
                                    _isLoading = true;
                                    confirmUser("Confirmed");
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.all(12),
                                  width: 98,
                                  decoration: BoxDecoration(
                                      color: AppTheme.gButton,
                                      borderRadius: BorderRadius.circular(15)
                                  ),
                                  child: Text("Confirm", style: TextStyle(color: AppTheme.white, fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
          ],
        ),
      ),
    );
  }
}
