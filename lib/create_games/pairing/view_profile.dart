import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/play_game.dart';
import 'package:gif/caddie/widget/button_rectangle_rounded_caddie.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/size_config.dart';
import 'package:gif/utils/Utils.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';


class ViewProfilePage extends StatefulWidget {
  const ViewProfilePage({Key key, this.userId, this.forCaddie}) : super(key: key);
  final userId, forCaddie;
  @override
  _ViewProfilePageState createState() => _ViewProfilePageState();
}

class _ViewProfilePageState extends State<ViewProfilePage> {
  SharedPreferences sharedPreferences;
  final List<String> errors = [];
  bool _isLoading = false;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  var dataUser;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }
  final snackbarKey = GlobalKey<ScaffoldState>();

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      _getDataTour();
      if(ratings == null){
        ratings = 3.0;
      }
    }
  }

  _getDataTour() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'id_user' : widget.userId,
    };

    Utils.postAPI(data, "get_profil").then((body) {
      if(body['status'] == 'success'){
        dataUser = body;
        setState(() {
          _isLoading = false;
        });
      }else{
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  double ratings;
  TextEditingController ratingMsg = TextEditingController();
  bool _validate = false;
  bool _isLoadingFeedbackButton = false;

  void sendFeedback() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent': widget.forCaddie['id_represent'],
      'bintang' : ratings,
      'feedback' : ratingMsg.text
    };

    Utils.postAPI(data, "entry_feedback").then((body) {
      if(body['status'] == 'success'){
        Utils.showToast(context, "success", "${body['message']}");
        setState(() {
          _isLoadingFeedbackButton = false;
          // _isLoading = false;
        });
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => HomePage(navbar: true,)));
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoadingFeedbackButton = false;
          // _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingFeedbackButton = false;
        // _isLoading = false;
      });
    });
  }

  TextEditingController reason = TextEditingController();

  void sendCancelRepresent() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_represent': widget.forCaddie['id_represent'],
      'alasan' : reason.text
    };

    Utils.postAPI(data, "tombol_cancel_represent").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          // _isLoading = false;
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage(navbar: true,)));
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          // _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }
  bool _formCancel = false;


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          child: Column(
            children: [
              widget.forCaddie != null && widget.forCaddie['turnamen_selesai'] == true ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Divider(thickness: 3, color: Colors.grey, indent: 100, endIndent: 100),
                  ),
                  Text("Give Feedback", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 24),),
                  RatingBar.builder(
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        ratings = rating;
                      });
                      print(rating);
                    },
                  ),
                  Text("Rating: $ratings", style: TextStyle(fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),),
                  SizedBox(height: 8,),
                  SizedBox(
                    width: 250,
                    child: TextField(
                      maxLines: 3,
                      controller: ratingMsg,
                      onChanged: (_){
                        setState(() {
                          _validate = false;
                        });
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter your feedback message.',
                        errorBorder:  _validate ? OutlineInputBorder(borderSide: BorderSide(
                            color: Colors.red
                        )) : null,
                        errorText: _validate ? 'Please fill the form' : null,
                      ),
                    ),
                  ),
                  SizedBox(height: 5,),
                  ButtonRectangleRoundedCaddie(textButton: "Submit", link: () {
                    if(ratingMsg.text.isEmpty){
                      setState(() {
                        _validate = true;
                      });
                    }else {
                      setState(() {
                        _isLoadingFeedbackButton = true;
                        sendFeedback();
                      });
                    }
                  }, loading: _isLoadingFeedbackButton,)
                ],
              ) : AnimatedCrossFade(
                firstChild: SizedBox(height: 40,),
                secondChild: Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Center(
                    child: Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 12, left: 12, right: 12),
                          width: 250,
                          child: TextField(
                            maxLines: 6,
                            controller: reason,
                            onChanged: (_){
                              setState(() {
                                _validate = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Enter your reason.',
                              errorBorder:  _validate ? OutlineInputBorder(borderSide: BorderSide(
                                  color: Colors.red
                              )) : null,
                              errorText: _validate ? 'Please fill the form' : null,
                            ),
                          ),
                        ),
                        Positioned(top: 0, left: 0, child: InkWell(
                          onTap: (){
                            setState(() {
                              _formCancel = false;
                            });
                          },
                          child: Container(padding: EdgeInsets.all(2),
                              margin: EdgeInsets.only(right: 8),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppTheme.gRed
                              ),
                              child: Icon(Icons.close_rounded, color: Colors.white,)),
                        ))
                      ],
                    ),
                  ),
                ),
                duration: Duration(milliseconds: 500),
                crossFadeState: _formCancel ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                sizeCurve: Curves.fastOutSlowIn,
              ),
              widget.forCaddie != null && widget.forCaddie['turnamen_selesai'] != true ? ButtonRectangleRoundedCaddie(textButton: "Cancel Appointment", link: (){
                setState(() {
                  _formCancel = true;
                  if(reason.text.isNotEmpty){
                    showDialog(
                        context: snackbarKey.currentContext,
                        builder: (BuildContext context) {
                          return AlertDialogCancelRepresent(role: "caddie", function: () {
                            sendCancelRepresent();
                          },);
                        }
                    );
                  }
                });
              }, color: AppTheme.bogies,) : Container(),
              SizedBox(height: 18,)
            ],
          ),
        ),
      );
  }

  Widget columnProfile(BuildContext context) {
    return Container(
      // padding: EdgeInsets.only(top:getProportionateScreenHeight(40)),
      child: Column(
        children: [
          dataUser == null ? Container() : dataUser['data']['foto'] != null ? CachedNetworkImage(
            imageUrl:
            Utils.url + 'upload/user/' + dataUser['data']['foto'],
            imageBuilder: (context, imageProvider) => Container(
              alignment: Alignment.center,
              // margin: EdgeInsets.only(top: 10),
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                image: DecorationImage(
                    image: imageProvider, fit: BoxFit.cover),
                shape: BoxShape.circle,
              ),
            ),
            placeholder: (context, url) => Container(
              alignment: Alignment.center,
              // margin: EdgeInsets.only(top: 10),
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                shape: BoxShape.circle,
              ),
              child: CircularProgressIndicator(),
            ),
            errorWidget: (context, url, error) => Container(
              alignment: Alignment.center,
              // margin: EdgeInsets.only(top: 10),
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                image: DecorationImage(
                  image: AssetImage("assets/images/userImage.png"),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ) : Container(
            alignment: Alignment.center,
            // margin: EdgeInsets.only(top: 10),
            height: 120,
            width: 120,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white),
              image: DecorationImage(
                image: AssetImage("assets/images/userImage.png"),
                fit: BoxFit.cover,
              ),
              shape: BoxShape.circle,
            ),
          ),
          indexProfile()
        ],
      ),
    );
  }

  Widget indexProfile() {
    return IndexProfileGolfer(dataUser: dataUser);
  }
}

class IndexProfileGolfer extends StatelessWidget {
  const IndexProfileGolfer({
    Key key,
    @required this.dataUser,
  }) : super(key: key);

  final dataUser;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          alignment: Alignment.center,
          child: Text(
            "${dataUser == null ? "" : dataUser['data']['nama_lengkap']}",
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 24),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15),
          alignment: Alignment.center,
          child: Text(
            "Home Golf Course",
          ),
        ),
        SizedBox(height: 5,),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "${dataUser == null ? "" : dataUser['data']['nama_lapangan']}\n${dataUser == null ? "" : dataUser['data']['kota']}, ${dataUser == null ? "" : dataUser['data']['negara']}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 5,),
        InkWell(
          // onTap: () => Navigator.push(this.context, MaterialPageRoute(builder: (context) => ReworkProfilePage())),
          child: Text(
            "GIF ID: ${dataUser == null ? "" : dataUser['data']['kode_user']}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gButton),
            textAlign: TextAlign.center,
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.symmetric(vertical: 4.0),
        //   child: Text("${dataUser == null ? "" : dataUser['data']['email']}\n ${dataUser == null ? "" : dataUser['data']['no_telp']}", textAlign: TextAlign.center, style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack),),
        // ),
      ],
    );
  }
}

class RowBackground extends StatelessWidget {
  const RowBackground({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: InkWell(onTap: () => Navigator.pop(context), child: Icon(Icons.arrow_back_ios_rounded, color: Colors.white,)),
        ),
        Container(
          padding: EdgeInsets.all(18),
          child: Image.asset(
            'assets/images/gif_logo.png',
            isAntiAlias: true,
            width: 65,
            height: 65,
          ),
        )
      ],
    );
  }
}
