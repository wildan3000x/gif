import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/create_games/created_games.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'headerCreate.dart';

class CreateGames extends StatefulWidget {
  CreateGames({Key key, this.roleCaddie, this.tokenCaddie, this.tokenGolfer, this.dataCaddie}) : super (key: key);
  final roleCaddie, tokenGolfer, tokenCaddie, dataCaddie;
  @override
  _CreateGamesState createState() => _CreateGamesState();
}

class _CreateGamesState extends State<CreateGames> {
  final _formKey = GlobalKey<FormState>();


  String _valCountry;
  String _valCity;
  String _valCourse;

  DateTime _date = DateTime.now();
  DateTime _dateDeadline = DateTime.now();
  TimeOfDay time;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    getCountry();
    // getProvince();
    CourseService.course.clear();
    super.initState();
    _getToken();
    time = TimeOfDay.now();
    if(widget.roleCaddie == true){
      tourCountry.text = widget.dataCaddie['negara'];
      tourCity.text = widget.dataCaddie['kota'];
      tourCourse.text = widget.dataCaddie['nama_lapangan'];
      courseId = widget.dataCaddie['id_lapangan'];
      getCourse();
    }
  }

  List<bool> _checksLoad = [false, false];

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  var myFormat = DateFormat('EEEE d MMM yyyy');
  String dateString = "";

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(picked);

    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
        dateString = formattedDate;
        // print(formattedDate);
      });
    }
  }

  Future<Null> selectDateDeadline(BuildContext context) async {
    final DateTime picked2 = await showDatePicker(
      context: context,
      initialDate: _dateDeadline,
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );

    if (picked2 != null && picked2 != _dateDeadline) {
      setState(() {
        _dateDeadline = picked2;
      });
    }
  }

  _pickTime() async {
    TimeOfDay t = await showTimePicker(context: context, initialTime: time);
    if (t != null) {
      setState(() {
        time = t;
        // print(time);
      });
    }
  }

  bool _loading = false;
  var token;

  void _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tokenJson = localStorage.getString('usertoken');
    var datatoken = json.decode(tokenJson);
    print(datatoken);
    setState(() {
      token = widget.roleCaddie == true ? widget.tokenGolfer : datatoken;
      getPenyelenggara();
    });
  }

  TextEditingController tourName = TextEditingController();
  TextEditingController tourOrga = TextEditingController();
  TextEditingController tourCountry = TextEditingController();
  TextEditingController tourCity = TextEditingController();
  TextEditingController tourCourse = TextEditingController();
  int courseId;

  FocusNode tourNameNode = FocusNode();
  FocusNode tourOrgaNode = FocusNode();
  FocusNode tourCityNode = FocusNode();
  FocusNode tourCourseNode = FocusNode();

  final snackbarKey = GlobalKey<ScaffoldState>();

  void getProvince() async {
    var data = {};

    Utils.postAPI(data, "get_kota").then((body){
      if(body['status'] == 'success'){
        try {
          List<dynamic> lPro = body["data"];
          CitiesService.cities = lPro;
        } catch (e) {
          print(e);
        }
      }else{
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  bool _isLoadingCourse = false;
  List<dynamic> _dataCourse = [];

  void getCourse() async {
    setState(() {
      _isLoadingCourse = true;
    });

    var data = {
      'kota' : tourCountry.text,
    };

    Utils.postAPI(data, "get_lapangan").then((body){
      CourseService.course.clear();
      if(body['status'] == 'success'){
        setState(() {
          _dataCourse = body['data'];
        });
        try{
          // var pro = json.decode(body);
          List<dynamic> lPro = body["data"];
          CourseService.course = lPro;
        }catch(e){
          print(e);
        }
      }else{
        Utils.showToast(context, "error", "${body['message']}");
        print(body['message']);
      }
      setState(() {
        _isLoadingCourse = false;
      });
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoadingCourse = false;
      });
    });
  }

  void getCountry() async {
    var data = {};

    Utils.postAPI(data, "get_negara").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CountryServices.cities = lPro;
          // print('Count: ${body.length}, Datas: ${lPro}');
          setState(() {
            _checksLoad[0] = true;
          });
        } catch (e) {
          print(e);
          getCountry();
        }
      } else {
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var dataOtherClub;
  List<dynamic> _dataClub = List();
  var _valGroup;
  var _valIdClub;
  bool _isLoadingClub= false;

  void getPenyelenggara() async {
    // setState(() {
    //   _isLoadingClub = true;
    // });
    var data = {
      "token": token,
      "pilihan_penyelenggara": _typeOrgaRadio
    };

    Utils.postAPI(data, "pilih_penyelenggara_turnamen").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          tourOrga.text = body['data']['nama_lengkap'];
          dataOtherClub = body['data']['club'];
          _dataClub = dataOtherClub;
          // _isLoadingClub = false;
          _valIdClub = dataOtherClub[0]['club_golf_id'];
          setState(() {
            _checksLoad[1] = true;
          });
        });
      } else {
        print(body['message']);
        setState(() {
          _isLoadingClub = false;
        });
      }
      print(body);
    }, onError: (error) {
      // getPenyelenggara();
      setState(() {
        print("Error == $error");
        _isLoadingClub = false;
      });
    });
  }

  Future<void> _saveTour() async {
    // print(token);
    var data = {
      'tipe': _typeMatchRadio,
      'nama': tourName.text,
      "pilihan_penyelenggara_turnamen": _typeOrgaRadio,
      'penyelenggara': _typeOrgaRadio == "Personal" ? tourOrga.text : _valGroup,
      "club_golf_id": _typeOrgaRadio == "Personal" ? null : _valIdClub,
      'negara':_valCountry,
      'kota': _valCity,
      'lapangan_id': courseId,
      'tanggal':
          DateTime(_date.year, _date.month, _date.day, time.hour, time.minute)
              .toString(),
      "pairing_deadline":DateTime(_dateDeadline.year, _dateDeadline.month, _dateDeadline.day).toString(),
      'partisipan': _invitationRadio,
      'token': token,
      'token_caddie': widget.roleCaddie == true ? widget.tokenCaddie : null
    };

    Utils.postAPI(data, "create_tournament").then((body) async {
      if(body['status'] == 'success'){
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('savetour', json.encode(body['data']['tournament']));
        setState(() {
          _loading = false;
        });
        // print(body);
        Navigator.pushReplacement(
            context, PageRouteBuilder(pageBuilder: (context, animation1, animation2) => CreatedGames()));
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
      // print(_date.toString() + time.toString());
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  String _invitationRadio = "By Invitation";
  String _typeMatchRadio = "Match";
  String _typeOrgaRadio = "Personal";

  void _handleInvitation(String value) {
    setState(() {
      _invitationRadio = value;
    });
  }

  void _handleTypeMatch(String value) {
    setState(() {
      _typeMatchRadio = value;
    });
  }

  void _handleTypeOrg(String value) {
    setState(() {
      _typeOrgaRadio = value;
    });
  }

  String _tourNameErrorText;
  bool _tourNameError = false;
  String _tourOrgaErrorText;
  bool _tourOrgaError = false;
  String _tourCityErrorText;
  bool _tourCityError = false;
  String _tourCountryErrorText;
  bool _tourCountryError = false;
  String _tourCourseErrorText;
  bool _tourCourseError = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _checksLoad.every((element) => element == true) ? false : true,
          child: Stack(
            children: [
              GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    Container(
                      margin: EdgeInsets.all(13),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            HeaderCreate(title: "Create\nNew Tournament", backSummary: true, disable: true,),
                            TitlePage(),
                            formCreate(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  Column formCreate() {
    return Column(
      children: [
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Type",
              style: TextStyle(color: AppTheme.gFont, fontSize: 14),
            )),
        Container(
          height: 40,
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Tournament",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Tournament",
                style: TextStyle(fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Mini Tournament",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Mini Tournament",
                style: TextStyle(fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Match",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Match",
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Name",
              style: TextStyle(color: _tourNameError ? AppTheme.gValidError : AppTheme.gButton),
            ),
        ),
        Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _tourNameError = true;
                _tourNameErrorText = '';
                return '';
              } else {
                // });
                _tourNameError = false;
                return null;
              }
            },
            onChanged: (value) {
              setState(() {
                _tourNameError = false;
                _tourNameErrorText = null; // R
                // print("ttttt $tourName");// esets the error
              });
            },
            onSaved: (value){
              setState(() {
                tourName.text = value;
                // print("ttttt $tourName");
              });
            },
            controller: tourName,
            inputFormatters: [
              LengthLimitingTextInputFormatter(50),
            ],
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              // errorText: _tourNameErrorText,
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),

              errorBorder: UnderlineInputBorder(
                // width: 0.0 produces a thin "hairline" border
                borderSide: BorderSide(color: Color(0xFFDADADA)),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                // width: 0.0 produces a thin "hairline" border
                borderSide: BorderSide(color: Color(0xFFDADADA)),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              // _formKey.currentState.validate();
              FocusScope.of(this.context).requestFocus(tourOrgaNode);
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Organizer",
              style: TextStyle(color: _tourOrgaError ? AppTheme.gValidError : AppTheme.gButton),
            ),
        ),
        Container(
          margin: EdgeInsets.only(top: 6),
          height: 18,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Organization",
                  groupValue: _typeOrgaRadio,
                  onChanged: _handleTypeOrg,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Organization",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Personal",
                  groupValue: _typeOrgaRadio,
                  onChanged: _handleTypeOrg,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Personal",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
            ],
          ),
        ),
        _typeOrgaRadio == "Personal" ? Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _tourOrgaError = true;
                _tourOrgaErrorText = '';
                return '';
              } else {
                // });
                _tourOrgaError = false;
                return null;
              }
              //   if(value.isEmpty) {
              //     _tourOrgaError = true;
              //     _tourOrgaErrorText = 'Please input this field';
              //     return '';
              //   }
              // return null;
            },
            onChanged: (value) {
              setState(() {
                _tourOrgaError = false;
                _tourOrgaErrorText = null; // Resets the error
              });
            },
            focusNode: tourOrgaNode,
            controller: tourOrga,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              // errorText: _tourOrgaErrorText,
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),

              errorBorder: UnderlineInputBorder(
                // width: 0.0 produces a thin "hairline" border
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                // width: 0.0 produces a thin "hairline" border
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              // _formKey.currentState.validate();
              FocusScope.of(this.context).requestFocus(tourCityNode);
            },
          ),
        ) : _isLoadingClub ? Center(
          child: Container(
            margin: EdgeInsets.only(top: 8),
            width: 20,
            height: 20,
            child: CircularProgressIndicator(),
          ),
        ) : Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: _dataClub.isEmpty ? Container(
            width: double.maxFinite,
            height: 40,
            child: Center(child: Text("you are still not in any club.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),)),
          ) : Container(
            padding: EdgeInsets.only(top: 10),
            child: DropdownButton(
              isExpanded: true,
              underline: SizedBox(),
              items: _dataClub.map((value) {
                setState(() {
                  if(_valGroup == null){
                    _valGroup = _dataClub[0]['nama_club'];
                  }
                });
                return DropdownMenuItem(
                  value: value['nama_club'],
                  onTap: (){
                    setState(() {
                      _valIdClub = value['club_golf_id'];
                    });
                    print("ID CLUB $_valIdClub");
                  },
                  child: Text(value['nama_club'], style: TextStyle(fontSize: 18),),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _valGroup = value;
                });
              },
              value: _valGroup,
              hint: Text(
                "${_dataClub[0]['nama_club']}", style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
              // value: _valWinners,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select Country",
              style: TextStyle(color: widget.roleCaddie == true ? Colors.grey : _tourCountryError
                  ? AppTheme.gValidError
                  : AppTheme.gButton,),
            )),
        Container(
          height: 40,
          child: TypeAheadFormField(
            noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No Country Found!")),
            textFieldConfiguration: TextFieldConfiguration(
              controller: this.tourCountry,
              onSubmitted: (_){
                // _formKey.currentState.validate();
              },
              onChanged: (value) {
                setState(() {
                  _tourCountryError = false;
                  _tourCountryErrorText = null; // Resets the error
                });
              },
              enabled: widget.roleCaddie == true ? false : true,
              decoration: InputDecoration(
                errorStyle: TextStyle(height: 0),
                contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                // errorText: _tourCityErrorText,
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFDADADA))),
                errorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Color(0xFFDADADA)),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Color(0xFFDADADA)),
                ),
              ),
            ),
            suggestionsCallback: (String pattern) {
              return CountryServices.getSuggestions(pattern);
            },
            itemBuilder: (context, suggestion) {
              return ListTile(
                title: Text(suggestion['nama']),
              );
            },
            transitionBuilder: (context, suggestionsBox, controller) {
              return suggestionsBox;
            },
            onSuggestionSelected: (suggestion) {
              this.tourCountry.text = suggestion["nama"];
              setState(() {
                _tourCountryError = false;
                // tourCourse.text = "";
                getCourse();
              });
            },
            validator: (value) {
              if (value.isEmpty) {
                _tourCountryError = true;
                _tourCountryErrorText = '';
                return '';
              } else {
                // });
                _tourCountryError = false;
                return null;
              }
            },
            onSaved: (value) => this._valCountry = value,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select Golf Course",
              style: TextStyle(color:widget.roleCaddie == true ? Colors.grey : _tourCourseError
                  ? AppTheme.gValidError
                  : AppTheme.gButton,),
            )),
        SearchableDropdown.single(
          items: _dataCourse.map((data) {
            return (DropdownMenuItem(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${data['nama_lapangan']}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                  Text("${data['nama']}")
                ],
              ),
              value: data,
            ));
          }).toList(),
          // value: selectedItem,
          onChanged: (value) {
            if(value != null){
              _valCity = value['nama'];
              courseId = value['id_lapangan'];
              setState(() {
                _tourCourseError = false;
              });
            }
          },
          dialogBox: true,
          isExpanded: true,
          displayClearIcon: false,
          onClear: () {},
          underline: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xFFDADADA))
                )
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(
              width: 1,
              color: AppTheme.gGrey,
            ),
          )),
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Select Date & Tee Off Time",
                    style: TextStyle(color: AppTheme.gFont),
                  )),
              Container(
                height: 40,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlatButton(
                      child: Text(
                        '${myFormat.format(_date)}',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.left,
                      ),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        selectDate(context);
                      },
                    ),
                    FlatButton(
                      child: Text('${time.hour}:${time.minute<10?"0"+time.minute.toString():time.minute}',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.normal)),
                      onPressed: _pickTime,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: AppTheme.gGrey,
                ),
              )),
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Deadline Pairing",
                    style: TextStyle(color: AppTheme.gFont),
                  )),
              Container(
                height: 40,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    FlatButton(
                      child: Text(
                        '${myFormat.format(_dateDeadline)}',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.left,
                      ),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        selectDateDeadline(context);
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Participant",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          height: 40,
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Open",
                  groupValue: _invitationRadio,
                  onChanged: _handleInvitation,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Open",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "By Invitation",
                  groupValue: _invitationRadio,
                  onChanged: _handleInvitation,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "By Invitation",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
            splashColor: Colors.grey.withOpacity(0.5),
            onPressed: () {
              if(!_loading){
                print("VALIDATE ${_formKey.currentState.validate()}");
                if (_formKey.currentState.validate() && _valCity != null && courseId != null) {
                  _formKey.currentState.save();
                  if(_dataClub.isEmpty && _typeOrgaRadio == "Organization"){
                    setState(() {
                      _tourOrgaError = true;
                    });
                  }else {
                    setState(() {
                      _saveTour();
                      setState(() {
                        _loading = true;
                      });
                    });
                  }
                }else if(!_formKey.currentState.validate() && _valCity != null && courseId != null){
                  setState(() {
                    _tourCourseError = false;
                  });
                }else{
                  setState(() {
                    _tourCourseError = true;
                    _tourCourseErrorText = '';
                  });
                }
              }
            },
            padding: const EdgeInsets.all(0.0),
            child: Ink(
                decoration: BoxDecoration(
                    color: AppTheme.gButton,
                    borderRadius: BorderRadius.circular(100)
                ),
                child: Container(
                    constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                    alignment: Alignment.center,
                    child: _loading
                        ? Center(
                        child: Container(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(),
                        ))
                        : Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
          ),
        )
      ],
    );
  }
}

class TitlePage extends StatelessWidget {
  const TitlePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "Tournament Detail",
          style: TextStyle(
            color: AppTheme.gFont,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
