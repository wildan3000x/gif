import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import '../main.dart';

class HeaderCreate extends StatefulWidget {
  HeaderCreate({
    Key key, @required this.title, this.backSummary, this.disable, this.disableBack
  }) : super(key: key);

  final title, backSummary, disable, disableBack;

  @override
  _HeaderCreateState createState() => _HeaderCreateState();
}

class _HeaderCreateState extends State<HeaderCreate> {
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    getSharedPreferences();
    // print("DISABLE BACK ${widget.disableBack}");
    super.initState();
  }

  var level;

  getSharedPreferences() async {
    sharedPreferences = await SharedPreferences.getInstance();
    level = sharedPreferences.getString("level");
    // print("LEVEL ${sharedPreferences.getString("level")}");
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 70,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppTheme.gBlue,
                AppTheme.gBlueOcean,
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      widget.disableBack == true ? Container() : Padding(
                        padding: const EdgeInsets.only(right: 3.0),
                        child: InkWell(splashColor: Colors.grey.withOpacity(0.3), onTap: () => Navigator.pop(context), child: Image.asset("assets/images/arrow_left.png", isAntiAlias: true, width: 23, height: 23,)),
                      ),
                      Text(
                        widget.title,
                        style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  return widget.disable == true ? null : showDialog(
                    context: context,
                    builder: (context) => new CustomAlertDialog(
                      title: 'Are you sure?',
                      contentText: " want to quit to the main menu?",
                      noButton: () => Navigator.of(context).pop(false),
                      yesButton: () {
                        if(level == "Caddie"){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => HomeCaddie()));
                        }else {
                          Navigator.pushAndRemoveUntil(context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HomePage(navbar: true,)), (
                                  Route<dynamic> route) => false);
                        }
                      },
                    )
                    // AlertDialog(
                    //   title: new Text('Are you sure want quit to main menu?', style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16),),actions: <Widget>[
                    //     RaisedButton(
                    //         child: Text('No'),
                    //         onPressed: () => Navigator.of(context).pop(false)),
                    //     RaisedButton(
                    //       color: AppTheme.gRed,
                    //       child: Text('Yes'),
                    //       onPressed: () {
                    //         if(level == "Caddie"){
                    //           Navigator.push(context, MaterialPageRoute(builder: (context) => HomeCaddie()));
                    //         }else
                    //           {
                    //             Navigator.pushAndRemoveUntil(context,
                    //                 MaterialPageRoute(
                    //                     builder: (BuildContext context) =>
                    //                         HomePage(navbar: true,)), (
                    //                     Route<dynamic> route) => false);
                    //           }
                    //       }),
                    //   ],
                    // ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Image.asset("assets/images/gif_logo.png", width: 60, height: 60,),
                ),
              ),
            ],
          ),
        ),
        widget.backSummary == null ? Column(
          children: [
            SizedBox(height: 3,),
            Text("click the GIF logo to return to the main menu", style: TextStyle(fontSize: 12, color: Color(0xFF828282)),)
          ],
        ) : Container()
      ],
    );
  }
}
