import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/create_games/created_games.dart';
import 'package:gif/create_games/set_winning.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/create_games/winning/set_winning_tournament.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'headerCreate.dart';

class EditGames extends StatefulWidget {
  const EditGames({Key key, this.editFromSummary, this.dataTour, this.viewFromNextEventClub}) : super(key: key);
  final editFromSummary, dataTour, viewFromNextEventClub;
  @override
  _EditGamesState createState() => _EditGamesState();
}

class _EditGamesState extends State<EditGames> {
  var myFormat = DateFormat('EEEE d MMM yyyy');
  var formatTime = DateFormat('h:mm');
  final _formKey = GlobalKey<FormState>();

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );
    var formatter = new DateFormat('yyyy-MM-dd');

    if (picked != null) {
      String formattedDate = formatter.format(picked);
      setState(() {
        dateController.text = myFormat.format(DateTime.parse(formattedDate));
      });
    }
  }

  Future<Null> selectDateDeadline(BuildContext context) async {
    final DateTime picked2 = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );
    var formatter = new DateFormat('yyyy-MM-dd');

    if (picked2 != null) {
      String formattedDate = formatter.format(picked2);
      setState(() {
        dateDeadlineController.text = myFormat.format(DateTime.parse(formattedDate));
      });
    }
  }

  bool _loading = false;

  TextEditingController tourName = TextEditingController();
  TextEditingController tourOrga = TextEditingController();
  TextEditingController tourCountry = TextEditingController();
  TextEditingController tourCity = TextEditingController();
  TextEditingController tourCourse = TextEditingController();
  TextEditingController dateController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController dateDeadlineController = TextEditingController();
  int courseId;

  FocusNode tourNameNode = FocusNode();
  FocusNode tourOrgaNode = FocusNode();
  FocusNode tourCountryNode = FocusNode();
  FocusNode tourCityNode = FocusNode();
  FocusNode tourCourseNode = FocusNode();

  var dataTour;

  String _invitationRadio;
  String _typeMatchRadio;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    getCountry();
    // getProvince();
    _getDataTour();
    _getToken();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  void getCountry() async {
    var data = {};

    Utils.postAPI(data, "get_negara").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CountryServices.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
          setState(() {
            _checks[1] = true;
          });
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void getProvince() async {
    var data = {};

    Utils.postAPI(data, "get_kota").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CitiesService.cities = lPro;
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  List<dynamic> _dataCourse = [];
  void getCourse() async {
    var data = {
      'kota': tourCity.text,
    };

    Utils.postAPI(data, "get_lapangan").then((body) {
      CourseService.course.clear();
      if (body['status'] == 'success') {
        setState(() {
          _dataCourse = body['data'];
          _checks[2] = true;
        });
      } else {
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var token;
  var level;

  void _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tokenJson = localStorage.getString('usertoken');
    var datatoken = json.decode(tokenJson);
    print("USER ${localStorage.getString('level')}");
    setState(() {
      token = datatoken;
      level = localStorage.getString('level');
      getPenyelenggara();
    });
  }

  void _getDataTour() async {
    // SharedPreferences localStorage = await SharedPreferences.getInstance();
    // var dataJson = localStorage.getString('savetour');
    // var data = json.decode(dataJson);
    setState(() {
      print("CLUB ID ${widget.dataTour['club_golf_id']}");
      dataTour = widget.dataTour;
      tourName.text = dataTour['nama_turnamen'];
      _typeOrgaRadio = dataTour['pilihan_penyelenggara_turnamen'];
      if(dataTour['pilihan_penyelenggara_turnamen'] == 'Personal'){
        tourOrga.text = dataTour['penyelenggara_turnamen'];
      }else{
        _valIdClub = dataTour['club_golf_id'];
        _valGroup = dataTour['penyelenggara_turnamen'];
      }
      tourCountry.text = dataTour['negara_turnamen'];
      tourCity.text = dataTour['kota_turnamen'];
      tourCourse.text = dataTour['nama_lapangan'];
      courseId = dataTour['id_lapangan'];
      _invitationRadio = dataTour['partisipan_turnamen'];
      _typeMatchRadio = dataTour['tipe_turnamen'];
      dateController.text = dataTour != null
          ? myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))
          : myFormat.format(DateTime.now());
      timeController.text = dataTour == null
          ? DateFormat('HH:ii').format(DateTime.now())
          : DateFormat.Hm().format(DateTime.parse(dataTour['tanggal_turnamen']));
      dateDeadlineController.text = dataTour != null
          ? myFormat.format(DateTime.parse(dataTour['pairing_deadline']))
          : myFormat.format(DateTime.now());
      getCourse();
    });
  }

  _pickTime() async {
    TimeOfDay t = await showTimePicker(context: context, initialTime: dataTour != null
        ? TimeOfDay.fromDateTime(DateTime.parse(dataTour['tanggal_turnamen']))
        : TimeOfDay.now());
    final localizations = MaterialLocalizations.of(context);
    if (t != null) {
      final formattedTimeOfDay = localizations.formatTimeOfDay(t, alwaysUse24HourFormat: true);
      print("TIME ${localizations.formatTimeOfDay(t, alwaysUse24HourFormat: true)}");
      setState(() {
        timeController.text = "$formattedTimeOfDay";
      });
    }
  }

  final snackbarKey = GlobalKey<ScaffoldState>();

  var dataOtherClub;
  List<dynamic> _dataClub = List();
  var _valGroup;
  var _valIdClub;
  bool _isLoadingClub= false;

  void getPenyelenggara() async {
    setState(() {
      _isLoadingClub = true;
    });
    var data = {
      "token": token,
      "pilihan_penyelenggara": _typeOrgaRadio
    };

    Utils.postAPI(data, "pilih_penyelenggara_turnamen").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          tourOrga.text = body['data']['nama_lengkap'];
          dataOtherClub = body['data']['club'];
          _dataClub = dataOtherClub;
          _isLoadingClub = false;
          _checks[3] = true;
          // _valIdClub = dataOtherClub[0]['club_golf_id'];
        });
      } else {
        print(body['message']);
        setState(() {
          _isLoadingClub = false;
          _checks[3] = true;
        });
      }
      print(body);
    }, onError: (error) {
      getPenyelenggara();
      setState(() {
        print("Error == $error");
        _isLoadingClub = false;
      });
    });
  }

  Future<void> _saveTour() async {
    var data = {
      'id': dataTour['id_turnamen'],
      'tipe': _typeMatchRadio,
      'nama': tourName.text,
      "pilihan_penyelenggara_turnamen": _typeOrgaRadio,
      'penyelenggara': _typeOrgaRadio == "Personal" ? tourOrga.text : _valGroup,
      "club_golf_id": _typeOrgaRadio == "Personal" ? null : _valIdClub,
      'negara': tourCountry.text,
      'kota': tourCity.text,
      'lapangan_id': courseId,
      'tanggal':
          "${DateFormat('yyyy-MM-dd').format(myFormat.parse(dateController.text))} ${timeController.text}:00",
      'pairing_deadline' : "${DateFormat('yyyy-MM-dd').format(myFormat.parse(dateDeadlineController.text))}",
      'partisipan': _invitationRadio,
      'token': token
    };

    Utils.postAPI(data, "edit_tournament").then((body) async {
      if (body['status'] == 'success') {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.remove('savetour');
        setState(() {
          localStorage.setString('savetour', json.encode(body['data']));
          _loading = false;
          // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CreatedGames()));
        });
        print("COURSE 1 ${dataTour['nama_lapangan']}, COURSE 2 ${tourCourse.text}");
        if(widget.editFromSummary == true){
          if(dataTour['tipe_turnamen'] != _typeMatchRadio || (dataTour['nama_lapangan'] != tourCourse.text /*&& _typeMatchRadio == 'Tournament'*/)) {
            Navigator.pop(context, "show_dialog_set_winning_warning");
            // if (_typeMatchRadio == 'Match') {
            //   Navigator.push(context, MaterialPageRoute(builder: (context) =>
            //       SetWinning(disable: true,
            //         dataTour: body['data'],
            //         template: body['data']['tipe_turnamen'],
            //         editFromSummary: true,)));
            // } else {


            //   Navigator.push(context, MaterialPageRoute(builder: (context) =>
            //       SetWinningTournament(dataTour: body['data'],
            //         isEdit: false,
            //         disable: true,
            //         template: body['data']['tipe_turnamen'],
            //         editFromSummary: true,
            //         viewFromNextEventClub: widget.viewFromNextEventClub,
            //       )));


            // }
          }else {
            // if(widget.viewFromNextEventClub == null){
            //   Navigator.pushReplacement(context, MaterialPageRoute(
            //       builder: (BuildContext context) =>
            //           SummaryGames(tourId: dataTour['id_turnamen'], viewFromNextEventsClub: widget.viewFromNextEventClub,)));
            // }else{
              Navigator.pop(context);
            // }
          }
        }else {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => CreatedGames()));
        }
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void _handleInvitation(String value) {
    setState(() {
      _invitationRadio = value;
    });
  }

  void _handleTypeMatch(String value) {
    setState(() {
      _typeMatchRadio = value;
    });
  }

  String _typeOrgaRadio = "Personal";

  void _handleTypeOrg(String value) {
    setState(() {
      _typeOrgaRadio = value;
    });
  }

  String _tourNameErrorText;
  bool _tourNameError = false;
  String _tourOrgaErrorText;
  bool _tourOrgaError = false;
  String _tourCountryErrorText;
  bool _tourCountryError = false;
  String _tourCityErrorText;
  bool _tourCityError = false;
  String _tourCourseErrorText;
  bool _tourCourseError = false;

  List<bool> _checks = [true, false, false, false];
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, "0");
        return true;
      },
      child: Scaffold(
        key: snackbarKey,
        body: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _checks.every((element) => element == true) ? false : true,
            child: Stack(
              children: [
                GestureDetector(
                  onTap: () => FocusScope.of(context).unfocus(),
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                      Container(
                        margin: EdgeInsets.all(13),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              HeaderCreate(title: "Edit Game",),
                              TitlePage(),
                              formCreate(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Column formCreate() {
    return Column(
      children: [
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Type",
              style: TextStyle(color: AppTheme.gFont, fontSize: 14),
            )),
        Container(
          height: 40,
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Tournament",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Tournament",
                style: TextStyle(fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Mini Tournament",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Mini Tournament",
                style: TextStyle(fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Match",
                  groupValue: _typeMatchRadio,
                  onChanged: _handleTypeMatch,
                ),
              ),
              Text(
                "Match",
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Name",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
                if(value.isEmpty) {
                  _tourNameError = true;
                  _tourNameErrorText = 'Please input this field';
                  return "Please input this field";
                }
              return null;
            },
            onChanged: (value) {
              setState(() {
                _tourNameError = false;
                _tourNameErrorText = null; // Resets the error
              });
            },
            controller: tourName,
            inputFormatters: [
              LengthLimitingTextInputFormatter(50),
            ],
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              errorText: _tourNameErrorText,
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              // _formKey.currentState.validate();
              FocusScope.of(this.context).requestFocus(tourOrgaNode);
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Organizer",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          margin: EdgeInsets.only(top: 6),
          height: 18,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Organization",
                  groupValue: _typeOrgaRadio,
                  onChanged: _handleTypeOrg,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Organization",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Personal",
                  groupValue: _typeOrgaRadio,
                  onChanged: _handleTypeOrg,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Personal",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
            ],
          ),
        ),
        _typeOrgaRadio == "Personal" ? Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
                if(value.isEmpty) {
                  _tourOrgaError = true;
                  _tourOrgaErrorText = 'Please input this field';
                  return 'Please input this field';
                }
              return null;
            },
            onChanged: (value) {
              setState(() {
                _tourOrgaError = false;
                _tourOrgaErrorText = null; // Resets the error
              });
            },
            controller: tourOrga,
            decoration: InputDecoration(
              errorText: _tourOrgaErrorText,
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              // _formKey.currentState.validate();
              FocusScope.of(this.context).requestFocus(tourCityNode);
            },
          ),
        ) : Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: _dataClub.isEmpty ? Container(
            width: double.maxFinite,
            height: 40,
            child: Center(child: Text("you are still not in any club.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),)),
          ) : Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: DropdownButton(
              isExpanded: true,
              underline: SizedBox(),
              items: _dataClub.map((value) {
                setState(() {
                  if(_valGroup == null){
                    _valGroup = _dataClub[0]['nama_club'];
                  }
                });
                return DropdownMenuItem(
                  value: value['nama_club'],
                  onTap: (){
                    setState(() {
                      _valIdClub = value['club_golf_id'];
                    });
                    print("ID CLUB $_valIdClub");
                  },
                  child: Text(value['nama_club'], style: TextStyle(fontSize: 18),),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _valGroup = value;
                });
              },
              value: _valGroup,
              hint: Text(
                "${dataTour['penyelenggara_turnamen']}", style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
              // value: _valWinners,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select Country",
              style: TextStyle(color: level == 'Caddie' ? Colors.grey : AppTheme.gFont),
            )),
        Container(
          height: 40,
          child: TypeAheadFormField(
            noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No Country Found!")),
            textFieldConfiguration: TextFieldConfiguration(
              controller: this.tourCountry,
              onSubmitted: (_){
                // _formKey.currentState.validate();
              },
              onChanged: (value) {
                setState(() {
                  _tourCountryError = false;
                  _tourCountryErrorText = null; // Resets the error
                });
              },
              enabled: level == 'Caddie' ? false : true,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                errorText: _tourCountryErrorText,
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFDADADA))),
              ),
            ),
            suggestionsCallback: (String pattern) {
              return CountryServices.getSuggestions(pattern);
            },
            itemBuilder: (context, suggestion) {
              return ListTile(
                title: Text(suggestion['nama']),
              );
            },
            transitionBuilder: (context, suggestionsBox, controller) {
              return suggestionsBox;
            },
            onSuggestionSelected: (suggestion) {
              this.tourCountry.text = suggestion["nama"];
              setState(() {
                // tourCourse.text = "";
                getCourse();
              });
            },
            validator: (value) {
              if(value.isEmpty) {
                _tourCountryError = true;
                _tourCountryErrorText = 'Please select a country';
                return 'Please select a country';
              }
              return null;
            },
            // onSaved: (value) => this._valCountry = value,
          ),
        ),
        // Padding
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select Golf Course",
              style: TextStyle(color: level == 'Caddie' ? Colors.grey : AppTheme.gFont),
            )),
        SearchableDropdown.single(
          items: _dataCourse.map((data) {
            return (DropdownMenuItem(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${data['nama_lapangan']}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                  Text("${data['nama']}")
                ],
              ),
              value: data,
            ));
          }).toList(),
          hint: Padding(
            padding: const EdgeInsets.only(bottom: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${tourCourse.text}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                Text("${tourCity.text}")
              ],
            ),
          ),
          underline: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xFFDADADA))
                )
            ),
          ),
          assertUniqueValue: true,
          onChanged: (value) {
            if(value != null){
              tourCity.text = value['nama'];
              courseId = value['id_lapangan'];
              tourCourse.text = value['nama_lapangan'];
              // homeGolf.text = value['nama_lapangan'];
            }
          },
          dialogBox: true,
          isExpanded: true,
          displayClearIcon: false,
          onClear: () {},
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(
              width: 1,
              color: AppTheme.gGrey,
            ),
          )),
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Select Date & Tee Off Time",
                    style: TextStyle(color: AppTheme.gFont),
                  )),
              Container(
                height: 40,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        flex: 2,
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "Date",
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppTheme.gGrey))),
                          enableInteractiveSelection: false,
                          onTap: () {
                            // Below line stops keyboard from appearing
                            FocusScope.of(context).requestFocus(new FocusNode());
                            selectDate(context);
                            // Show Date Picker Here
                          },
                          onChanged: (value){

                          },
                          controller: dateController,
                        ),
                    ),
                    Flexible(
                        flex: 1,
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "Time",
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppTheme.gGrey))),
                          enableInteractiveSelection: false,
                          onTap: () {
                            // Below line stops keyboard from appearing
                            FocusScope.of(context).unfocus();
                            _pickTime();
                            // Show Date Picker Here
                          },
                          controller: timeController,
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: AppTheme.gGrey,
                ),
              )),
          child: Column(
            children: [
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Deadline Pairing",
                    style: TextStyle(color: AppTheme.gFont),
                  )),
              Container(
                height: 40,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        flex: 2,
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "Deadline",
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppTheme.gGrey))),
                          enableInteractiveSelection: false,
                          onTap: () {
                            // Below line stops keyboard from appearing
                            FocusScope.of(context).unfocus();
                            selectDateDeadline(context);
                            // Show Date Picker Here
                          },
                          controller: dateDeadlineController,
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tournament Participant",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          height: 40,
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 15,
                child: Radio(
                  value: "Open",
                  groupValue: _invitationRadio,
                  onChanged: _handleInvitation,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "Open",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30),
              ),
              SizedBox(
                width: 15,
                child: Radio(
                  value: "By Invitation",
                  groupValue: _invitationRadio,
                  onChanged: _handleInvitation,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
              ),
              Text(
                "By Invitation",
                style: TextStyle(fontSize: 14, color: Color(0xFF837B7B)),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  Navigator.pop(context, "0");
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  if(!_loading){
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      if(_dataClub.isEmpty && _typeOrgaRadio == "Organization"){
                        setState(() {
                          _tourOrgaError = true;
                        });
                      }else {
                        setState(() {
                          _saveTour();
                          _loading = true;
                        });
                      }
                    }
                  }
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: _loading
                            ? Center(
                            child: Container(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(),
                            ))
                            : Text("Edit", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class TitlePage extends StatelessWidget {
  const TitlePage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "Tournament Detail",
          style: TextStyle(
            color: AppTheme.gFont,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
