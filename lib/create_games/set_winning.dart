import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/invitation/invite_golfers.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/create_games/winning/set_winning_tournament.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io' show InternetAddress, Platform, SocketException;
import 'package:connectivity/connectivity.dart';

import '../app_theme.dart';
import '../main.dart';
import 'created_games.dart';

class SetWinning extends StatefulWidget {
  const SetWinning({Key key, this.template, this.disable, this.editFromSummary, this.data, this.dataTour}) : super(key: key);

  final template;
  final data;
  final disable;
  final editFromSummary;
  final dataTour;

  @override
  _SetWinningState createState() => _SetWinningState();
}

class _SetWinningState extends State<SetWinning> {
  // String _valWinners = "1";
  // String _valTemplate;
  // List _listWinners = ["1", "2", "3"];
  // String _valOverAll = "1";
  // var valChecksOverAllCer = [false, false, false], valChecksOverAllTro = [false, false, false];
  // var _arrTextBestMettOverall = ["null", "null", "null"];
  final snackbarKey = GlobalKey<ScaffoldState>();

  String _valTemplate;
  String _valWinners = "1";
  String _valWinners10 = "10";
  String _valWinners20 = "20";
  String _valWinnersGross = "1";
  String _valFlight = "1";
  String _valOverAll = "1";
  String _valNearsToTheLin = "3";
  String _valLongesDrive = "3";
  List<String> _locations = ['1', '2', '3'];
  //hole in one (hole, prize)
  List<String> _arrValHoleInOne = List<String>();
  List<String> _arrTextHoleInOne = List<String>();
  //nearest to the line (hole, prize)
  List<String> _arrValNearsToThePin = List<String>();
  List<String> _arrTextNearPin = List<String>();

  var _arrValWinners = List<String>();
  var _arrMinRange = ["1", "10", ""];
  var _arrMaxRange = ["", "20", "1"];
  var _arrTextBestMettOverall = ["null", "null", "null"];
  var _arrTextBestGros = ["null", "null", "null"];
  var _arrTittleFlight = ["Flight A", "Flight B", "Flight C"];
  var _arrColorFlight = [Color(0xFF2D9CDB), AppTheme.gFont, Color(0xFFF3BF37)];

//  var jumWinner = _listWinners;

  //checkbox hole
  List<bool> valCheckHoleOneCer = List<bool>();
  List<bool> valCheckHoleOneTro = List<bool>();
  //checkbox near pin
  List<bool> valChecksNearPinCer = List<bool>();
  List<bool> valChecksNearPinTro = List<bool>();

  var mapBestNetCer = Map<String, dynamic>();
  var mapBestNetTro = Map<String, dynamic>();
  var mapBestNetPri = Map<String, dynamic>();
  var valChecksOverAllCer = [false, false, false], valChecksOverAllTro = [false, false, false];
  var valChecksGrossCer = [false, false, false], valChecksGrossTro = [false, false, false];
  // var valChecksHoleOneCer = [false, false, false, false], valChecksHoleOneTro = [false, false, false, false];
  // var valChecksNearPinCer = [false, false, false, false], valChecksNearPinTro = [false, false, false, false];
  var valChecksNearLineCer = false, valChecksNearLineTro = false;
  var valChecksLongestCer = false, valChecksLongestTro = false;

  bool _dropBestNett = false;
  bool _dropBestGross = false;
  bool _dropSkillAward = false;
  bool _loading = false;

  var dataTour;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    if (widget.data == null) {
      _getToken();
    }else {
      _valFlight = widget.data["rule_bn"]["flight"].toString();
      if (widget.data["rule_fl"].isNotEmpty) {
        for (int i = 0; i < widget.data["rule_bn"]["flight"]; i++) {
          _arrValWinners.add(widget.data["rule_fl"][i]["winner"].toString());
          if (i == 0) {
            _arrMinRange[i] = widget.data["rule_fl"][i]["min_range"].toString();
            _valWinners = widget.data["rule_fl"][i]["min_range"].toString();
          } else if (i == 1) {
            _arrMinRange[i] = widget.data["rule_fl"][i]["min_range"].toString();
            _arrMaxRange[i] = widget.data["rule_fl"][i]["max_range"].toString();
            _valWinners10 = widget.data["rule_fl"][i]["min_range"].toString();
            _valWinners20 = widget.data["rule_fl"][i]["max_range"].toString();
          } else if (i == 2) {
            _arrMaxRange[i] = widget.data["rule_fl"][i]["max_range"].toString();
            _valWinners = widget.data["rule_fl"][i]["max_range"].toString();
          }
        }
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  var token;
  void _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tokenJson = localStorage.getString('usertoken');
    var datatoken = json.decode(tokenJson);
    print(datatoken);
    setState(() {
      token = datatoken;
      _getDataTour();
    });
  }

  void _getDataTour() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var dataJson = localStorage.getString('savetour');
    var data = json.decode(dataJson);
    setState(() {
      dataTour = widget.dataTour;
    });
  }


  var myFormat = DateFormat('d MMM yyyy');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Stack(
          children: [
            ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: [
                Container(
                  margin: EdgeInsets.all(13),
                  child: Column(
                    children: [
                      HeaderCreate(title: "Create\nNew Tournament",),
                      titleAward(),
                      winningRules()],
                  ),
                ),
              ],
            ),
            _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
          ],
        ),
      ),
    );
  }

  Container winningRules() {
    return Container(
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              "Winning Rules & Prizes",
              style: TextStyle(
                color: AppTheme.gBlack,
                fontSize: 24,
                fontWeight: FontWeight.w900,
              ),
            ),
            widget.disable ? Container(padding: EdgeInsets.symmetric(vertical: 10), child: Text(
              "${widget.template}",
              style: TextStyle(color: AppTheme.gOrange, fontWeight: FontWeight.bold),
            ),) : Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Text(
                    "Select Award Template",
                    style: TextStyle(color: AppTheme.gOrange),
                  ),
                  Container(
                    width: 200,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(width: 1, color: AppTheme.gGrey))),
                    child: widget.disable ? DropdownButton(isExpanded: true, underline: SizedBox(), hint: Text(widget.data == null ? "${dataTour==null?"":dataTour['tipe_turnamen']}" : widget.data["turnamen"]["tipe_turnamen"]),) : DropdownButton(
                      isExpanded: true,
                      underline: SizedBox(),
                      hint: Text(
                        widget.template=='Match' ? "Match" : "${dataTour==null?"":dataTour['tipe_turnamen']}",
                        textAlign: TextAlign.center,
                      ),
                      value: _valTemplate,
                      items: <String>['Match', 'Mini Tournament', 'Tournament'].map((value) {
                        return DropdownMenuItem(
                          child: Text(value),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _valTemplate = value;
                          if(_valTemplate == 'Tournament'){
                            Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, animation1, animation2) => SetWinningTournament(isEdit: true, template: 'Tournament', disable: false, dataTour: dataTour,)));
                          }else if(_valTemplate == 'Mini Tournament'){
                            Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, animation1, animation2) => SetWinningTournament(isEdit: false, template: 'Mini Tournament', disable: false, dataTour: dataTour,)));
                          }
                        });
                      },
                      // value: dataTour==null?"":dataTour['tipe_turnamen'],
//                          icon: Icon(Icons.arrow_drop_down),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5),
              child: Column(
                children: [
                  Text(
                    "Winners Rules & Awards Configuration",
                    style: TextStyle(
                        color: AppTheme.gFont,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (_dropBestNett == false) {
                          _dropBestNett = true;
                        } else if (_dropBestNett == true) {
                          _dropBestNett = false;
                        }
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical:10),
                      padding: EdgeInsets.all(15),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: AppTheme.gGrey),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                SvgPicture.asset('assets/svg/check.svg'),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Best Nett", style: TextStyle(fontSize: 18),),
                                ),
                              ],
                            ),
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    if (_dropBestNett == false) {
                                      _dropBestNett = true;
                                    } else if (_dropBestNett == true) {
                                      _dropBestNett = false;
                                    }
                                  });
                                },
                                child: _dropBestNett == true
                                    ? Image.asset(
                                        'assets/images/arrow down.png')
                                    : Image.asset(
                                        'assets/images/arrow left.png')),
                          ]),
                    ),
                  ),
                  AnimatedCrossFade(
                    firstChild: Container(),
                    secondChild: dropDownBestNett(),
                    duration: Duration(milliseconds: 500),
                    crossFadeState: _dropBestNett ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                    sizeCurve: Curves.fastOutSlowIn,
                  ),
                  // _dropBestNett == true ? dropDownBestNett() : emptyCon(),
                  buttonBackSave()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Container emptyCon() {
    return Container();
  }

  Container buttonBackSave() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                    setState(() {
                      _loading = true;
                      _saveMatch();
                    });
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                          alignment: Alignment.center,
                          child: _loading
                              ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),))
                              : Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Column dropDownBestNett() {
    var commentWidgets = List<Widget>();
    int no = 1;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 5),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Text("HC System", style: TextStyle(color: AppTheme.gFont)),
                Container(
                  width: 150,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    hint: Text(
                      "System 36",
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "# Flights",
                  style: TextStyle(color: AppTheme.gFont),
                ),
                Container(
                  width: 100,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1, color: AppTheme.gGrey))),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    hint: Text(
                      widget.data == null ? "1" : widget.data["rule_bn"]["flight"].toString(),
                      textAlign: TextAlign.center,
                    ),
                    value: _valFlight,
                    items: <String>['1', '2', '3'].map((value) {
                      return DropdownMenuItem(
                        child: Text(value),
                        value: value,
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        _valFlight = value;
                      });
                    },
                  ),
                ),
              ],
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
        ),
        int.parse(_valFlight)==1 ? emptyCon() : ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: int.parse(_valFlight)  ,
            itemBuilder: (BuildContext ctxt, int index) {
              _arrValWinners.length = int.parse(_valFlight);
              if (_arrValWinners.length<int.parse(_valFlight)){
                _arrValWinners.add("1");
              }
              // ignore: unnecessary_statements
              _arrValWinners[index]==null?_arrValWinners[index]="1":_arrValWinners[index];
              return Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 10),
                      padding: EdgeInsets.all(25),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(15)),
                      child: Align(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            "HC Range",
                                            style: TextStyle(color: AppTheme.gFont),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        Container(
                                          width: 140,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1, color: AppTheme.gGrey))),
                                          child: index + no == 1
                                              ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("Lower Than", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: <String>['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners = value;
                                                      _arrMinRange[0] = value;
                                                    });
                                                  },
                                                  hint: Text(
                                                    widget.data == null ? "1" : _arrMinRange[0],
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners,
                                                ),
                                              ),
                                            ],
                                          )
                                              : index + no == 2
                                              ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: <String>['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'].map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners10 = value;
                                                      _arrMinRange[1] = value;
                                                    });
                                                  },
                                                  hint: Text(
                                                    "10",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners10,
                                                ),
                                              ),
                                              Text("To", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: <String>['20', '21', '22', '23', '24', '25'].map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners20 = value;
                                                      _arrMaxRange[1] = value;
                                                    });
                                                  },
                                                  hint: Text(
                                                    "20",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners20,
                                                ),
                                              ),
                                            ],
                                          ) : Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text("Higher Than", style: TextStyle(color: Color(0xFFBDBDBD)),),
                                              SizedBox(
                                                width: 50,
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  underline: SizedBox(),
                                                  items: <String>['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].map((value) {
                                                    return DropdownMenuItem(
                                                      value: value,
                                                      child: Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _valWinners = value;
                                                      _arrMaxRange[2] = value;
                                                    });
                                                  },
                                                  hint: Text(
                                                    "1",
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  value: _valWinners,
                                                ),
                                              ),

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        "# Winners",
                                        style: TextStyle(color: AppTheme.gFont),
                                      ),
                                      Container(
                                        width: 80,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 1, color: AppTheme.gGrey))),
                                        child: DropdownButton(
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          items: _locations.map((value) {
                                            return DropdownMenuItem(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              _arrValWinners[index] = value;
                                            });
                                          },
                                          hint: Text(
                                            "1",
                                            textAlign: TextAlign.center,
                                          ),
                                          value: _arrValWinners[index],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 15),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Text(
                                      "Rank",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Text(
                                      "Certificate",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 20.0),
                                    child: Text(
                                      "Trophy",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.symmetric(horizontal: 20.0),
                                    child: Text(
                                      "Prize",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: setCB(index, _arrValWinners[index]!=null?int.parse(_arrValWinners[index]):1),
                              )
                            ],
                          )
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.symmetric(horizontal: 95),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: _arrColorFlight[index],
                      ),
                      child: Text(
                        _arrTittleFlight[index],
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              );
            }
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(15)),
                child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                          "# Winners",
                          style: TextStyle(color: AppTheme.gFont),
                        ),
                        Container(
                          width: 80,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 1, color: AppTheme.gGrey))),
                          child: DropdownButton(
                            isExpanded: true,
                            underline: SizedBox(),
                            hint: Text(
                              widget.data == null ? "1" : widget.data["rule_bn"]["winner"].toString(),
                              textAlign: TextAlign.center,
                            ),
                            value: _valOverAll,
                            items: <String>['1', '2', '3'].map((value) {
                              return DropdownMenuItem(
                                child: Text(value),
                                value: value,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valOverAll = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Rank",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                "Certificate",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Text(
                                "Trophy",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20.0),
                              child: Text(
                                "Prize",
                                style: TextStyle(color: AppTheme.gFont),
                              ),
                            ),
                          ],
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: int.parse(_valOverAll),
                            itemBuilder: (context, index) =>
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 3.0),
                                      child: Text(
                                        Utils.formatSuffix(index + no),
                                        style: TextStyle(color: AppTheme.gGrey),
                                      ),
                                    ),
                                    Padding(
                                      padding: index + no == 2
                                          ? EdgeInsets.only(left: 21)
                                          : EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksOverAllCer[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksOverAllCer[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 25.0),
                                      child: Checkbox(
                                        value: valChecksOverAllTro[index],
                                        onChanged: (value) {
                                          setState(() {
                                            valChecksOverAllTro[index] = value;
                                          });
                                        },
                                      ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.only(left: 20),
                                        width: 100,
                                        child: TextFormField(
                                          onChanged: (value) {
                                            setState(() {
                                              _arrTextBestMettOverall[index] = value.toString();
                                            });
                                          },
                                        ))
                                  ],
                                )
                        ),
                      ],
                    )),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 70),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: int.parse(_valFlight)==1 ? AppTheme.gFont : Color(0xFFE31212),
                ),
                child: Text(
                  int.parse(_valFlight)==1 ? "Best Nett" : "Best Nett Overall",
                  style:
                      TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> setCB(int mPosition, int mLenght){
    int no = 1;
    var commentWidgets = List<Widget>();
    var arrValChecksBNC = List<bool>();
    var arrValChecksBNT = List<bool>();
    var arrPrice = List<String>();
    // var arrCon = List<TextEditingController>();
    mapBestNetCer.putIfAbsent((mPosition + no).toString(), () => arrValChecksBNC);
    mapBestNetTro.putIfAbsent((mPosition + no).toString(), () => arrValChecksBNT);
    mapBestNetPri.putIfAbsent((mPosition + no).toString(), () => arrPrice);

    for (int a = 0; a < mLenght; a++) {
      if (arrValChecksBNC.length<mLenght && arrValChecksBNT.length<mLenght ) {
        arrValChecksBNC.add(false);
        arrValChecksBNT.add(false);
        arrPrice.add("");
        // TextEditingController edCon = TextEditingController();
        // arrCon.add(edCon);
      }
      mapBestNetCer.update((mPosition + no).toString(), (value) => arrValChecksBNC = value);
      mapBestNetTro.update((mPosition + no).toString(), (value) => arrValChecksBNT = value);
      mapBestNetPri.update((mPosition + no).toString(), (value) => arrPrice = value);

      commentWidgets.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 3.0),
              child: Text(
                Utils.formatSuffix(a + no),
                style: TextStyle(color: AppTheme.gGrey),
              ),
            ),
            Padding(
              padding: a + no == 2
                  ? EdgeInsets.only(left: 21)
                  : EdgeInsets.only(left: 25.0),
              child: Checkbox(
                value: mapBestNetCer[(mPosition + no).toString()][a],
                onChanged: (valueCer) {
                  setState(() {
                    mapBestNetCer[(mPosition + no).toString()][a] = valueCer;
                  });
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25.0),
              child: Checkbox(
                value: mapBestNetTro[(mPosition + no).toString()][a],
                onChanged: (valueTro) {
                  setState(() {
                    mapBestNetTro[(mPosition + no).toString()][a] = valueTro;
                  });
                },
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 20),
                width: 100,
                child: TextFormField(
                  onChanged: (valuePrize) {
                    setState(() {
                      arrPrice[a] = valuePrize;
                    });
                  },
                ))
            // Container(
            //     padding: EdgeInsets.only(left: 20),
            //     width: 100,
            //     child: TextFormField(
            //       // controller: arrCon[a],
            //       onChanged: (valuePrize) {
            //         setState(() {
            //           mapBestNetPri[(mPosition + no).toString()][a] = valuePrize;
            //           arrPrice[a] = valuePrize;
            //           // arrCon[a].text = mapBestNetPri[(mPosition + no).toString()][a];
            //         });
            //       },
            //     ))
          ],
        ),
      );
    }

    return commentWidgets;
  }

  Container titleAward() {
    return Container(
      padding: EdgeInsets.only(top: 25, bottom: 15),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            Text(
              widget.data == null ? "${dataTour==null?"":dataTour['nama_turnamen']} (ID ${dataTour==null?"":dataTour['kode_turnamen']})"
                  :
              "${widget.data["turnamen"]['nama_turnamen']} (ID ${widget.data["turnamen"]['kode_turnamen']})",
              style: TextStyle(
                color: AppTheme.gFont,
                fontSize: 18,
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                widget.data == null ? "${dataTour==null?"":dataTour['lapangan_turnamen']}, ${dataTour==null?"":myFormat.format(DateTime.parse(dataTour['tanggal_turnamen']))}"
                    :
                "${widget.data["turnamen"]['lapangan_turnamen']}, ${myFormat.format(DateTime.parse(widget.data["turnamen"]['tanggal_turnamen']))}",
                style: TextStyle(color: Color(0xFF4F4F4F)),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _saveMatch() async {
    if (_arrValWinners.isEmpty){
      _arrValWinners.add("1");
      mapBestNetCer.putIfAbsent("1", () => [false]);
      mapBestNetTro.putIfAbsent("1", () => [false]);
      mapBestNetPri.putIfAbsent("1", () => [""]);
    }

    var data;

    if(_valFlight == "1"){
      data = {
        "id_turnamen":dataTour==null?"0":dataTour['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        // "nama_flight":_arrTittleFlight.getRange(0, int.parse(_valFlight)).toList(),
        // "winner_fl":_arrValWinners,
        // "min_range":_arrMinRange,
        // "max_range":_arrMaxRange,
        // "certificate_fl":mapBestNetCer,
        // "trophy_fl":mapBestNetTro,
        // "prize_fl":mapBestNetPri,
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":_arrTextBestMettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        'token' : token
      };
      print(data);
    }else{
      data = {
        // "id_turnamen":dataTour==null?"0":dataTour['id_turnamen'],
        // "sistem":"System 36",
        // "flights":1,
        // "winners":int.parse(_valOverAll),
        // "certificate":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        // "trophy":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        // "prize":_arrTextBestMettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        "id_turnamen":dataTour==null?"0":dataTour['id_turnamen'],
        "sistem":"System 36",
        "flights":int.parse(_valFlight),
        "nama_flight":_arrTittleFlight.getRange(0, int.parse(_valFlight)).toList(),
        "winner_fl":_arrValWinners,
        "min_range":_arrMinRange,
        "max_range":_arrMaxRange,
        "certificate_fl":mapBestNetCer,
        "trophy_fl":mapBestNetTro,
        "prize_fl":mapBestNetPri,
        "winner_bn":int.parse(_valOverAll),
        "certificate_bn":valChecksOverAllCer.getRange(0, int.parse(_valOverAll)).toList(),
        "trophy_bn":valChecksOverAllTro.getRange(0, int.parse(_valOverAll)).toList(),
        "prize_bn":_arrTextBestMettOverall.getRange(0, int.parse(_valOverAll)).toList(),
        'token' : token
      };
      print(data);
    }

    Utils.postAPI(data, "create_wrpm").then((body) async {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
          if(widget.editFromSummary == false){
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) =>
                    InviteGolfer(
                      data: dataTour == null ? "0" : dataTour['id_turnamen'],
                      skip: true,)));
          }else{
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (BuildContext context) => SummaryGames(tourId: dataTour['id_turnamen'],)),
              ModalRoute.withName('/'),
            );
            // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SummaryGames()));
          }
          // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => InviteGolfer(data: dataTour==null?"0":dataTour['id_turnamen'], skip: true,)));

        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }
}
