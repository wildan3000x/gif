import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/invitation/invite_golfers.dart';
import 'package:gif/create_games/set_winning.dart';
import 'package:gif/create_games/winning/set_winning_tournament.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/tournament_detail.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';

class CreatedGames extends StatefulWidget {
  @override
  _CreatedGamesState createState() => _CreatedGamesState();
}

class _CreatedGamesState extends State<CreatedGames> {
  SharedPreferences sharedPreferences;
  var dataWinner;
  var dataTour;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    } else {
      _getDataTour();
    }
  }

  void _getDataTour() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var dataJson = localStorage.getString('savetour');
    var data = json.decode(dataJson);
    setState(() {
      dataTour = data;
      _getSummary();
    });
  }

  bool _isLoading = false;
  void _getSummary() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      "id_turnamen": dataTour == null ? "0" : dataTour['id_turnamen'],
      'token': sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_ts").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          dataWinner = body;
        });
        print("DATAWINNER = $dataWinner");
      } else {
        // setState(() {
        //   _isLoading = false;
        // });
        print("ERROR");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Utils.showToast(context, "error", "Please finish all the game rules first");
        return false;
      },
      child: Scaffold(
        body: SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: Stack(
              children: [
                ListView(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  children: [
                    Container(
                      margin: EdgeInsets.all(13),
                      child: Column(
                        children: [
                          HeaderCreate(
                            title: "Create\nNew Tournament",
                            disableBack: true,
                            backSummary: true,
                            disable: true,
                          ),
                          titlePage(),
                          dataTour==null?Container():TournamentDetailWidget(
                            dataTournament: dataTour,
                            additionalWidget: Positioned(
                              right: 10,
                              child: InkWell(
                                onTap: () {
                                  _navigateAndDisplaySelection(context);
                                },
                                child: Text(
                                  "Edit",
                                  style: TextStyle(color: Colors.red, fontSize: 14),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 4, bottom: 15),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                "*Golfer can join the tournament by entering tournament ID,\nand subject to your approval",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.italic),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          tournamentWinning(context),
                          menuButton(context)
                        ],
                      ),
                    ),
                  ],
                ),
                _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container menuButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Column(
        children: [
          Container(
            width: 80,
            child: InkWell(
              splashColor: Colors.grey.withOpacity(0.2),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InviteGolfer(
                          data: dataWinner != null
                              ? dataWinner['turnamen']['id_turnamen']
                              : "0",
                        )));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Next",
                      style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 18,
                          color: AppTheme.gFont)),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Image.asset(
                      "assets/images/arrow right.png",
                      width: 20,
                      height: 20,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              "Invite Golfers",
              style: TextStyle(color: Color(0xFF828282), fontSize: 18),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  splashColor: Colors.grey.withOpacity(0.5),
                  onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => SetWinningTournament(
                                dataTour: dataWinner==null?null:dataWinner['turnamen'],
                                isEdit: false,
                                disable: true,
                                template: dataWinner == null ? "" : dataWinner['turnamen']['tipe_turnamen'],
                                editFromSummary: false,
                              ),),);
                  },
                  padding: const EdgeInsets.all(0.0),
                  child: Ink(
                      decoration: BoxDecoration(
                          color: AppTheme.gButton,
                          borderRadius: BorderRadius.circular(100)
                      ),
                      child: Container(
                          constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                          alignment: Alignment.center,
                          child: Text("Next", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Stack tournamentWinning(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          right: 10,
          child: InkWell(
            onTap: () {
              /*if (dataTour == null
                  ? ""
                  : dataTour['tipe_turnamen'] == 'Match') {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SetWinning(disable: false, dataTour: dataWinner['turnamen'])));
              } else {*/
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SetWinningTournament(isEdit: true, disable: false, dataTour: dataWinner==null?null:dataWinner['turnamen'],)));
              // }
            },
            child: Text(
              "Edit",
              style: TextStyle(color: Colors.red),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.all(13),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15)),
          child: Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 13.0, bottom: 8),
                    child: Text(
                      "${dataTour == null ? "" : dataTour['tipe_turnamen']}",
                      style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),
                    ),
                  ),
                  Text(
                    "Top 3 Best Nett, HC System 36, No Prizes",
                    style: TextStyle(color: AppTheme.gFont),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.symmetric(horizontal: 55),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Color(0xFFF15411),
          ),
          child: Text(
            "Winning Rules & Prizes",
            style: TextStyle(color: Colors.white, fontSize: 18),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditGames(dataTour: dataWinner['turnamen'],)),
    );
    setState(() {
      if (result != "0") {
        var data = json.decode(result);
        dataTour = data;
      }
    });
  }

  Container titlePage() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "Tournament is Created",
          style: TextStyle(
            color: AppTheme.gFont,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
