import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/my_appointment.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/set_winning.dart';
import 'package:gif/create_games/summary/add_admin.dart';
import 'package:gif/create_games/summary/best_gross.dart';
import 'package:gif/create_games/summary/participant_status.dart';
import 'package:gif/create_games/summary/skill_award.dart';
import 'package:gif/create_games/winning/edit_winning_tournament.dart';
import 'package:gif/create_games/winning/set_winning_tournament.dart';
import 'package:gif/main.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
import 'package:gif/widget/tournament_detail.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'menu_participants/manage_participants.dart';

class SummaryGames extends StatefulWidget {
  const SummaryGames({Key key, this.tourId, this.onlyView, this.viewFromPairing, this.viewFromNextEventsClub}) : super(key: key);
  final tourId, onlyView, viewFromPairing, viewFromNextEventsClub;
  @override
  _SummaryGamesState createState() => _SummaryGamesState();
}

class _SummaryGamesState extends State<SummaryGames> {
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataWinner;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  var _arrTittleFlight = ["Flight A", "Flight B", "Flight C"];
  var _arrColorFlight = [Color(0xFF2D9CDB), AppTheme.gFont, Color(0xFFF3BF37)];

  bool _dropBestNett = false;
  bool _dropBestGross = false;
  bool _dropSkillAward = false;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  bool _isLoading = false;
  var _valTemplate;
  int id;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    id = widget.tourId;
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      _getDataTour();
    }
  }

  void _getDataTour() async {
    if(id == null) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      var dataJson = localStorage.getString('savetour');
      var data = json.decode(dataJson);
      dataTour = data;
      setState(() {
        _getSummary();
      });
    }else{
      _getSummary();
    }
  }

  bool checkStatusRunningGame = false;
  void _getSummary() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      "id_turnamen":id,
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_ts").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          dataWinner = body;
          checkStatusRunningGame = body['cek_turnamen_berjalan'];
          _isLoading = false;
        });
        _valTemplate = dataWinner['turnamen']['template_turnamen'];
        print("TEMPLATE GAME $_valTemplate");
        setState(() {
          if(dataWinner['turnamen']['template_turnamen'] == 'Match'){
            showBestGross = false;
            showAwardSkill = false;
          }else if(dataWinner['turnamen']['template_turnamen'] == 'Tournament'){
            showBestGross = true;
            showAwardSkill = true;
          }else{
            showBestGross = true;
            showAwardSkill = false;
          }
        });
      }else if(body['status'] == 'error' && body['message'] == 'Token missmatch'){
        _getSummary();
      }else{
        setState(() {
          _isLoading = false;
        });
        print("ERROR");
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }


  void removeOrganizer({int idUser}) async {

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_turnamen': id,
      'id_user': idUser
    };

    Utils.postAPI(data, "remove_turnamen_organizer").then((body) {
      if(body['status'] == 'success'){
        Navigator.pop(context);
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.blueAccent,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        _getSummary();
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  bool showBestGross, showAwardSkill = false;

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    checkLoginStatus();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return widget.onlyView == true ?
    Scaffold(
      key: _scaffoldKey,
      body: mainBodyWidget(context),
    ) : WillPopScope(
      onWillPop: () async {
        return dataWinner==null?"":dataWinner['invited']==0&&dataWinner['applying']==0&&dataWinner['confirmed']==1?showDialog(
          context: _scaffoldKey.currentContext,
          builder: (context) => CustomAlertDialog(
            title: 'Are you sure?',
            contentText: "you still haven't invite anyone yet, sure want to exit?",
            yesButton: () => Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                    (Route<dynamic> route) => false
            ),
            noButton: () => Navigator.of(context).pop(false),
          )
        ) : Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                (Route<dynamic> route) => false
        );
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: mainBodyWidget(context),
      ),
    );
  }

  SafeArea mainBodyWidget(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          _isLoading
              ? Center(
            child: Container(
              width: 30,
              height: 30,
              child: CircularProgressIndicator(),
            ),
          ) : SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                margin: EdgeInsets.all(15),
                child: Column(
                  children: [
                    HeaderCreate(title: "Game Summary", backSummary: true, disableBack: widget.onlyView == true || widget.viewFromNextEventsClub == true ? false : true,),
                    titlePage(),
                    dataWinner==null?Container():TournamentDetailWidget(
                      dataTournament: dataWinner['turnamen'],
                      additionalWidget: widget.onlyView == true || checkStatusRunningGame
                          ? Container()
                          : Positioned(
                              right: 10,
                              bottom: 0,
                              child: Container(
                                padding: EdgeInsets.only(top: 8, right: 8),
                                child: InkWell(
                                  onTap: () {
                                    _navigateAndDisplaySelection(context);
                                  },
                                  child: Text(
                                    "Save",
                                    style: TextStyle(color: Colors.red, fontSize: 14),
                                  ),
                                ),
                              ),
                      ),
                    ),
                    winningRulesConfiguration(),
                    ParticipantStatus(
                      data: dataWinner,
                      edit: widget.onlyView == true ? true : null,
                      linkToInvited: () => linkToManageParticipants("Invited").then((value) => checkLoginStatus()),
                      linkToApplying: () => linkToManageParticipants("Applying").then((value) => checkLoginStatus()),
                      linkToConfirmed: () => linkToManageParticipants("Confirmed").then((value) => checkLoginStatus()),
                    ),
                    addOrganizer(context),
                    buttonViewPairing(),
                    widget.onlyView == true || widget.viewFromNextEventsClub == true ? Container ()
                        : BackToHomeButton(
                        onPressed: () {
                          if(widget.onlyView == true) {
                            Navigator.pop(context);
                          }else if(sharedPreferences.getString("level") == "Caddie"){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyAppointmentCaddie()));
                          }else {
                            return dataWinner == null ? "" : dataWinner['invited']==0&&dataWinner['applying']==0&&dataWinner['confirmed']==1? showDialog(
                              context: _scaffoldKey.currentContext,
                              builder: (context) => CustomAlertDialog(
                                title: 'Are you sure?',
                                contentText: "you still haven't invite anyone yet, sure want to exit?",
                                yesButton: () => Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(builder: (BuildContext context) => new HomePage(navbar: true,)),
                                        (Route<dynamic> route) => false
                                ),
                                noButton: () => Navigator.of(context).pop(false),
                              ),
                            ) : Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) => new HomePage(
                                      navbar: true,)),
                                    (Route<dynamic> route) => false
                            );
                          }
                        },
                        textButton: widget.onlyView == true ? "Back" : "back to home"
                    )
                  ],
                ),
              ),
            ),
          ),
          _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
        ],
      ),
      );
  }

  Container addOrganizer(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Text("Tournament Organizer", style: TextStyle(color: AppTheme.gFont, fontSize: 18, fontWeight: FontWeight.bold),),
          SafeArea(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 8),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                  color: Color(0xFF828282)
                )
              ),
              child: Column(
                children: [
                  Text("Organization", style: TextStyle(color: AppTheme.gFont),),
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 15),
                    child: Text("${dataWinner == null ? "" : dataWinner['turnamen']['penyelenggara_turnamen']}", style: TextStyle(fontSize: 18),),
                  ),
                  Text("Tournament Officer", style: TextStyle(color: AppTheme.gFont),),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: dataWinner!=null?dataWinner['turnamen_organizer'].length:0,
                      itemBuilder: (context, index) => Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("${dataWinner!=null?dataWinner['turnamen_organizer'][index]['nama_lengkap']:""}", style: TextStyle(fontSize: 18),),
                              dataWinner['turnamen_organizer'][index]['turnamen_maker'] == true || dataWinner['cek_turnamen_organizer'] == false ? Container() : Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: InkWell(
                                  splashColor: Colors.grey.withOpacity(0.3),
                                  onTap: () {
                                    showDialog(
                                        context: _scaffoldKey.currentContext,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                            contentPadding: EdgeInsets.all(8),
                                            title: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("Confirm", style: TextStyle(color: AppTheme.bogies, fontWeight: FontWeight.bold, fontSize: 18),),
                                                SizedBox(height: 5,),
                                                Text("Remove ${dataWinner['turnamen_organizer'][index]['nama_lengkap']} from organizer ?", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                              ],
                                            ),
                                            content: Container(
                                              height: 60,
                                              width: double.maxFinite,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Container(
                                                    child: RaisedButton(
                                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                                      splashColor: Colors.grey.withOpacity(0.5),
                                                      onPressed: () { Navigator.pop(context);},
                                                      padding: const EdgeInsets.all(0.0),
                                                      child: Ink(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: AppTheme.bogies,
                                                              borderRadius: BorderRadius.circular(100)
                                                          ),
                                                          child: Container(
                                                              constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                                              alignment: Alignment.center,
                                                              child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                                    ),
                                                  ),
                                                  Container(
                                                    child: RaisedButton(
                                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                                      splashColor: Colors.grey.withOpacity(0.5),
                                                      onPressed: () {
                                                        setState(() {
                                                          removeOrganizer(idUser: dataWinner['turnamen_organizer'][index]['user_id']);
                                                        });
                                                      },
                                                      padding: const EdgeInsets.all(0.0),
                                                      child: Ink(
                                                          height: 40,
                                                          decoration: BoxDecoration(
                                                              color: AppTheme.gButton,
                                                              borderRadius: BorderRadius.circular(100)
                                                          ),
                                                          child: Container(
                                                              constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                                              alignment: Alignment.center,
                                                              child: Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        }
                                    );
                                  },
                                  child: Icon(Icons.close_rounded, color: Colors.red,),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 3.0, bottom: 8),
                            child: Text("(GIF ID : ${dataWinner!=null?dataWinner['turnamen_organizer'][index]['kode_user']:""}, PHONE : ${dataWinner!=null?dataWinner['turnamen_organizer'][index]['no_telp']:""})", style: TextStyle(color: Color(0xFF828282)),),
                          )
                        ],
                      ),
                    ),
                  ),
                  widget.onlyView == true ? Container() : Container(
                    padding: EdgeInsets.all(15),
                    child: InkWell(onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AddAdmin(tourId: id, dataTour: dataWinner['turnamen'],)));
                      }, child: Text("Add Tournament Officer", style: TextStyle(color: Color(0xFFF15411)),)),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Container winningRulesConfiguration() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Text(
            "Winners Rules & Awards Configuration",
            style: TextStyle(
                color: AppTheme.gFont,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
          dataWinner != null ? showBestGross ?
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 3),
            child: Stack(
              children: [
                AnimatedCrossFade(
                  firstChild: Container(),
                  secondChild: BestGross(data: dataWinner),
                  duration: Duration(milliseconds: 500),
                  crossFadeState: _dropBestGross ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                  sizeCurve: Curves.fastOutSlowIn,
                ),
                // _dropBestGross == true ? BestGross(data: dataWinner) : emptyCon(),
                InkWell(
                  onTap: () {
                    setState(() {
                      if (_dropBestGross == false) {
                        _dropBestGross = true;
                      } else if (_dropBestGross == true) {
                        _dropBestGross = false;
                      }
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.all(12),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Color(0xFFBDBDBD)),
                        color: AppTheme.gLightGrey),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset('assets/svg/check.svg'),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(
                                  "Best Gross",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          _dropBestGross == true
                              ? Image.asset('assets/images/arrow down.png')
                              : Image.asset('assets/images/arrow left.png'),
                        ]),
                  ),
                ),
              ],
            ),
          ) : Container() : Container(),
          Container(
            margin: EdgeInsets.symmetric(vertical: 3),
            child: Column(
              children: [
                Stack(
                  children: [
                    AnimatedCrossFade(
                      firstChild: Container(),
                      secondChild: Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(color: Color(0xFFBDBDBD))),
                          child: Container(
                            padding:
                            const EdgeInsets.only(top: 55.0, bottom: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding:
                                      const EdgeInsets.only(left: 8.0),
                                      child: Row(
                                        children: [
                                          Text(
                                            "HC : ",
                                            style: TextStyle(
                                                color: AppTheme.gFont),
                                          ),
                                          Text("${dataWinner==null?"":dataWinner['rule_bn']==null?"0":dataWinner['rule_bn']['sistem']}")
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                      const EdgeInsets.only(right: 8.0),
                                      child: Row(
                                        children: [
                                          Text(
                                            "# Flight : ",
                                            style: TextStyle(
                                                color: AppTheme.gFont),
                                          ),
                                          Text("${dataWinner==null?"":dataWinner['rule_bn']==null?"0":dataWinner['rule_bn']['flight']}")
                                        ],
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )),
                      duration: Duration(milliseconds: 500),
                      crossFadeState: _dropBestNett ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                      sizeCurve: Curves.fastOutSlowIn,
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          if (_dropBestNett == false) {
                            _dropBestNett = true;
                          } else if (_dropBestNett == true) {
                            _dropBestNett = false;
                          }
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(12),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(color: Color(0xFFBDBDBD)),
                            color: AppTheme.gLightGrey),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  SvgPicture.asset('assets/svg/check.svg'),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      "Best Nett",
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ),
                                ],
                              ),
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      if (_dropBestNett == false) {
                                        _dropBestNett = true;
                                      } else if (_dropBestNett == true) {
                                        _dropBestNett = false;
                                      }
                                    });
                                  },
                                  child: _dropBestNett == true
                                      ? Image.asset(
                                          'assets/images/arrow down.png')
                                      : Image.asset(
                                          'assets/images/arrow left.png')),
                            ]),
                      ),
                    ),
                  ],
                ),
                AnimatedCrossFade(
                  firstChild: Container(),
                  secondChild: dropDownBestNett(),
                  duration: Duration(milliseconds: 500),
                  crossFadeState: _dropBestNett ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                  sizeCurve: Curves.fastOutSlowIn,
                ),
                // _dropBestNett == true ? dropDownBestNett() : emptyCon(),
              ],
            ),
          ),
          // dataTour==null ? emptyCon() : dataTour['tipe_turnamen'] == 'Mini Tournament' && !widget.isEdit || widget.template == 'Mini Tournament' ? emptyCon() :
          dataWinner != null ? showAwardSkill ?
          InkWell(
            onTap: () {
              setState(() {
                if (_dropSkillAward == false) {
                  _dropSkillAward = true;
                } else if (_dropSkillAward == true) {
                  _dropSkillAward = false;
                }
              });
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 3),
              padding: EdgeInsets.all(12),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Color(0xFFBDBDBD)),
                  color: AppTheme.gLightGrey),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset('assets/svg/check.svg'),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Skill Awards",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    _dropSkillAward == true
                        ? Image.asset('assets/images/arrow down.png')
                        : Image.asset('assets/images/arrow left.png'),
                  ]),
            ),
          ) : Container() : Container(),
          AnimatedCrossFade(
            firstChild: Container(),
            secondChild: SkillAward(data: dataWinner),
            duration: Duration(milliseconds: 500),
            crossFadeState: _dropSkillAward ? CrossFadeState.showSecond : CrossFadeState.showFirst,
            sizeCurve: Curves.fastOutSlowIn,
          ),
          // _dropSkillAward == true ? SkillAward(data: dataWinner) : emptyCon(),
          // buttonBackSave()
          widget.onlyView == true || checkStatusRunningGame ? Container() : Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 15, top: 3),
              child: InkWell(
                onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                            EditWinningTournament(
                                isEdit: true,
                                disable: true,
                                data: dataWinner,
                                template: dataWinner["turnamen"]["tipe_turnamen"])));
                },
                child: Text(
                  "Save",
                  style: TextStyle(color: Colors.red, fontSize: 14),
                ),
              ),
          )
        ],
      ),
    );
  }

  _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) =>
        EditGames(
          editFromSummary: true,
          dataTour: dataWinner['turnamen'],
          viewFromNextEventClub: widget.viewFromNextEventsClub,
        )),
    ).then((value) => setState(() {
      print("VALUE BACK $value");
      if(value == "show_dialog_set_winning_warning"){
        showDialog(context: context, builder: (context){
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            contentPadding: EdgeInsets.only(bottom: 12, left: 12, right: 12, top: 8),
            titlePadding: EdgeInsets.only(top: 12),
            title: Text("Warning", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            content: Container(
              // height: 60,
              width: double.maxFinite,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("you have changed the game type/course, some of the winning settings may not match with the current condition, please edit first",
                    style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                  // SizedBox(height: widget.optionalHeight ?? 8,), ,
                  SizedBox(height: 5,)
                ],
              ),
            ),
          );
        });
      }
      _getDataTour();
    }));
  }

  Container dropDownBestNett() {
    int no = 1;
    return Container(
      child: Column(
        children: [
          dataWinner==null||dataWinner['rule_fl'].isEmpty ? Container() : ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: dataWinner['rule_fl'].length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        padding: EdgeInsets.only(top: 5, left: 5, bottom: 15, right: 5),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15)),
                        child: Align(
                            alignment: Alignment.center,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 25.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                "HC Range",
                                                style: TextStyle(
                                                    color: AppTheme.gFont),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            width: 140,
                                            padding: EdgeInsets.all(8),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        width: 1,
                                                        color:
                                                            AppTheme.gGrey))),
                                            child: index + no == 1
                                                ? Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "HC",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xFFBDBDBD)),
                                                      ),
                                                      SvgPicture.asset("assets/svg/is-less-than-or-equal-to-mathematical-symbol.svg", width:11, height: 11,),
                                                      Text("${dataWinner['rule_fl'][index]['min_range']}"),
                                                    ],
                                                  )
                                                : index + no == 2
                                                    ? Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text("${dataWinner['rule_fl'][index]['min_range']}"),
                                                          Text(
                                                            "To",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xFFBDBDBD)),
                                                          ),
                                                          Text("${dataWinner['rule_fl'][index]['max_range']}"),
                                                        ],
                                                      )
                                                    : Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            "HC",
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xFFBDBDBD)),
                                                          ),
                                                          SvgPicture.asset("assets/svg/is-equal-to-or-greater-than-symbol.svg", width: 11, height: 11,),
                                                          Text("${dataWinner['rule_fl'][index]['max_range']}"),
                                                        ],
                                                      ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            "# Winners",
                                            style: TextStyle(
                                                color: AppTheme.gFont),
                                          ),
                                          Container(
                                              padding: EdgeInsets.all(8),
                                              width: 80,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  border: Border(
                                                      bottom: BorderSide(
                                                          width: 1,
                                                          color:
                                                              AppTheme.gGrey))),
                                              child: Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Text("${dataWinner['rule_fl'][index]['winner']}"))),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 15),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Text(
                                          "Rank",
                                          style:
                                              TextStyle(color: AppTheme.gFont),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Text(
                                          "Certificate",
                                          style:
                                              TextStyle(color: AppTheme.gFont),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 20.0),
                                        child: Text(
                                          "Trophy",
                                          style:
                                              TextStyle(color: AppTheme.gFont),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20.0),
                                        child: Text(
                                          "Prize",
                                          style:
                                              TextStyle(color: AppTheme.gFont),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                ListView.builder(
                                  primary: false,
                                  shrinkWrap: true,
                                  itemCount: dataWinner['rule_fl'][index]['price_fl'].length,
                                  itemBuilder: (context, index2) {
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 15.0),
                                          child: Text(
                                              Utils.formatSuffix(index2 + no),
                                              style: TextStyle(
                                                  color: AppTheme.gGrey),
                                          ),
                                        ),
                                        Padding(
                                            padding: index2 + no == 2
                                                ? EdgeInsets.only(left: 16, bottom: 5, top: 8)
                                                : EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                            child: SvgPicture.asset(
                                                dataWinner['rule_fl'][index]['price_fl'][index2]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                                                    : "assets/svg/check_bold.svg",
                                                width: 16,
                                                height: 16)),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 25.0, bottom: 5, top: 8),
                                            child: SvgPicture.asset(
                                              dataWinner['rule_fl'][index]['price_fl'][index2]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                                                  : "assets/svg/check_bold.svg",
                                              width: 16,
                                              height: 16,
                                            )),
                                        Container(
                                            padding: EdgeInsets.only(left: 0),
                                            width: 100,
                                            child: Text("${dataWinner['rule_fl'][index]['price_fl'][index2]['price'] == null ? "(no prize)" : dataWinner['rule_fl'][index]['price_fl'][index2]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis))
                                      ],
                                    );
                                  },
                                ),
                              ],
                            )),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.symmetric(horizontal: 100),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: _arrColorFlight[index],
                        ),
                        child: Text(
                          _arrTittleFlight[index],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                );
              }),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.only(top: 5, left: 5, bottom: 15, right: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 25.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      "# Winners",
                                      style: TextStyle(color: AppTheme.gFont),
                                    ),
                                    Container(
                                        padding: EdgeInsets.all(8),
                                        width: 80,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 1,
                                                    color: AppTheme.gGrey))),
                                        child: Align(
                                            alignment: Alignment.center,
                                            child: Text("${dataWinner==null?"":dataWinner['price_bn'].length}"))),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Rank",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Certificate",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text(
                                    "Trophy",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20.0),
                                  child: Text(
                                    "Prize",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: dataWinner==null?0:dataWinner['price_bn'].length,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text(
                                            Utils.formatSuffix(index + no),
                                            style: TextStyle(
                                                color: AppTheme.gGrey)),
                                  ),
                                  Padding(
                                    padding: index + no == 2
                                        ? EdgeInsets.only(left: 16, bottom: 5, top: 8)
                                        : EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                        child: SvgPicture.asset(
                                            dataWinner['price_bn'][index]['sertifikat'] == 0? "assets/svg/uncheck.svg" : "assets/svg/check_bold.svg",
                                          width: 16,
                                          height: 16,),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(
                                          left: 25.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        dataWinner['price_bn'][index]['tropi'] == 0? "assets/svg/uncheck.svg" : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,
                                      )),
                                  Container(
                                      padding: EdgeInsets.only(left: 0),
                                      width: 100,
                                      child: Text("${dataWinner['price_bn'][index]['price'] == null || dataWinner['price_bn'][index]['price'] == "null" ? "(no prize)" : dataWinner['price_bn'][index]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,))
                                ],
                              );
                            },
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.symmetric(horizontal: 70),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: AppTheme.gRed,
                  ),
                  child: Text(
                    "Best Nett Overall",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buttonViewPairing() {
    return widget.viewFromPairing == true ? Container() : Padding(
      padding: const EdgeInsets.only(top: 25.0, bottom: 12),
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
        splashColor: Colors.grey.withOpacity(0.5),
        onPressed: () {
          if(widget.onlyView == true) {
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                PairingGames(id: dataWinner == null ? "0" : dataTour == null ? id : dataTour['id_turnamen'], viewFromSummary: true)));
          }else{
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                PairingGames(id: dataWinner == null ? "0" : dataTour == null ? id : dataTour['id_turnamen'], viewFromSummary: true, organizer: true, userId: dataWinner['turnamen']['user_id'],)));
          }
          // Navigator.pop(context);
        },
        padding: const EdgeInsets.all(0.0),
        child: Ink(
            decoration: BoxDecoration(
                color: AppTheme.gButton,
                borderRadius: BorderRadius.circular(100)
            ),
            child: Container(
                constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                alignment: Alignment.center,
                child: Text("View Pairing", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
      ),
    );
  }

  Container titlePage() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          "Tournament Summary",
          style: TextStyle(
            color: AppTheme.gFont,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Container emptyCon() {
    return Container();
  }

  Future<Navigator> linkToManageParticipants(type) {
    return Utils.navLink(context, ManageParticipants(
      idTournament: dataWinner['turnamen']['id_turnamen'],
      typeParticipant: type,
    ));
  }

}

class BackToHomeButton extends StatelessWidget {
  const BackToHomeButton({
    Key key, @required this.onPressed, @required this.textButton
  }) : super(key: key);

  final onPressed;
  final textButton;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.zero),
          backgroundColor: MaterialStateProperty.all(AppTheme.gLightGrey,),
          side: MaterialStateProperty.all(BorderSide(color: Colors.grey.withOpacity(0.3))),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0))),
          overlayColor: MaterialStateProperty.all(Colors.grey.withOpacity(0.2))
      ),
      onPressed: onPressed,
      child: Container(
        width: 130,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 20, height: 20,),
            ),
            SizedBox(width: 3,),
            Text(textButton, style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.normal),),
          ],
        ),
      ),
    );
  }
}
