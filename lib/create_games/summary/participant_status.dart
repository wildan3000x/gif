import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/create_games/invitation/invite_golfers.dart';
import 'package:gif/create_games/summary/menu_participants/manage_participants.dart';
import 'package:gif/utils/Utils.dart';

class ParticipantStatus extends StatefulWidget {
  const ParticipantStatus({Key key, this.data, this.edit, this.linkToInvited, this.linkToApplying, this.linkToConfirmed}) : super(key: key);
  final data, edit, linkToInvited, linkToApplying, linkToConfirmed;
  @override
  _ParticipantStatusState createState() => _ParticipantStatusState();
}

class _ParticipantStatusState extends State<ParticipantStatus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Text(
            "Participant Status",
            style: TextStyle(
                color: AppTheme.gFont,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
          SizedBox(height: 3,),
          widget.edit == true ? Container() : Text("click to manage participants in this game", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
          InkWell(
            onTap: widget.edit == true ? (){} : widget.linkToInvited,
            child: Container(
              margin: EdgeInsets.only(top: 5, bottom: 2),
              padding: EdgeInsets.all(9),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Color(0xFFBDBDBD)),
                  color: AppTheme.gLightGrey),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Invited",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "${widget.data != null ? widget.data['invited']:"0"}",
                      style: TextStyle(fontSize: 18, color: AppTheme.gFont),
                    )
                  ]),
            ),
          ),
          InkWell(
            onTap: widget.edit == true ? (){} : widget.linkToApplying,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 2),
              padding: EdgeInsets.all(9),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Color(0xFFBDBDBD)),
                  color: AppTheme.gLightGrey),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Applying",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "${widget.data != null ? widget.data['applying']:"0"}",
                      style: TextStyle(fontSize: 18, color: AppTheme.gFont),
                    )
                  ]),
            ),
          ),
          InkWell(
            onTap: widget.edit == true ? (){} : widget.linkToConfirmed,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 2),
              padding: EdgeInsets.all(9),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Color(0xFFBDBDBD)),
                  color: AppTheme.gLightGrey),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Confirmed",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "${widget.data != null ? widget.data['confirmed']:"0"}",
                      style: TextStyle(fontSize: 18, color: AppTheme.gFont),
                    )
                  ]),
            ),
          ),
          widget.edit == true ? Container() : Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 15, top: 3),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => InviteGolfer(backSummary: true, data: widget.data != null ? widget.data['turnamen']['id_turnamen'] : "0")));
                },
                child: Text(
                  "Invite More",
                  style: TextStyle(color: Colors.red, fontSize: 14),
                ),
              ))
        ],
      ),
    );
  }
}
