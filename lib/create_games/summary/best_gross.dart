import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/utils/Utils.dart';

class BestGross extends StatefulWidget {
  const BestGross({Key key, this.data}) : super(key: key);
  final data;
  @override
  _BestGrossState createState() => _BestGrossState();
}

class _BestGrossState extends State<BestGross> {
  @override
  Widget build(BuildContext context) {
    BestGross args = ModalRoute.of(context).settings.arguments;
    // log("DATA GROSS = ${widget.data}");
    int no = 1;
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Color(0xFFBDBDBD))),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 55.0, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Text(
                    "Rank",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Text(
                    "Certificate",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: Text(
                    "Trophy",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    "Prize",
                    style: TextStyle(color: AppTheme.gFont),
                  ),
                ),
              ],
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: widget.data!=null?widget.data['rule_bg']['winner']:0,
              itemBuilder: (context, index) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, bottom: 10),
                    child: Text(
                      Utils.formatSuffix(index + no),
                      style: TextStyle(color: AppTheme.gGrey),
                    ),
                  ),
                  Padding(
                      padding: index + no == 2
                          ? EdgeInsets.only(left: 16, bottom: 5, top: 8)
                          : EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                      child: SvgPicture.asset(
                          widget.data!=null?widget.data['price_bg'][index]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                              : "assets/svg/check_bold.svg":"",
                          width: 16,
                          height: 16)),
                  Padding(
                      padding: const EdgeInsets.only(
                          left: 25.0, bottom: 5, top: 8),
                      child: SvgPicture.asset(
                        widget.data!=null?widget.data['price_bg'][index]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                            : "assets/svg/check_bold.svg":"",
                        width: 16,
                        height: 16,
                      )),
                  Container(
                      padding: EdgeInsets.only(left: 0),
                      width: 100,
                      child: Text("${widget.data!=null?widget.data['price_bg'][index]['price'] == null || widget.data['price_bg'][index]['price'] == "null" ? "(no prize)" : widget.data['price_bg'][index]['price']:""}", textAlign: TextAlign.center,))
                ],
              )),
        ],
      ),
    );
  }
}
