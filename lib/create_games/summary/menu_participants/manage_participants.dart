import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/caddie/play_game.dart';
import 'package:gif/create_games/invitation/confirmation_page.dart';
import 'package:gif/main.dart';
import 'package:gif/socials/buddys/add_buddy.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../headerCreate.dart';

class ManageParticipants extends StatefulWidget {
  const ManageParticipants({Key key, @required this.idTournament, @required this.typeParticipant}) : super(key: key);
  final idTournament, typeParticipant;
  @override
  _ManageParticipantsState createState() => _ManageParticipantsState();
}

class _ManageParticipantsState extends State<ManageParticipants> {

  bool _isLoading = false;
  String infoHeader = "";

  @override
  void initState() {
    getParticipants();
    if(widget.typeParticipant == "Invited"){
      infoHeader = "click to cancel invitation";
    }else if(widget.typeParticipant == "Applying"){
      infoHeader = "click to confirm or reject golfer's joining request";
    }else{
      infoHeader = "click to remove golfer from the game";
    }
    super.initState();
  }

  var dataParticipants;
  String _noParticipants;
  List<DefaultMembers> _member = [];
  List<CustomMembers> _uiMember = [];
  List<CustomMembers> _allMember = [];
  var idAdmin;

  Future<void> getParticipants() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("ID ${prefs.getString('id')}");

    setState(() {
      _isLoading = true;
      idAdmin = prefs.getString('id');
      _member.clear();
      _uiMember.clear();
      _allMember.clear();
    });

    final _token = await Utils.getToken(context);
    var data = {
      'token' : _token,
      'id_turnamen': widget.idTournament,
      'status': widget.typeParticipant
    };

    Utils.postAPI(data, "get_detail_participant").then((body) {
      if(body['code'] == 200 && body['data'].isNotEmpty){
        setState(() {
          dataParticipants = body['data'];
          _noParticipants = "false";
          for (int i = 0; i < dataParticipants.length; i++) {
            DefaultMembers def = DefaultMembers(
              id: dataParticipants[i]['id'],
              nama_lengkap: dataParticipants[i]["nama_lengkap"],
              nickName: dataParticipants[i]["nama_pendek"],
              kursus: dataParticipants[i]['nama_lapangan'],
              kota: dataParticipants[i]["kota"],
              negara: dataParticipants[i]["negara"],
              gif_id: dataParticipants[i]["kode_user"],
              foto: dataParticipants[i]["foto"],
              buttonAdd: dataParticipants[i]["button_add"],
              allData: dataParticipants[i]
            );
            _member.add(def);
            _allMember.add(CustomMembers(showAll: def));
            _uiMember.add(CustomMembers(showAll: def));
            // print("DATA ${_uiMember[i].showAll.nama_lengkap}");
          }
        });
      }else if(body['code'] == 200 && body['data'].isEmpty){
        setState(() {
          if(widget.typeParticipant == "Applying"){
            _noParticipants = "there's no joining request at this moment.";
          }else{
            _noParticipants = "so lonely here. Have you invited someone?";
          }
        });
      }else{
        Utils.showSnackBar(context, "err", body['message']);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      Utils.showSnackBar(context, "err", 'Ops, something wrong, please try again later.');
      setState(() {
        _isLoading = false;
        print("Error == $error");
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.nickName==null?item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase()):item.showAll.nickName.toLowerCase().contains(mQuery.toLowerCase())
          // || item.showAll.kursus.toLowerCase().contains(mQuery.toLowerCase())
          // || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase())
          // || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase())
          // || item.showAll.gif_id.toLowerCase().contains(mQuery.toLowerCase())
      ).toList();
      // _isLoading = false;
    });
  }
  
  Future<void> cancelOrRemoveInvitation(int idUser, int idGolferInvite) async {
    Navigator.pop(context);
    setState(() {
      _isLoading = true;
    });

    final _token = await Utils.getToken(context);
    var data = {
      'token' : _token,
      'id_turnamen': widget.idTournament,
      'id_user': idUser,
      'id_golfer_invite': idGolferInvite,
      "alasan":""
    };

    //Invited = expired_invitation
    //Confirmed/Applying = cancel_invitation
    Utils.postAPI(data, widget.typeParticipant == "Invited" ? "expired_invitation" : "cancel_invitation").then((body) {
      if(body['status'] == 'success'){
        Utils.showSnackBar(context, "info", body['message']);
        getParticipants();
      }else{
        Utils.showSnackBar(context, "err", body['message']);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      Utils.showSnackBar(context, "err", error);
      setState(() {
        _isLoading = false;
        print("Error == $error");
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getParticipants();
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: SmartRefresher(
            onRefresh: _onRefresh,
            controller: _refreshController,
            child: Container(
              margin: EdgeInsets.all(15),
              child: Column(
                children: [
                  HeaderCreate(title: "${widget.typeParticipant} Golfers",),
                  SizedBox(height: 8,),
                  TextAfterHeader(
                    text: "Manage Your Participants Here",
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    width: 250,
                    height: 40,
                    child: TextFormField(
                      // controller: inputGolferManually,
                      decoration: InputDecoration(
                        hintText: "Search the Participant",
                        isDense: true,
                        contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 3),
                        // icon: Icon(Icons.search_rounded),
                        suffixIcon: Icon(Icons.search_rounded),
                      ),
                      // controller: controllerSearch3,
                      onChanged: (value) {
                        _searchMembers(value);
                      },
                    ),
                  ),
                  SizedBox(height: 8,),
                  Expanded(
                    child: _noParticipants == "false" ? Column(
                      children: [
                        Text("$infoHeader", style: TextStyle(color: AppTheme.bogies, fontStyle: FontStyle.italic),),
                        SizedBox(height: 5,),
                        Expanded(
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              itemCount: _uiMember==null || _uiMember.length <= 0 ? 0 : _uiMember.length,
                              itemBuilder: (context, i){
                                return InkWell(
                                  onTap: (){
                                    if(_uiMember[i].showAll.id.toString() == idAdmin){
                                      Utils.showToast(context, "error", "you cannot kick yourself!");
                                    }else if(_uiMember[i].showAll.allData['button_canceled'] == false){
                                      Utils.showToast(context, "error", "you cannot kick the owner of this game");
                                    } else if(widget.typeParticipant == "Invited"){
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return CustomAlertDialog(
                                            title: "Cancel Invitation",
                                            contentText: "sure want to cancel ${_uiMember[i].showAll.nama_lengkap} invitation?",
                                            noButton: (){
                                              Navigator.pop(context);
                                            },
                                            yesButton: () => cancelOrRemoveInvitation(
                                                _uiMember[i].showAll.allData['user_id'],
                                                _uiMember[i].showAll.allData['id_golfer_invite']),
                                            optionalHeight: 15,
                                          );
                                        },
                                      );
                                    }else if(widget.typeParticipant == "Applying"){
                                      Utils.navLink(context, ConfirmPage(
                                        id: _uiMember[i].showAll.allData,
                                        idNotif: _uiMember[i].showAll.allData['id_notifikasi'],)).then((value) {
                                        setState(() {
                                          getParticipants();
                                        });
                                      });
                                    }else if(widget.typeParticipant == "Confirmed"){
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return CustomAlertDialog(
                                            title: "Remove from Game",
                                            contentText: "sure want to remove ${_uiMember[i].showAll.nama_lengkap} from this game?",
                                            noButton: (){
                                              Navigator.pop(context);
                                            },
                                            yesButton: () => cancelOrRemoveInvitation(
                                                _uiMember[i].showAll.allData['user_id'],
                                                _uiMember[i].showAll.allData['id_golfer_invite']),
                                            optionalHeight: 15,
                                          );
                                        },
                                      );
                                    }else{
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(builder: (BuildContext context) => HomePage()),
                                              (Route<dynamic> route) => false);
                                    }
                                  },
                                  child: GolferCard(
                                    picture: _uiMember[i].showAll.foto,
                                    nickName: _uiMember[i].showAll.nickName,
                                    fullName: _uiMember[i].showAll.nama_lengkap,
                                    course: _uiMember[i].showAll.kursus,
                                    city: _uiMember[i].showAll.kota,
                                    country: _uiMember[i].showAll.negara,
                                    buttonAction: widget.typeParticipant == "Invited"
                                        ? CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Colors.lightBlue,
                                      child: Icon(Icons.mail, color: Colors.white, size: 21,),)
                                        : widget.typeParticipant == "Applying"
                                        ? CircleAvatar(
                                      radius: 15,
                                      backgroundColor: AppTheme.bogies,
                                      child: SvgPicture.asset("assets/svg/warning_orange_rounded.svg", color: Colors.white, height: 25, width: 25,),)
                                        : SvgPicture.asset("assets/svg/games.svg", color: AppTheme.gButton, height: 25, width: 25,),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ) : Center(child: Text(_noParticipants ?? "", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),)),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
