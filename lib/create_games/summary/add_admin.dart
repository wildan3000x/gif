import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/tournament_detail.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddAdmin extends StatefulWidget {
  const AddAdmin({Key key, this.tourId, this.dataTour}) : super(key: key);
  final tourId, dataTour;
  @override
  _AddAdminState createState() => _AddAdminState();
}

class _AddAdminState extends State<AddAdmin> {
  SharedPreferences sharedPreferences;
  var dataTour;
  var dataWinner;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');
  bool _loading = false;

  TextEditingController addAdmin = TextEditingController();

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  final snackbarKey = GlobalKey<ScaffoldState>();

  _addAdmin() async {
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_turnamen' : dataTour == null ? "" : dataTour['id_turnamen'],
      'data_user' : addAdmin.text
    };
    setState(() {
      _loading = true;
    });
    Utils.postAPI(data, "add_TO").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _loading = false;
        });
        print("ADD ADMIN = ${body}");
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SummaryGames(tourId: dataTour['id_turnamen'],)));
        // // dataTransaksi = body["data"];
        // countNotifGolfer = body["count"];
        // print("COUNT : ${countNotifGolfer}");
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    } else {
      _getDataTour();
    }
  }

  void _getDataTour() async {
    // SharedPreferences localStorage = await SharedPreferences.getInstance();
    // var dataJson = localStorage.getString('savetour');
    var data = widget.dataTour;
    dataTour = data;
    setState(() {
      _getSummary();
    });
  }

  void _getSummary() async {
    var data = {
      "id_turnamen": dataTour == null ? "0" : dataTour['id_turnamen'],
      'token': sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_ts").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          dataWinner = body;
        });
      } else {
        // setState(() {
        //   _isLoading = false;
        // });
        print("ERROR");
      }
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Stack(
          children: [
            GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  margin: EdgeInsets.all(15),
                  child: Column(
                    children: [
                      HeaderCreate(title: "Add\nNew Organizer",),
                      Padding(padding: EdgeInsets.symmetric(vertical: 15)),
                      dataTour==null?Container():TournamentDetailWidget(dataTournament: dataTour, additionalWidget: Container(),),
                      SafeArea(child: Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: [
                                // Text("Tournament Admin", style: TextStyle(color: AppTheme.gFont),),
                                Container(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  child: ListView.builder(
                                    primary: false,
                                    shrinkWrap: true,
                                    itemCount: dataWinner!=null?dataWinner['turnamen_organizer'].length:0,
                                    itemBuilder: (context, index) => Column(
                                      children: [
                                        Text("${dataWinner!=null?dataWinner['turnamen_organizer'][index]['nama_lengkap']:""}", style: TextStyle(fontSize: 18),),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 3.0),
                                          child: Text("(GIF ID : ${dataWinner!=null?dataWinner['turnamen_organizer'][index]['kode_user']:""}, PHONE : ${dataWinner!=null?dataWinner['turnamen_organizer'][index]['no_telp']:""})", style: TextStyle(color: AppTheme.gFont),),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(horizontal: 65),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color(0xFF2D9CDB),
                            ),
                            child: Text(
                              "Tournament Officer",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                          ),
                        ],
                      )),
                      Container(
                        // padding: EdgeInsets.symmetric(vertical: 10),
                        child: Column(
                          children: [
                            Text("Add New Tournament Officer", style: TextStyle(color: AppTheme.gFont, fontSize: 18, fontWeight: FontWeight.bold),),
                            Padding(
                              padding: EdgeInsets.only(top:10, bottom: 5),
                              child: Text("GIF ID / Mobile / Email", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 16),),
                            ),
                            Form(
                              child: Container(
                                width: 180,
                                height: 30,
                                child: TextFormField(
                                  controller: addAdmin,
                                  decoration: InputDecoration(
                                    contentPadding: new EdgeInsets.symmetric(vertical: 0.5, horizontal: 10.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 30),
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                splashColor: Colors.grey.withOpacity(0.5),
                                onPressed: () {
                                  setState(() {
                                    _loading = true;
                                    _addAdmin();
                                  });
                                },
                                padding: const EdgeInsets.all(0.0),
                                child: Ink(
                                    decoration: BoxDecoration(
                                        color: AppTheme.gButton,
                                        borderRadius: BorderRadius.circular(100)
                                    ),
                                    child: Container(
                                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                        alignment: Alignment.center,
                                        child: _loading
                                            ? Center(
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              child: CircularProgressIndicator(),
                                            ))
                                            : Text("Submit", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
          ],
        ),
      ),
    );

  }
  _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditGames()),
    );
    setState(() {
      if (result != "0") {
        var data = json.decode(result);
        dataTour = data;
      }
    });
  }
}
