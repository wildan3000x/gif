import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';

class SkillAward extends StatefulWidget {
  const SkillAward({Key key, this.data}) : super(key: key);
  final data;
  @override
  _SkillAwardState createState() => _SkillAwardState();
}

class _SkillAwardState extends State<SkillAward> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "#Hole",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Certificate",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text(
                                    "Trophy",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                                  child: Text(
                                    "Prize",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: widget.data==null?0:widget.data['price_ho'].length,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      width: 42,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 1, color: Colors.grey))),
                                      child: Text("${widget.data['price_ho'][index]['hole']}", textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data['price_ho'][index]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(left: 33.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data['price_ho'][index]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      width: 110,
                                      child: Text("${widget.data['price_ho'][index]['price'] == null || widget.data['price_ho'][index]['price'] == "null" ? "(no prize)" : widget.data['price_ho'][index]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,)
                                  )
                                ],
                              );
                            },
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.symmetric(horizontal: 90),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xFFF15411),
                  ),
                  child: Text(
                    "Hole In One",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "#Hole",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Certificate",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text(
                                    "Trophy",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                                  child: Text(
                                    "Prize",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: widget.data==null?0:widget.data['price_np'].length,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      width: 42,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 1, color: Colors.grey))),
                                      child: Text("${widget.data['price_np'][index]['hole']}", textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data['price_np'][index]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(left: 33.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data['price_np'][index]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      width: 110,
                                      child: Text("${widget.data['price_np'][index]['price'] == null || widget.data['price_np'][index]['price'] == "null" ? "(no prize)" : widget.data['price_np'][index]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis)
                                  )
                                ],
                              );
                            },
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.symmetric(horizontal: 70),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xFFF2C94C),
                  ),
                  child: Text(
                    "Nearest to the Pin",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          widget.data['price_nl'].isEmpty ? Container() : Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "#Hole",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Certificate",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text(
                                    "Trophy",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                                  child: Text(
                                    "Prize",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: 1,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      width: 42,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 1, color: Colors.grey))),
                                      child: Text("${widget.data==null||widget.data['price_nl'].isEmpty?"":widget.data['price_nl'][index]['hole']}", textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data==null||widget.data['price_nl'].isEmpty?"assets/svg/uncheck.svg":widget.data['price_nl'][index]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(left: 33.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data==null||widget.data['price_nl'].isEmpty?"assets/svg/uncheck.svg":widget.data['price_nl'][index]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      width: 110,
                                      child: Text("${widget.data==null||widget.data['price_nl'].isEmpty?"":widget.data['price_nl'][index]['price'] == null || widget.data['price_nl'][index]['price'] == "null" ? "(no prize)" : widget.data['price_nl'][index]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis)
                                  )
                                ],
                              );
                            },
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.symmetric(horizontal: 70),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xFF2D9CDB),
                  ),
                  child: Text(
                    "Nearest to the Line",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          widget.data['price_ld'].isEmpty ? Container() : Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(15)),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "#Hole",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: Text(
                                    "Certificate",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  child: Text(
                                    "Trophy",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                                  child: Text(
                                    "Prize",
                                    style: TextStyle(color: AppTheme.gFont),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: 1,
                            itemBuilder: (context, index) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 0.0),
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      width: 42,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  width: 1, color: Colors.grey))),
                                      child: Text("${widget.data==null||widget.data['price_ld'].isEmpty?"":widget.data['price_ld'][index]['hole']}", textAlign: TextAlign.center,),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 20.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data==null||widget.data['price_ld'].isEmpty?"assets/svg/uncheck.svg":widget.data['price_ld'][index]['sertifikat'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(left: 33.0, bottom: 5, top: 8),
                                      child: SvgPicture.asset(
                                        widget.data==null||widget.data['price_ld'].isEmpty?"assets/svg/uncheck.svg":widget.data['price_ld'][index]['tropi'] == 0 ? "assets/svg/uncheck.svg"
                                            : "assets/svg/check_bold.svg",
                                        width: 16,
                                        height: 16,)
                                  ),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      width: 110,
                                      child: Text("${widget.data==null||widget.data['price_ld'].isEmpty?"":widget.data['price_ld'][index]['price'] == null || widget.data['price_ld'][index]['price'] == "null" ? "(no prize)" : widget.data['price_ld'][index]['price']}", textAlign: TextAlign.center, overflow: TextOverflow.ellipsis)

                                  )
                                ],
                              );
                            },
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.symmetric(horizontal: 80),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: AppTheme.gFont,
                  ),
                  child: Text(
                    "Longest Drive",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
