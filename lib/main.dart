import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/caddie/represent_golfer.dart';
import 'package:gif/create_games/create_games.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/header_home.dart';
import 'package:gif/models/NavItem.dart';
import 'package:gif/my_games/see_all_my_next_games_and_recent_score.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/error_page.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'auth/profile/profile_page.dart';
import 'create_games/pairing/pairing_games.dart';
import 'join_games/join_games.dart';
import 'my_games/my_games.dart';
import 'my_games/scorecards/my_score.dart';
import 'navbar/bottomNavBar.dart';
import 'dart:io' show Platform;
import 'package:connectivity/connectivity.dart';

import 'socials/buddys/add_buddy.dart';
import 'widget/caddie_card.dart';
import 'widget/custom_button.dart';


class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  HttpOverrides.global = MyHttpOverrides();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  var tokenLogin = prefs.getString('token');
  var level = prefs.getString('level');
  print(tokenLogin);
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(ChangeNotifierProvider(
    create: (context) => NavItems(),
    child: MaterialApp(
      title: 'GIF',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        scaffoldBackgroundColor: Colors.white,
        fontFamily: "Muli",
        appBarTheme: appBarTheme(),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/' : (context) => HomePage(),
        '/login' : (context) => LoginPage(),
        '/summary' : (context) => SummaryGames(),
        '/homeCaddie' : (context) => HomeCaddie(),
      },
      initialRoute: tokenLogin == null ? '/login' : level == 'Caddie' ? '/homeCaddie' : '/',
      builder: (BuildContext context, Widget child) {
        ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
          return CustomError(errorDetails: errorDetails);
        };
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
          child: child,
        );
      },
      // home: ScorePage(),
      //   home: tokenLogin == null ? LoginPage() : HomePage(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: Colors.white,
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: Colors.black),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18),
    ),
  );
}

class HomePage extends StatefulWidget {
  HomePage({/*this.app,*/ this.navbar});
  // final FirebaseApp app;
  final navbar;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  SharedPreferences sharedPreferences;
  bool _isLoading = false;
  bool _isLoading2 = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  var dataTransaksi;
  bool _isCheck = false;
  int countNotifGolfer;
  int countNotifOrga;
  // DatabaseError _error;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getAllData();
    }
  }

  var countRunningNotif;
  var dataRecentScoreBuddy;
  var dataTournament;
  var dataCaddieRequest;
  var dataCaddieConfirm;

  getAllData() async {
    setState(() {
      _isLoading2 = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "main_tournament").then((body) {
      if(body['status'] == 'success'){
        dataTournament = body;
        dataRecentScoreBuddy = body;
        dataCaddieRequest = body['caddie_request'];
        dataCaddieConfirm = body['caddie_confirm'];
        countNotifGolfer = body['notifikasi_count'];
        countRunningNotif = body['notifikasi_rg_count'];
        setState(() {
          _isLoading2 = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading2 = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading2 = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getAllData();
    // getSumRunningNotif();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    // getDataNotFromFirebaseButFromLocal();
    getAllData();
    if(mounted)
      setState(() {
        getAllData();
        // getSumRunningNotif();
      });
    _refreshController.loadComplete();
  }

  bool _seeAll = false;
  bool _seeAllPromo = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var itemList = MediaQuery.of(context).size.width;
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(main: true,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: Stack(
            children: [
              SmartRefresher(
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    margin: EdgeInsets.all(13),
                    child: Column(
                      children: [
                        HeaderHome(
                          countNotifGolfer: countNotifGolfer,
                          countRunningNotif: countRunningNotif,
                          count1: countNotifGolfer,
                          context: context,
                          text: "Home",
                          page: "home",
                          otherOnTapFunction: [
                            () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyGames())),
                            () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SearchJoinGames())).then((value) {
                                  getAllData();
                                }),
                            () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CreateGames())),
                          ],
                        ),
                        _isLoading2 ? LoadingMenu(withoutMargin: true,) :
                        Column(
                          children: [
                            dataTournament == null || dataTournament['turnamen_berjalan'].isEmpty
                                ? Container()
                                : Column(
                              children: [
                                SizedBox(height: 8,),
                                Column(
                                  children: [
                                    Text("there's one running game right now,",
                                      style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack),),
                                    InkWell(
                                      onTap: (){
                                        Utils.navLink(context, MyGames());
                                      },
                                      child: Text("click here to view!", style: TextStyle(color: AppTheme.gButton,
                                          fontStyle: FontStyle.italic, fontWeight: FontWeight.bold, fontSize: 16),),
                                    )
                                  ],
                                ),
                                // Text("there's a game running right now, click here to view!", style: TextStyle(fontStyle: FontStyle.italic),)
                              ],
                            ),
                            // SizedBox(height: 5),
                            listNextGames(),
                            dataCaddieRequest==null||dataCaddieRequest.isEmpty ? Container() : dataCaddieList(data: dataCaddieRequest, type: "Caddy's Request"),
                            dataCaddieConfirm==null||dataCaddieConfirm.isEmpty ? Container() : dataCaddieList(data: dataCaddieConfirm, type: "Your Caddie"),
                            buddiesRecentScore(itemList),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  // GlobalKey _toolTipKey = GlobalKey();
  List <GlobalKey> _toolTipKey = [];

  Column dataCaddieList({String type, data}) {
    return Column(
      children: [
        TitleListMenu(text: "$type"),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: data==null?0:data.length,
          itemBuilder: (context, i){
            _toolTipKey.add(GlobalKey());
            return Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.only(bottom: 4),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF9F9F9),
                      border: Border.all(
                          color: Color(0xFFE5E5E5)
                      )
                  ),
                  child: FlatButton(
                    padding: EdgeInsets.all(5),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    splashColor: Colors.grey.withOpacity(0.2),
                    textTheme: ButtonTextTheme.normal,
                    onPressed: () {
                      if(type == "Caddy's Request") {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) =>
                                RepresentGolfer(
                                  role: "Golfer", dataGolferCaddie: data[i], fromHomeGolfer: true,)));
                      }else if(type == "Your Caddie"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(userId: data[i]['caddie_id'], forCaddie: data[i],)));
                      }
                    },
                    child: data[i]['cek_turnamen'] == "tidak ada" ? Row(
                      children: [
                        data[i]['foto'] == null ? Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.asset(
                              "assets/images/userImage.png",
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + data[i]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                        SizedBox(width: 8,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text("${data[i]['nama_lengkap']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),),
                              Text("${data[i]['nama_lapangan']}, ${data[i]['kota']}, ${data[i]['negara']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                              Text("Appointment date: ${data[i]['tanggal_janji']}", style: TextStyle(color: AppTheme.par, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),),
                            ],
                          ),
                        )
                      ],
                    ) :
                    CaddieCard(
                      dataTournament: data[i]['data_turnamen'],
                      dataCaddie: data[i],
                      withRequestedText: null,
                    ),
                  ),
                ),
                type == "Your Caddie" && data[i]['turnamen_selesai'] == true ? Positioned(top: 0, right: 0, child: GestureDetector(
                  onTap: (){
                    final dynamic tooltip = _toolTipKey[i].currentState;
                    tooltip.ensureTooltipVisible();
                  },
                    child: Tooltip(key: _toolTipKey[i], message: "Leave a feedback to this Caddie first.", child: iconWarningRed(null)))) : Container()
              ],
            );
          },
        ),
        type == "Your Caddie" ? Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            iconWarningRed(18),
            SizedBox(width: 3,),
            Text("Feedback Required", style: TextStyle(color: Color(0xFF828282), fontStyle: FontStyle.italic, fontSize: 13),),
          ],
        ) : Container(),
      ],
    );
  }

  Icon iconWarningRed(double size) => Icon(Icons.error_outlined, color: Colors.red, size: size,);


  Column listNextGames() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TitleListMenu(text: "My Next Games",),
            dataTournament==null||dataTournament['nextgames']==null||dataTournament['nextgames'].length<6
                ? Container()
                : SeeAllButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    SeeAllMyNextGamesAndRecentScore(screenCategory: "my next games",))).then((value) => setState(() {
                  getAllData();
                  print("RELOAD");
                }));
              },
            )
          ],
        ),
        // onRefresh: getDataNotFromFirebaseButFromLocal,
        dataTournament==null||dataTournament['nextgames']==null||dataTournament['nextgames'].length<=0 ?
        Container(margin: EdgeInsets.all(45), child: Center(child: Text("You have no invitations."))) :
        listViewNextGames(),
        ConfirmedNotConfirmedText(withPencilIcon: true,),
      ],
    );
  }

  Column buddiesRecentScore(double itemList) {
    return Column(
      children: [
        SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TitleListMenu(text: "Buddies Recent Scorecards"),
            dataRecentScoreBuddy==null||dataRecentScoreBuddy['recent_score'].isEmpty
                ? Container()
                : SeeAllButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => AddBuddy(buddyScore: true,))).then((value) => getAllData());
              },
            ),
          ],
        ),
        Container(
          height: 106,
          width: itemList,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
                image: AssetImage("assets/images/bg_gradient_buddy.png"),
                fit: BoxFit.fill
            ),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: dataRecentScoreBuddy==null||dataRecentScoreBuddy['recent_score'].isEmpty? Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Add some buddies to see their scores here!", style: TextStyle(color: Colors.white),),
                SizedBox(height: 8,),
                InkWell(
                  splashColor: Colors.grey.withOpacity(0.2),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddBuddy(buddyScore: false,)));
                  },
                  child: Text("Add Buddy", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 18),),
                )
              ],
            ),
          ) : ListView.builder(
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: dataRecentScoreBuddy==null?0:dataRecentScoreBuddy['recent_score'].length>4?4:dataRecentScoreBuddy['recent_score'].length,
            itemBuilder: (context, index){
              return Tooltip(
                message: dataRecentScoreBuddy['recent_score'][index]['nama_pendek']!=null?dataRecentScoreBuddy['recent_score'][index]['nama_pendek']+", "+dataRecentScoreBuddy['recent_score'][index]['nama_lengkap']:dataRecentScoreBuddy['recent_score'][index]['nama_lengkap'],
                child: Container(
                  width: itemList/4.6,
                  child: InkWell(
                    splashColor: Colors.grey.withOpacity(0.2),
                    onTap: (){
                      if(dataRecentScoreBuddy['recent_score'][index]['turnamen']!=null) {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) =>
                              MyScore(
                                idTour: dataRecentScoreBuddy['recent_score'][index]['turnamen']['turnamen_id'],
                                buddyScore: true,
                                buddyToken: dataRecentScoreBuddy['recent_score'][index]['token'],
                              ),),
                        );
                      }
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(dataRecentScoreBuddy['recent_score'][index]['nett'].toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 3.0),
                          child: dataRecentScoreBuddy['recent_score'][index]['foto'] == null ? ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.asset(
                              "assets/images/userImage.png",
                              width: 50,
                              height: 50,
                            ),
                          ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                              CircularProgressIndicator(
                                value: progress.progress,
                              ), imageUrl: Utils.url + 'upload/user/' + dataRecentScoreBuddy['recent_score'][index]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 1),
                          child: Text(dataRecentScoreBuddy['recent_score'][index]['nama_lengkap'], overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }

  Widget listViewNextGames() {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: dataTournament==null?0:dataTournament['nextgames'].length<6?dataTournament['nextgames'].length:5,
        itemBuilder: (context, index) => ListContainerHome(
          date: dataTournament['nextgames'][index]['tanggal_turnamen'],
          nameTour: dataTournament['nextgames'][index]['nama_turnamen'],
          courseTour: dataTournament['nextgames'][index]['nama_lapangan'],
          cityTour: dataTournament['nextgames'][index]['kota_turnamen'],
          orgaTour: dataTournament['nextgames'][index]['penyelenggara_turnamen'],
          link: () {
            if (dataTournament['nextgames'][index]["status_create"] == true) {
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  SummaryGames(tourId: dataTournament['nextgames'][index]['id_turnamen'])));
            } else if(dataTournament['nextgames'][index]["status"] == "Applying" || dataTournament['nextgames'][index]['status'] == "Not Confirmed"){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  PairingGames(
                    data: dataTournament['nextgames'][index]['status'],
                    msg: dataTournament['nextgames'][index]['status'] == "Applying" ? "Waiting For Approval" : dataTournament['nextgames'][index]['status'] == "Not Confirmed" ? "You have been Rejected" : null,
                    sub_msg: dataTournament['nextgames'][index]['status'] == "Applying" ? "You still need approval from Organizer before join this game" : dataTournament['nextgames'][index]['status'] == "Not Confirmed" ? "your request to enter this game has been denied\nso you cannot enter this game." : null,
                    id: dataTournament['nextgames'][index]['id_turnamen'],
                    userId: dataTournament['nextgames'][index]['user_id'],))).then((value) => setState(() {
                getAllData();
                print("RELOAD");
              }));
            } else if(dataTournament['nextgames'][index]["status"] == "Invited"){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  JoinGames(data: dataTournament['nextgames'][index]))).then((value) => setState(() {
                getAllData();
                print("RELOAD");
              }));
            } else{
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  StartGame(
                    data: dataTournament['nextgames'][index],
                    status: dataTournament['nextgames'][index]['status'],
                    id: dataTournament['nextgames'][index]['id_turnamen'],
                    userId: dataTournament['nextgames'][index]['user_id'],
                  )));
            }
          },
          widgetCorner: dataTournament['nextgames'][index]["status_create"] == true ?
                        Icon(Icons.circle, color: Color(0xFFff8000), size: 20,) :
                        SvgPicture.asset("assets/svg/check.svg", width: 18, height: 18, color: dataTournament['nextgames'][index]["status"] ==  "Confirmed" ? AppTheme.gFont : Color(0xFFF15411),),
        )
    );
  }
}

class SeeAllButton extends StatelessWidget {
  final onPressed;
  const SeeAllButton({
    Key key, @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 0, top: 5),
      child: SizedBox(
        height: 30,
        child: TextButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.zero),
          ),
          child: Text("See All"),
          onPressed: onPressed,
        ),
      )/*InkWell(
        splashColor: Colors.grey.withOpacity(0.2),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => AddBuddy(buddyScore: true,))).then((value) => getAllData());
        },
        child: Text("See All"),
      )*/,
    );
  }
}


class LoadingMenu extends StatelessWidget {
  const LoadingMenu({
    Key key, this.withoutMargin
  }) : super(key: key);

  final withoutMargin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: withoutMargin == true ? EdgeInsets.zero : EdgeInsets.all(45),
      height: withoutMargin == true ? MediaQuery.of(context).size.height/2 : null,
      child: Center(
      // heightFactor: withoutMargin == true ? 0 : 15,
      child: Container(width: 20, height: 20, child: CircularProgressIndicator(strokeWidth: 3.5,),),),
    );
  }
}


