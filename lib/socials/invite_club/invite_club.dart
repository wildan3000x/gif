import 'package:collection/collection.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/socials/club/home_club.dart';
import 'package:gif/socials/club/view_members.dart';
import 'package:gif/socials/logo_and_name.dart';
import 'package:gif/utils/Utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show Platform;

class InviteClub extends StatefulWidget {
  const InviteClub({Key key, this.dataClub, this.msg, this.success, this.fromAllMember, this.dataAll}) : super(key: key);
  final dataClub, msg, success, fromAllMember, dataAll;
  @override
  _InviteClubState createState() => _InviteClubState();
}

class _InviteClubState extends State<InviteClub> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var dataCreateClub;
  String _valTypeInv = 'My Phone Contacts';

  bool _loading = false;
  bool _tabSelectMem = false;
  bool _checkMember = false;
  bool _checkMember2 = false;
  bool _checkMember3 = false;
  bool _successScreen = false;
  bool _isLoading = false;

  String _tipeInv = "1";
  String _valSearchCM;
  int _valGroup;

  double sizeH;
  int addLine;

  TextEditingController searchCM = TextEditingController();

  List<Widget> arrAddText = List<Widget>();
  List<TextEditingController> names = List<TextEditingController>();
  List<TextEditingController> numbs = List<TextEditingController>();

  List<DefaultGroups> _group = new List<DefaultGroups>();
  List<CustomGroup> _uiGroup = List<CustomGroup>();
  List<CustomGroup> _allGroup = List<CustomGroup>();

  List<DefaultBuddies> _buddies = new List<DefaultBuddies>();
  List<CustomBuddies> _uiBuddies = List<CustomBuddies>();
  List<CustomBuddies> _allBuddies = List<CustomBuddies>();

  List<Contact> _contacts = new List<Contact>();
  List<CustomContact> _uiCustomContacts = List<CustomContact>();
  List<CustomContact> _allContacts = List<CustomContact>();
  List<bool> arrSelected2;
  List<bool> arrSelected3;
  List<String> arrNameSelected = new List();
  List<String> arrIdSelected = new List();
  List<String> arrNumberSelected = new List();
  TextEditingController controllerSearch = TextEditingController();
  TextEditingController controllerSearch2 = TextEditingController();
  TextEditingController controllerSearch3 = TextEditingController();


  @override
  void initState() {
    dataCreateClub = widget.dataClub;
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    // checkLoginStatus();
    getPermission();
    getInvitation();
    super.initState();
  }

  SharedPreferences sharedPreferences;

  void getInvitation() async {
    sharedPreferences = await SharedPreferences.getInstance();
    getBuddies();
    getOtherClub();
    setArrText(0);
  }

  void getPermission() async {
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus.isGranted){
      refreshContacts();
      // getGroup();
      getContact();
    }else {
      showDialog(
          context: context,
          builder: (BuildContext context) => Platform.isIOS ? CupertinoAlertDialog(
            title: Text('Permissions Not Granted'),
            content: Text('Please enable contacts access permission in system app settings'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ) : AlertDialog(
            title: Text('Permissions Not Granted'),
            content: Text('Please enable contacts access permission in system app settings'),
            actions: <Widget>[
              ElevatedButton(
                child: Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ));
    }
  }

  var contacts2;

  void getContact() async {
    var data = {
      'tipe_invite' : 'My Contacts',
      'id_golf_club' : widget.dataClub!=null?widget.dataClub['id_club_golf']:"0"
    };

    Utils.postAPI(data, "get_Contact").then((body) {
      if (body['status'] == 'success') {
        contacts2 = body["sudah_invite"];
      } else {
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var dataOtherClub;
  List<dynamic> _dataClub = List();
  bool _isEmptyClub = false;

  void getOtherClub() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'tipe_invite' : 'Members of My Other Club',
      'id_golf_club' : widget.dataClub!=null?widget.dataClub['id_club_golf']:"0",
    };

    Utils.postAPI(data, "get_GC").then((body) {
      if (body['status'] == 'success') {
        dataOtherClub = body["data"];
        setState(() {
          getGroup(dataOtherClub[0]['id_club_golf']);
          _dataClub  = dataOtherClub;
          _isEmptyClub = false;
        });
      } else {
        print(body['message']);
        setState(() {
          _isEmptyClub = true;
        });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var getIdGroup;
  bool emptyClub = false;

  void getGroup(int id) async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'id_golf_club' : widget.dataClub!=null?widget.dataClub['id_club_golf']:"0",
      'id_golf_club_lain' : id
    };

    Utils.postAPI(data, "get_UGC").then((body) {
      setState(() {
        _isLoading = false;
      });
      if (body['status'] == 'success') {
        var l = body["data"];
        // print("DATA GOLFER $l");
        setState(() {
          _group.clear();
          _allGroup.clear();
          _uiGroup.clear();
          getIdGroup = body['id_club_golf'];
          emptyClub = false;
          for (int i = 0; i < l.length; i++) {
            DefaultGroups def = DefaultGroups(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l.contains(l[i]['status_invite']) ? "null" : l[i]['status_invite']);
            _group.add(def);
            _allGroup.add(CustomGroup(showAll: def, isChecked: false));
            _uiGroup.add(CustomGroup(showAll: def, isChecked: false));
          }
        });
      } else {
        setState(() {
          getIdGroup = body['id_club_golf'];
          _group.clear();
          _allGroup.clear();
          _uiGroup.clear();
          emptyClub = true;
        });
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  bool emptyBuddy = false;

  void getBuddies() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'tipe_invite' : 'My Buddies',
      'id_golf_club' : widget.dataClub!=null?widget.dataClub['id_club_golf']:"0"
    };

    Utils.postAPI(data, "get_Buddies").then((body) {
      if (body['status'] == 'success') {
        var l = body["data"];
        // print("DATA GOLFER $l");
        setState(() {
          for (int i = 0; i < l.length; i++) {
            DefaultBuddies def = DefaultBuddies(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l.contains(l[i]['status_invite']) ? "null" : l[i]['status_invite']);
            // DefaultBuddies def = DefaultBuddies(id: l[i]["id"].toString(), nama_lengkap: l[i]["nama_lengkap"], no_telp: l[i]["no_telp"], invited: l[i]['status_invite']);
            _buddies.add(def);
            _allBuddies.add(CustomBuddies(showAll: def, isChecked: false));
            _uiBuddies.add(CustomBuddies(showAll: def, isChecked: false));
          }
          emptyBuddy = false;
        });
      } else {
        setState(() {
          emptyBuddy = true;
        });
        print(body['message']);
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  var validGroup;
  bool _successInv = false;
  var textSuccessInv;

  _invite() async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'club_golf_id' : widget.dataClub!=null?widget.dataClub['id_club_golf']:"0",
      'tipe_invite' : _valTypeInv,
      'golf_club' : _valGroup,
      'tipe_im' : _tipeInv,
      'user_id' : arrIdSelected,
      'nama' : arrNameSelected,
      'no_telp' : arrNumberSelected
    };

    Utils.postAPI(data, "club_invite_golfer").then((body) {
      if(body['status'] == 'success'){
        textSuccessInv = body['body_message'];
        setState(() {
          _loading = false;
          _successInv = true;
        });
        // Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, animation1, animation2) => InviteClub(dataClub: widget.dataId, msg: body['body_message'], success: 'success', dataAll: widget.dataAll, fromAllMember: widget.fromAllMember,)));
      }else{
        print(body['body_message']);
        setState(() {
          _loading = false;
        });
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        arrIdSelected.clear();
        arrNameSelected.clear();
        arrNumberSelected.clear();
      });
    }, onError: (error) {
      setState(() {
        arrIdSelected.clear();
        arrNameSelected.clear();
        arrNumberSelected.clear();
      });
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void _handleInv(String value) {
    setState(() {
      _tipeInv = value;
    });

    if (value == '2') {
      setState(() {
        _tabSelectMem = true;
      });
    } else {
      setState(() {
        _tabSelectMem = false;
      });
    }
  }

  void setArrText (int index) {
    names.add(TextEditingController());
    numbs.add(TextEditingController());
    arrAddText.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: TextFormField(controller: names[index],)),
        Padding(padding: EdgeInsets.only(left: 15)),
        Expanded(child: TextFormField(controller: numbs[index],)),
      ],
    ));
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(13),
            child: Column(
              children: [
                HeaderCreate(title: 'Golf Club',),
                ImageAndNameClub(dataMyClub: dataCreateClub),
                _successInv ?
                successScreen(context) : selectMenuInvite(),
                _successInv ? Container() : MenuInv()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget MenuInv() {
    // print("ID = ${widget.data['turnamen']['id_turnamen']}");
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = size.width/12;
    final double item2Height = size.width/6;
    final double itemWidth = size.width / 2;
    return Container(
      child: Form(
        child: Column(
          children: [
            _valTypeInv == 'Members of My Other Club' ? _isEmptyClub ? Container(
              padding: EdgeInsets.only(top: 25),
              child: Text("You have'nt joined any other club yet.", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
            ) : dropdownGroupType() : Container(),
            _valTypeInv == 'Members of My Other Club' || _valTypeInv == 'My Buddies in GIF' ? _valTypeInv == 'Members of My Other Club' && _isEmptyClub ? Container() : _valTypeInv == "My Buddies in GIF" && emptyBuddy ? Container(margin: EdgeInsets.only(top: 25), child: Text(emptyBuddy?"You have no buddies.":"", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),) : radioButtonMenu() : Container(),
            _valTypeInv == 'My Phone Contacts' || _valTypeInv == 'Add Manually' ? Padding(padding: EdgeInsets.only(top: 25)) : Container(),
            //radio select members
            _valTypeInv == "My Phone Contacts" ? searchMember(itemWidth, itemHeight) : Container(),
            _valTypeInv == "My Phone Contacts" ? Container() :
            _valTypeInv == "Add Manually" ?
            formAddManually(itemWidth, item2Height)
                : _tabSelectMem
                ? searchMember(itemWidth, itemHeight)
                : Container(),
            Button()
          ],
        ),
      ),
    );
  }

  Container formAddManually(double itemWidth, double item2Height) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 8)),
              Text("Name", style: TextStyle(color: AppTheme.gFont),),
              Padding(padding: EdgeInsets.only(left: 20)),
              Text("Mobile Phone Number", style: TextStyle(color: AppTheme.gFont),)
            ],
          ),
          Container(
            // height: 200,
            child: ListView.builder(
                itemCount: arrAddText.length<=10?arrAddText.length:10,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (context, index){
                  return arrAddText[index];
                }
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset("assets/svg/+.svg"),
                Padding(padding: EdgeInsets.only(left: 8)),
                InkWell(onTap: () {
                  setState(() {
                    setArrText(arrAddText.length);
                  });
                }, child: Text("Add More Line", style: TextStyle(color: Color(0xFFF15411)),))
              ],
            ),
          )
        ],
      ),
    );
  }

  Container searchMember(double itemWidth, double itemHeight) {
    return Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Text(
              _valTypeInv == 'My Phone Contacts' ? "Search Contact" : "Search Club Member",
              style: TextStyle(color: AppTheme.gFont),
            ),
            Container(
                width: 230,
                child: _valTypeInv == 'My Phone Contacts' ?
                //Conatct
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    suffixIcon: Icon(Icons.search),
                  ),
                  controller: controllerSearch,
                  onChanged: (value) {
                    _searchContacts(value);
                  },
                )
                    : _valTypeInv == "My Buddies in GIF" ?
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    suffixIcon: Icon(Icons.search),
                  ),
                  controller: controllerSearch3,
                  onChanged: (value) {
                    _searchBuddies(value);
                  },
                ) :TextFormField(
                  decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    suffixIcon: Icon(Icons.search),
                  ),
                  controller: controllerSearch2,
                  onChanged: (value) {
                    _searchGroup(value);
                  },
                )
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      if (_valTypeInv == "My Phone Contacts"){
                        setState(() {
                          _allContacts.forEach((element) { element.isChecked = true; });
                          _uiCustomContacts = _allContacts;
                        });
                      }else if(_valTypeInv == "Members of My Other Club"){
                        setState(() {
                          _allGroup.forEach((element) { element.isChecked = true; });
                          _uiGroup = _allGroup;
                        });
                      }else if(_valTypeInv == "My Buddies in GIF"){
                        setState(() {
                          _allBuddies.forEach((element) { element.isChecked = true; });
                          _uiBuddies = _allBuddies;
                        });
                      }else {
                        setState(() {
                          arrSelected2.fillRange(0, arrSelected2.length, true);
                          arrSelected3.fillRange(0, arrSelected3.length, true);
                        });
                      }
                    },
                    child: Text(
                      "Select All",
                      style: TextStyle(
                          color: AppTheme.gFont, fontSize: 18),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (_valTypeInv == "My Phone Contacts"){
                          setState(() {
                            _allContacts.forEach((element) { element.isChecked = false; });
                            _uiCustomContacts = _allContacts;
                          });
                        }else if(_valTypeInv == "Members of My Other Club"){
                          setState(() {
                            _allGroup.forEach((element) { element.isChecked = false; });
                            _uiGroup = _allGroup;
                          });
                        }else if(_valTypeInv == "My Buddies in GIF"){
                          setState(() {
                            _allBuddies.forEach((element) { element.isChecked = false; });
                            _uiBuddies = _allBuddies;
                          });
                        }else {
                          arrSelected2.fillRange(0, arrSelected2.length, false);
                          arrSelected3.fillRange(0, arrSelected3.length, false);
                        }
                      });
                    },
                    child: Text(
                      "Deselect All",
                      style: TextStyle(
                          color: AppTheme.gFont, fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
            _isLoading && _valTypeInv == "My Phone Contacts" || _isLoading && _valTypeInv == "Members of My Other Club" ? Padding(
              padding: const EdgeInsets.all(25.0),
              child: Center(
                  child: Container(
                    width: 20,
                    height: 20,
                    child: CircularProgressIndicator(),
                  )),
            ) :
            boxInvitation(itemWidth, itemHeight),
            Container(
              padding: EdgeInsets.only(top: 10),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/svg/check.svg',
                    color: AppTheme.gGrey,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "Already Invited",
                      style: TextStyle(
                        color: AppTheme.gGrey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Padding boxInvitation(double itemWidth, double itemHeight) {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: Container(
        height: 130,
        child: _valTypeInv == "Members of My Other Club" && emptyClub ? Container(margin: EdgeInsets.all(50), child: Text("No Member in this group.", style: TextStyle(color: AppTheme.gFontBlack),),) :
        _valTypeInv == "My Buddies in GIF" && emptyBuddy ? Container(margin: EdgeInsets.all(50), child: Text("You have no buddy.", style: TextStyle(color: AppTheme.gFontBlack),),) :
        GridView.count(
          crossAxisCount: 2,
          padding: EdgeInsets.only(right: 25),
          crossAxisSpacing: 20,
          mainAxisSpacing: 10,
          childAspectRatio: (itemWidth / itemHeight),
          children: _valTypeInv == "My Phone Contacts" ? List.generate(_uiCustomContacts==null || _uiCustomContacts.length <= 0 ? 0 : _uiCustomContacts.length, (index) {
            // var _phonesList = _contact.contact.phones.toList();
            return _buildListTile(_uiCustomContacts[index]/*, _phonesList*/);
          }) : _valTypeInv == "My Buddies in GIF" ? List.generate(_uiBuddies==null || _uiBuddies.length <= 0 ? 0 : _uiBuddies.length, (index) {
            CustomBuddies budie = _uiBuddies?.elementAt(index);
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                    padding:
                    EdgeInsets.only(top: 5, right: 10)),
                InkWell(
                  onTap: () {
                    setState(() {
                      if (budie.isChecked == false) {
                        budie.isChecked = true;
                      } else {
                        budie.isChecked = false;
                      }
                    });
                  },
                  child: _uiBuddies[index].showAll.invited == "sudah" ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : budie.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
                ),
                Flexible(
                  child: Container(
                    padding:
                    const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "${_uiBuddies[index].showAll.nama_lengkap}",
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      softWrap: false,
                      style: TextStyle(
                          color: _checkMember2
                              ? AppTheme.gGrey
                              : Color(0xFF828282)),
                    ),
                  ),
                )
              ],
            );
          }) : List.generate(_uiGroup==null || _uiGroup.length < 0 ? 0 : _uiGroup.length, (index) {
            CustomGroup group = _uiGroup?.elementAt(index);
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                    padding:
                    EdgeInsets.only(top: 5, right: 10)),
                InkWell(
                  onTap: () {
                    setState(() {
                      if (group.isChecked == false) {
                        group.isChecked = true;
                      } else {
                        group.isChecked = false;
                      }
                    });
                  },
                  child: _uiGroup[index].showAll.invited == "sudah" ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : group.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
                  // child: _uiGroup[index].showAll.invited == "sudah" ? group.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,),
                ),
                Flexible(
                  child: Container(
                    padding:
                    const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "${_uiGroup[index].showAll.nama_lengkap}",
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      softWrap: false,
                      style: TextStyle(
                          color: _checkMember3
                              ? AppTheme.gGrey
                              : Color(0xFF828282)),
                    ),
                  ),
                )
              ],
            );
          }),
        ),
      ),
    );
  }

  Row radioButtonMenu() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          width: 15,
          child: Radio(
            value: "1",
            groupValue: _tipeInv,
            onChanged: _handleInv,
          ),
        ),
        Text(
          _valTypeInv == 'My Buddies in GIF' ? "Invite All Buddies" : "Invite All Member",
          style: TextStyle(color: Color(0xFF837B7B)),
        ),
        Padding(padding: EdgeInsets.only(right: 25)),
        SizedBox(
          width: 15,
          child: Radio(
            value: "2",
            groupValue: _tipeInv,
            onChanged: _handleInv,
          ),
        ),
        Text(
            _valTypeInv == 'My Buddies in GIF' ? "Select Buddies" : "Select Members",
            style: TextStyle(color: Color(0xFF837B7B)))
      ],
    );
  }

  Column dropdownGroupType() {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.only(top: 8),
            alignment: Alignment.centerLeft,
            child: Text(
              "My Golf Club",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: _dataClub == null || _dataClub.isEmpty ? Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: LinearProgressIndicator(),
          ) : DropdownButton(
            isExpanded: true,
            underline: SizedBox(),
            items: _dataClub.map((value) {
              return DropdownMenuItem(
                value: value['id_club_golf'],
                child: Text(value['nama_club']),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                print("DDD $value");
                _valGroup = value;
                if(value != getIdGroup) {
                  getGroup(value);
                }
              });
            },
            value: _valGroup,
            hint: Text(
              "${_dataClub[0]['nama_club']}",
              textAlign: TextAlign.center,
            ),
            // value: _valWinners,
          ),
        ),
      ],
    );
  }

  Padding Button() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
              splashColor: Colors.grey.withOpacity(0.5),
              onPressed: () {
                if(widget.fromAllMember == true){
                  // Navigator.pushReplacement(context, MaterialPageRoute(
                  //     builder: (context) =>
                  //         ViewMembers(dataClub: widget.dataAll,)));
                  Navigator.pop(context, 'reload');
                }else{
                  Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) =>
                          HomeClub(idClub: widget.dataClub['id_club_golf'],)));
                }
              },
              padding: const EdgeInsets.all(0.0),
              child: Ink(
                  decoration: BoxDecoration(
                      color: AppTheme.gButton,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: Container(
                      constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                      alignment: Alignment.center,
                      child: Text(widget.fromAllMember == true ? "Back" : "Not Now", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
            ),
          ),
          Container(
            child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
              splashColor: Colors.grey.withOpacity(0.5),
              onPressed: _valTypeInv == "Members of My Other Club" && _isEmptyClub || _valTypeInv == "My Buddies in GIF" && emptyBuddy ? null : () {
                if(!_loading){
                  if (_valTypeInv == 'My Phone Contacts') {
                    int mCounter = _allContacts.where((contact) => contact.isChecked == true).toList().length;
                    if (mCounter == 0){
                      var snackbar = SnackBar(
                        content: Text("Please invite at least one person"),
                        backgroundColor: Colors.red,
                      );
                      _scaffoldKey.currentState.showSnackBar(snackbar);
                    }else {
                      for (int i = 0; i < _allContacts.length; i++) {
                        if (_allContacts[i].isChecked){
                          arrNameSelected.add(_allContacts[i].contact.displayName);
                          arrNumberSelected.add(Utils.getValidPhoneNumber(_allContacts[i].contact.phones));
                        }
                      }
                      setState(() {
                        _loading = true;
                        _invite();
                      });
                    }
                  }else if(_valTypeInv == "My Buddies in GIF"){
                    int mCounter = _uiBuddies
                        .where((buddies) => buddies.isChecked == true)
                        .toList()
                        .length;
                    if (_tipeInv == "1") {
                      // _uiBuddies.forEach((element) {
                      //   element.isChecked = true;
                      // });
                      _allBuddies = _uiBuddies;
                      for (int i = 0; i < _uiBuddies.length; i++) {
                        if (_uiBuddies[i].showAll.invited != "sudah") {
                          arrIdSelected.add(_uiBuddies[i].showAll.id);
                          arrNameSelected.add(_uiBuddies[i].showAll.nama_lengkap);
                          arrNumberSelected.add(_uiBuddies[i].showAll.no_telp);
                        }
                      }
                      if(arrIdSelected.isEmpty){
                        var snackbar = SnackBar(
                          content: Text("All of your buddy has been invited."),
                          backgroundColor: Colors.red,
                        );
                        _scaffoldKey.currentState.showSnackBar(snackbar);
                      }else{
                        setState(() {
                          _loading = true;
                          _invite();
                        });
                      }
                    } else {
                      if (mCounter == 0) {
                        var snackbar = SnackBar(
                          content: Text("Please invite at least one person"),
                          backgroundColor: Colors.red,
                        );
                        _scaffoldKey.currentState.showSnackBar(snackbar);
                      } else {
                        for (int i = 0; i < _uiBuddies.length; i++) {
                          if (_uiBuddies[i].isChecked) {
                            arrIdSelected.add(_uiBuddies[i].showAll.id);
                            arrNameSelected
                                .add(_uiBuddies[i].showAll.nama_lengkap);
                            arrNumberSelected.add(_uiBuddies[i].showAll.no_telp);
                          }
                        }
                        setState(() {
                          _loading = true;
                          _invite();
                        });
                      }
                    }
                  }else if(_valTypeInv == "Members of My Other Club"){
                    int mCounter = _uiGroup.where((group) => group.isChecked == true).toList().length;
                    if (_tipeInv=="1"){
                      // _uiGroup.forEach((element) { element.isChecked = true; });
                      _allGroup = _uiGroup;
                      for (int i = 0; i < _uiGroup.length; i++) {
                        if (_uiGroup[i].showAll.invited != "sudah") {
                          arrIdSelected.add(_uiGroup[i].showAll.id);
                          arrNameSelected.add(_uiGroup[i].showAll.nama_lengkap);
                          arrNumberSelected.add(_uiGroup[i].showAll.no_telp);
                        }
                      }
                      if(arrIdSelected.isEmpty){
                        var snackbar = SnackBar(
                          content: Text("All golfers in this club has been invited."),
                          backgroundColor: Colors.red,
                        );
                        _scaffoldKey.currentState.showSnackBar(snackbar);
                      }else {
                        setState(() {
                          _loading = true;
                          _invite();
                        });
                      }
                    }else {
                      if (mCounter == 0) {
                        var snackbar = SnackBar(
                          content: Text("Please invite at least one person"),
                          backgroundColor: Colors.red,
                        );
                        _scaffoldKey.currentState.showSnackBar(snackbar);
                      } else {
                        for (int i = 0; i < _uiGroup.length; i++) {
                          if (_uiGroup[i].isChecked) {
                            arrIdSelected.add(_uiGroup[i].showAll.id);
                            arrNameSelected.add(_uiGroup[i].showAll.nama_lengkap);
                            arrNumberSelected.add(_uiGroup[i].showAll.no_telp);
                          }
                        }
                        setState(() {
                          _loading = true;
                          _invite();
                        });
                      }
                    }
                  }else{
                    if(_valTypeInv == "Add Manually"){
                      for (int i = 0; i < arrAddText.length; i++) {
                        if (names[i].text.isNotEmpty && numbs[i].text.isNotEmpty) {
                          arrNameSelected.add(names[i].text);
                          arrNumberSelected.add(numbs[i].text);
                        }
                      }
                    }
                    setState(() {
                      if ((arrNameSelected.length<=0 || arrNumberSelected.length<=0) && _tipeInv == null){
                        var snackbar = SnackBar(
                          content: Text("Please invite at least one person"),
                          backgroundColor: Colors.red,
                        );
                        _scaffoldKey.currentState.showSnackBar(snackbar);
                      }else {
                        _loading = true;
                        _invite();
                      }
                    });
                  }
                }
              },
              padding: const EdgeInsets.all(0.0),
              child: Ink(
                  decoration: BoxDecoration(
                      color: _valTypeInv == "Members of My Other Club" && _isEmptyClub || _valTypeInv == "My Buddies in GIF" && emptyBuddy ? AppTheme.gBlack : AppTheme.gButton,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: Container(
                      constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                      alignment: Alignment.center,
                      child: _loading
                          ? Center(
                          child: Container(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(),
                          ))
                          : Text("Invite", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
            ),
          ),
        ],
      ),
    );
  }

  Row _buildListTile(CustomContact c/*, List<Item> list*/) {
    List<bool> check = List<bool>();
    Function eq = const DeepCollectionEquality.unordered().equals;
    if(contacts2!=null) {
      for (int a = 0; a < contacts2.length; a++) {
        var l = contacts2[a]['cek_no_telp'];
        c.contact.phones.forEach((element) {
          check.add(eq(element.value, l));
        });
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
            padding:
            EdgeInsets.only(top: 5, right: 10)),
        InkWell(
          onTap: () {
            setState(() {
              if (c.isChecked) {
                c.isChecked = false;
              } else {
                c.isChecked = true;
              }
            });
          },
          child: contacts2 == null ? Container() : check.contains(true) ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: AppTheme.gGrey,) : c.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
          // child: c.isChecked ? SvgPicture.asset( 'assets/svg/check.svg', width: 20, height: 20, color: Colors.black,) : SvgPicture.asset( 'assets/svg/uncheck.svg', width: 20, height: 20, color: Colors.black,),
        ),
        Flexible(
          child: Container(
            padding:
            const EdgeInsets.only(left: 8.0),
            child: Text(
              /*kanggo nampilno or ngambil nomer*/
              // Utils.getValidPhoneNumber(contact.phones) ?? '',
              /*kanggo nampilno or ngambil nomer*/
              "${c.contact.displayName}",
              overflow: TextOverflow.fade,
              maxLines: 1,
              softWrap: false,
              style: TextStyle(
                  color: _checkMember
                      ? AppTheme.gGrey
                      : Color(0xFF828282)),
            ),
          ),
        )
      ],
    );
  }

  Column selectMenuInvite() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 22),
          child: Text(
            "Add Members",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF4F4F4F)),
          ),
        ),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Select from",
              style: TextStyle(color: AppTheme.gFont),
            )),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: AppTheme.gGrey,
                width: 1.0,
              ),
            ),
          ),
          child: DropdownButton(
            isExpanded: true,
            underline: SizedBox(),
            items: <String>[
              'My Phone Contacts',
              'My Buddies in GIF',
              'Members of My Other Club',
              'Add Manually'
            ].map((value) {
              return DropdownMenuItem(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                _valTypeInv = value;
                _handleInv("1");
              });
            },
            value: _valTypeInv,
            hint: Text(
              'My Phone Contacts',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }

  Container selectPictureClubContainer() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(3),
      width: double.maxFinite,
      height: 120,
      decoration: BoxDecoration(
          color: Color(0xFFF9F9F9),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          /*_image == null ? */InkWell(
            // onTap: () => chooseImage(ImageSource.gallery),
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Center(child: Text("Upload Logo", style: TextStyle(color: AppTheme.gFontBlack),),),
            ),
          )/* : InkWell(
            onTap: () => chooseImage(ImageSource.gallery),
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  image: DecorationImage(
                      image: FileImage(_image),
                      fit: BoxFit.cover
                  )
              ),
              child: Center(child: Text(_image == null ? "Upload Logo" : "", style: TextStyle(color: AppTheme.gFontBlack),),),
            ),
          )*/,
          Expanded(child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("${dataCreateClub==null?"":dataCreateClub['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
                Text("${dataCreateClub==null?"":dataCreateClub['singkatan']} - ${dataCreateClub==null?"":dataCreateClub['kota']}", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center)
              ],
            ),
          ))
        ],
      ),
    );
  }


  Container successScreen(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Column(
        children: [
          Text("Invitation Were Sent", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Color(0Xff2D9CDB)),),
          Align(alignment: Alignment.center, child: Padding(padding: EdgeInsets.symmetric(vertical: 20), child: Text("$textSuccessInv", textAlign: TextAlign.center,),)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25.0),
            child:
            Container(
              child: RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  if(widget.fromAllMember == true){
                    // Navigator.pushReplacement(context, MaterialPageRoute(
                    //     builder: (context) =>
                    //         ViewMembers(dataClub: widget.dataAll,)));
                    Navigator.pop(context, 'reload');
                  }else{
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) =>
                            HomeClub(idClub: widget.dataClub['id_club_golf'],)));
                  }
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: Text("Back", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
              ),
            ),
          ),
        ],
      ),
    );
  }
  refreshContacts() async {
    setState(() {
      _isLoading = true;
    });
    var contacts = await ContactsService.getContacts();
    _populateContacts(contacts);
  }

  void _populateContacts(Iterable<Contact> contacts) {
    _contacts = contacts.where((item) => item.displayName != null).toList();
    _contacts.sort((a, b) => a.displayName.compareTo(b.displayName));
    _allContacts = _contacts.map((contact) => CustomContact(contact: contact)).toList();
    setState(() {
      _uiCustomContacts = _allContacts;
      _isLoading = false;
    });
  }

  void _searchContacts(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiCustomContacts = _allContacts.where((item) => item.contact.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void _searchGroup(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiGroup = _allGroup.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void _searchBuddies(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiBuddies = _allBuddies.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }
}

class CustomContact {
  final Contact contact;
  bool isChecked;

  CustomContact({
    this.contact,
    this.isChecked = false,
  });
}

class DefaultGroups{
  final String nama_lengkap;
  final String no_telp;
  final String id;
  final String invited;
  final String showAll;
  DefaultGroups({
    this.showAll,
    this.nama_lengkap,
    this.no_telp,
    this.invited,
    this.id,
  });
}

class CustomGroup {
  final DefaultGroups showAll;
  bool isChecked;
  CustomGroup({
    this.showAll,
    this.isChecked = false,
  });
}

class DefaultBuddies{
  final String nama_lengkap;
  final String no_telp;
  final String id;
  final String invited;
  final String showAll;
  DefaultBuddies({
    this.showAll,
    this.nama_lengkap,
    this.no_telp,
    this.id,
    this.invited,
  });
}

class CustomBuddies {
  final DefaultBuddies showAll;
  bool isChecked;
  CustomBuddies({
    this.showAll,
    this.isChecked = false,
  });
}

