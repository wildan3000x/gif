import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/my_games.dart';
import 'package:gif/socials/home_groups.dart';
import 'package:gif/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class RequestClub extends StatefulWidget {
  const RequestClub({Key key, this.dataClub, this.dataAll}) : super (key: key);
  final dataClub, dataAll;
  @override
  _RequestClubState createState() => _RequestClubState();
}

class _RequestClubState extends State<RequestClub> {
  var dataMyClub;
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  bool _isLoading = false;

  TextEditingController reason = TextEditingController();

  @override
  void initState(){
    dataMyClub = widget.dataClub;
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      dataMyClub = widget.dataClub;
    }
  }

  String msgSuccess;
  bool _successPage = false;

  void sendRequestClub() async {

    var data = {
      'token' : sharedPreferences.getString("token"),
      'club_golf_id': dataMyClub['id_club_golf'],
      'alasan' : reason.text
    };

    Utils.postAPI(data, "join_club_request").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          msgSuccess = body['message'];
          _successPage = true;
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Container(
              margin: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCreate(title: dataMyClub['singkatan'], disableBack: true,),
                  clubNameLogo(),
                  SizedBox(height: 8,),
                  _successPage ? Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        SizedBox(height: 15,),
                        Text(msgSuccess, style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                        SizedBox(height: 15,),
                        Container(
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                            splashColor: Colors.grey.withOpacity(0.5),
                            onPressed: () {
                              // Navigator.pop(context);
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeGroups()));
                            },
                            padding: const EdgeInsets.all(0.0),
                            child: Ink(
                                decoration: BoxDecoration(
                                    color: AppTheme.gButton,
                                    borderRadius: BorderRadius.circular(100)
                                ),
                                child: Container(
                                    constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                    alignment: Alignment.center,
                                    child: const Text("Back", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                          ),
                        ),
                      ],
                    ),
                  ) : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("You are not a member of this club.", style: TextStyle(color: AppTheme.bogies, fontSize: 18),),
                      Text("Fill the form below to join", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18),),
                      SizedBox(height: 10,),
                      Text("Write down the reason why\nyou should be accepted by the club", style: TextStyle(color: Color(0xFF828282), fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
                      SizedBox(height: 8,),
                      Container(
                        width: 250,
                        child: TextField(
                          maxLines: 6,
                          controller: reason,
                          onChanged: (_){
                            setState(() {
                              _validate = false;
                            });
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Enter your reason.',
                            errorBorder:  _validate ? OutlineInputBorder(borderSide: BorderSide(
                              color: Colors.red
                            )) : null,
                            errorText: _validate ? 'Please fill the form' : null,
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                          splashColor: Colors.grey.withOpacity(0.5),
                          onPressed: () {
                            setState(() {
                              if(reason.text.isEmpty){
                                _validate = true;
                              }else {
                                _validate = false;
                                _isLoading = true;
                                sendRequestClub();
                              }
                            });
                            // Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                          },
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                              decoration: BoxDecoration(
                                  color: AppTheme.gButton,
                                  borderRadius: BorderRadius.circular(100)
                              ),
                              child: Container(
                                  constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                  alignment: Alignment.center,
                                  child: _isLoading ? Center(child: Container(
                                    width: 20,
                                    height: 20,
                                    child: CircularProgressIndicator(),
                                  ),) : Text("Join", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget clubNameLogo() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(left: 10),
                child: dataMyClub['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
              ),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(width: 200, child: Center(child: Text("${dataMyClub['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,))),
                  Text("${dataMyClub['kota']}, ${dataMyClub['negara']}", style: TextStyle(color: AppTheme.gFontBlack),)
                ],
              )
            ],
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 5), child: InkWell(onTap: ()=>Navigator.pop(context, 'reload'), child: Icon(Icons.arrow_back))),
            Expanded(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0, bottom: 10, right: 15),
                  child: Text("Request to Join The Club", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22),),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
