import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/socials/invite_club/invite_club.dart';
import 'package:gif/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class ViewMembers extends StatefulWidget {
  const ViewMembers({Key key, this.dataClub}) : super (key: key);
  final dataClub;
  @override
  _ViewMembersState createState() => _ViewMembersState();
}

class _ViewMembersState extends State<ViewMembers> {
  var dataMyClub;
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    dataMyClub = widget.dataClub;
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getMyClub();
    }
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();


  var dataClub;
  var checkAdmin;

  void getMyClub() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.dataClub['club']['id_club_golf']
    };

    setState(() {
      _member.clear();
      _allMember.clear();
      _uiMember.clear();
    });

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataClub = body['data'];
        checkAdmin = body['admin'];
        setState(() {
          for (int i = 0; i < dataClub['member_club'].length; i++) {
            DefaultMembers def = DefaultMembers(
                id: dataClub['member_club'][i]["user_id"],
                nama_lengkap: dataClub['member_club'][i]["nama_lengkap"],
                nama_pendek: dataClub['member_club'][i]["nama_pendek"],
                kota: dataClub['member_club'][i]["kota"],
                negara: dataClub['member_club'][i]["negara"],
                clubMaker: dataClub['member_club'][i]["club_maker"],
                foto: dataClub['member_club'][i]["foto"],
                data: dataClub['member_club'][i],
            );
            _member.add(def);
            _allMember.add(CustomMembers(showAll: def));
            _uiMember.add(CustomMembers(showAll: def));
          }
        });
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  bool _isLoadingKick = false;
  void kickMember({int idMember}) async {

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.dataClub['club']['id_club_golf'],
      'id_user': idMember
    };

    Utils.postAPI(data, "leave_club").then((body) {
      if(body['status'] == 'success'){
        Navigator.pop(context);
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.blueAccent,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        getMyClub();
        setState(() {
          _isLoadingKick = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  void updateInformation(String information) {
    setState(() {
      if(information == 'reload'){
        getMyClub();
      }
    });
  }

  void moveToInv() async {
    final information = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => InviteClub(dataClub: dataMyClub['club'], dataAll: dataMyClub, fromAllMember: true,)));
    updateInformation(information);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(13),
            child: Column(
              children: [
                HeaderCreate(title: "Member's Club", disableBack: true,),
                clubNameLogo(),
                Container(
                  width: 200,
                  height: 45,
                  padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 8),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Search",
                      hintText: "Search by name, city, or country",
                      suffixIcon: Icon(Icons.search),
                      contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                    ),
                    controller: controllerSearch3,
                    onChanged: (value) {
                      _searchMembers(value);
                    },
                  ),
                ),
                checkAdmin == 'true' ? Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: InkWell(onTap: (){
                    moveToInv();
                  }, child: Text("Invite More Members", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold),)),
                ) : Container(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: _isLoading ? Center(child: Container(margin: EdgeInsets.only(top: 50), width: 20, height: 20, child: CircularProgressIndicator(),),) :
                  _uiMember==null || _uiMember.length <= 0 ? Container(margin: EdgeInsets.all(45), child: Text("So lonely here.", style: TextStyle(color: AppTheme.gFontBlack),),) :
                  ListView.builder(
                    itemCount: _uiMember==null || _uiMember.length <= 0 ? 0 : _uiMember.length,
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: (context, i){
                      return ClubMemberCard(
                          dataMember: _uiMember[i].showAll.data,
                          city: _uiMember[i].showAll.kota,
                          country: _uiMember[i].showAll.negara,
                          fullName: _uiMember[i].showAll.nama_lengkap,
                          shortName: _uiMember[i].showAll.nama_pendek,
                          photo: _uiMember[i].showAll.foto,
                          additionalWidget: checkAdmin == 'true' && _uiMember[i].showAll.clubMaker == false ? IconButton(
                            icon: Icon(Icons.close_rounded, color: Colors.red,),
                            onPressed: (){
                              showDialog(
                                  context: snackbarKey.currentContext,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      contentPadding: EdgeInsets.all(8),
                                      title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          _uiMember[i].showAll.foto == null ? ClipRRect(
                                            borderRadius: BorderRadius.circular(10.0),
                                            child: Image.asset(
                                              "assets/images/userImage.png",
                                              width: 60,
                                              height: 60,
                                            ),
                                          ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                              CircularProgressIndicator(
                                                value: progress.progress,
                                              ), imageUrl: Utils.url + 'upload/user/' + _uiMember[i].showAll.foto, width: 60, height: 60, fit: BoxFit.cover,)),
                                          SizedBox(height: 5,),
                                          Text("Kick ${_uiMember[i].showAll.nama_lengkap} ?", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                                        ],
                                      ),
                                      content: Container(
                                        height: 60,
                                        width: double.maxFinite,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Container(
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                                splashColor: Colors.grey.withOpacity(0.5),
                                                onPressed: () { Navigator.pop(context);},
                                                padding: const EdgeInsets.all(0.0),
                                                child: Ink(
                                                    height: 40,
                                                    decoration: BoxDecoration(
                                                        color: AppTheme.bogies,
                                                        borderRadius: BorderRadius.circular(100)
                                                    ),
                                                    child: Container(
                                                        constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                                        alignment: Alignment.center,
                                                        child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                              ),
                                            ),
                                            Container(
                                              child: RaisedButton(
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                                splashColor: Colors.grey.withOpacity(0.5),
                                                onPressed: () {
                                                  setState(() {
                                                    setState(() {
                                                      _isLoadingKick = true;
                                                    });
                                                    kickMember(idMember: _uiMember[i].showAll.id);
                                                  });
                                                },
                                                padding: const EdgeInsets.all(0.0),
                                                child: Ink(
                                                    height: 40,
                                                    decoration: BoxDecoration(
                                                        color: AppTheme.gButton,
                                                        borderRadius: BorderRadius.circular(100)
                                                    ),
                                                    child: Container(
                                                        constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                                        alignment: Alignment.center,
                                                        child: _isLoadingKick ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),) : Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                              );
                            },
                            splashColor: Colors.grey.withOpacity(0.3),
                            padding: EdgeInsets.zero,
                            splashRadius: 20,
                          ) : Container(),
                      );
                    },),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget clubNameLogo() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(left: 10),
                child: widget.dataClub['club']['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/club/' + widget.dataClub['club']['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
              ),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(width: 200, child: Center(child: Text("${widget.dataClub['club']['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,))),
                  Text("${widget.dataClub['club']['kota']}, ${widget.dataClub['club']['negara']}", style: TextStyle(color: AppTheme.gFontBlack),)
                ],
              )
            ],
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 5), child: InkWell(onTap: ()=>Navigator.pop(context, 'reload'), child: Icon(Icons.arrow_back))),
            Expanded(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0, bottom: 10, right: 15),
                  child: Text("All Members (${dataClub==null?"0":dataClub['total_member_club']})", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22),),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class ClubMemberCard extends StatelessWidget {
  const ClubMemberCard({
    Key key,
    @required this.dataMember, @required this.photo, @required this.fullName, @required this.city, @required this.country, @required this.shortName,
    this.additionalWidget
  }) : super(key: key);

  final Widget additionalWidget;
  final dataMember, photo, fullName, city, country, shortName;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Utils.linkToGolferProfile(context, dataMember),
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        child: Row(
          children: [
            photo == null ? ClipRRect(
              borderRadius: BorderRadius.circular(10000.0),
              child: Image.asset(
                "assets/images/userImage.png",
                width: 35,
                height: 35,
              ),
            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/user/' + photo, width: 35, height: 35, fit: BoxFit.cover,)),
            SizedBox(width: 10,),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${shortName==null?fullName:shortName+", "+fullName}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 16), overflow: TextOverflow.clip,),
                Text("$city, $country", style: TextStyle(color: AppTheme.gButton),),
              ],
            )),
            additionalWidget
            // Padding(
            //   padding: const EdgeInsets.only(right: 8.0),
            //   child: Text("${dataMyClub['member_club'][i]['kota']}, ${dataMyClub['member_club'][i]['negara']}", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.end,),
            // )
          ],
        ),
      ),
    );
  }
}


class DefaultMembers{
  final int id;
  final String nama_lengkap;
  final String nama_pendek;
  final String kota;
  final String negara;
  final String foto;
  final bool clubMaker;
  final data;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.nama_lengkap,
    this.nama_pendek,
    this.kota,
    this.negara,
    this.foto,
    this.data,
    this.clubMaker,
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
