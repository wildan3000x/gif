import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/widget/button_rectangle_rounded_caddie.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/socials/club/view_members.dart';
import 'package:gif/socials/home_groups.dart';
import 'package:gif/socials/invite_club/request_club.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/custom_alert_dialog.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class ProfileClub extends StatefulWidget {
  const ProfileClub({Key key, this.dataClub, this.dataAll, this.fromSearch, this.idNotif, this.disableViewMember}) : super (key: key);
  final dataClub, dataAll, fromSearch, idNotif, disableViewMember;
  @override
  _ProfileClubState createState() => _ProfileClubState();
}

class _ProfileClubState extends State<ProfileClub> {
  var dataMyClub;
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    print("FROM SEARCH ${widget.fromSearch}");
    if(widget.fromSearch == null) {
      dataMyClub = widget.dataClub;
    }
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getMyClub();
    }
  }

  var dataClub;
  var checkAdmin;
  var checkRequest;
  bool _isLoading = false;

  void getMyClub() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.fromSearch == true ? widget.dataClub : widget.dataClub['id_club_golf'],
      'id_notifikasi': widget.idNotif==null?null:widget.idNotif
    };

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataClub = body['data'];
        checkAdmin = body['admin'];
        checkRequest = body['status_request'];
        if(widget.fromSearch == true){
          dataMyClub = body['data']['club'];
        }
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void leaveClub() async {

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.fromSearch == true ? widget.dataClub : widget.dataClub['id_club_golf'],
      'id_user': sharedPreferences.getString("id")
    };

    Utils.postAPI(data, "leave_club").then((body) {
      if(body['status'] == 'success'){
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    HomeGroups()), (
                Route<dynamic> route) => false);
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          // _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void updateInformation(String information) {
    setState(() {
      if(information == 'reload'){
        getMyClub();
      }
    });
  }

  void moveToAllMember() async {
    final information = await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            ViewMembers(dataClub: dataClub,)));
    updateInformation(information);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SingleChildScrollView(
            child: /*widget.fromSearch == true && _isLoading ? Center(
              heightFactor: 30,
              child: Container(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(),
              ),
            ) :*/
            Container(
              margin: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCreate(title: "Club Profile", disableBack: true,),
                  clubNameLogo(),
                  profileInfo(),
                  clubStats(),
                  widget.fromSearch == true ? Column(
                    children: [
                      SizedBox(height: 18,),
                      checkRequest == "Request" ? Text("You have requested to join this club, waiting for confirmation from the admin club.", style: TextStyle(color: Color(0xFFF15411),), textAlign: TextAlign.center,)
                      : Column(
                        children: [
                          Text("You are not a member of this club.", style: TextStyle(color: Color(0xFFF15411)),),
                          Text("Request to join", style: TextStyle(color: AppTheme.gFontBlack),),
                          SizedBox(height: 8,),
                          Container(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                              splashColor: Colors.grey.withOpacity(0.5),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => RequestClub(dataClub: dataMyClub,)));
                              },
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                  decoration: BoxDecoration(
                                      color: AppTheme.gButton,
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: Container(
                                      constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                                      alignment: Alignment.center,
                                      child: const Text("Join", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                            ),
                          ),
                        ],
                      )
                    ],
                  ) : Column(
                    children: [
                      SizedBox(height: 40,),
                      Container(
                        margin: EdgeInsets.only(bottom: 15),
                        alignment: Alignment.bottomCenter,
                        child: ButtonRectangleRoundedCaddie(color: AppTheme.bogies, textButton: "Exit Club",
                          link: () { showDialog(
                              context: snackbarKey.currentContext,
                              builder: (BuildContext context) {
                                return CustomAlertDialog(
                                  title: "Confirm Exit",
                                  contentText: "are you sure you want to exit this club?",
                                  yesButton: () {
                                    leaveClub();
                                  },
                                  noButton: () { Navigator.pop(context);},
                                );
                              }
                          );},),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column clubStats() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(right: 40, top: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(left: 10),
                child: dataMyClub==null?Container():dataMyClub['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
              ),
              SizedBox(width: 8,),
              Text("Club Stats", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22),),
            ],
          ),
        ),
        SizedBox(height: 5,),
        widget.disableViewMember == null ? Text("Tap for more info.", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 13),) : Container(),
        SizedBox(height: 5,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InkWell(
              onTap: () {
                if(widget.disableViewMember == null){
                  moveToAllMember();
                }
              },
              child: Stack(
                children: [
                  Container(
                    height: 50,
                    width: 150,
                    padding: EdgeInsets.only(left: 45),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0xFFF9F9F9),
                        border: Border.all(color: Color(0xFFE5E5E5))
                    ),
                    child: Center(child: Text("Members", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),)),
                  ),
                  Positioned(
                    left: 0,
                    child: Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppTheme.gButton,
                      ),
                      child: Center(child: Text(dataClub==null?"0":dataClub['total_member_club'].toString(), style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),)),
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  height: 50,
                  width: 150,
                  padding: EdgeInsets.only(left: 40),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF9F9F9),
                      border: Border.all(color: Color(0xFFE5E5E5))
                  ),
                  child: Center(child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Events", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),),
                      Text("Per Year", style: TextStyle(color: AppTheme.gFontBlack),)
                    ],
                  )),
                ),
                Positioned(
                  left: 0,
                  child: Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF15411),
                    ),
                    child: Center(child: Text(dataClub==null?"0":dataClub['total_event'].toString(), style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),)),
                  ),
                ),
              ],
            )
          ],
        )
      ],
    );
  }

  Container profileInfo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Club Slogan", style: TextStyle(color: AppTheme.gFont),),
          Container(
            width: double.maxFinite,
            padding: EdgeInsets.only(bottom: 2, top: 5),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Color(0xFFDADADA))
              )
            ),
            child: Text("${dataMyClub==null?"":dataMyClub['slogan_club']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
          SizedBox(height: 8,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Contact Name", style: TextStyle(color: AppTheme.gFont),),
                    Container(
                      width: 150,
                      padding: EdgeInsets.only(bottom: 2, top: 5),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Color(0xFFDADADA))
                          )
                      ),
                      child: Text("${dataMyClub==null?"":dataMyClub['nama_kontak']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Contact Number", style: TextStyle(color: AppTheme.gFont),),
                    Container(
                      width: 200,
                      padding: EdgeInsets.only(bottom: 2, top: 5),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Color(0xFFDADADA))
                          )
                      ),
                      child: Text("${dataMyClub==null?"":dataMyClub['no_kontak']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 8,),
          Text("Date of Found", style: TextStyle(color: AppTheme.gFont),),
          Container(
            width: double.maxFinite,
            padding: EdgeInsets.only(bottom: 2, top: 5),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xFFDADADA))
                )
            ),
            child: Text("${dataMyClub==null?"":dataMyClub['tgl_club']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
          SizedBox(height: 8,),
          Text("Home Golf Course", style: TextStyle(color: AppTheme.gFont),),
          Container(
            width: double.maxFinite,
            padding: EdgeInsets.only(bottom: 2, top: 5),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xFFDADADA))
                )
            ),
            child: Text("${dataMyClub==null?"":dataMyClub['nama_lapangan']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
          SizedBox(height: 8,),
          Text("Affiliate Organization", style: TextStyle(color: AppTheme.gFont),),
          Container(
            width: double.maxFinite,
            padding: EdgeInsets.only(bottom: 2, top: 5),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xFFDADADA))
                )
            ),
            child: Text("${dataMyClub==null?"":dataMyClub['organisasi_afiliasi']}", style: TextStyle(fontSize: 18, color: AppTheme.gFontBlack),),),
        ],
      ),
    );
  }

  Widget clubNameLogo() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(left: 10),
                child: dataMyClub==null?Container():dataMyClub['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
              ),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(width: 200, child: Center(child: Text("${dataMyClub==null?"":dataMyClub['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,))),
                  Text("${dataMyClub==null?"":dataMyClub['kota']}, ${dataMyClub==null?"":dataMyClub['negara']}", style: TextStyle(color: AppTheme.gFontBlack),)
                ],
              )
            ],
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 5), child: InkWell(onTap: ()=>Navigator.pop(context, 'reload'), child: Icon(Icons.arrow_back))),
            Expanded(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0, bottom: 10, right: 15),
                  child: Text("Club Profile", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22),),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
