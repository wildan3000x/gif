import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/widget/button_rectangle_rounded_caddie.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/my_games/scorecards/my_score.dart';
import 'package:gif/socials/club/join_request.dart';
import 'package:gif/socials/club/leaderboards_club.dart';
import 'package:gif/socials/club/next_event_club.dart';
import 'package:gif/socials/club/profile_club.dart';
import 'package:gif/socials/club/view_members.dart';
import 'package:gif/socials/create_group.dart';
import 'package:gif/socials/invite_club/invite_club.dart';
import 'package:gif/socials/logo_and_name.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeClub extends StatefulWidget {
  const HomeClub({Key key, this.idClub, this.idNotif}) : super (key: key);
  final idClub, idNotif;
  @override
  _HomeClubState createState() => _HomeClubState();
}

class _HomeClubState extends State<HomeClub> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getMyClub();
      getRequestJoin();
    }
  }

  var dataMyClub;
  var checkAdmin;
  var date;

  void getMyClub() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.idClub,
      'id_notifikasi': widget.idNotif==null?null:widget.idNotif
    };

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataMyClub = body['data'];
        checkAdmin = body['admin'];
        date = body['data']['next_event'];
        date.removeWhere((item) => item['status'] == "Not Invited" && item['partisipan_turnamen'] == "By Invitation");
        dataMyClub['recent_score'].removeWhere((item) => item['turnamen'] == null || item['nett'] == "N.A");
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataRequestJoin;

  void getRequestJoin() async {

    var data = {
      'token' : sharedPreferences.getString("token"),
      'club_golf_id': widget.idClub
    };

    Utils.postAPI(data, "all_request_jc").then((body) {
      if(body['status'] == 'success'){
        dataRequestJoin = body['data'];
      }else{
        dataRequestJoin = body['code'];
        print("ZERO REQUEST $dataRequestJoin");
      }
    }, onError: (error) {
      print("Error == $error");
    });
  }

  void updateInformation(String information) {
    setState(() {
      if(information == 'reload'){
        getMyClub();
        getRequestJoin();
      }
    });
  }

  moveToEdit() async {
    final information = await Navigator.push(context, MaterialPageRoute(builder: (context) => CreateGroup(dataClub: dataMyClub['club'], nextPage: false, editFromHome: true,)));
    updateInformation(information);
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getMyClub();
    getRequestJoin();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getMyClub();
    getRequestJoin();
    if(mounted)
      setState(() {
        getMyClub();
        getRequestJoin();
      });
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(clickable: true,),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: /*_isLoading ? Center(
                  heightFactor: 20,
                  child: Container(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                ) : */Column(
                  children: [
                    HeaderCreate(title: "Club Detail", backSummary: false,),
                    imageAndNameProfile(context),
                    // ImageAndNameClub(dataMyClub: dataMyClub['club'], dataAll: dataMyClub, goToProfile: true, admin: checkAdmin, routeForPop: moveToEdit(),),
                    SizedBox(height: 3,),
                    Text("tap the club name to view the club profile", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 13, fontStyle: FontStyle.italic),),
                    SizedBox(height: 10,),
                    dataRequestJoin!=null && dataRequestJoin!=250 && checkAdmin=='true'? pendingJoinRequestlnfo(context) : Container(),
                    leaderBoards(context),
                    SizedBox(height: 15,),
                    nextEvent(),
                    SizedBox(height: 15,),
                    recentScores(),
                    SizedBox(height: 15,),
                    Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xFFF9F9F9),
                              border: Border.all(color: Color(0xFFE5E5E5))
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: dataMyClub==null?Container():dataMyClub['new_member'].isEmpty ? Center(child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 8.0),
                              child: Text("there is no new members within 30 days.", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                            ),) : Column(
                              children: List.generate(dataMyClub==null?0:dataMyClub['new_member'].length>16?
                              15:dataMyClub['new_member'].length, (index) {
                                // print("DATAS ${dataMyClub['new_member'][index]}");
                                  return ClubMemberCard(
                                    dataMember: dataMyClub['new_member'][index],
                                    photo: dataMyClub['new_member'][index]['foto'],
                                    fullName: dataMyClub['new_member'][index]['nama_lengkap'],
                                    city: dataMyClub['new_member'][index]['kota'],
                                    country: dataMyClub['new_member'][index]['negara'],
                                    shortName: dataMyClub['new_member'][index]['nama_pendek'],
                                    additionalWidget: Container(),
                                  );}),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                          // width: 112,
                          decoration: BoxDecoration(
                              color: AppTheme.eagle,
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child: Text("New Members", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                        ),
                        dataMyClub==null||dataMyClub['new_member'].isEmpty ? Container() : Positioned(
                          top: 0,
                          right: 10,
                          child: InkWell(
                              splashColor: Colors.grey.withOpacity(0.2),
                              onTap: () => moveToAllMember(),
                              child: Text("See All Members", style: TextStyle(color: AppTheme.gFontBlack),)),)
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void moveToAllMember() async {
    final information = await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            ViewMembers(dataClub: dataMyClub,)));
    updateInformation(information);
  }

  Column pendingJoinRequestlnfo(BuildContext context) {
    return Column(
      children: [
        FlatButton(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          height: 30,
          splashColor: Colors.grey.withOpacity(0.2),
          onPressed: () {
            if(dataRequestJoin!=null || dataRequestJoin==250){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  JoinRequest(idClub: dataMyClub['club']['id_club_golf']))).then((value) => setState(() {
                    getMyClub();
                    getRequestJoin();
              }));
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Pending Join Request", style: TextStyle(color: AppTheme.gRed, fontSize: 16, fontWeight: FontWeight.bold),),
              Container(
                margin: EdgeInsets.only(left: 3),
                padding: EdgeInsets.all(dataRequestJoin==null?4:dataRequestJoin==250?4:dataRequestJoin.length>=10?2:4),
                decoration: BoxDecoration(
                  color: AppTheme.gRed,
                  shape: BoxShape.circle
                ),
                child: Center(child: Text("${dataRequestJoin==null?"0":dataRequestJoin==250?"0":dataRequestJoin.length}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
              )
            ],
          ),
        ),
        SizedBox(height: 10,),
      ],
    );
  }

  Widget imageAndNameProfile(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(3),
      width: double.maxFinite,
      height: 120,
      decoration: BoxDecoration(
          color: Color(0xFFF9F9F9),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              dataMyClub==null||dataMyClub['club']['logo'] == null ? InkWell(
                // onTap: () => chooseImage(ImageSource.gallery),
                child: Container(
                  width: 110,
                  height: 110,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: Center(child: Text("Upload Logo", style: TextStyle(color: AppTheme.gFontBlack),),),
                ),
              ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(
                progressIndicatorBuilder: (context, url, progress) =>
                  SizedBox(
                    width: 30,
                    height: 30,
                    child: Center(
                      child: CircularProgressIndicator(
                        value: progress.progress,
                      ),
                    ),
                  ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['club']['logo'], width: 110, height: 110, fit: BoxFit.cover,)),
              Expanded(child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) =>
                          ProfileClub(dataClub: dataMyClub['club'], dataAll: dataMyClub,))).then((value) => setState(() {
                    getMyClub();
                    getRequestJoin();
                  }));
                },
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(dataMyClub==null?"":dataMyClub['club']['nama_club'], style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 22), textAlign: TextAlign.center,),
                      Text(dataMyClub==null?"":dataMyClub['club']['slogan_club'], style: TextStyle(color: AppTheme.gFont, fontSize: 14), textAlign: TextAlign.center, overflow: TextOverflow.ellipsis,),
                      SizedBox(height: 5),
                      Text("${dataMyClub==null?"":dataMyClub['club']['singkatan']} - ${dataMyClub==null?"":dataMyClub['club']['kota']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 12), textAlign: TextAlign.center)
                    ],
                  ),
                ),
              ))
            ],
          ),
          checkAdmin=='true'? Positioned(
            top: 0,
            right: 0,
            child: InkWell(
                onTap: () => moveToEdit(),
                child: Text("Edit", style: TextStyle(color: Color(0xFFF15411)),)),) : Container()
        ],
      ),
    );
  }

  Stack recentScores() {
    var itemList = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 16),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFF9F9F9),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Container(
            margin: const EdgeInsets.only(top: 12.0),
            height: 90,
            width: itemList,
            child: dataMyClub==null?Container():dataMyClub['recent_score'].isEmpty ? Center(child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text("there is no recent score yet.", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
            ),) : ListView.builder(
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: dataMyClub==null?0:dataMyClub['recent_score'].length,
              itemBuilder: (context, index){
                return /*dataMyClub['recent_score'][index]['turnamen'] == null ? Container() : */Container(
                  width: itemList/4.6,
                  child: InkWell(
                    splashColor: Colors.grey.withOpacity(0.2),
                    onTap: (){
                      if(dataMyClub['recent_score'][index]['turnamen']!=null) {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) =>
                                MyScore(
                                  idTour: dataMyClub['recent_score'][index]['turnamen']['turnamen_id'],
                                  buddyScore: true,
                                  buddyToken: dataMyClub['recent_score'][index]['token'],)));
                      }
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(dataMyClub['recent_score'][index]['nett'].toString(), style: TextStyle(color: AppTheme.gFont), textAlign: TextAlign.center,),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 3.0),
                          child: dataMyClub['recent_score'][index]['foto'] == null ? ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.asset(
                              "assets/images/userImage.png",
                              width: 50,
                              height: 50,
                            ),
                          ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                              CircularProgressIndicator(
                                value: progress.progress,
                              ), imageUrl: Utils.url + 'upload/user/' + dataMyClub['recent_score'][index]['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                        ),
                        Tooltip(message: dataMyClub['recent_score'][index]['nama_lengkap'], child: Text(dataMyClub['recent_score'][index]['nama_pendek']==null?dataMyClub['recent_score'][index]['nama_lengkap']:dataMyClub['recent_score'][index]['nama_pendek'], overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10),
          padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
          // width: 112,
          decoration: BoxDecoration(
              color: Color(0xFF2D9CDB),
              borderRadius: BorderRadius.circular(10)
          ),
          child: Text("Recent Scores", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        dataMyClub==null||dataMyClub['recent_score'].isEmpty ? Container() : Positioned(
          top: 0,
          right: 10,
          child: InkWell(
            splashColor: Colors.grey.withOpacity(0.2),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => LeaderBoardsClub(dataClub: dataMyClub['club'], fromSeeAllRecentScoreClub: true,))).then((value) => setState(() {
                getMyClub();
                getRequestJoin();
              })),
              child: Text("See All", style: TextStyle(color: AppTheme.gFontBlack),)),)
      ],
    );
  }

  Stack leaderBoards(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 16),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFF9F9F9),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: dataMyClub==null?Container():dataMyClub['leaderboard']['best_nett']==""&&dataMyClub['leaderboard']['best_gross']==""&&dataMyClub['leaderboard']['best_achievment']==""&&dataMyClub['leaderboard']['the_strongest']==""? Center(child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text("empty Leaderboard.", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
            ),) : Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                dataMyClub==null||dataMyClub['leaderboard']['best_nett']==""?Container():Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Best\nNett", style: TextStyle(color: AppTheme.gFont), textAlign: TextAlign.center,),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: dataMyClub==null?Container():dataMyClub['leaderboard']['best_nett']['foto'] == null ? ClipRRect(
                          borderRadius: BorderRadius.circular(10000.0),
                          child: Image.asset(
                            "assets/images/userImage.png",
                            width: 50,
                            height: 50,
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + dataMyClub['leaderboard']['best_nett']['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                      ),
                      Tooltip(message: dataMyClub==null?"":dataMyClub['leaderboard']['best_nett']['nama'], child: Text("${dataMyClub==null?"":dataMyClub['leaderboard']['best_nett']['nama1']==null?dataMyClub['leaderboard']['best_nett']['nama']:dataMyClub['leaderboard']['best_nett']['nama1']/*+", "+dataMyClub['leaderboard']['best_nett']['nama']*/}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),))
                    ],
                  ),
                ),
                dataMyClub==null||dataMyClub['leaderboard']['best_gross']==""?Container():Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Best\nGross", style: TextStyle(color: AppTheme.gFont), textAlign: TextAlign.center,),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: dataMyClub==null?Container():dataMyClub['leaderboard']['best_gross']['foto'] == null ? ClipRRect(
                          borderRadius: BorderRadius.circular(10000.0),
                          child: Image.asset(
                            "assets/images/userImage.png",
                            width: 50,
                            height: 50,
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + dataMyClub['leaderboard']['best_gross']['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                      ),
                      Tooltip(message: dataMyClub==null?"":dataMyClub['leaderboard']['best_gross']['nama'], child: Text("${dataMyClub==null?"":dataMyClub['leaderboard']['best_gross']['nama1']==null?dataMyClub['leaderboard']['best_gross']['nama']:dataMyClub['leaderboard']['best_gross']['nama1']/*+", "+dataMyClub['leaderboard']['best_gross']['nama']*/}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),))
                    ],
                  ),
                ),
                dataMyClub==null||dataMyClub['leaderboard']['best_achievment']==""?Container():Container(
                  width: 100,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Best\nAchievement", style: TextStyle(color: AppTheme.gFont), textAlign: TextAlign.center,),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 3.0),
                          child: dataMyClub==null?Container():dataMyClub['leaderboard']['best_achievment']['foto'] == null ? ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.asset(
                              "assets/images/userImage.png",
                              width: 50,
                              height: 50,
                            ),
                          ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                              CircularProgressIndicator(
                                value: progress.progress,
                              ), imageUrl: Utils.url + 'upload/user/' + dataMyClub['leaderboard']['best_achievment']['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                        ),
                        Tooltip(message: dataMyClub==null?"":dataMyClub['leaderboard']['best_achievment']['nama'], child: Text("${dataMyClub==null?"":dataMyClub['leaderboard']['best_achievment']['nama1']==null?dataMyClub['leaderboard']['best_achievment']['nama']:dataMyClub['leaderboard']['best_achievment']['nama1']/*+", "+dataMyClub['leaderboard']['best_achievment']['nama']*/}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),))
                      ],
                    ),
                  ),
                ),
                dataMyClub==null||dataMyClub['leaderboard']['the_strongest']==""?Container():Expanded(
                  child: Column(
                    children: [
                      Text("The\nStrongest", style: TextStyle(color: AppTheme.gFont), textAlign: TextAlign.center,),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: dataMyClub==null?Container():dataMyClub['leaderboard']['the_strongest']['foto'] == null ? ClipRRect(
                          borderRadius: BorderRadius.circular(10000.0),
                          child: Image.asset(
                            "assets/images/userImage.png",
                            width: 50,
                            height: 50,
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + dataMyClub['leaderboard']['the_strongest']['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                      ),
                      Tooltip(message: dataMyClub==null?"":dataMyClub['leaderboard']['the_strongest']['nama'], child: Text("${dataMyClub==null?"":dataMyClub['leaderboard']['the_strongest']['nama1']==null?dataMyClub['leaderboard']['the_strongest']['nama']:dataMyClub['leaderboard']['the_strongest']['nama1']/*+", "+dataMyClub['leaderboard']['the_strongest']['nama']*/}", overflow: TextOverflow.ellipsis, style: TextStyle(color: AppTheme.gFontBlack),))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10),
          padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
          // width: 112,
          decoration: BoxDecoration(
              color: Color(0xFFF15411),
              borderRadius: BorderRadius.circular(10)
          ),
          child: Text("Leaderboards", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        dataMyClub==null?Container():dataMyClub['leaderboard']['best_nett']==""&&dataMyClub['leaderboard']['best_gross']==""&&dataMyClub['leaderboard']['best_achievment']==""&&dataMyClub['leaderboard']['the_strongest']=="" ? Container() : Positioned(
          top: 0,
          right: 10,
          child: InkWell(
            splashColor: Colors.grey.withOpacity(0.2),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => LeaderBoardsClub(dataClub: dataMyClub['club'],))).then((value) => setState(() {
                getMyClub();
                getRequestJoin();
              })),
              child: Text("See All", style: TextStyle(color: AppTheme.gFontBlack),)),)
      ],
    );
  }

  Stack nextEvent() {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 16),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFF9F9F9),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                date==null||date.isEmpty ? Center(child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text("there is no next games at this moment.", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                ),) : ListView.builder(
                  shrinkWrap: true,
                  itemCount: date==null||date.isEmpty?0:date.length>3?3:date.length,
                  primary: false,
                  itemBuilder: (context, i){
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                          border: Border.all(
                              color: Color(0xFFE5E5E5)
                          )
                      ),
                      margin: EdgeInsets.only(top: i == 0 ? 2 : 5),
                      child: FlatButton(
                        onPressed: (){
                          if(date[i]['status_organizer'] == true){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                SummaryGames(tourId: date[i]['id_turnamen'], viewFromNextEventsClub: true,))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status_join'] == false && date[i]['partisipan_turnamen'] == 'Open' && date[i]['status'] == "Not Invited" || date[i]['status'] == "Invited"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                JoinGames(data: date[i]))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status_join'] == false && date[i]['partisipan_turnamen'] == 'By Invitation' && date[i]['status'] == "Invited"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                JoinGames(data: date[i]))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status_join'] == false && date[i]['partisipan_turnamen'] == 'By Invitation' && date[i]['status'] == "Not Invited"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                JoinGames(data: date[i], notInvited: true,))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status_join'] == false && date[i]['status'] == "Applying"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                PairingGames(
                                  data: date[i]['status'],
                                  msg: date[i]['status'] == "Applying" ? "Waiting For Approval" : date[i]['status'] == "Not Confirmed" ? "You have been Rejected" : null,
                                  sub_msg: date[i]['status'] == "Applying" ? "You still need approval from Organizer before join this game" : date[i]['status'] == "Not Confirmed" ? "your request to enter this game has been denied\nso you cannot enter this game." : null,
                                  id: date[i]['id_turnamen'],
                                  userId: sharedPreferences.getString('id'),
                                ))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status'] == 'Confirmed'){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => StartGame(data: date[i], status: date[i]['status'], id: date[i]['id_turnamen'], userId: date[i]['user_id'],))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }else if(date[i]['status'] == "Not Confirmed"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                JoinGames(data: date[i]))).then((value) => setState(() {
                              getMyClub();
                              getRequestJoin();
                            }));
                          }
                        },
                        padding: EdgeInsets.all(5),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        splashColor: Colors.grey.withOpacity(0.2),
                        textTheme: ButtonTextTheme.normal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 48,
                                      height: 25,
                                      padding: EdgeInsets.all(3),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                                          color: DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Aug" ||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Sep" ||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Oct"||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Nov"||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton
                                      ),
                                      child: Center(child: Text("${DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen']))}", style: TextStyle(fontSize: 16, color: Colors.white),)),
                                    ),
                                    Container(
                                      width: 48,
                                      height: 32,
                                      padding: EdgeInsets.all(3),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                                          color: Color(0xFFF9F9F9),
                                          border: Border.all(color: DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Aug" ||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Sep" ||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Oct"||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Nov"||
                                              DateFormat("MMM").format(DateTime.parse(date[i]['tanggal_turnamen'])) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton, width: 2)
                                      ),
                                      child: Center(child: Text("${DateFormat("d").format(DateTime.parse(date[i]['tanggal_turnamen']))}", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),)),
                                    )
                                  ],
                                ),
                                Container(
                                  width: 250,
                                  padding: const EdgeInsets.only(left: 8.0, top: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("${date[i]['nama_turnamen']}", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold, fontSize: 14), textAlign: TextAlign.start, overflow: TextOverflow.ellipsis,),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child: Text("${date[i]['nama_lapangan']}, ${date[i]['kota_turnamen']}", style: TextStyle(color: Color(0xFF4F4F4F)), overflow: TextOverflow.ellipsis,),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },)
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10),
          padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
          // width: 112,
          decoration: BoxDecoration(
              color: AppTheme.gButton,
              borderRadius: BorderRadius.circular(10)
          ),
          child: Text("Next Events", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        date==null||date.length <= 3 ? Container() : Positioned(
          top: 0,
          right: 10,
          child: InkWell(
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => NextEventClub(dataClub: dataMyClub))).then((value) => setState(() {
              getMyClub();
              getRequestJoin();
            })),
              child: Text("See All Events", style: TextStyle(color: AppTheme.gFontBlack),)),)
      ],
    );
  }
}

