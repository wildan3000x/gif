import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/scorecards/my_score.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeaderBoardsClub extends StatefulWidget {
  const LeaderBoardsClub({Key key, this.dataClub, this.fromSeeAllRecentScoreClub}) : super (key: key);
  final dataClub, fromSeeAllRecentScoreClub;
  @override
  _LeaderBoardsClubState createState() => _LeaderBoardsClubState();
}

class _LeaderBoardsClubState extends State<LeaderBoardsClub> {

  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  bool _catGross = true;
  bool _catNett = false;
  bool _catAchievement = false;
  bool _catStrongest = false;
  bool _catNettOverall = false;
  bool _catNettFlightA = false;
  bool _catNettFlightB = false;
  bool _catNettFlightC = false;

  var myFormat = DateFormat('d MMM yyyy');


  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      if(widget.fromSeeAllRecentScoreClub==null) {
        getLeaderBoardClub();
      }else{
        getRecentScoreClub();
      }
    }
  }

  var dataLeaderBoard;

  void getLeaderBoardClub([String s, String s2]) async {
    setState(() {
      _isLoading = true;
      dataLeaderBoard = null;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.dataClub['id_club_golf'],
      'aksi': s==null?'Gross':s,
      'aksi1': s2
    };

    Utils.postAPI(data, "get_CL").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
        });
        dataLeaderBoard = body['data'];
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataRecentScoreClub;
  bool _isLoadingRecentScore = false;
  void getRecentScoreClub() async {
    setState(() {
      _isLoadingRecentScore = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.dataClub['id_club_golf'],
    };

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataRecentScoreClub = body['data'];
        setState(() {
          _isLoadingRecentScore = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoadingRecentScore = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingRecentScore = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBar(),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading || _isLoadingRecentScore,
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCreate(title: widget.fromSeeAllRecentScoreClub == null ? "Club Leaderboard" : "Club Recent Scores",),
                  ClubNameLogo(
                    dataClub: widget.dataClub,
                  ),
                  widget.fromSeeAllRecentScoreClub == null ? Column(
                    children: [
                      leaderBoardCategory(),
                      listLeaderBoard(),
                      SizedBox(height: 15,),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 0),
                        child: Text("Leaderboard based average score since Jan 1st each year", style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 13), textAlign: TextAlign.center,),
                      ),
                    ],
                  ) : Column(
                  children: [
                    Text("click to see their full scores.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
                    listScoreCard(),
                  ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column listScoreCard() {
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataRecentScoreClub==null?0:dataRecentScoreClub['recent_score'].length,
          itemBuilder: (context, index){
            return Container(
              margin: EdgeInsets.only(bottom: 4),
              // padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xFFF9F9F9),
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color(0xFFE5E5E5))
              ),
              child: FlatButton(
                padding: EdgeInsets.all(5),
                splashColor: Colors.grey.withOpacity(0.2),
                textTheme: ButtonTextTheme.normal,
                onPressed: (){
                  if(dataRecentScoreClub['recent_score'][index]['turnamen']!=null) {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            MyScore(
                              idTour: dataRecentScoreClub['recent_score'][index]['turnamen']['turnamen_id'],
                              buddyScore: true,
                              buddyToken: dataRecentScoreClub['recent_score'][index]['token'],)));
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    dataRecentScoreClub['recent_score']==null?Container():dataRecentScoreClub['recent_score'][index]['foto'] == null ? ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: Image.asset(
                        "assets/images/userImage.png",
                        width: 40,
                        height: 40,
                      ),
                    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                        CircularProgressIndicator(
                          value: progress.progress,
                        ), imageUrl: Utils.url + 'upload/user/' + dataRecentScoreClub['recent_score'][index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                    SizedBox(width: 8,),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${dataRecentScoreClub['recent_score']==null?"":dataRecentScoreClub['recent_score'][index]['nama_pendek']!=null?dataRecentScoreClub['recent_score'][index]['nama_pendek']+", ":""}${dataRecentScoreClub['recent_score'][index]['nama_lengkap']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,),
                        dataRecentScoreClub['recent_score'][index]['turnamen'] == null ? Container() : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${dataRecentScoreClub['recent_score'][index]['turnamen']['nama_turnamen']}", style: TextStyle(color: AppTheme.par),),
                            Text("${dataRecentScoreClub['recent_score'][index]['turnamen']['penyelenggara_turnamen']}, ${myFormat.format(DateTime.parse(dataRecentScoreClub['recent_score'][index]['turnamen']['tanggal_turnamen']))}", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                          ],
                        ),
                      ],
                    )),
                    Container(
                      // padding: EdgeInsets.all(10),
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              colors: [Color(0xFF1857B7), Color(0xFF5DEED4)]),
                          shape: BoxShape.circle
                      ),
                      child: Center(child: Text("${dataRecentScoreClub['recent_score']==null?"":dataRecentScoreClub['recent_score'][index]['nett']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,)),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );
  }


  Column listLeaderBoard() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15.0, bottom: 10),
          child: Text(_catGross ? "Gross" : _catNettOverall ? "Nett Overall" : _catNettFlightA ? "Flight A" : _catNettFlightB ? "Flight B" : _catNettFlightC ? "Flight C" : _catAchievement ?  "Achievement Point" : "Strongest", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22), textAlign: TextAlign.center
        )),
        _isLoading ? Container() : dataLeaderBoard==null||dataLeaderBoard.isEmpty ? Container(child: Text("Nothing to show here.", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),) : ListView.builder(
          primary: false,
          shrinkWrap: true,
          itemCount: dataLeaderBoard.length,
          itemBuilder: (context, i){
            return i == 0 ? InkWell(
              onTap: () => Utils.linkToGolferProfile(context, dataLeaderBoard[i]['profil']),
              child: Container(
                padding: EdgeInsets.all(3),
                width: double.maxFinite,
                // height: 106,
                decoration: BoxDecoration(
                  color: Color(0xFFF9F9F9),
                  borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Color(0xFFE5E5E5))
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 6,),
                    Text("#${dataLeaderBoard[i]['rank']}", style: TextStyle(color: Color(0xFFF2C94C), fontSize: 22, fontWeight: FontWeight.bold),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(width: 10,),
                        dataLeaderBoard[i]['foto'] == null ? ClipRRect(
                          borderRadius: BorderRadius.circular(10000.0),
                          child: Image.asset(
                            "assets/images/userImage.png",
                            width: 60,
                            height: 60,
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + dataLeaderBoard[i]['foto'], width: 60, height: 60, fit: BoxFit.cover,)),
                        SizedBox(width: 8,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(dataLeaderBoard[i]['nama'], style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack), overflow: TextOverflow.clip,),
                              Text("Played Round ${dataLeaderBoard[i]['played_round']}${_catNett?", HC "+dataLeaderBoard[i]['hc'].toString():""}", style: TextStyle(color: AppTheme.gFont),)
                            ],
                          ),
                        ),
                        SizedBox(width: 8,),
                        dataLeaderBoard == "empty" ? Container() : Padding(
                          padding: const EdgeInsets.only(bottom: 0.0),
                          child: Text(dataLeaderBoard == null ? "0" : "${_catNett?dataLeaderBoard[i]['nett']:_catGross?dataLeaderBoard[i]['gross']:_catAchievement?dataLeaderBoard[i]['poin']:dataLeaderBoard[i]['distance']}", style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),),
                        ),
                        SizedBox(width: 5,),
                      ],
                    ),
                    SizedBox(height: 6,),
                  ],
                ),
              ),
            ) : i == 1 || i == 2 ? InkWell(
              onTap: () => Utils.linkToGolferProfile(context, dataLeaderBoard[i]['profil']),
              child: Container(
                margin: EdgeInsets.only(top: 5),
                padding: EdgeInsets.all(8),
                width: double.maxFinite,
                // height: 70,
                decoration: BoxDecoration(
                    color: Color(0xFFF9F9F9),
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Color(0xFFE5E5E5))
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        // SizedBox(width: 5,),
                        Container(width: 40, child: Text("#${dataLeaderBoard[i]['rank']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 22, fontWeight: FontWeight.bold), textAlign: TextAlign.center,)),
                        SizedBox(width: 5,),
                        dataLeaderBoard[i]['foto'] == null ? ClipRRect(
                          borderRadius: BorderRadius.circular(10000.0),
                          child: Image.asset(
                            "assets/images/userImage.png",
                            width: 40,
                            height: 40,
                          ),
                        ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                            CircularProgressIndicator(
                              value: progress.progress,
                            ), imageUrl: Utils.url + 'upload/user/' + dataLeaderBoard[i]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                        SizedBox(width: 8,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${dataLeaderBoard[i]['nama']}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack), overflow: TextOverflow.clip,),
                              Text("Played Round ${dataLeaderBoard[i]['played_round']}${_catNett?", HC "+dataLeaderBoard[i]['hc'].toString():""}", style: TextStyle(color: AppTheme.gFont),)
                            ],
                          ),
                        ),
                        SizedBox(width: 8,),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0.0),
                          child: Text("${_catNett?dataLeaderBoard[i]['nett']:_catGross?dataLeaderBoard[i]['gross']:_catAchievement?dataLeaderBoard[i]['poin']:dataLeaderBoard[i]['distance']}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),),
                        ),
                        SizedBox(width: 5,),
                      ],
                    ),
                  ],
                ),
              ),
            ) : InkWell(
              onTap: () => Utils.linkToGolferProfile(context, dataLeaderBoard[i]['profil']),
              child: Container(
                margin: EdgeInsets.only(top: 5),
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width: 20,),
                          Container(width: 20, child: Text("${dataLeaderBoard[i]['rank']}", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,)),
                          SizedBox(width: 22,),
                          dataLeaderBoard[i]['foto'] == null ? ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.asset(
                              "assets/images/userImage.png",
                              width: 20,
                              height: 20,
                            ),
                          ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                              CircularProgressIndicator(
                                value: progress.progress,
                              ), imageUrl: Utils.url + 'upload/user/' + dataLeaderBoard[i]['foto'], width: 20, height: 20, fit: BoxFit.cover,)),
                          SizedBox(width: 8,),
                          Flexible(child: Text("${dataLeaderBoard[i]['nama']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16), overflow: TextOverflow.clip,)),
                          SizedBox(width: 5,),
                          Text("(${dataLeaderBoard[i]['played_round']}${_catNett?", "+dataLeaderBoard[i]['hc'].toString():""})", style: TextStyle(color: AppTheme.gFont, fontSize: 16),),
                          SizedBox(width: 5,),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 18.0),
                      child: Text("${_catNett?dataLeaderBoard[i]['nett']:_catGross?dataLeaderBoard[i]['gross']:_catAchievement?dataLeaderBoard[i]['poin']:dataLeaderBoard[i]['distance']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16), textAlign: TextAlign.right,),
                    )
                  ],
                ),
              ),
            );
          },),
      ],
    );
  }

  Stack leaderBoardCategory() {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(top: 16),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xFFF9F9F9),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: (){
                          setState(() {
                            if(_catGross==false){
                              _catNett = false;
                              _catGross = true;
                              _catAchievement = false;
                              _catStrongest = false;
                              getLeaderBoardClub("Gross", "");
                            }
                          });
                        }, child: Text("Gross", style: TextStyle(color: _catGross ? Color(0xFFF15411) : AppTheme.gFont),)),
                    InkWell(
                        onTap: (){
                          setState(() {
                            if(_catNett==false){
                              _catNett = true;
                              _catGross = false;
                              _catAchievement = false;
                              _catStrongest = false;
                              _catNettOverall=true;
                              _catNettFlightA=false;
                              _catNettFlightB=false;
                              _catNettFlightC=false;
                              getLeaderBoardClub("Nett", "Overall");
                            }
                          });
                        }, child: Text("Nett", style: TextStyle(color: _catNett ? Color(0xFFF15411) : AppTheme.gFont),)),
                    InkWell(onTap: (){
                      setState(() {
                        if(_catAchievement==false){
                          _catNett = false;
                          _catGross = false;
                          _catAchievement = true;
                          _catNettOverall=false;
                          _catNettFlightA=false;
                          _catNettFlightB=false;
                          _catNettFlightC=false;
                          _catStrongest = false;
                          getLeaderBoardClub("Achievement");
                        }
                      });
                    }, child: Text("Achievement", style: TextStyle(color: _catAchievement ? Color(0xFFF15411) : AppTheme.gFont),)),
                    InkWell(onTap: (){
                      setState(() {
                        if(_catStrongest==false){
                          _catNett = false;
                          _catGross = false;
                          _catAchievement = false;
                          _catStrongest = true;
                          _catNettOverall=false;
                          _catNettFlightA=false;
                          _catNettFlightB=false;
                          _catNettFlightC=false;
                          getLeaderBoardClub("Strongest", "Overall");
                        }
                      });
                    }, child: Text("Strongest", style: TextStyle(color: _catStrongest ? Color(0xFFF15411) : AppTheme.gFont),)),
                  ],
                ),
              ),
              _catNett ? Container(
                margin: EdgeInsets.only(top: 8),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  border: Border.all(color: Color(0xFFE5E5E5))
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                        onTap: (){
                          setState(() {
                            if(_catNettOverall==false){
                              _catNettOverall=true;
                              _catNettFlightA=false;
                              _catNettFlightB=false;
                              _catNettFlightC=false;
                              getLeaderBoardClub("Nett", "Overall");
                            }
                          });
                        }, child: Text("Overall", style: TextStyle(color: _catNettOverall ? Color(0xFFF15411) : AppTheme.gFontBlack),)),
                    InkWell(
                        onTap: (){
                          setState(() {
                            if(_catNettFlightA==false){
                              _catNettOverall=false;
                              _catNettFlightA=true;
                              _catNettFlightB=false;
                              _catNettFlightC=false;
                              getLeaderBoardClub("Nett", "Flight A");
                            }
                          });
                        }, child: Text("Flight A", style: TextStyle(color: _catNettFlightA ? Color(0xFFF15411) : AppTheme.gFontBlack),)),
                    InkWell(
                        onTap: (){
                          setState(() {
                            if(_catNettFlightB==false){
                              _catNettOverall=false;
                              _catNettFlightA=false;
                              _catNettFlightB=true;
                              _catNettFlightC=false;
                              getLeaderBoardClub("Nett", "Flight B");
                            }
                          });
                        }, child: Text("Flight B", style: TextStyle(color: _catNettFlightB ? Color(0xFFF15411) : AppTheme.gFontBlack),)),
                    InkWell(onTap: (){
                      setState(() {
                        if(_catNettFlightC==false){
                          _catNettOverall=false;
                          _catNettFlightA=false;
                          _catNettFlightB=false;
                          _catNettFlightC=true;
                          getLeaderBoardClub("Nett", "Flight C");
                        }
                      });
                    }, child: Text("Flight C", style: TextStyle(color: _catNettFlightC ? Color(0xFFF15411) : AppTheme.gFontBlack),)),
                  ],
                ),
              ) : Container()
            ],
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.only(left: 10),
            padding: EdgeInsets.all(5),
            width: 112,
            decoration: BoxDecoration(
                color: Color(0xFFF15411),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Text("Leaderboards", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
          ),
        ),
      ],
    );
  }
}

class ClubNameLogo extends StatelessWidget {
  const ClubNameLogo({
    Key key,
    @required this.dataClub
  }) : super(key: key);

  final dataClub;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(margin: EdgeInsets.only(left: 10),
              child: dataClub['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                  CircularProgressIndicator(
                    value: progress.progress,
                  ), imageUrl: Utils.url + 'upload/club/' + dataClub['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
          ),
          SizedBox(width: 8,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(width: 200, child: Center(child: Text("${dataClub['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,))),
              Text("${dataClub['kota']}, ${dataClub['negara']}", style: TextStyle(color: AppTheme.gFontBlack),)
            ],
          )
        ],
      ),
    );
  }
}
