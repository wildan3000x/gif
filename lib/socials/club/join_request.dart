import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class JoinRequest extends StatefulWidget {
  const JoinRequest({Key key, this.idClub, this.idNotif}) : super (key: key);
  final idClub, idNotif;
  @override
  _JoinRequestState createState() => _JoinRequestState();
}

class _JoinRequestState extends State<JoinRequest> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getRequestAll();
      getMyClub();
    }
  }

  var dataRequestAll;

  void getRequestAll() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'club_golf_id': widget.idClub
    };

    Utils.postAPI(data, "all_request_jc").then((body) {
      if(body['status'] == 'success'){
        dataRequestAll = body['data'];
        setState(() {
          // _isLoading = false;
        });
      }else{
        dataRequestAll = null;
        // var snackbar = SnackBar(
        //   content: Text(body['message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataMyClub;
  var checkAdmin;

  void getMyClub() async {
    setState(() {
      // _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_club_golf': widget.idClub
    };

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataMyClub = body['data'];
        checkAdmin = body['admin'];
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  List<bool> _isLoadingAction = [];
  void confirmRequestClub(int id, String action, [int index]){
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_golfer_invite': id,
      'aksi': action,
      'id_notifikasi': null
    };

    Utils.postAPI(data, "confirm_request_jc").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoadingAction[index] = false;
        });
        var snackbar = SnackBar(
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.blueAccent,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        getRequestAll();
        getMyClub();
      }else{
        setState(() {
          _isLoadingAction[index] = false;
        });
        var snackbar = SnackBar(
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        getRequestAll();
        getMyClub();
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingAction[index] = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(13),
              child: /*_isLoading ? Center(
                heightFactor: 20,
                child: Container(
                  width: 30,
                  height: 30,
                  child: CircularProgressIndicator(),
                ),
              ) : */Column(
                children: [
                  HeaderCreate(title: dataMyClub==null?"":dataMyClub['club']['singkatan'], disableBack: true,),
                  clubNameLogo(),
                  SizedBox(height: 10,),
                  dataRequestAll == null || dataRequestAll.length == 0 ? Container(
                    margin: EdgeInsets.all(45),
                    child: Text("No request at this moment."),
                  ) : ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: dataRequestAll == null ? 0 : dataRequestAll.length,
                    itemBuilder: (context, index){
                      _isLoadingAction.add(false);
                      return Container(
                        margin: EdgeInsets.only(bottom: 8),
                        padding: EdgeInsets.only(top: 5, left: 5, right: 5, bottom: 10),
                        decoration: BoxDecoration(
                          color: Color(0xFFF9F9F9),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Color(0xFFE5E5E5))
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            dataRequestAll[index]['foto'] == null ? ClipRRect(
                              borderRadius: BorderRadius.circular(10000.0),
                              child: Image.asset(
                                "assets/images/userImage.png",
                                width: 40,
                                height: 40,
                              ),
                            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                CircularProgressIndicator(
                                  value: progress.progress,
                                ), imageUrl: Utils.url + 'upload/user/' + dataRequestAll[index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                            SizedBox(width: 8,),
                            Expanded(child: Column(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(child: Text("${dataRequestAll[index]['nama']}", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold, fontSize: 20),)),
                                    Text("HC: ${dataRequestAll[index]['hc']}")
                                  ],
                                ),
                                SizedBox(height: 3,),
                                Container(
                                    padding: EdgeInsets.all(3),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: Colors.grey.shade200)
                                    ),
                                    width: double.maxFinite,
                                    child: Text("${dataRequestAll[index]['alasan']}", style: TextStyle(color: AppTheme.gFont), overflow: TextOverflow.clip,)),
                                SizedBox(height: 8,),
                                _isLoadingAction[index] ? Center(
                                  child: Container(
                                    width: 15,
                                    height: 15,
                                    child: CircularProgressIndicator(),
                                  ),
                                ) : Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    InkWell(
                                      onTap: (){
                                        setState(() {
                                          _isLoadingAction[index] = true;
                                          confirmRequestClub(dataRequestAll[index]['id_golfer_invite'], "Approve", index);
                                        });
                                      },
                                      child: Row(
                                        children: [
                                          SvgPicture.asset("assets/svg/check.svg", height: 18, width: 18, color: AppTheme.gButton,),
                                          Text("Approve", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold),)
                                        ],
                                      ),
                                    ),
                                    InkWell(
                                      onTap: (){
                                        setState(() {
                                          _isLoadingAction[index] = true;
                                          confirmRequestClub(dataRequestAll[index]['id_golfer_invite'], "Reject", index);
                                        });
                                      },
                                      child: Row(
                                        children: [
                                          SvgPicture.asset("assets/svg/reject.svg", height: 18, width: 18, color: Color(0xFFF15411),),
                                          Text("Reject", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold),)
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ))
                          ],
                        )
                        /*Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                dataRequestAll[index]['foto'] == null ? ClipRRect(
                                  borderRadius: BorderRadius.circular(10000.0),
                                  child: Image.asset(
                                    "assets/images/userImage.png",
                                    width: 40,
                                    height: 40,
                                  ),
                                ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                    CircularProgressIndicator(
                                      value: progress.progress,
                                    ), imageUrl: Utils.url + 'upload/user/' + dataRequestAll[index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                                SizedBox(width: 5,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Expanded(child: Text("${dataRequestAll[index]['nama']}", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold, fontSize: 20),)),
                                          SizedBox(width: 20,),
                                          Text("HC: ${dataRequestAll[index]['hc']}")
                                        ],
                                      ),
                                      SizedBox(height: 3,),
                                      Container(
                                        padding: EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(5),
                                            border: Border.all(color: Colors.grey.shade200)
                                          ),
                                          width: 270,
                                          child: Text("${dataRequestAll[index]['alasan']}", style: TextStyle(color: AppTheme.gFont), overflow: TextOverflow.clip,)),
                                      SizedBox(height: 10,),
                                      Container(
                                        width: 250,
                                        child: _isLoadingAction[index] ? Center(
                                          child: Container(
                                            width: 15,
                                            height: 15,
                                            child: CircularProgressIndicator(),
                                          ),
                                        ) : Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            InkWell(
                                              onTap: (){
                                                setState(() {
                                                  _isLoadingAction[index] = true;
                                                  confirmRequestClub(dataRequestAll[index]['id_golfer_invite'], "Approve", index);
                                                });
                                              },
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset("assets/svg/check.svg", height: 18, width: 18, color: AppTheme.gButton,),
                                                  Text("Approve", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold),)
                                                ],
                                              ),
                                            ),
                                            InkWell(
                                              onTap: (){
                                                setState(() {
                                                  _isLoadingAction[index] = true;
                                                  confirmRequestClub(dataRequestAll[index]['id_golfer_invite'], "Reject", index);
                                                });
                                              },
                                              child: Row(
                                                children: [
                                                  SvgPicture.asset("assets/svg/reject.svg", height: 18, width: 18, color: Color(0xFFF15411),),
                                                  Text("Reject", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold),)
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        )*/,
                      );
                    },)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget clubNameLogo() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(left: 10),
                child: dataMyClub==null||dataMyClub['club']['logo']==null ? ClipRRect(borderRadius: BorderRadius.circular(5), child: Image.asset("assets/images/no-image.png", width: 40, height: 40,)) : ClipRRect(borderRadius: BorderRadius.circular(5.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    CircularProgressIndicator(
                      value: progress.progress,
                    ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['club']['logo'], width: 40, height: 40, fit: BoxFit.cover,)),
              ),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(width: 200, child: Center(child: Text("${dataMyClub==null?"":dataMyClub['club']['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,))),
                  Text("${dataMyClub==null?"":dataMyClub['club']['kota']}, ${dataMyClub==null?"":dataMyClub['club']['negara']}", style: TextStyle(color: AppTheme.gFontBlack),)
                ],
              )
            ],
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 5), child: InkWell(onTap: ()=> Navigator.pop(context, 'reload'), child: Icon(Icons.arrow_back))),
            Expanded(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0, bottom: 10),
                  child: Text("Request To Join The Club", style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 22),),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
