import 'package:flutter/material.dart';
import 'package:gif/create_games/edit_games.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'leaderboards_club.dart';

class NextEventClub extends StatefulWidget {
  const NextEventClub({Key key, @required this.dataClub}) : super(key: key);
  final dataClub;

  @override
  _NextEventClubState createState() => _NextEventClubState();
}

class _NextEventClubState extends State<NextEventClub> {
  var dataNextEvent;
  bool _isLoading = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;


  @override
  void initState() {
    getDataNextEvent();
    // TODO: implement initState
    super.initState();
  }

  Future<void> getDataNextEvent() async {
    sharedPreferences = await SharedPreferences.getInstance();

    final mToken = await Utils.getToken(context);
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : mToken,
      'id_club_golf': widget.dataClub['club']['id_club_golf'],
      'id_notifikasi': null
    };

    Utils.postAPI(data, "get_HC").then((body) {
      if(body['status'] == 'success'){
        dataNextEvent = body['data']['seeall_turnamen'];
        dataNextEvent.removeWhere((item) => item['status'] == "Not Invited" && item['partisipan_turnamen'] == "By Invitation");
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getDataNextEvent();
    _refreshController.refreshCompleted();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderCreate(title: "Club Next Events",),
                    ClubNameLogo(
                      dataClub: widget.dataClub['club'],
                    ),
                    Text("Next Events", style: TextStyle(color: Color(0xFFF15411), fontWeight: FontWeight.bold, fontSize: 22),),
                    ListView.builder(
                      itemCount: dataNextEvent==null?0:dataNextEvent.length,
                      shrinkWrap: true,
                      primary: false,
                      itemBuilder: (context, i) {
                        return dataNextEvent[i]['status'] == "Not Invited" && dataNextEvent[i]['partisipan_turnamen'] == "By Invitation" ? Container()
                            : ListContainerHome(
                          widgetCorner: Container(),
                          link: (){
                            if(dataNextEvent[i]['status_organizer'] == true){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  SummaryGames(tourId: dataNextEvent[i]['id_turnamen'], viewFromNextEventsClub: true,))).then((value) => setState(() {
                                getDataNextEvent();
                              }));
                            }else if(dataNextEvent[i]['status_join'] == false && dataNextEvent[i]['partisipan_turnamen'] == 'Open' && dataNextEvent[i]['status'] == "Not Invited" || dataNextEvent[i]['status'] == "Invited"){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  JoinGames(data: dataNextEvent[i]))).then((value) => setState(() {
                                getDataNextEvent();
                              }));
                            }else if(dataNextEvent[i]['status_join'] == false && dataNextEvent[i]['partisipan_turnamen'] == 'By Invitation' && dataNextEvent[i]['status'] == "Invited"){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  JoinGames(data: dataNextEvent[i]))).then((value) => setState(() {
                                getDataNextEvent();
                              }));
                            }else if(dataNextEvent[i]['status_join'] == false && dataNextEvent[i]['status'] == "Applying"){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  PairingGames(data: dataNextEvent[i]['status'],
                                    msg: dataNextEvent[i]['status'] == "Applying" ? "Waiting For Approval" : dataNextEvent[i]['status'] == "Not Confirmed" ? "You have been Rejected" : null,
                                    sub_msg: dataNextEvent[i]['status'] == "Applying" ? "You still need approval from Organizer before join this game" : dataNextEvent[i]['status'] == "Not Confirmed" ? "your request to enter this game has been denied\nso you cannot enter this game." : null,
                                    id: dataNextEvent[i]['id_turnamen'],
                                    userId: sharedPreferences.getString('id'),
                                  ))).then((value) => setState(() {
                                getDataNextEvent();
                                print("RELOAD");
                              }));
                            }else if(dataNextEvent[i]['status'] == 'Confirmed'){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => StartGame(data: dataNextEvent[i], status: dataNextEvent[i]['status'], id: dataNextEvent[i]['id_turnamen'], userId: dataNextEvent[i]['user_id'],))).then((value) => setState(() {
                                getDataNextEvent();
                              }));
                            }else if(dataNextEvent[i]['status'] == "Not Confirmed"){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  JoinGames(data: dataNextEvent[i]))).then((value) => setState(() {
                                getDataNextEvent();
                              }));
                            }
                          },
                          date: dataNextEvent[i]['tanggal_turnamen'],
                          cityTour: dataNextEvent[i]['kota_turnamen'],
                          courseTour: dataNextEvent[i]['nama_lapangan'],
                          nameTour: dataNextEvent[i]['nama_turnamen'],
                          orgaTour: dataNextEvent[i]['penyelenggara_turnamen'],
                          forColorHistory: null,
                        );
                      },)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
