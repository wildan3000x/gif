import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/my_games/scorecards/my_score.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';

class AddBuddy extends StatefulWidget {
  const AddBuddy({Key key, this.buddyScore}) : super (key: key);
  final buddyScore;
  @override
  _AddBuddyState createState() => _AddBuddyState();
}

class _AddBuddyState extends State<AddBuddy> {
  var dataMyClub;
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('d MMM yyyy');


  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      if(widget.buddyScore){
        getBuddyScore();
      }else {
        getAllBuddy();
      }
    }
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();
  RefreshController refreshController = RefreshController(initialRefresh: false);
  int queryPage = 1;


  var dataBuddy;
  var checkAdmin;

  void getAllBuddy({bool initialLoading = true}) async {
    if (initialLoading) {
      setState(() {
        _isLoading = true;
      });
      refreshController.refreshCompleted();
      queryPage = 1;
    } else {
      queryPage++;
    }

    var data = {
      'token' : sharedPreferences.getString("token"),
      'page': queryPage
    };

    // setState(() {
    //   _member.clear();
    //   _allMember.clear();
    //   _uiMember.clear();
    // });

    Utils.postAPI(data, "all_user").then((body) {
      if(body['status'] == 'success'){
        dataBuddy = body['data'];
        setState(() {
          List<DefaultMembers> tempMember = new List<DefaultMembers>();
          List<CustomMembers> tempUiMember = List<CustomMembers>();
          List<CustomMembers> tempAllMember = List<CustomMembers>();
          for (int i = 0; i < dataBuddy.length; i++) {
            DefaultMembers def = DefaultMembers(id: dataBuddy[i]['id'], nama_lengkap: dataBuddy[i]["nama_lengkap"], nickName: dataBuddy[i]["nama_pendek"], kursus: dataBuddy[i]['nama_lapangan'], kota: dataBuddy[i]["kota"], negara: dataBuddy[i]["negara"], gif_id: dataBuddy[i]["kode_user"], foto: dataBuddy[i]["foto"], buttonAdd: dataBuddy[i]["button_add"]);
            tempMember.add(def);
            tempAllMember.add(CustomMembers(showAll: def));
            tempUiMember.add(CustomMembers(showAll: def));
            // print("DATA ${_uiMember[i].showAll.nama_lengkap}");
          }

          if (!initialLoading) {
            _member.addAll(tempMember);
            _uiMember.addAll(tempAllMember);
            _allMember.addAll(tempUiMember);
            refreshController.loadComplete();
          } else {
            _member = tempMember;
            _uiMember = tempAllMember;
            _allMember = tempUiMember;
          }
        });

        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataScoreBuddy;
  void getBuddyScore() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_recent_score").then((body) {
      if(body['status'] == 'success'){
        dataScoreBuddy = body['data'];
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  List<bool> _isLoadingAction = [];
  void addBuddies(int id, [int index]){
    var data = {
      'token' : sharedPreferences.getString("token"),
      'id_user': id,
    };

    Utils.postAPI(data, "add_buddies").then((body) {
      if(body['status'] == 'success'){
        Navigator.pop(context);
        var snackbar = SnackBar(
          duration: Duration(seconds: 2),
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.blueAccent,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        setState(() {
          _isLoadingAction[index] = false;
        });
        getAllBuddy();
      }else{
        Navigator.pop(context);
        var snackbar = SnackBar(
          duration: Duration(seconds: 2),
          content: Text(body['message'], style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        print("MSG ${body['message']}");
        setState(() {
          _isLoadingAction[index] = false;
        });
        getAllBuddy();
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase()) || item.showAll.nickName==null?item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase()):item.showAll.nickName.toLowerCase().contains(mQuery.toLowerCase()) || item.showAll.kursus.toLowerCase().contains(mQuery.toLowerCase()) || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase()) || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase()) || item.showAll.gif_id.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: refreshController,
            onRefresh: () => getAllBuddy(),
            enablePullUp: widget.buddyScore ? false : true,
            enablePullDown: widget.buddyScore ? false : true,
            onLoading: () => getAllBuddy(initialLoading: false),
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderCreate(title: widget.buddyScore ? "Buddies Recent Score" : "Add Buddy",),
                    Container(
                      padding: EdgeInsets.only(bottom: 8, top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Padding(
                          //   padding: const EdgeInsets.only(right: 6.0, top: 2),
                          //   child: InkWell(onTap: () => Navigator.pop(context), child: SvgPicture.asset("assets/svg/entry_arrow_left.svg", color: AppTheme.gButton, width: 30, height: 30,)),
                          // ),
                          Expanded(
                            child: TextAfterHeader(text: widget.buddyScore ? "List of Buddies Recent Score" :"Add Buddy",),
                          ),
                        ],
                      ),
                    ),
                    widget.buddyScore ? Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: InkWell(
                        splashColor: Colors.grey.withOpacity(0.2),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => AddBuddy(buddyScore: false,))).then((value) => checkLoginStatus());
                        },
                        child: Text("Add More Buddy", style: TextStyle(color: AppTheme.bogies, fontWeight: FontWeight.bold, fontSize: 14),),
                      ),
                    ) : Container(),
                    widget.buddyScore ? Column(
                      children: [
                        Text("click the bubble to see their full scores.", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
                        listScoreCard(),
                      ],
                    ) : searchAddBuddy(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column listScoreCard() {
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataScoreBuddy==null?0:dataScoreBuddy.length,
          itemBuilder: (context, index){
            return Container(
              margin: EdgeInsets.only(bottom: 4),
              // padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xFFF9F9F9),
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color(0xFFE5E5E5))
              ),
              child: FlatButton(
                // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                materialTapTargetSize: MaterialTapTargetSize.padded,
                padding: EdgeInsets.all(5),
                splashColor: Colors.grey.withOpacity(0.2),
                textTheme: ButtonTextTheme.normal,
                onPressed: (){
                  if(dataScoreBuddy[index]['turnamen']!=null) {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => GolferProfileScore(
                          dataGolfer: dataScoreBuddy[index],
                        )
                            // MyScore(
                            //   idTour: dataScoreBuddy[index]['turnamen']['turnamen_id'],
                            //   buddyScore: true,
                            //   buddyToken: dataScoreBuddy[index]['token'],)
                    ));
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    dataScoreBuddy==null?Container():dataScoreBuddy[index]['foto'] == null ? ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: Image.asset(
                        "assets/images/userImage.png",
                        width: 40,
                        height: 40,
                      ),
                    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                        CircularProgressIndicator(
                          value: progress.progress,
                        ), imageUrl: Utils.url + 'upload/user/' + dataScoreBuddy[index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                    SizedBox(width: 8,),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${dataScoreBuddy==null?"":dataScoreBuddy[index]['nama_pendek']!=null?dataScoreBuddy[index]['nama_pendek']+", ":""}${dataScoreBuddy[index]['nama_lengkap']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,),
                        dataScoreBuddy[index]['turnamen'] == null ? Container() : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${dataScoreBuddy[index]['turnamen']['nama_turnamen']}", style: TextStyle(color: AppTheme.par),),
                            Text("${dataScoreBuddy[index]['turnamen']['penyelenggara_turnamen']}, ${myFormat.format(DateTime.parse(dataScoreBuddy[index]['turnamen']['tanggal_turnamen']))}", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                          ],
                        ),
                      ],
                    )),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => MyScore(
                              idTour: dataScoreBuddy[index]['turnamen']['turnamen_id'],
                              buddyScore: true,
                              buddyToken: dataScoreBuddy[index]['token'],)
                        ));
                      },
                      child: Container(
                        // padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                                colors: [Color(0xFF1857B7), Color(0xFF5DEED4)]),
                            shape: BoxShape.circle
                        ),
                        width: 40,
                        height: 40,
                        child: Center(child: Text("${dataScoreBuddy==null?"":dataScoreBuddy[index]['nett']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,)),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );
  }
  
  Column searchAddBuddy() {
    return Column(
      children: [
        Container(
          width: 250,
          height: 45,
          padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 4, top: 0),
          child: TextFormField(
            decoration: InputDecoration(
              labelText: "Search Buddy",
              hintText: "Search",
              suffixIcon: Icon(Icons.search),
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
            ),
            controller: controllerSearch3,
            onChanged: (value) {
              _searchMembers(value);
            },
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 5.0, top: 5),
        //   child: Text("showing ${dataBuddy==null?0:_uiMember.length} out of ${dataBuddy==null?0:dataBuddy.length} golfers", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
        // ),
        Padding(
          padding: const EdgeInsets.only(top: 0.0),
          child: _uiMember==null || _uiMember.length <= 0 ? Container(margin: EdgeInsets.all(45), child: Text("There's no one here.", style: TextStyle(color: AppTheme.gFontBlack),),) :
          ListView.builder(
            itemCount: _uiMember==null || _uiMember.length <= 0 ? 0 : /*_uiMember.length>=10?10:*/_uiMember.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, i){
              _isLoadingAction.add(false);
              return GolferCard(
                picture: _uiMember[i].showAll.foto,
                nickName: _uiMember[i].showAll.nickName,
                fullName: _uiMember[i].showAll.nama_lengkap,
                course: _uiMember[i].showAll.kursus,
                city: _uiMember[i].showAll.kota,
                country: _uiMember[i].showAll.negara,
                buttonAction: _uiMember[i].showAll.buttonAdd == "true" ? InkWell(onTap: (){
                  showDialog(
                      context: snackbarKey.currentContext,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                          contentPadding: EdgeInsets.all(8),
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _uiMember[i].showAll.foto == null ? ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Image.asset(
                                  "assets/images/userImage.png",
                                  width: 60,
                                  height: 60,
                                ),
                              ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                                  CircularProgressIndicator(
                                    value: progress.progress,
                                  ), imageUrl: Utils.url + 'upload/user/' + _uiMember[i].showAll.foto, width: 60, height: 60, fit: BoxFit.cover,)),
                              SizedBox(height: 5,),
                              Text("Add ${_uiMember[i].showAll.nama_lengkap} ?", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                            ],
                          ),
                          content: Container(
                            height: 60,
                            width: double.maxFinite,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                    splashColor: Colors.grey.withOpacity(0.5),
                                    onPressed: () { Navigator.pop(context);},
                                    padding: const EdgeInsets.all(0.0),
                                    child: Ink(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: AppTheme.bogies,
                                            borderRadius: BorderRadius.circular(100)
                                        ),
                                        child: Container(
                                            constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                            alignment: Alignment.center,
                                            child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                  ),
                                ),
                                Container(
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                    splashColor: Colors.grey.withOpacity(0.5),
                                    onPressed: () {
                                      setState(() {
                                        addBuddies(_uiMember[i].showAll.id, i);
                                      });
                                    },
                                    padding: const EdgeInsets.all(0.0),
                                    child: Ink(
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: AppTheme.gButton,
                                            borderRadius: BorderRadius.circular(100)
                                        ),
                                        child: Container(
                                            constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                            alignment: Alignment.center,
                                            child: Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }
                  );
                }, child: SvgPicture.asset("assets/svg/add_friend.svg", width: 22, height: 22, color: AppTheme.par,)) :
                Container(
                  child: SvgPicture.asset("assets/svg/friends.svg", width: 22, height: 22, color: AppTheme.gButton,),
                ),
              );
            },),
        )
      ],
    );
  }
}

class GolferCard extends StatelessWidget {
  const GolferCard({
    Key key,
    @required this.picture,
    @required this.fullName,
    @required this.nickName,
    @required this.course,
    @required this.city,
    @required this.country,
    @required this.buttonAction,
  }) : super(key: key);

  final picture, nickName, fullName, course, city, country;
  final Widget buttonAction;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      padding: EdgeInsets.all(6),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xFFE5E5E5)),
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Text("${i+1}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 16),),
          // SizedBox(width: 5,),
          picture == null ? ClipRRect(
            borderRadius: BorderRadius.circular(10000.0),
            child: Image.asset(
              "assets/images/userImage.png",
              width: 40,
              height: 40,
            ),
          ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
              CircularProgressIndicator(
                value: progress.progress,
              ), imageUrl: Utils.url + 'upload/user/' + picture, width: 40, height: 40, fit: BoxFit.cover,)),
          SizedBox(width: 8,),
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${nickName!=null?nickName+", ":""}$fullName", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,),
              Text("$course", style: TextStyle(color: AppTheme.par),),
              Text("$city, $country", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic),),
            ],
          )),
          SizedBox(width: 5,),
          buttonAction,
        ],
      ),
    );
  }
}

class TextAfterHeader extends StatelessWidget {
  const TextAfterHeader({
    Key key,
    @required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "$text",
        style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Color(0xFF4F4F4F),
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class DefaultMembers{
  final int id;
  final String nama_lengkap;
  final String nickName;
  final String kursus;
  final String kota;
  final String negara;
  final String gif_id;
  final String foto;
  final String buttonAdd;
  final dynamic allData;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.nama_lengkap,
    this.nickName,
    this.kursus,
    this.kota,
    this.negara,
    this.gif_id,
    this.foto,
    this.buttonAdd,
    this.allData
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
