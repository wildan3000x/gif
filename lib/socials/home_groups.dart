import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/auth/profile/profile_page.dart';
import 'package:gif/create_games/pairing/view_profile.dart';
import 'package:gif/socials/club/home_club.dart';
import 'package:gif/socials/club/profile_club.dart';
import 'package:gif/socials/create_group.dart';
import 'package:gif/header_home.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/notification/notif_golfer.dart';
import 'package:gif/notification/notif_running.dart';
import 'package:gif/utils/Utils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../main.dart';
import 'buddys/add_buddy.dart';
import 'see_all_social.dart';


class HomeGroups extends StatefulWidget {
  @override
  _HomeGroupsState createState() => _HomeGroupsState();
}

class _HomeGroupsState extends State<HomeGroups> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    checkLoginStatus();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    _isLoadingAll = false;
    _isLoading = false;
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getHomeClub();
      getBuddy();
      getSumRunningNotif();
    }
  }

  List<bool> _checkLoad = [false, false, false];

  var countRunningNotif;
  getSumRunningNotif() async {
    setState(() {
      _checkLoad[2] = false;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "main_tournament").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          countRunningNotif = body['notifikasi_rg_count'];
          countRequestNotif = body['notifikasi_count'];
        });
      }else{
        print("CCC ${countRunningNotif}");
      }
      setState(() {
        _checkLoad[2] = true;
      });
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }


  var countRequestNotif;

  var dataClub;
  var MyInvitedClub;

  getHomeClub() async {
    setState(() {
      _checkLoad[0] = false;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "get_MGC").then((body) {
      if(body['status'] == 'success'){
        dataClub = body['data']['club_create'];
        MyInvitedClub = body['data']['my_club'];
      }else{
        MyInvitedClub = [];
        // Utils.showSnackBar(context, "errr", "${body['message']}");
        print("failed ${body['message']}");
      }
      setState(() {
        _checkLoad[0] = true;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _checkLoad[0] = true;
      });
    });
  }

  var dataBuddy;
  void getBuddy() async {
    setState(() {
      _checkLoad[1] = false;
    });

    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    Utils.postAPI(data, "get_buddies_user").then((body) {
      if(body['status'] == 'success'){
        dataBuddy = body['data'];
      }else{
        dataBuddy = [];
        print("failed ${body['message']}");
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _checkLoad[1] = true;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataAllClub;
  bool _isLoadingAll = false;

  getAllClub() async {
    setState(() {
      _isLoadingAll = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };

    setState(() {
      _member.clear();
      _allMember.clear();
      _uiMember.clear();
    });

    Utils.postAPI(data, "all_club").then((body) {
      if(body['status'] == 'success'){
        dataAllClub = body['data'];
        for (int i = 0; i < dataAllClub.length; i++) {
          DefaultMembers def = DefaultMembers(
              id: dataAllClub[i]["id_club_golf"],
              nama_lengkap: dataAllClub[i]["nama_club"],
              kota: dataAllClub[i]["kota"],
              negara: dataAllClub[i]["negara"],
              foto: dataAllClub[i]["logo"],
              singkatan: dataAllClub[i]["singkatan"],
              status: dataAllClub[i]["status"]);
          _member.add(def);
          _allMember.add(CustomMembers(showAll: def));
          _uiMember.add(CustomMembers(showAll: def));
        }
      }else{
        print("failed ${body['message']}");
      }
      setState(() {
        _isLoadingAll = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingAll = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.singkatan.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getHomeClub();
    if(_joinClub){
      getAllClub();
    }
    getSumRunningNotif();
    getBuddy();
    _refreshController.refreshCompleted();
  }

  bool _joinClub = false;

  @override
  Widget build(BuildContext context) {
    print("LOAD $_checkLoad");
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderHome(
                      countNotifGolfer: countRequestNotif,
                      countRunningNotif: countRunningNotif,
                      count1: countRequestNotif,
                      context: context,
                      text: "My Games",
                      page: "socials",
                      otherOnTapFunction: [() => Navigator.push(context, MaterialPageRoute(builder: (context) => AddBuddy(buddyScore: false,))), () {
                        setState(() {
                          getAllClub();
                          _joinClub = true;
                        });
                      }, () => Navigator.push(context, MaterialPageRoute(builder: (context) => CreateGroup()))],
                    ),
                    AnimatedCrossFade(
                      firstChild: _checkLoad.every((element) => element == false) ? LoadingMenu(withoutMargin: true,) : homeClub(),
                      secondChild: _joinClub ? joinClub() : Container(),
                      duration: Duration(milliseconds: 200),
                      crossFadeState: _joinClub ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                      sizeCurve: Curves.decelerate,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();

  Column joinClub() {
    return Column(
      children: [
        Container(
          width: 250,
          height: 45,
          padding: EdgeInsets.only(bottom: 3), margin: EdgeInsets.only(bottom: 4, top: 15),
          child: TextFormField(
            decoration: InputDecoration(
              labelText: "Search Club",
              hintText: "Search",
              suffixIcon: Icon(Icons.search),
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
            ),
            controller: controllerSearch3,
            onChanged: (value) {
              _searchMembers(value);
            },
          ),
        ),
        Row(
          children: [
            Padding(padding: EdgeInsets.only(top: 5), child: InkWell(onTap: (){
              setState(() {
                _joinClub = false;
              });
            }, child: Icon(Icons.arrow_back))),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Container(
                    margin: EdgeInsets.only(top:10),
                    padding: EdgeInsets.all(5),
                    child: Text("All Club", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,)
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 5.0, top: 5),
          child: Text("showing ${dataAllClub==null?0:_uiMember.length>=10?10:_uiMember.length} out of ${dataAllClub==null?0:dataAllClub.length} clubs", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
        ),
        _isLoadingAll ? LoadingMenu(withoutMargin: true,)
            : _uiMember==null || _uiMember.length <= 0 ? Container(margin: EdgeInsets.all(45), child: Text("No Club.", style: TextStyle(color: AppTheme.gFontBlack),),)
            : ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: _uiMember==null || _uiMember.length <= 0 ? 0 : _uiMember.length>=10?10:_uiMember.length,
          itemBuilder: (context, i){
            return CardClub(
              link: () {
                if(_uiMember[i].showAll.status == 'true') {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomeClub(idClub: _uiMember[i].showAll.id,)));
                }else{
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>
                          ProfileClub(dataClub: _uiMember[i]
                              .showAll.id, fromSearch: true, disableViewMember: true,)));
                }
              },
              logo: _uiMember[i].showAll.foto,
              abbreviation: _uiMember[i].showAll.singkatan,
              nameClub: _uiMember[i].showAll.nama_lengkap,
              cityClub: _uiMember[i].showAll.kota,
              countryClub: _uiMember[i].showAll.negara,
            );
          },),
      ],
    );
  }

  Widget homeClub() {
    return _joinClub ? Container() :
    MyInvitedClub==null||dataBuddy==null? Container() : MyInvitedClub.length<1 && dataBuddy.length<1
        ? SizedBox(
      height: MediaQuery.of(context).size.height/2,
      child: Center(child: Text("nothing to show here. Add some buddy or join a club first", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,)),
    ) : Column(
      children: [
        SizedBox(height: 5,),
        Container(
          margin: EdgeInsets.only(top:4),
          padding: EdgeInsets.only(left: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("My Club", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
              MyInvitedClub==null||MyInvitedClub.length <= 5
                  ? Container()
                  : SeeAllButton(onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) =>
                      SeeAllSocials(
                        socialType: "Clubs",
                        titlePage: "My Club(s)",
                      ),
                  ),
                );
              },)
            ],
          ),
        ),
        SizedBox(height: 5,),
        MyInvitedClub==null||MyInvitedClub.length==0 ?
        Container(padding: EdgeInsets.all(25),
          child: Center(child: Text("You have no club, create your own or find one!", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,)),)
            : listClub(data: MyInvitedClub),
        dataBuddy==null||dataBuddy.length < 1 ? Container() : Container(
          margin: EdgeInsets.only(top:4),
          padding: EdgeInsets.only(left: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("My Buddies", style: TextStyle(color: Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
              dataBuddy==null||dataBuddy.length <= 5
                  ? Container(margin: EdgeInsets.only(bottom: 28))
                  : SeeAllButton(onPressed: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) =>
                      SeeAllSocials(
                        socialType: "Buddies",
                        titlePage: "My Buddies",
                      ),
                  ),
                );
              })
            ],
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataBuddy==null?0:dataBuddy.length>=5?5:dataBuddy.length,
          itemBuilder: (context, index){
            return Container(
              margin: EdgeInsets.only(bottom: 4),
              // padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xFFF9F9F9),
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color(0xFFE5E5E5))
              ),
              child: FlatButton(
                onPressed: () => Utils.linkToGolferProfile(context, dataBuddy[index]),
                padding: EdgeInsets.all(5),
                splashColor: Colors.grey.withOpacity(0.2),
                textTheme: ButtonTextTheme.normal,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    dataBuddy[index]['foto'] == null ? ClipRRect(
                      borderRadius: BorderRadius.circular(10000.0),
                      child: Image.asset(
                        "assets/images/userImage.png",
                        width: 40,
                        height: 40,
                      ),
                    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                        CircularProgressIndicator(
                          value: progress.progress,
                        ), imageUrl: Utils.url + 'upload/user/' + dataBuddy[index]['foto'], width: 40, height: 40, fit: BoxFit.cover,)),
                    SizedBox(width: 8,),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${dataBuddy[index]['nama_pendek']==null?dataBuddy[index]['nama_lengkap']:dataBuddy[index]['nama_lengkap']+", "+dataBuddy[index]['nama_pendek']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,),
                        Text("${dataBuddy[index]['nama_lapangan']}", style: TextStyle(color: AppTheme.par),),
                        Text("${dataBuddy[index]['kota']}, ${dataBuddy[index]['negara']}", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic),),
                      ],
                    )),
                  ],
                ),
              ),
            );
          },
        )
      ],
    );
  }

  ListView listClub({data}) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: data==null?0:data.length>=5?5:data.length,
        itemBuilder: (context, i){
          return CardClub(
            link: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeClub(idClub: data[i]['id_club_golf'],))).then((value) {
              getHomeClub();
              getAllClub();
              getSumRunningNotif();
              getBuddy();
            }),
            logo: data[i]['logo'],
            abbreviation: data[i]['singkatan'],
            nameClub: data[i]['nama_club'],
            cityClub: data[i]['kota'],
            countryClub: data[i]['negara'],
          );
        },);
  }
}

class CardClub extends StatelessWidget {
  const CardClub({
    Key key,
    @required this.link,
    @required this.logo,
    @required this.nameClub,
    @required this.abbreviation,
    @required this.cityClub,
    @required this.countryClub
  }) : super(key: key);

  final link, logo, nameClub, abbreviation, cityClub, countryClub;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        onPressed: link,
        child: Row(
          children: [
            logo==null?ClipRRect(borderRadius: BorderRadius.circular(1000), child: Image.asset("assets/images/no-image.png", width: 55, height: 55,))
                : ClipRRect(borderRadius: BorderRadius.circular(1000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/club/' + logo, width: 55, height: 55, fit: BoxFit.cover,)),
            SizedBox(width: 10,),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("$abbreviation, $nameClub", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 16),),
                  Row(
                    children: [
                      Text("based in ", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                      Text("$cityClub,", style: TextStyle(color: AppTheme.par, fontWeight: FontWeight.normal),),
                    ],
                  ),
                  Text("$countryClub", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class DefaultMembers{
  final int id;
  final String nama_lengkap;
  final String singkatan;
  final String kota;
  final String negara;
  final String foto;
  final String status;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.nama_lengkap,
    this.singkatan,
    this.kota,
    this.negara,
    this.foto,
    this.status,
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
