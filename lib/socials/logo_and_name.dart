import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/socials/club/profile_club.dart';
import 'package:gif/utils/Utils.dart';

import '../app_theme.dart';
import 'create_group.dart';

class ImageAndNameClub extends StatelessWidget {
  const ImageAndNameClub({
    Key key,
    @required this.dataMyClub, this.dataAll, this.goToProfile, this.admin, this.routeForPop
  }) : super(key: key);

  final dataMyClub;
  final dataAll;
  final admin;
  final routeForPop;
  final bool goToProfile;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(3),
      width: double.maxFinite,
      height: 120,
      decoration: BoxDecoration(
          color: Color(0xFFF9F9F9),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              dataMyClub['logo'] == null ? InkWell(
                // onTap: () => chooseImage(ImageSource.gallery),
                child: Container(
                  width: 110,
                  height: 110,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: Center(child: Text("No Logo", style: TextStyle(color: AppTheme.gFontBlack),),),
                ),
              ) : ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                  Container(
                    width: 30,
                    height: 30,
                    child: RefreshProgressIndicator(
                      value: progress.progress,
                    ),
                  ), imageUrl: Utils.url + 'upload/club/' + dataMyClub['logo'], width: 110, height: 110, fit: BoxFit.cover,)),
              Expanded(child: InkWell(
                onTap: () {
                  if(goToProfile == true) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                            ProfileClub(dataClub: dataMyClub, dataAll: dataAll,)));
                  }
                },
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(dataMyClub['nama_club'], style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 22), textAlign: TextAlign.center,),
                      Text("${dataMyClub['singkatan']} - ${dataMyClub['kota']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16), textAlign: TextAlign.center)
                    ],
                  ),
                ),
              ))
            ],
          ),
          admin=='true'? Positioned(
            top: 0,
            right: 0,
            child: InkWell(onTap: () {}, child: Text("Edit Profile", style: TextStyle(color: Color(0xFFF15411)),)),) : Container()
        ],
      ),
    );
  }
}
