import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/view_profile.dart';
import 'package:gif/utils/Utils.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import '../app_theme.dart';
import 'club/home_club.dart';
import 'home_groups.dart';

class SeeAllSocials extends StatefulWidget {
  SeeAllSocials({@required this.socialType, @required this.titlePage});
  final socialType;
  String titlePage;
  @override
  _SeeAllSocialsState createState() => _SeeAllSocialsState();
}

class _SeeAllSocialsState extends State<SeeAllSocials> {
  final snackbarKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if(widget.socialType == "Buddies"){
      getBuddys();
    }else{
      getAllClub();
    }
    super.initState();
  }

  TextEditingController controllerSearch3 = TextEditingController();
  List<DefaultMembers> _member = new List<DefaultMembers>();
  List<CustomMembers> _uiMember = List<CustomMembers>();
  List<CustomMembers> _allMember = List<CustomMembers>();

  var dataAllClub;
  bool _isLoadingAll = false;

  getAllClub() async {
    final mToken = await Utils.getToken(context);

    setState(() {
      _isLoadingAll = true;
    });

    var data = {
      'token' : mToken,
    };

    setState(() {
      _member.clear();
      _allMember.clear();
      _uiMember.clear();
    });

    Utils.postAPI(data, "get_MGC").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          _isLoadingAll = false;
        });
        if(widget.socialType == "Clubs"){
          dataAllClub = body['data']['my_club'];
        }else{
          dataAllClub = body['data']['club_create'];
        }
        for (int i = 0; i < dataAllClub.length; i++) {
          DefaultMembers def = DefaultMembers(
              id: dataAllClub[i]["id_club_golf"],
              nama_lengkap: dataAllClub[i]["nama_club"],
              kota: dataAllClub[i]["kota"],
              negara: dataAllClub[i]["negara"],
              foto: dataAllClub[i]["logo"],
              singkatan: dataAllClub[i]["singkatan"],
              status: dataAllClub[i]["status"]);
          _member.add(def);
          _allMember.add(CustomMembers(showAll: def));
          _uiMember.add(CustomMembers(showAll: def));
        }
      }else{
        setState(() {
          _isLoadingAll = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingAll = false;
      });
    });
  }

  var dataBuddy;
  void getBuddys() async {
    final mToken = await Utils.getToken(context);

    setState(() {
      _isLoadingAll = true;
    });

    var data = {
      'token' : mToken,
    };

    Utils.postAPI(data, "get_buddies_user").then((body) {
      if(body['status'] == 'success'){
        dataBuddy = body['data'];
        for (int i = 0; i < dataBuddy.length; i++) {
          DefaultMembers def = DefaultMembers(
            nama_lengkap: dataBuddy[i]["nama_lengkap"],
            kota: dataBuddy[i]["kota"],
            negara: dataBuddy[i]["negara"],
            foto: dataBuddy[i]["foto"],
            kursus: dataBuddy[i]["nama_lapangan"],
            singkatan: dataBuddy[i]["nama_pendek"],
            id: dataBuddy[i]['user_id_buddies'],
            data: dataBuddy[i],
          );
          _member.add(def);
          _allMember.add(CustomMembers(showAll: def));
          _uiMember.add(CustomMembers(showAll: def));
        }
        setState(() {
          _isLoadingAll = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoadingAll = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingAll = false;
      });
    });
  }

  void _searchMembers(String mQuery) async {
    // print(_contacts.where((item) => item.displayName.toLowerCase().contains(mQuery.toLowerCase())).toList().length);
    setState(() {
      _uiMember = _allMember.where((item) => item.showAll.nama_lengkap.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.singkatan.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.kota.toLowerCase().contains(mQuery.toLowerCase())
          || item.showAll.negara.toLowerCase().contains(mQuery.toLowerCase())).toList();
      // _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoadingAll,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(13),
              child: Column(
                children: [
                  HeaderCreate(title: "My Social", backSummary: true,),
                  SizedBox(height: 8,),
                  Text(
                    widget.titlePage,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF4F4F4F),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 8,),
                  listDataSocial(_uiMember)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  ListView listDataSocial(var data) {
    return ListView.builder(
    shrinkWrap: true,
    primary: false,
    itemCount: _uiMember.length,
    itemBuilder: (context, i){
      return widget.socialType == "Clubs" || widget.socialType == "My Clubs" ? CardClub(
        link: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeClub(idClub: _uiMember[i].showAll.id,))),
        logo: _uiMember[i].showAll.foto,
        abbreviation: _uiMember[i].showAll.singkatan,
        nameClub: _uiMember[i].showAll.nama_lengkap,
        cityClub: _uiMember[i].showAll.kota,
        countryClub: _uiMember[i].showAll.negara,
      ) : Container(
        margin: EdgeInsets.only(bottom: 4),
        // padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: Color(0xFFF9F9F9),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Color(0xFFE5E5E5))
        ),
        child: FlatButton(
          onPressed: () => Utils.linkToGolferProfile(context, _uiMember[i].showAll.data),
          padding: EdgeInsets.all(5),
          splashColor: Colors.grey.withOpacity(0.2),
          textTheme: ButtonTextTheme.normal,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _uiMember[i].showAll.foto == null ? ClipRRect(
                borderRadius: BorderRadius.circular(10000.0),
                child: Image.asset(
                  "assets/images/userImage.png",
                  width: 40,
                  height: 40,
                ),
              ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                  CircularProgressIndicator(
                    value: progress.progress,
                  ), imageUrl: Utils.url + 'upload/user/' + _uiMember[i].showAll.foto, width: 40, height: 40, fit: BoxFit.cover,)),
              SizedBox(width: 8,),
              Expanded(child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${_uiMember[i].showAll.singkatan==null?_uiMember[i].showAll.nama_lengkap:_uiMember[i].showAll.nama_lengkap+", "+_uiMember[i].showAll.singkatan}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), overflow: TextOverflow.ellipsis,),
                  Text("${_uiMember[i].showAll.kursus}", style: TextStyle(color: AppTheme.par),),
                  Text("${_uiMember[i].showAll.kota}, ${_uiMember[i].showAll.negara}", style: TextStyle(color: AppTheme.gButton, fontStyle: FontStyle.italic),),
                ],
              )),
            ],
          ),
        ),
      );
    },);
  }
}

class DefaultMembers{
  final int id;
  final String nama_lengkap;
  final String singkatan;
  final String kota;
  final String kursus;
  final String negara;
  final String foto;
  final String status;
  final data;
  final String showAll;
  DefaultMembers({
    this.showAll,
    this.id,
    this.nama_lengkap,
    this.singkatan,
    this.kursus,
    this.kota,
    this.negara,
    this.foto,
    this.status,
    this.data,
  });
}

class CustomMembers {
  final DefaultMembers showAll;
  bool isChecked;
  CustomMembers({
    this.showAll,
  });
}
