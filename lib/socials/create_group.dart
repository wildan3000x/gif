import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/constrant.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/socials/club/home_club.dart';
import 'package:gif/socials/invite_club/invite_club.dart';
import 'package:gif/utils/Utils.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path/path.dart';

class CreateGroup extends StatefulWidget {
  const CreateGroup({Key key, this.dataClub, this.nextPage, this.editFromHome}) : super (key: key);
  final dataClub, nextPage, editFromHome;
  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {
  bool _nextPage = false;
  bool _loadingCourse = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  SharedPreferences sharedPreferences;
  bool _isLoading = false;
  bool _isLoading2 = false;


  var myFormat = DateFormat('EEEE d MMM yyyy');
  String dateString = "";
  DateTime _date = DateTime.now();

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(picked);

    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
        dateString = formattedDate;
        print(formattedDate);
      });
    }
  }

  @override
  void initState() {
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    CourseService.course.clear();
    // getProvince();
    getCountry();
    _getToken();
    if(widget.editFromHome == true){
    //   _nextPage = true;
      dataCreateClub = widget.dataClub;
      clubName.text = widget.dataClub['nama_club'];
      nickName.text = widget.dataClub['singkatan'];
      homeCountry.text = widget.dataClub['negara'];
      homeCity.text = widget.dataClub['kota'];
      clubSloganCon.text = widget.dataClub['slogan_club'];
      contactNameCon.text = widget.dataClub['nama_kontak'];
      contactNumberCon.text = widget.dataClub['no_kontak'];
      affiliateCon.text = widget.dataClub['organisasi_afiliasi'];
      homeCourse.text = widget.dataClub['nama_lapangan'];
      courseId = widget.dataClub['id_lapangan'];
      dateController.text = widget.dataClub != null
          ? myFormat.format(DateTime.parse(widget.dataClub['tgl_club']))
          : myFormat.format(DateTime.now());
      getCourse();
    }
    super.initState();
  }

  var token;

  void _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var tokenJson = localStorage.getString('usertoken');
    var datatoken = json.decode(tokenJson);
    print(datatoken);
    setState(() {
      token = datatoken;
    });
  }

  String _selectedCountry;
  String _selectedCity;
  String _selectedCourse;

  TextEditingController clubName = TextEditingController();
  TextEditingController nickName = TextEditingController();
  TextEditingController homeCountry = TextEditingController();
  TextEditingController homeCity = TextEditingController();
  TextEditingController homeCourse = TextEditingController();
  TextEditingController contactNameCon = TextEditingController();
  TextEditingController contactNumberCon = TextEditingController();
  TextEditingController clubSloganCon = TextEditingController();
  TextEditingController affiliateCon = TextEditingController();
  TextEditingController dateController = TextEditingController();
  FocusNode nickNode = FocusNode();
  FocusNode sloganNode = FocusNode();
  FocusNode countryNode = FocusNode();
  FocusNode cityNode = FocusNode();
  FocusNode contactNameNode = FocusNode();
  FocusNode contactNumberNode = FocusNode();
  int courseId;

  String _nameErrorText;
  bool _nameError = false;
  String _nickErrorText;
  bool _nickError = false;
  String _countryErrorText;
  bool _countryError = false;
  String _cityErrorText;
  bool _cityError = false;
  String _courseErrorText;
  bool _courseError = false;
  String _contactNameErrorText;
  bool __contactNameError = false;
  String _contactNumberErrorText;
  bool _contactNumberError = false;
  String _affiliateErrorText;
  bool _affiliateError = false;
  String _sloganErrorText;
  bool _sloganError = false;

  var dataCreateClub;

  void getCountry() async {
    var data = {};

    Utils.postAPI(data, "get_negara").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CountryServices.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _loading = false;
      });
    });
  }

  // void getProvince() async {
  //   setState(() {
  //     _isLoading2 = true;
  //   });
  //   var data = {};
  //
  //   Utils.postAPI(data, "get_kota").then((body) {
  //     if (body['status'] == 'success') {
  //       try {
  //         List<dynamic> lPro = body["data"];
  //         CitiesService.cities = lPro;
  //         print('Count: ${body.length}, Datas: ${lPro}');
  //       } catch (e) {
  //         print(e);
  //       }
  //     } else {
  //       print(body['message']);
  //       var snackbar = SnackBar(
  //         content: Text(body['message']),
  //         backgroundColor: Colors.red,
  //       );
  //       snackbarKey.currentState.showSnackBar(snackbar);
  //     }
  //     print(body);
  //     setState(() {
  //       _isLoading2 = false;
  //     });
  //   }, onError: (error) {
  //     setState(() {
  //       print("Error == $error");
  //       _isLoading2 = false;
  //     });
  //   });
  // }

  List<dynamic> _dataCourse = [];
  void getCourse() async {
    setState(() {
      _loadingCourse = true;
    });
    var data = {
      'kota': homeCountry.text,
    };

    Utils.postAPI(data, "get_lapangan").then((body) {
      CourseService.course.clear();
      if (body['status'] == 'success') {
        _dataCourse = body['data'];
      } else {
        print(body['message']);
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _loadingCourse = false;
      });
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        // _loading = false;
      });
    });
  }

  File _image;
  final snackbarKey = GlobalKey<ScaffoldState>();

  Future<void> chooseImage(ImageSource source) async {
    var imageFile = await ImagePicker.pickImage(source: source);
    //set source: ImageSource.camera to get image from camera
    // setState(() {
    //   _image = imageFile;
    // });
    _cropImage(imageFile);
  }

  _cropImage(File picked) async {
    File cropped = await ImageCropper().cropImage(sourcePath: picked.path, aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0));
    setState(() {
      _image = cropped;
    });
  }

  createFirstPageClub() async {
      var data = {
        'token': token,
        'id_club_golf': dataCreateClub==null?null:dataCreateClub['id_club_golf'],
        'nama_club': clubName.text,
        'singkatan': nickName.text,
        'negara': homeCountry.text,
        'kota': homeCity.text
      };

      Utils.postAPI(data, "add_GC1").then((body) {
        if (body['status'] == 'success') {
          setState(() {
            dataCreateClub = body['data'];
            _isLoading = false;
            _nextPage = true;
          });
        } else {
          var snackbar = SnackBar(
            content: Text(body['body_message']),
            backgroundColor: Colors.red,
          );
          _scaffoldKey.currentState.showSnackBar(snackbar);
          setState(() {
            _isLoading = false;
          });
        }
      }, onError: (error) {
        var snackbar = SnackBar(
          content: Text('Ops, something wrong, please try again later.'),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          print("Error == $error");
          _isLoading = false;
        });
      });
  }

  createOrEditClub(File ImageFile, BuildContext context) async {
    print("IMM $ImageFile");
    if (ImageFile != null) {
      var stream = new http.ByteStream(DelegatingStream.typed(ImageFile.openRead()));
      var length = await ImageFile.length();
      var uri = Uri.parse(Utils.url_API + "add_GC1");
      var request = new http.MultipartRequest("POST", uri);
      var multipartFile = new http.MultipartFile("logo", stream, length,
          filename: basename(ImageFile.path));
      request.fields['id_club_golf'] = dataCreateClub==null?"":dataCreateClub['id_club_golf'].toString();
      request.fields['nama_club'] = clubName.text;
      request.fields['singkatan'] = nickName.text;
      request.fields['negara'] = homeCountry.text;
      request.fields['kota'] = homeCity.text;
      request.fields['slogan_club'] = clubSloganCon.text;
      request.fields['nama_kontak'] = contactNameCon.text;
      request.fields['no_kontak'] = contactNumberCon.text;
      request.fields['lapangan_id'] = courseId.toString();
      request.fields['organisasi_afiliasi'] = affiliateCon.text;
      request.fields['tgl_club'] = DateTime(_date.year, _date.month, _date.day).toString();
      request.fields['token'] = token;
      if (ImageFile == null) {
        request.fields['logo'] = '0';
      } else {
        request.files.add(multipartFile);
      }

      var body;
      var response = await request.send();
      //Get the response from the server
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      print("RESPONSE ${responseString}");
      if (response.statusCode == 200) {
        body = json.decode(responseString);
        print("PPP ${body['data']}");
        if (widget.editFromHome == true) {
          Navigator.pop(context, 'reload');
        } else {
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => InviteClub(dataClub: body['data'],)));
        }
        setState(() {
          _isLoading = false;
        });
      } else {
        var snackbar = SnackBar(
          content: Text('Ops, something wrong, please try again later.'),
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }else {
      var data = {
        'token': token,
        'id_club_golf': dataCreateClub==null?null:dataCreateClub['id_club_golf'],
        'nama_club': clubName.text,
        'singkatan': nickName.text,
        'negara': homeCountry.text,
        'kota': homeCity.text,
        'slogan_club': clubSloganCon.text,
        'nama_kontak': contactNameCon.text,
        'no_kontak': contactNumberCon.text,
        'tgl_club': DateTime(_date.year, _date.month, _date.day).toString(),
        'lapangan_id': courseId,
        'organisasi_afiliasi': affiliateCon.text
      };

      Utils.postAPI(data, "add_GC1").then((body) {
        if (body['status'] == 'success') {
          if (widget.editFromHome == true) {
            Navigator.pop(context, 'reload');
            // Navigator.pushReplacement(context, MaterialPageRoute(
            //     builder: (context) => HomeClub(dataClub: body['data'],)));
          } else {
            Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => InviteClub(dataClub: body['data'],)));
          }
          setState(() {
            _isLoading = false;
          });
        } else {
          var snackbar = SnackBar(
            content: Text(body['message']),
            backgroundColor: Colors.red,
          );
          _scaffoldKey.currentState.showSnackBar(snackbar);
          setState(() {
            _isLoading = false;
          });
        }
      }, onError: (error) {
        var snackbar = SnackBar(
          content: Text('Ops, something wrong, please try again later.'),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackbar);
        setState(() {
          print("Error == $error");
          _isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _loadingCourse,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderCreate(title: "Golf Club",),
                    /*!_nextPage ? */Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          selectPictureClubContainer(),
                          textCreateNewClub('${widget.editFromHome == true ? 'Edit' : 'Create New'} Golf Club'),
                          formFieldCreateClub(),
                          buttonCreate()
                        ],
                      ),
                    )/* : Form(
                      key: _formKey2,
                      child: Column(
                        children: [
                          textCreateNewClub('Club Profile'),
                          formFieldCreateClub(),
                          buttonSave()
                        ],
                      ),
                    )*/
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container selectPictureClubContainer() {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(3),
      width: double.maxFinite,
      height: 120,
      decoration: BoxDecoration(
        color: Color(0xFFF9F9F9),
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          widget.editFromHome == true && widget.dataClub['logo']!=null?
          InkWell(
            onTap: () => chooseImage(ImageSource.gallery),
            child: Stack(
              children: [
                ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                    Container(
                      width: 30,
                      height: 30, child: RefreshProgressIndicator(value: progress.progress,),), imageUrl: Utils.url + 'upload/club/' + widget.dataClub['logo'], width: 110, height: 110, fit: BoxFit.cover,)),
                _image == null ? Container() : Container(
                  width: 110,
                  height: 110,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      image: DecorationImage(
                          image: FileImage(_image),
                          fit: BoxFit.cover
                      )
                  ),
                  child: Center(child: Text(_image == null ? "Upload Logo" : "", style: TextStyle(color: AppTheme.gFontBlack),),),
                ),
              ],
            ),
          ) : _image == null ? InkWell(
            onTap: () => chooseImage(ImageSource.gallery),
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Center(child: Text("Upload Logo", style: TextStyle(color: AppTheme.gFontBlack),),),
            ),
          ) : InkWell(
            onTap: () => chooseImage(ImageSource.gallery),
            child: Container(
              width: 110,
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                image: DecorationImage(
                  image: FileImage(_image),
                  fit: BoxFit.cover
                )
              ),
              child: Center(child: Text(_image == null ? "Upload Logo" : "", style: TextStyle(color: AppTheme.gFontBlack),),),
            ),
          ),
          Expanded(child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("${dataCreateClub==null?"":dataCreateClub['nama_club']}", style: TextStyle(color: AppTheme.gButton, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
                Text("${dataCreateClub==null?"":dataCreateClub['singkatan']+" - "}${dataCreateClub==null?"":dataCreateClub['kota']}", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center)
              ],
            ),
          ))
        ],
      ),
    );
  }

  Container textCreateNewClub(String s) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          s,
          style: TextStyle(
            color: s == 'Create New Golf Club' ? AppTheme.gButton : Color(0xFFF15411),
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget buttonCreate() {
    return Container(
        margin: EdgeInsets.only(top: 30),
        child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0)),
            splashColor: Colors.grey.withOpacity(0.5),
            onPressed: () {
              if(!_isLoading){
                setState(() {
                  if (_formKey.currentState.validate() && homeCity.text.isNotEmpty && courseId != null) {
                    _formKey.currentState.save();
                    createOrEditClub(_image, this.context);
                    _isLoading = true;
                  }else if(!_formKey.currentState.validate() && homeCity.text.isNotEmpty && courseId != null){
                    setState(() {
                      _courseError = false;
                    });
                  } else{
                    setState(() {
                      _courseError = true;
                      _courseErrorText = '';
                    });
                  }
                });
              }
            },
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: BoxDecoration(
                  color: AppTheme.gButton,
                  borderRadius: BorderRadius.circular(100)),
              child: Container(
                constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                alignment: Alignment.center,
                child: Center(
                    child: _isLoading ? Container(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(),
                          ) : Text(
                            widget.editFromHome == true ? "Save" : "Create",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )),
              ),
            )));
  }

  Column formFieldCreateClub() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Golf Club Name",
          style: TextStyle(color: _nameError ? AppTheme.gValidError : AppTheme.gButton),
        ),
        Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _nameError = true;
                _nameErrorText = '';
                return '';
              } else {
                _nameError = false;
                return null;
              }
            },
            onChanged: (value) {
              setState(() {
                _nameError = false;
                _nameErrorText = null;
              });
            },
            onSaved: (value){
              setState(() {
                clubName.text = value;
              });
            },
            controller: clubName,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              FocusScope.of(this.context).requestFocus(nickNode);
            },
          ),
        ),
        SizedBox(height: 10,),
        Text(
          "Nickname / Abbreviation",
          style: TextStyle(color: _nickError ? AppTheme.gValidError : AppTheme.gButton),
        ),
        Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _nickError = true;
                _nickErrorText = '';
                return '';
              } else {
                _nickError = false;
                return null;
              }
            },
            onChanged: (value) {
              setState(() {
                _nickError = false;
                _nickErrorText = null;
              });
            },
            onSaved: (value){
              setState(() {
                nickName.text = value;
              });
            },
            controller: nickName,
            focusNode: nickNode,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              FocusScope.of(this.context).requestFocus(sloganNode);
            },
          ),
        ),
        SizedBox(height: 10,),
        Text(
          "Club Slogan",
          style: TextStyle(color: _sloganError ? AppTheme.gValidError : AppTheme.gButton),
        ),
        Container(
          // height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _sloganError = true;
                _sloganErrorText = '';
                return '';
              } else {
                _sloganError = false;
                return null;
              }
            },
            onChanged: (value) {
              setState(() {
                _sloganError = false;
                _sloganErrorText = null;
              });
            },
            onSaved: (value){
              setState(() {
                clubSloganCon.text = value;
              });
            },
            controller: clubSloganCon,
            focusNode: sloganNode,
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              FocusScope.of(this.context).requestFocus(contactNameNode);
            },
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Contact Name",
                        style: TextStyle(color: __contactNameError ? AppTheme.gValidError : AppTheme.gButton),
                      ),
                      Container(
                        height: 40,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              __contactNameError = true;
                              _contactNameErrorText = '';
                              return '';
                            } else {
                              __contactNameError = false;
                              return null;
                            }
                          },
                          onChanged: (value) {
                            setState(() {
                              __contactNameError = false;
                              _contactNameErrorText = null;
                            });
                          },
                          onSaved: (value){
                            setState(() {
                              contactNameCon.text = value;
                            });
                          },
                          controller: contactNameCon,
                          focusNode: contactNameNode,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Color(0xFFDADADA))),
                            errorStyle: TextStyle(height: 0),
                            errorBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                            focusedErrorBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                          ),
                          style: TextStyle(
                            fontSize: 18,
                          ),
                          onFieldSubmitted: (_) {
                            FocusScope.of(this.context).requestFocus(contactNumberNode);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 20,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Contact Number",
                        style: TextStyle(color: _contactNumberError ? AppTheme.gValidError : AppTheme.gButton),
                      ),
                      Container(
                        height: 40,
                        child: TextFormField(
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          validator: (value) {
                            if (value.isEmpty) {
                              _contactNumberError = true;
                              _contactNumberErrorText = '';
                              return '';
                            } else {
                              _contactNumberError = false;
                              return null;
                            }
                          },
                          onChanged: (value) {
                            setState(() {
                              _contactNumberError = false;
                              _contactNumberErrorText = null;
                            });
                          },
                          onSaved: (value){
                            setState(() {
                              contactNumberCon.text = value;
                            });
                          },
                          controller: contactNumberCon,
                          focusNode: contactNumberNode,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 0.5),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Color(0xFFDADADA))),
                            errorStyle: TextStyle(height: 0),
                            errorBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                            focusedErrorBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0),
                            ),
                          ),
                          style: TextStyle(
                            fontSize: 18,
                          ),
                          onFieldSubmitted: (_) {
                            // FocusScope.of(this.context).requestFocus(countryNode);
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 10,),
            Text(
              "Date Of Found",
              style: TextStyle(color: __contactNameError ? AppTheme.gValidError : AppTheme.gButton),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: AppTheme.gGrey,
                    ),
                  )),
              height: 40,
              alignment: Alignment.centerLeft,
              child: widget.nextPage == true ? TextFormField(
                decoration: InputDecoration(
                    hintText: "Date",
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                        BorderSide(color: AppTheme.gGrey))),
                enableInteractiveSelection: false,
                onTap: () {
                  // Below line stops keyboard from appearing
                  FocusScope.of(this.context).requestFocus(new FocusNode());
                  selectDate(this.context);
                  // Show Date Picker Here
                },
                controller: dateController,
              ) : FlatButton(
                child: Text(
                  '${myFormat.format(_date)}',
                  style: TextStyle(
                      color: AppTheme.gFontBlack,
                      fontWeight: FontWeight.normal),
                  textAlign: TextAlign.left,
                ),
                onPressed: () {
                  selectDate(this.context);
                },
              ),
            ),
          ],
        ),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Home Country",
                      style: TextStyle(
                          color: _countryError
                              ? AppTheme.gValidError
                              : AppTheme.gButton,
                          fontSize: 14),
                    ),
                  ),
                  TypeAheadFormField(
                    noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No Country Found!")),
                    textFieldConfiguration: TextFieldConfiguration(
                      controller: this.homeCountry,
                      onChanged: (value) {
                        setState(() {
                          // addError(error: kRegister);
                          _countryError = false;
                          _countryErrorText = null; // Resets the error
                        });
                      },
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFDADADA))),
                        // errorText: _countryErrorText,
                        errorStyle: TextStyle(height: 0),
                        errorBorder: UnderlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide:
                          BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide:
                          BorderSide(color: Colors.grey, width: 1.0),
                        ),
                      ),
                    ),
                    suggestionsCallback: (String pattern) {
                      return CountryServices.getSuggestions(pattern);
                    },
                    itemBuilder: (context, suggestion) {
                      return ListTile(
                        title: Text(suggestion['nama']),
                      );
                    },
                    transitionBuilder:
                        (context, suggestionsBox, controller) {
                      return suggestionsBox;
                    },
                    onSuggestionSelected: (suggestion) {
                      this.homeCountry.text = suggestion["nama"];
                      setState(() {
                        _countryError = false;
                        getCourse();
                        _loadingCourse = true;
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        _countryError = true;
                        _countryErrorText = '';
                        return '';
                      } else {
                        _countryError = false;
                        return null;
                      }
                    },
                    onSaved: (value) => this._selectedCountry = value,
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            SizedBox(height: 10,),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Home Golf Course (Most Frequent)",
                style: TextStyle(
                    color: _courseError
                        ? AppTheme.gValidError
                        : AppTheme.gButton,
                    fontSize: 14),
              ),
            ),
            SearchableDropdown.single(
              items: _dataCourse.map((data) {
                return (DropdownMenuItem(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${data['nama_lapangan']}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                      Text("${data['nama']}")
                    ],
                  ),
                  value: data,
                ));
              }).toList(),
              // value: selectedItem,
              onChanged: (value) {
                if(value != null){
                  homeCity.text = value['nama'];
                  courseId = value['id_lapangan'];
                  setState(() {
                    _courseError = false;
                  });
                }
              },
              hint: Padding(
                padding: EdgeInsets.only(bottom: 6),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${homeCourse.text}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                    Text("${homeCity.text}")
                  ],
                ),
              ),
              dialogBox: true,
              isExpanded: true,
              displayClearIcon: false,
              onClear: () {},
              underline: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Color(0xFFDADADA))
                    )
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 10,),
        Text(
          "Affiliate Organization",
          style: TextStyle(color: _affiliateError ? AppTheme.gValidError : AppTheme.gButton),
        ),
        Container(
          height: 40,
          child: TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _affiliateError = true;
                _affiliateErrorText = '';
                return '';
              } else {
                _affiliateError = false;
                return null;
              }
            },
            onChanged: (value) {
              setState(() {
                _affiliateError = false;
                _affiliateErrorText = null;
              });
            },
            onSaved: (value){
              setState(() {
                affiliateCon.text = value;
              });
            },
            controller: affiliateCon,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0.5),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFFDADADA))),
              errorStyle: TextStyle(height: 0),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
            ),
            style: TextStyle(
              fontSize: 18,
            ),
            onFieldSubmitted: (_) {
              FocusScope.of(this.context).requestFocus(countryNode);
            },
          ),
        ),
      ],
    );
  }
}
