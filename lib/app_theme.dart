import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();


  static const Color gBlueOcean = Color(0xFF57e2d2);
  static const Color gFontBlack = Color(0xFF4F4F4F);
  static const Color gBlue = Color(0xFF1857b8);
  static const Color gGrey = Color(0xFFdfdfdf);
  static const Color gGrey2 = Color(0xFFe5e5e5);
  static const Color gWhite = Color(0xFFFFFfFF);
  static const Color gFont = Color(0xFF6fdac7);
  static const Color gButton = Color(0xFF1ac5a6);
  static const Color gOrange = Color(0xFFf69165);
  static const Color gBlack = Color(0xFF919191);
  static const Color gBlueButton = Color(0xFF2358BD);
  static const Color gRed = Color(0xFFE31212);
  static const Color gValidError = Color(0xFFF70707);
  static const Color gLightGrey = Color(0xFFF9F9F9);
  static const Color birdie = Color(0xFF1AC5A6);
  static const Color eagle = Color(0xFFF2C94C);
  static const Color par = Color(0xFF2D9CDB);
  static const Color bogies = Color(0xFFF15411);
  static const Color white = Color(0xFFFFFFFF);
  static const Color input = Color(0xFF364A54);

  static const Color gcart = Color(0xFF1AC5A6);
  static const Color gjoin = Color(0xFFF15411);
  static const Color gcreate = Color(0xFF1857B7);
  static const Color ggolf_caddy = Color(0xFF1AC5A6);
  static const Color gtrophy = Color(0xFF1857B7);
  static const Color gadd_friend = Color(0xFF1AC5A6);
  static const Color ggroups = Color(0xFFF15411);
  static const Color gadd_group = Color(0xFF1857B7);
  static const Color gBlueBoxTrophies = Color(0xFF2c70b9);
  static const Color gStar = Color(0xFFecc565);
  static const Color gButtonFindCaddie = Color(0xFF4FC4CB);
}
