import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

import '../app_theme.dart';

class ListContainerHome extends StatelessWidget {
  const ListContainerHome({
    Key key,
    @required this.widgetCorner, @required this.link, @required this.date,
    @required this.cityTour, @required this.courseTour, @required this.nameTour,
    @required this.orgaTour, this.forColorHistory
  }) : super(key: key);

  final widgetCorner, link;
  final date, nameTour, courseTour, orgaTour, cityTour;
  final forColorHistory;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(
              color: Color(0xFFE5E5E5)
          )
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: link,
        child: Stack(
          children: [
            Row(
              // mainAxisSize: MainAxisSize.max,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 48,
                      height: 25,
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                          color: forColorHistory != null ? Color(0xFF4f4f4f) : DateFormat("MMM").format(DateTime.parse(date)) == "Aug" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Sep" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Oct"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Nov"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton
                      ),
                      child: Center(child: Text("${DateFormat("MMM").format(DateTime.parse(date))}", style: TextStyle(fontSize: 16, color: Colors.white),)),
                    ),
                    Container(
                      width: 48,
                      height: 32,
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                          color: Color(0xFFF9F9F9),
                          border: Border.all(color: forColorHistory != null ? Color(0xFF4f4f4f) : DateFormat("MMM").format(DateTime.parse(date)) == "Aug" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Sep" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Oct"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Nov"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gButton, width: 2)
                      ),
                      child: Center(child: Text("${DateFormat("d").format(DateTime.parse(date))}", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),)),
                    )
                  ],
                ),
                SizedBox(width: 6,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Expanded(child: Text("$nameTour", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 16, fontWeight: FontWeight.bold),)),
                          SizedBox(width: 20,)
                        ],
                      ),
                      Text("$courseTour, $cityTour", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                      Text("$orgaTour", style: TextStyle(
                          color: forColorHistory != null ? Color(0xFF4f4f4f) : DateFormat("MMM").format(DateTime.parse(date)) == "Aug" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Sep" ||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Oct"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Nov"||
                              DateFormat("MMM").format(DateTime.parse(date)) == "Dec"? Color(0xFF2D9CDB) : AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                    ],
                  ),
                ),
                // Align(alignment: Alignment.topRight, child: SvgPicture.asset("assets/svg/check.svg", width: 20, height: 20, color: AppTheme.gFont,))
              ],
            ),
            Positioned(
              top: 0,
              right: 0,
              child: widgetCorner,
            )
          ],
        ),
      ),
    );
  }
}

class TitleListMenu extends StatelessWidget {
  TitleListMenu({Key key, @required this.text, this.color}) : super (key: key);
  final text, color;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
          margin: EdgeInsets.only(top:3),
          padding: EdgeInsets.all(5),
          child: Text("$text", style: TextStyle(color: color == null ? Color(0xFF4F4F4F) : color, fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,)
      ),
    );
  }
}

class ConfirmedNotConfirmedText extends StatelessWidget {
  ConfirmedNotConfirmedText({this.withPencilIcon});
  bool withPencilIcon = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 2.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  SvgPicture.asset("assets/svg/check_bold.svg", color: AppTheme.gFont,),
                  Padding(
                    padding: const EdgeInsets.only(left: 2.0),
                    child: Text("Confirmed", style: TextStyle(color: Color(0xFF828282), fontStyle: FontStyle.italic),),
                  )
                ],
              ),
              Row(
                children: [
                  SvgPicture.asset("assets/svg/check_bold.svg", color: Color(0xFFF15411),),
                  Padding(
                    padding: const EdgeInsets.only(left: 2.0),
                    child: Text("Confirmation Required", style: TextStyle(color: Color(0xFF828282), fontStyle: FontStyle.italic),),
                  )
                ],
              ),
              // withPencilIcon ? Expanded(
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       Icon(Icons.circle, color: Color(0xFFff8000), size: 20,),
              //       // SvgPicture.asset("assets/svg/check_bold.svg", color: Color(0xFFF15411),),
              //       Expanded(
              //         child: Padding(
              //           padding: const EdgeInsets.only(left: 2.0),
              //           child: Text("Your Created Games", style: TextStyle(color: Color(0xFF828282), fontStyle: FontStyle.italic),),
              //         ),
              //       )
              //     ],
              //   ),
              // ) : Container()
            ],
          ),
          SizedBox(height: 3,),
        ],
      ),
    );
  }
}


