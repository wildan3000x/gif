import 'package:flutter/material.dart';

import 'custom_button.dart';

class CustomError extends StatelessWidget {
  final FlutterErrorDetails errorDetails;

  const CustomError({
    Key key,
    @required this.errorDetails,
  })  : assert(errorDetails != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(13),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(child: Text("Whoops,", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,)),
              Text("something went wrong...,", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              Text(
                "\n\nmostly application problems, we'll try to fix it.\nBut could be your internet?",
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.grey
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 51,),
              CustomButton(
                width: 91,
                height: 36,
                link: () => Navigator.pop(context),
                customColor: Colors.red,
                textButton: "go back",
              )
            ],
          ),
        ),
      ),
    );
  }
}
