import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../app_theme.dart';

class TournamentDetailWidget extends StatelessWidget {
  TournamentDetailWidget({Key key, @required this.dataTournament, @required this.additionalWidget, this.isTournamentInfo}) : super(key: key);

  final dataTournament;
  Widget additionalWidget;
  var isTournamentInfo;
  var myFormat = DateFormat('EEEE, d MMM yyyy   h:mm a');

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        additionalWidget,
        Container(
          margin: EdgeInsets.only(top: 20, bottom: 20),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15)),
          child: Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      "${dataTournament['nama_turnamen']}",
                      style: TextStyle(fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0),
                    child: Text(
                        "Organized by ${dataTournament['penyelenggara_turnamen']}",
                        style: TextStyle(color: AppTheme.gFont)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                    child: Text(
                      "${dataTournament['nama_lapangan']}, ${dataTournament['kota_turnamen']}",
                      style: TextStyle(fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(
                    "${myFormat.format(DateTime.parse(dataTournament['tanggal_turnamen']))}",
                    style: TextStyle(color: AppTheme.gFont),
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "${dataTournament['partisipan_turnamen']} - ${dataTournament['tipe_turnamen']}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xFF2D9CDB)),
                    ),
                  ),
                ],
              )),
        ),
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.symmetric(horizontal: 45),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppTheme.gFont,
          ),
          child: Text(
            isTournamentInfo == null ? "Tournament ID* : ${dataTournament['kode_turnamen']}" : "Tournament Info",
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
        ),
      ],
    );
  }
}
