import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({Key key, this.height, this.width, @required this.textButton,
    this.customColor, this.link}) : super(key: key);
  final textButton, link;
  final double width, height;
  final Color customColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 51,
      width: width ?? 113,
      child: ElevatedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.zero),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100)
          )),
          backgroundColor: MaterialStateProperty.all(customColor ?? AppTheme.gButton),
        ),
        onPressed: link ?? (){},
        child: Center(child: Text("$textButton", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16),
            textAlign: TextAlign.center)),
      ),
    );
  }
}
