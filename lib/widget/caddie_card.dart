import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';

import '../app_theme.dart';

class CaddieCard extends StatelessWidget {
  const CaddieCard({
    Key key, @required this.dataCaddie, @required this.dataTournament, @required this.withRequestedText
  }) : super(key: key);

  final dataCaddie, dataTournament, withRequestedText;

  @override
  Widget build(BuildContext context) {
    return Row(
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            Container(
              width: 48,
              height: 25,
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                  color: AppTheme.gButton
              ),
              child: Center(child: Text("${DateFormat("MMM").format(DateTime.parse(dataTournament['tanggal_turnamen']))}", style: TextStyle(fontSize: 16, color: Colors.white),)),
            ),
            Container(
              width: 48,
              height: 32,
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                  color: Color(0xFFF9F9F9),
                  border: Border.all(color: AppTheme.gButton, width: 2)
              ),
              child: Center(child: Text("${DateFormat("d").format(DateTime.parse(dataTournament['tanggal_turnamen']))}", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: AppTheme.gFontBlack),)),
            )
          ],
        ),
        SizedBox(width: 6,),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text("${dataTournament['nama_turnamen']}", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 18, fontWeight: FontWeight.bold),),
              Text("${dataTournament['nama_lapangan']}, ${dataTournament['kota_turnamen']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
              withRequestedText == null
                  ? Text("Requested on ${DateFormat("d MMM yyyy").format(DateTime.parse(dataTournament['tanggal_turnamen']))}", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),)
                  : Container(),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 0.0, left: 5, right: 5),
          child: Center(
            child: Container(
              width: 50,
              child: Column(
                children: [
                  dataCaddie['foto'] == null ? ClipRRect(
                    borderRadius: BorderRadius.circular(10000.0),
                    child: Image.asset(
                      "assets/images/userImage.png",
                      width: 45,
                      height: 45,
                    ),
                  ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                      CircularProgressIndicator(
                        value: progress.progress,
                      ), imageUrl: Utils.url + 'upload/user/' + dataCaddie['foto'], width: 50, height: 50, fit: BoxFit.cover,)),
                  Text("${dataCaddie['nama_lengkap']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal), overflow: TextOverflow.ellipsis,),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
