import 'package:flutter/material.dart';

import '../app_theme.dart';

class CustomAlertDialog extends StatefulWidget {
  const CustomAlertDialog({Key key, @required this.title, @required this.contentText,
    @required this.noButton, @required this.yesButton, this.optionalHeight}) : super(key: key);
  final title, contentText, yesButton, noButton;
  final double optionalHeight;

  @override
  _CustomAlertDialogState createState() => _CustomAlertDialogState();
}

class _CustomAlertDialogState extends State<CustomAlertDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      contentPadding: EdgeInsets.all(8),
      title: Text("${widget.title}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
      content: Container(
        // height: 60,
        width: double.maxFinite,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("${widget.contentText}", textAlign: TextAlign.center,),
            SizedBox(height: widget.optionalHeight ?? 8,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                    splashColor: Colors.grey.withOpacity(0.5),
                    onPressed: widget.noButton,
                    padding: const EdgeInsets.all(0.0),
                    child: Ink(
                        height: 40,
                        decoration: BoxDecoration(
                            color: AppTheme.bogies,
                            borderRadius: BorderRadius.circular(100)
                        ),
                        child: Container(
                            constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                            alignment: Alignment.center,
                            child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                  ),
                ),
                Container(
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                    splashColor: Colors.grey.withOpacity(0.5),
                    onPressed: widget.yesButton,
                    padding: const EdgeInsets.all(0.0),
                    child: Ink(
                        height: 40,
                        decoration: BoxDecoration(
                            color: AppTheme.gButton,
                            borderRadius: BorderRadius.circular(100)
                        ),
                        child: Container(
                            constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                            alignment: Alignment.center,
                            child: Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                  ),
                )
              ],
            ),
            SizedBox(height: 5,)
          ],
        ),
      ),
    );
  }
}
