import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/utils/Utils.dart';
import 'package:simple_rich_text/simple_rich_text.dart';

import '../app_theme.dart';


class ProfilePicForCaddieGolfer extends StatelessWidget {
  const ProfilePicForCaddieGolfer({
    Key key, @required this.data
  }) : super(key: key);

  final data;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Row(
        children: [
          Container(
            alignment: Alignment.centerRight,
            child: ProfilePicture(
              dataUser: data['foto'],
              height: 90,
              width: 90,
            ),),
          SizedBox(width: 8,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${data['nama_pendek']==null?data['nama_lengkap']:data['nama_pendek']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 24),),
                profileText("Full Name", data['nama_lengkap']),
                profileText("GIF ID", data['kode_user']),
                profileText("Home Course", data['nama_lapangan']),
              ],
            ),
          )
        ],
      ),
    );
  }

  Row profileText(String label, var dataProfile) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SimpleRichText("/$label/ : ", textAlign: TextAlign.left,style: TextStyle(color: AppTheme.gFontBlack),),
        Expanded(child: SimpleRichText("$dataProfile", textAlign: TextAlign.left,style: TextStyle(color: AppTheme.gFontBlack),)),
      ],
    );
  }
}

class ProfilePicture extends StatelessWidget {
  ProfilePicture({
    Key key, @required this.dataUser, @required this.height, @required this.width
  }) : super(key: key);

  final dataUser;
  final double height, width;

  @override
  Widget build(BuildContext context) {
    return  dataUser == null ? ClipRRect(
      borderRadius: BorderRadius.circular(10000.0),
      child: Image.asset(
        "assets/images/userImage.png",
        width: width,
        height: height,
      ),
    ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
        CircularProgressIndicator(
          value: progress.progress,
        ), imageUrl: Utils.url + 'upload/user/' + dataUser, width: width, height: height, fit: BoxFit.cover,));
  }
}