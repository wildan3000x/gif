import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gif/utils/Utils.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';
import 'package:social_share/social_share.dart';

import '../app_theme.dart';

class ShareSosMed extends StatefulWidget {
  const ShareSosMed({Key key, @required this.screenShotController}) : super(key: key);
  final ScreenshotController screenShotController;

  @override
  _ShareSosMedState createState() => _ShareSosMedState();
}

class _ShareSosMedState extends State<ShareSosMed> {
  File _imageFile;

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.storage.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.storage].request();
      return permissionStatus[Permission.storage] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }

  Future<void> captureScreen() async {
    PermissionStatus permissionStatus = await _getPermission();
    if(permissionStatus.isGranted) {
      _imageFile = null;
      widget.screenShotController.capture(
          pixelRatio: 1.5, delay: Duration(milliseconds: 10)).then((
          File image) async {
        //print("Capture Done");
        setState(() {
          _imageFile = image;
        });
        final result = await ImageGallerySaver.saveImage(image
            .readAsBytesSync()); // Save image to gallery,  Needs plugin  https://pub.dev/packages/image_gallery_saver
        Utils.showToast(context, "success", "Image successfully saved to gallery!");
      }).catchError((onError) {
        print(onError);
      });
    }else {
      showDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text('Permissions Not Granted'),
            content: Text('Please enable storage access permission in system app settings'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('OK'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 35,
      height: 35,
      child: FloatingActionButton(
        onPressed: (){
          widget.screenShotController.capture(pixelRatio: 2).then((File image) {
            SocialShare.shareOptions("", imagePath: image.path);
          });
        },
        child: const Icon(Icons.share_rounded, color: Colors.white, size: 20,),
        backgroundColor: AppTheme.bogies,
        mini: true,
        elevation: 8,
        tooltip: "Share this score",
      ),
    );
    //   Container(
    //   child: Column(
    //     children: [
    //       Container(
    //         margin: EdgeInsets.all(8),
    //         padding: EdgeInsets.all(8),
    //         width: double.maxFinite,
    //         decoration: BoxDecoration(
    //           color: Colors.grey,
    //           borderRadius: BorderRadius.circular(8)
    //         ),
    //           child: Text("Share this score to:", style: TextStyle(color: Colors.white),),),
    //       // SizedBox(height: 10,),
    //       // Row(
    //       //   mainAxisAlignment: MainAxisAlignment.center,
    //       //   children: [
    //       //     Tooltip(message: "Share to Whatsapp", child: InkWell(
    //       //       onTap: () async {
    //       //         final pixelRatio = MediaQuery.of(context).devicePixelRatio;
    //       //         widget.screenShotController.capture(pixelRatio: 2).then((File image) async {
    //       //           setState(() {
    //       //             _imageFile = image;
    //       //           });
    //       //           List<int> imageBytes = image.readAsBytesSync();
    //       //           String base64Image = base64Encode(imageBytes);
    //       //           print("PHOTO $base64Image");
    //       //           // SocialShare.shareOptions('hello world', imagePath: image.path);
    //       //           FlutterShareMe().shareToWhatsApp(base64Image: 'data:image/jpeg;base64,'+base64Image, msg: "THIS IS MY SCORE");
    //       //         }).catchError((onError) {
    //       //           print(onError);
    //       //         });
    //       //       },
    //       //       child: SvgPicture.asset("assets/svg/whatsapp.svg", width: 30, height: 30,),)),
    //       //     SizedBox(width: 15,),
    //       //     Tooltip(message: "Share to Facebook", child: InkWell(onTap: () async {
    //       //       widget.screenShotController.capture(pixelRatio: 2).then((File image) async {
    //       //         // await SocialSharePlugin.shareToFeedFacebook(path: image.path);
    //       //         SocialShare.shareFacebookStory(image.path,"#ffffff","#000000",
    //       //             "https://google.com",
    //       //             appId: "653089088712327");
    //       //       });
    //       //     }, child: SvgPicture.asset("assets/svg/facebook.svg", width: 30, height: 30,))),
    //       //     SizedBox(width: 15,),
    //       //     Tooltip(message: "Share to Instagram", child: InkWell(
    //       //         onTap: () {
    //       //           widget.screenShotController.capture(pixelRatio: 2).then((File image) {
    //       //             SocialShare.shareOptions("", imagePath: image.path);
    //       //           });
    //       //         },
    //       //         child: SvgPicture.asset("assets/svg/instagram.svg", width: 30, height: 30,))),
    //       //     SizedBox(width: 15,),
    //       //     Tooltip(message: "Save to gallery", child: InkWell(
    //       //       onTap: () => captureScreen(),
    //       //       child: Image.asset("assets/svg/screenshot.png", width: 30, height: 30,),)),
    //       //   ],
    //       // )
    //     ],
    //   ),
    // );
  }
}
