import 'package:flutter/material.dart';

class TextErrorNoConnection extends StatelessWidget {
  const TextErrorNoConnection({
    Key key,
    @required String connectionStatus,
  }) : _connectionStatus = connectionStatus, super(key: key);

  final String _connectionStatus;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.all(10),
        color: Color(0xFFf6b10c),
        width: double.infinity,
        child: Text(
          '${_connectionStatus == "No Internet Connection" ? "You are offline. Please check your Internet Connection" : _connectionStatus}',
          style: TextStyle(
              color: Colors.white,
              decorationColor: Colors.red
          ),
        ),
      ),
    );
  }
}