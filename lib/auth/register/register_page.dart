import 'package:flutter/material.dart';
import 'package:gif/auth/register/register_form.dart';

class RegisteraPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: <Widget>[
            Scaffold(
              extendBodyBehindAppBar: true,
              backgroundColor: Colors.transparent,
              body: ListView(
                padding: EdgeInsets.only(top: 0),
                children: [
                  new Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [0.1, 0.4, 0.55, 0.8],
                          colors: [
                            Color(0xFF2b5fbe),
                            Color(0xFFafe6e9),
                            Colors.white,
                            Colors.white,
                          ],
                        ),
                      ),
                      padding:
                          EdgeInsets.only(bottom: 20.0, left: 20, right: 20),
                      child: SafeArea(
                        child: Center(
                          child: Column(
                            children: <Widget>[
//                              RegisterForm(),
//                      _FormError(),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ],
        ));
  }
}
