import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'package:gif/app_theme.dart';
import 'package:gif/api/province.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import '../constrant.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final snackbarKey = GlobalKey<ScaffoldState>();
  String _selectedCountry;
  String _selectedCity;
  String _selectedCourse;
  String email;
  String password;
  String reenterPassword;
  String name;
  String phone;
  String nick;
  String hcity;
  String hcountry;
  String hgc;
  bool remember = false;
  String _levelRadio;
  String _genderRadio;
  String _verifMethodRadio;
  final List<String> errors = [];
  bool _autoValidate = false;
  bool _obscureConfirmText = true;
  bool _obscureText = true;
  var dataVerif;


  void getVerification() async {
    var data = {
      'jenis_verifikasi': "Auth"
    };

    Utils.postAPI(data, "get_verifikasi").then((body) {
      if (body['status'] == 'success') {
        dataVerif = body['data'];
        setState(() {});
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void getCountry() async {
    var data = {};

    Utils.postAPI(data, "get_negara").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CountryServices.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void getProvince() async {
    var data = {};

    Utils.postAPI(data, "get_kota").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CitiesService.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  bool _loadingCourse = false;
  List<dynamic> _dataCourse = [];

  void getCourse() async {
    var data = {
      'kota': cityCon.text,
    };

    Utils.postAPI(data, "get_lapangan").then((body) {
      CourseService.course.clear();
      if (body['status'] == 'success') {
        setState(() {
          _dataCourse = body['data'];
          _loadingCourse = false;
        });
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    CourseService.course.clear();
    getVerification();
    getProvince();
    getCountry();
    super.initState();
    setState(() {
      _levelRadio = "Golfer";
      _genderRadio = "Male";
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  DateTime _date = DateTime.now();
  DateTime _dateValid = DateTime.now();

  var myFormat = DateFormat('yMd');
  var myFormat2 = DateFormat('y-M-d');
  var dateeee;

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1800),
      lastDate: DateTime(2100),
    );

    dateeee = myFormat2.format(_date) + " 00:00:00.000";
    print("TRTRTRTT $dateeee");

    if (picked == null || picked.toString() == dateeee) {
      setState(() {
        addError(error: "Please input date of birth");
      });
    }else{
      setState(() {
        _date = picked;
        print("TTT" + _date.toString());
        removeError(error: "Please input date of birth");
        _timeError = false;
      });
    }
  }

  TextEditingController fullNameCon = TextEditingController();
  TextEditingController nickNameCon = TextEditingController();
  TextEditingController emailCon = TextEditingController();
  TextEditingController passCon = TextEditingController();
  TextEditingController reenterPassCon = TextEditingController();
  TextEditingController phoneCon = TextEditingController();
  TextEditingController countryCon = TextEditingController();
  TextEditingController cityCon = TextEditingController();
  TextEditingController courseCon = TextEditingController();
  int courseId;

  FocusNode nickNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode passNode = FocusNode();
  FocusNode reenterPassNode = FocusNode();
  FocusNode phoneNode = FocusNode();
  FocusNode countryNode = FocusNode();
  FocusNode cityNode = FocusNode();
  FocusNode courseNode = FocusNode();

  bool _loading = false;
  bool _success = false;

  void _register() async {
    var data = {
      'sebagai': _levelRadio,
      'nama_lengkap': fullNameCon.text,
      'nama_pendek': nickNameCon.text,
      'jenis_kelamin': _genderRadio,
      'tgl_lahir': _date.toString(),
      'email': emailCon.text,
      'password': passCon.text,
      'no_telp': phoneCon.text,
      'negara': countryCon.text,
      'kota': cityCon.text,
      'lapangan_id': courseId,
      'id_verifikasi': _verifMethodRadio
    };
    errors.clear();
    Utils.postAPI(data, "registrasi").then((body) {
      if (body['status'] == 'success') {
        setState(() {
          _loading = false;
          _success = true;
        });
      } else {
        _homeController.animateTo(
          0.0,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
        setState(() {
          if (!errors.contains(body['body_message'])) {
            errors.add(body['body_message']);
          }
          _loading = false;
        });
      }
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void _handleGender(String value) {
    setState(() {
      _genderRadio = value;
    });
  }

  void _handleVerifMethod(String value) {
    setState(() {
      _verifMethodRadio = value;
    });
  }

  void _handleLevel(String value) {
    setState(() {
      _levelRadio = value;
    });
  }

  String _fullNameErrorText;
  bool _fullNameError = false;
  String _emailErrorText;
  String _passwordErrorText;
  String _reenterPasswordErrorText;
  String _phoneErrorText;
  String _countryErrorText;
  bool _emailError = false;
  bool _passwordError = false;
  bool _reenterPasswordError = false;
  bool _phoneError = false;
  bool _countryError = false;
  String _tourCityErrorText;
  bool _tourCityError = false;
  String _tourCourseErrorText;
  bool _tourCourseError = false;

  bool _timeError = false;

  final ScrollController _homeController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: snackbarKey,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: <Widget>[
            Scaffold(
              extendBodyBehindAppBar: true,
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  ListView(
                    padding: EdgeInsets.only(top: 0),
                    controller: _homeController,
                    children: [
                      GestureDetector(
                        onTap: () => FocusScope.of(context).unfocus(),
                        child: new Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                stops: _success == true
                                    ? [0.1, 0.45, 0.65, 0.8]
                                    : [0.05, 0.2, 0.3, 0.5],
                                colors: [
                                  Color(0xFF2b5fbe),
                                  Color(0xFF9edbe4),
                                  Colors.white,
                                  Colors.white,
                                ],
                              ),
                            ),
                            padding:
                                EdgeInsets.all(13),
                            child: SafeArea(
                              child: Center(
                                child: Column(
                                  children: <Widget>[
                                    _success == true
                                        ? Column(
                                            children: [
                                              _iconRegister(),
                                              _successScreen(_verifMethodRadio),
                                            ],
                                          )
                                        : Form(
                                            key: _formKey,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                              children: [
                                                _iconRegister(),
                                                _titleDesc(),
                                                FormErrorRegister(errors: errors),
                                                _buildCheckBoxes(),
                                                _textField(),
                                                _buildButton(context),
                                              ],
                                            ),
                                          )
//                      _FormError(),
                                  ],
                                ),
                              ),
                            )),
                      ),
                    ],
                  ),
                  _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _iconRegister() {
    return Image.asset(
      "assets/images/GIF for Colored BG.PNG",
      width: 200.0,
      height: 200.0,
    );
  }

  Widget _titleDesc() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        Text(
          "Register New User",
          style: TextStyle(
            color: Colors.black,
            fontSize: 24,
          ),
        ),
      ],
    );
  }

  Widget _buildCheckBoxes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "I am a",
          style: TextStyle(color: AppTheme.gButton, fontSize: 16),
        ),
        SizedBox(
          width: 45,
          child: Radio(
            value: "Golfer",
            groupValue: _levelRadio,
            onChanged: _handleLevel,
          ),
        ),
        Text("Golfer"),
        SizedBox(
          width: 45,
          child: Radio(
            value: "Caddie",
            groupValue: _levelRadio,
            onChanged: _handleLevel,
          ),
        ),
        Text("Caddie")
      ],
    );
  }

  Widget _textField() {
    return new Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Full Name",
                        style: TextStyle(
                            color: _fullNameError
                                ? AppTheme.gValidError
                                : AppTheme.gButton,
                            fontSize: 14),
                      ),
                    ),
                    TextFormField(
                      controller: fullNameCon,
                      onSaved: (newValue) => name = newValue,
                      // onChanged: (value) {
                      //   if (value.isNotEmpty) {
                      //     removeError(error: kRegister);
                      //   }
                      //   return null;
                      // },
                      // validator: (value) {
                      //   if (value.isEmpty) {
                      //     addError(error: kRegister);
                      //     return "";
                      //   }
                      //   return null;
                      // },
                      validator: (value) {
                        if (value.isEmpty) {
                          addError(error: kRegister);
                          _fullNameError = true;
                          _fullNameErrorText = '';
                          return '';
                        } else {
                          _fullNameError = false;
                          return null;
                        }
                      },
                      onChanged: (value) {
                        setState(() {
                          _fullNameError = false;
                          _fullNameErrorText = null; // Resets the error
                        });
                        // removeError(error: kRegister);
                      },
                      style: TextStyle(color: AppTheme.input),
                      autofocus: false,
                      onFieldSubmitted: (_) {
                        FocusScope.of(this.context).requestFocus(nickNode);
                      },
                      decoration: InputDecoration(
                        errorText: _fullNameErrorText,
                        errorStyle: TextStyle(height: 0),
                        errorBorder: UnderlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        focusedErrorBorder: UnderlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 30,),
              Expanded(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Nick Name",
                        style: TextStyle(
                            color: AppTheme.gButton, fontSize: 14),
                      ),
                    ),
                    TextFormField(
                      focusNode: nickNode,
                      controller: nickNameCon,
                      onSaved: (newValue) => nick = newValue,
                      decoration: InputDecoration(hintText: "(Optional)"),
                      style: TextStyle(color: AppTheme.input),
                      autofocus: false,
                      onFieldSubmitted: (_) {
                        FocusScope.of(this.context).requestFocus(emailNode);
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.grey,
                    ),
                  )),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Gender",
                          style: TextStyle(
                              color: AppTheme.gButton, fontSize: 14),
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                          SizedBox(
                            width: 26,
                            child: Radio(
                              value: "Male",
                              groupValue: _genderRadio,
                              onChanged: _handleGender,
                            ),
                          ),
                          Text("Male"),
                          SizedBox(
                            width: 26,
                            child: Radio(
                              value: "Female",
                              groupValue: _genderRadio,
                              onChanged: _handleGender,
                            ),
                          ),
                          Text("Female")
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 30,),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Colors.grey,
                    ),
                  )),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Date Of Birth",
                          style: TextStyle(
                              color: _timeError
                                  ? AppTheme.gValidError
                                  : AppTheme.gButton, fontSize: 14),
                        ),
                      ),
                      FlatButton.icon(
                        label: Text('${myFormat.format(_date)}'),
                        icon: Icon(Icons.date_range),
                        onPressed: () {
                          selectDate(context);
                        },
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Email",
              style: TextStyle(
                  color:
                      _emailError ? AppTheme.gValidError : AppTheme.gButton,
                  fontSize: 14),
            ),
          ),
          // Padding(
          //   padding: EdgeInsets.only(top: 7.0),
          // ),
          new Container(
            height: 50,
            child: TextFormField(
              focusNode: emailNode,
              controller: emailCon,
              onSaved: (newValue) => email = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  _emailError = true;
                  addError(error: kRegister);
                  _emailErrorText = "";
                  return '';
                } else if (!emailValidatorRegExp.hasMatch(value)) {
                  addError(error: kEmail);
                  _emailError = true;
                  _emailErrorText = "";
                  return '';
                } else {
                  removeError(error: kEmail);
                  _emailError = false;
                  return null;
                }
              },
              onChanged: (value) {
                if (value.isNotEmpty) {
                  setState(() {
                    _emailError = false;
                    _emailErrorText = null;
                    // removeError(error: kRegister);
                  });
                } else if (emailValidatorRegExp.hasMatch(value)) {
                  setState(() {
                    _emailError = false;
                    _emailErrorText = null;
                  });
                  removeError(error: kEmail);
                } else if (value.isNotEmpty &&
                    emailValidatorRegExp.hasMatch(value)) {
                  setState(() {
                    _emailError = false;
                    _emailErrorText = null;
                  });
                  removeError(error: kEmail);
                }
                // _emailError = false;
                // _emailErrorText = null; // Resets the error
              },
              style: TextStyle(color: AppTheme.input),
              autofocus: false,
              onFieldSubmitted: (_) {
                // _formKey.currentState.validate();
                FocusScope.of(this.context).requestFocus(phoneNode);
              },
              decoration: InputDecoration(
                errorText: _emailErrorText,
                errorStyle: TextStyle(height: 0),
                errorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Password",
              style: TextStyle(
                  color: _passwordError
                      ? AppTheme.gValidError
                      : AppTheme.gButton,
                  fontSize: 14),
            ),
          ),
          new Container(
            height: 50,
            child: TextFormField(
              focusNode: reenterPassNode,
              controller: passCon,
              onSaved: (newValue) => password = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  addError(error: kRegister);
                  _passwordError = true;
                  _passwordErrorText = '';
                  return '';
                } else {
                  return null;
                }
              },
              onChanged: (value) {
                setState(() {
                  _passwordError = false;
                  _passwordErrorText = null; // Resets the error
                });
                // removeError(error: kRegister);
              },
              style: TextStyle(color: AppTheme.input),
              autofocus: false,
              obscureText: _obscureText,
              decoration: InputDecoration(
                errorText: _passwordErrorText,
                errorStyle: TextStyle(height: 0),
                suffixIcon: InkWell(
                  onTap: (){
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(_obscureText ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                ),
                errorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Reenter Password",
              style: TextStyle(
                  color: _reenterPasswordError ? AppTheme.gValidError : AppTheme.gButton,
                  fontSize: 14),
            ),
          ),
          new Container(
            height: 50,
            child: TextFormField(
              focusNode: phoneNode,
              controller: reenterPassCon,
              onSaved: (newValue) => reenterPassword = newValue,
              validator: (value) {
                if (value.isEmpty) {
                  addError(error: kRegister);
                  _reenterPasswordError = true;
                  _reenterPasswordErrorText = '';
                  return '';
                } else if (value != passCon.text) {
                  addError(error: "password does'nt match");
                  _reenterPasswordError = true;
                  _passwordError = true;
                  _reenterPasswordErrorText = '';
                  _passwordErrorText = '';
                  return '';
                } /*else if (value == passCon.text) {
                  removeError(error: "password does'nt match");
                  _reenterPasswordError = false;
                  _passwordError = false;
                  _reenterPasswordErrorText = null;
                  _passwordErrorText = null;
                  return '';
                } */else {
                  return null;
                }
              },
              onChanged: (value) {
                setState(() {
                  _reenterPasswordError = false;
                  _passwordError = false;
                  _reenterPasswordErrorText = null;
                  _passwordErrorText = null;
                });
                // removeError(error: kRegister);
              },
              style: TextStyle(color: AppTheme.input),
              autofocus: false,
              obscureText: _obscureConfirmText,
              decoration: InputDecoration(
                suffixIcon: InkWell(
                  onTap: (){
                    setState(() {
                      _obscureConfirmText = !_obscureConfirmText;
                    });
                  },
                  child: Icon(_obscureConfirmText ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                ),
                errorText: _reenterPasswordErrorText,
                errorStyle: TextStyle(height: 0),
                errorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Mobile Phone No.",
              style: TextStyle(
                  color:
                      _phoneError ? AppTheme.gValidError : AppTheme.gButton,
                  fontSize: 14),
            ),
          ),
          new Container(
            height: 50,
            child: TextFormField(
              controller: phoneCon,
              onSaved: (newValue) => phone = newValue,
              validator: (value) {
                // setState(() {
                if (value.isEmpty) {
                  addError(error: kRegister);
                  _phoneError = true;
                  _phoneErrorText = '';
                  return '';
                } else {
                  // });
                  _phoneError = false;
                  return null;
                }
              },
              onChanged: (value) {
                setState(() {
                  _phoneError = false;
                  _phoneErrorText = null; // Resets the error
                });
                // removeError(error: kRegister);
              },
              style: TextStyle(color: AppTheme.input),
              autofocus: false,
              onFieldSubmitted: (_) {
                FocusScope.of(this.context).requestFocus(countryNode);
              },
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
              ],
              keyboardType: TextInputType.number,
              maxLength: 15,
              decoration: InputDecoration(
                counterText: "",
                errorStyle: TextStyle(height: 0),
                errorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Home Country",
                        style: TextStyle(
                            color: _countryError
                                ? AppTheme.gValidError
                                : AppTheme.gButton,
                            fontSize: 14),
                      ),
                    ),
                    TypeAheadFormField(
                      noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No Country Found!")),
                      textFieldConfiguration: TextFieldConfiguration(
                        controller: this.countryCon,
                        onChanged: (value) {
                          setState(() {
                            // addError(error: kRegister);
                            _countryError = false;
                            _countryErrorText = null; // Resets the error
                          });
                        },
                        decoration: InputDecoration(
                          // errorText: _countryErrorText,
                          errorStyle: TextStyle(height: 0),
                          errorBorder: UnderlineInputBorder(
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0),
                          ),
                          focusedErrorBorder: UnderlineInputBorder(
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0),
                          ),
                        ),
                      ),
                      suggestionsCallback: (String pattern) {
                        return CountryServices.getSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          title: Text(suggestion['nama']),
                        );
                      },
                      transitionBuilder:
                          (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        this.countryCon.text = suggestion["nama"];
                        getCourse();
                        setState(() {
                          _countryError = false;
                        });
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          _countryError = true;
                          _countryErrorText = '';
                          return '';
                        } else {
                          _countryError = false;
                          return null;
                        }
                      },
                      onSaved: (value) => this._selectedCountry = value,
                    ),
                  ],
                ),
              ),
              // SizedBox(width: 30,),
              // Expanded(
              //   child: Column(
              //     children: [
              //       Align(
              //         alignment: Alignment.centerLeft,
              //         child: Text(
              //           "Home City",
              //           style: TextStyle(
              //               color: _tourCityError
              //                   ? AppTheme.gValidError
              //                   : AppTheme.gButton,
              //               fontSize: 14),
              //         ),
              //       ),
              //       TypeAheadFormField(
              //         noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No City Found!")),
              //         textFieldConfiguration: TextFieldConfiguration(
              //           controller: this.cityCon,
              //           onSubmitted: (_) {
              //             // _formKey.currentState.validate();
              //           },
              //           onChanged: (value) {
              //             setState(() {
              //               addError(error: kRegister);
              //               _tourCityError = false;
              //               _tourCityErrorText = null; // Resets the error
              //             });
              //           },
              //           decoration: InputDecoration(
              //             // errorText: _tourCityErrorText,
              //             errorStyle: TextStyle(height: 0),
              //             errorBorder: UnderlineInputBorder(
              //               // width: 0.0 produces a thin "hairline" border
              //               borderSide:
              //                   BorderSide(color: Colors.grey, width: 1.0),
              //             ),
              //             focusedErrorBorder: UnderlineInputBorder(
              //               // width: 0.0 produces a thin "hairline" border
              //               borderSide:
              //                   BorderSide(color: Colors.grey, width: 1.0),
              //             ),
              //           ),
              //         ),
              //         suggestionsCallback: (String pattern) {
              //           return CitiesService.getSuggestions(pattern);
              //         },
              //         itemBuilder: (context, suggestion) {
              //           return ListTile(
              //             title: Text(suggestion['nama']),
              //           );
              //         },
              //         transitionBuilder:
              //             (context, suggestionsBox, controller) {
              //           return suggestionsBox;
              //         },
              //         onSuggestionSelected: (suggestion) {
              //           this.cityCon.text = suggestion["nama"];
              //           setState(() {
              //             _tourCityError = false;
              //             courseCon.text = "";
              //             getCourse();
              //             _loadingCourse = true;
              //           });
              //         },
              //         validator: (value) {
              //           // setState(() {
              //           if (value.isEmpty) {
              //             _tourCityError = true;
              //             _tourCityErrorText = '';
              //             return '';
              //           } else {
              //             // });
              //             _tourCityError = false;
              //             return null;
              //           }
              //         },
              //         onSaved: (value) => this._selectedCity = value,
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Home Golf Course (Most Frequent)",
              style: TextStyle(
                  color: _tourCourseError
                      ? AppTheme.gValidError
                      : AppTheme.gButton,
                  fontSize: 14),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          // TypeAheadFormField(
          //   noItemsFoundBuilder: (context) => Container(padding: EdgeInsets.all(8), child: Text("No Course Found!")),
          //   textFieldConfiguration: TextFieldConfiguration(
          //     controller: this.courseCon,
          //     onChanged: (value) {
          //       setState(() {
          //         // addError(error: kRegister);
          //         _tourCourseError = false;
          //         _tourCourseErrorText = null; // Resets the error
          //       });
          //     },
          //     decoration: InputDecoration(
          //       // errorText: _tourCityErrorText,
          //       errorStyle: TextStyle(height: 0),
          //       errorBorder: UnderlineInputBorder(
          //         // width: 0.0 produces a thin "hairline" border
          //         borderSide: BorderSide(color: Colors.grey, width: 1.0),
          //       ),
          //       focusedErrorBorder: UnderlineInputBorder(
          //         // width: 0.0 produces a thin "hairline" border
          //         borderSide: BorderSide(color: Colors.grey, width: 1.0),
          //       ),
          //     ),
          //   ),
          //   suggestionsCallback: (String pattern) {
          //     return CourseService.getSuggestions(pattern);
          //   },
          //   itemBuilder: (context, suggestion) {
          //     return _loadingCourse ? Center(child: Container(width: 15, height: 15, child: CircularProgressIndicator(),),) : ListTile(
          //       title: Text(suggestion['nama_lapangan']),
          //     );
          //   },
          //   transitionBuilder: (context, suggestionsBox, controller) {
          //     return suggestionsBox;
          //   },
          //   onSuggestionSelected: (suggestion) {
          //     this.courseCon.text = suggestion["nama_lapangan"];
          //     setState(() {
          //       courseId = suggestion["id_lapangan"];
          //       _tourCourseError = false;
          //     });
          //   },
          //   validator: (value) {
          //     if (value.isEmpty) {
          //       _tourCourseError = true;
          //       _tourCourseErrorText = '';
          //       return '';
          //     } else {
          //       _tourCourseError = false;
          //       return null;
          //     }
          //     // setState(() {
          //     //   if(value.isEmpty) {
          //     //     _tourCourseError = true;
          //     //     _tourCourseErrorText = '';
          //     //   }
          //     // });
          //     return null;
          //   },
          //   onSaved: (value) => this._selectedCourse = value,
          // ),
          SearchableDropdown.single(
            items: _dataCourse.map((data) {
              return (DropdownMenuItem(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${data['nama_lapangan']}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                    Text("${data['nama']}")
                  ],
                ),
                value: data,
              ));
            }).toList(),
            // value: selectedItem,
            onChanged: (value) {
              if(value != null){
                cityCon.text = value['nama'];
                courseId = value['id_lapangan'];
                setState(() {
                  _tourCourseError = false;
                });
              }
            },
            dialogBox: true,
            isExpanded: true,
            displayClearIcon: false,
            onClear: () {},
            underline: Container(
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.grey)
                  )
              ),
            ),
          ),
          SizedBox(height: 8,),
          Container(
            decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1,
                    color: Colors.grey,
                  ),
                )),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Verification Method",
                    style: TextStyle(
                        color: AppTheme.gButton, fontSize: 14),
                  ),
                ),
                Row(
                  children: List.generate(dataVerif == null ? 0 : dataVerif.length, (index) {
                    return Padding(
                      padding: EdgeInsets.only(right: index == 0 ? 16 : 0.0),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 26,
                            child: Radio(
                              value: dataVerif[index]['id_verifikasi'].toString(),
                              groupValue: _verifMethodRadio,
                              onChanged: _handleVerifMethod,
                            ),
                          ),
                          SizedBox(width: 8,),
                          Text("${dataVerif[index]['verifikasi']}"),
                        ],
                      ),
                    );
                  }),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButton(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 30.0),
        ),
        RaisedButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
          splashColor: Colors.grey.withOpacity(0.5),
          onPressed: () {
            setState(() {});
            var date1 = myFormat2.format(_date);
            var date2 = myFormat2.format(_dateValid);
            print("Test ${_formKey.currentState.validate()}");
            if (_formKey.currentState.validate()
                && date1 != date2
                && passCon.text == reenterPassCon.text
                && cityCon.text.isNotEmpty && courseId != null) {
              _formKey.currentState.save();
              setState(() {
                _timeError = false;
              });
              errors.clear();
              if(!_loading){
                _register();
              }
              setState(() {
                _loading = true;
              });
            }else{
              _homeController.animateTo(
                0.0,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 300),
              );
              if(date1 == date2) {
                setState(() {
                  _timeError = true;
                  addError(error: "Please input date of birth");
                });
              }
              if(cityCon.text.isEmpty && courseId == null){
                setState(() {
                  _tourCourseError = true;
                  _tourCourseErrorText = '';
                });
              }
            }
          },
          padding: const EdgeInsets.all(0.0),
          child: Ink(
              decoration: BoxDecoration(
                  color: AppTheme.gButton,
                  borderRadius: BorderRadius.circular(100)
              ),
              child: Container(
                  constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                  alignment: Alignment.center,
                  child: _loading
                      ? Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      ))
                      : Text("Save", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600), textAlign: TextAlign.center,))),
        ),
        Padding(
          padding: EdgeInsets.only(top: 40.0),
        ),
      ],
    );
  }
}

Widget _successScreen(verifId) {
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 16.0),
      ),
      Text(
        "Registration is\n Successful",
        style: TextStyle(
          color: Colors.black,
          fontSize: 24,
        ),
        textAlign: TextAlign.center,
      ),
      Padding(
        padding: EdgeInsets.only(top: 26.0),
      ),
      Text(
        "Please check your ${verifId == "1" ? "email" : "whatsapp"}",
        style: TextStyle(color: Color(0xFF837B7B), fontSize: 16),
        textAlign: TextAlign.center,
      ),
      Padding(
        padding: EdgeInsets.only(top: 20.0),
      ),
      Text(
        verifId == "1" ? kEmailSent : kWhatsappSent,
        style: TextStyle(color: Color(0xFF837B7B), fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

class FormErrorRegister extends StatelessWidget {
  const FormErrorRegister({
    Key key,
    @required this.errors,
  }) : super(key: key);

  final List<String> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length, (index) => formErrorRegister(error: errors[index])),
    );
  }

  Row formErrorRegister({String error}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 35),
        ),
        Text(
          error,
          style: TextStyle(
              color: Colors.red, fontSize: 15, fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
