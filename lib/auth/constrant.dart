  import 'package:flutter/cupertino.dart';

final RegExp emailValidatorRegExp =
RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

const String kCountID = "Please enter a valid email/ID";
const String kPass = "Please input your password";
const String kWrongID = "Wrong ID or Password";
const String kRegister = "Please complete registration form";
const String kEmailSent = "GIF has sent you an email. Please click\n the link in the email body\nto activate your ID";
const String kWhatsappSent = "GIF has sent you a whatsapp. Please click\n the link in the whatsapp body\nto activate your ID";
const String kResetPassEmailSent = "GIF has sent you an email. Please click\n the link in the email body\nto reset your password";
const String kEmail = "Please enter a valid email";
const String kReenterPass = "your password and confirmation password\ndidnt match";
const String kEmpty = "Please enter the form";
const String kPassSalah = "Password lama yang anda masukkan salah";
const String kPassLama = "Password baru sama dengan password lama";

const String kResetEmpty = "Please enter your email";

const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
