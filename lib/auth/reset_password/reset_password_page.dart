import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/register/register_form.dart';
import 'package:gif/utils/Utils.dart';

import '../constrant.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  String email;
  final List<String> errors = [];

  bool _status = false;
  bool _loading = false;

  final _formKey = GlobalKey<FormState>();
  final snackbarKey = GlobalKey<ScaffoldState>();

  TextEditingController emails = TextEditingController();

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  Future<void> _resetEmail() async{
    setState(() {
      _loading = true;
    });

    var data = {
      'email' : emails.text
    };

    errors.clear();
    Utils.postAPI(data, "reset").then((body) {
      if(body['status'] == 'success'){
        print(body);
        setState(() {
          _loading = false;
          _status = true;
        });
      }else{
        setState(() {
          if (!errors.contains(body['body_message'])) {
            errors.add(body['body_message']);
          }
          _loading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      key: snackbarKey,
        body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.1, 0.45, 0.65, 0.8],
                colors: [
                  Color(0xFF2b5fbe),
                  Color(0xFF9edbe4),
                  Colors.white,
                  Colors.white,
                ],
              ),
            ),
            padding: EdgeInsets.all(20.0),
            child: Center(
              child: _status == true ? Column(
                  children: [
                    _iconReset(),
                    _contentSuccess(),
                  ],
                ) : Form(
                  key: _formKey,
                  child: GestureDetector(
                    onTap: () => FocusScope.of(context).unfocus(),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          _iconReset(),
                          _contentReset(),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: FormErrorRegister(errors: errors),
                          ),
                        ],
                      ),
                    ),
                  ),
                ))));
  }

  Widget _iconReset() {
    return Image.asset(
      "assets/images/GIF for Colored BG.PNG",
      width: 200.0,
      height: 200.0,
    );
  }

  Widget _contentReset() {
    return Column(
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              Text(
                "Reset Password",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 24),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
              ),
              Text(
                "Please enter your email below to\n reset your password",
                style: TextStyle(color: Color(0xFF837B7B), fontSize: 16),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
              ),
              new Container(
                height: 30,
                child: TextFormField(
                  onSaved: (newValue) => email = newValue,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      removeError(error: kResetEmpty);
                    } else if (emailValidatorRegExp.hasMatch(value)) {
                      removeError(error: kEmail);
                    }
                    return null;
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      addError(error: kResetEmpty);
                      return "";
                    } else if (!emailValidatorRegExp.hasMatch(value)) {
                      addError(error: kEmail);
                      return "";
                    }
                    return null;
                  },
                  controller: emails,
                  decoration: InputDecoration(
                      errorStyle: TextStyle(height: 0),
                      filled: true,
                      fillColor: Colors.white,
                      contentPadding: new EdgeInsets.symmetric(vertical: 0.5, horizontal: 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          )),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ))),
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              InkWell(
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    _resetEmail();
                  }
                },
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  width: 80,
                  height: 36,
                  child: _loading
                      ? Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(),
                      ))
                      : Text(
                    'Submit',
                    style: TextStyle(color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  decoration: BoxDecoration(
                    color: Color(0xFF2358BD),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _contentSuccess() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 30),
        ),
        Text(
          "Reset Password",
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
        ),
        Padding(
          padding: EdgeInsets.only(top: 26.0),
        ),
        Text(
          "Please check your email",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: EdgeInsets.only(top: 20.0),
        ),
        Text(
          kResetPassEmailSent,
          style: TextStyle(color: Colors.black87),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

}

