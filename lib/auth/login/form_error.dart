import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constrant.dart';

class FormError extends StatelessWidget {
  const FormError({
    Key key,
    @required this.errors,
  }) : super(key: key);

  final List<String> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length, (index) => formErrorText(error: errors[index])),
    );
  }

  Container formErrorText({String error}) {
    return Container(
        margin: EdgeInsets.only(top: 30),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: const Color(0xFF0E3311).withOpacity(0.7),
        ),
        child: Column(
          children: [
            Text(error, style: TextStyle(color: Colors.greenAccent, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            if(error == "Form is Empty")
              Text(
                "Please enter a valid email/ID and password",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            if (error == "Email does not exist")
              Text(
                "Your email is not registered yet",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            if (error == "Wrong password")
              Text(
                "Please check your login password",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            if (error == "Your ID is not yet activated")
              Text(
                "GIF has sent you an email. Please click\nthe link in the email body\nto activate your ID",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            if(error == "ID / Mobile / Email / does not exist")
              Text(
                "Your ID / Mobile / Email / is not registered yet",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            if(error == "Username or Password is Not Valid")
              Text(
                "Please check your credentials and try again",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
          ],
        ));
  }
}
