import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gif/auth/register/register_form.dart';
import 'package:gif/auth/reset_password/reset_password_page.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app_theme.dart';
import '../../main.dart';
import '../constrant.dart';
import 'form_error.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  double resizeForKeyboard = 0;
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  bool remember = false;
  final List<String> errors = [];
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _validEmail = false;
  bool _validPass = false;
  bool _obscureText = true;

  FocusNode passNode = FocusNode();

  bool _isLoading = false;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState(){
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  final ScrollController _homeController = ScrollController();


  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  signIn() async {
    var data = {
      'email' : emailController.text,
      'password' : passwordController.text,
    };

    SharedPreferences prefs = await SharedPreferences.getInstance();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    errors.clear();
    setState(() {
      _isLoading = true;
    });
    Utils.postAPI(data, "login").then((body) {
      setState(() {
        _isLoading = false;
      });
      if(body['status'] == 'success'){
        setState(() {
          _isLoading = false;
          prefs.setString('token', body['token']);
          prefs.setString('id', body['data']['id'].toString());
          prefs.setString("level", body['data']['level']);
          sharedPreferences.setString("token", body['token']);
          sharedPreferences.setString("user", json.encode(body));
          sharedPreferences.setString("id", body['data']['id'].toString());
          sharedPreferences.setString("level", body['data']['level']);
          sharedPreferences.setString("usertoken", json.encode(body['data']['token']));
          // sharedPreferences.setString("userId", json.encode(body['data']['id']));
          var dataJson = sharedPreferences.getString('usertoken');
          var data = json.decode(dataJson);
          print(data);
          if(body['data']['level'] == 'Golfer') {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) =>
                    HomePage(navbar: true,)));
          }else{
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) =>
                    HomeCaddie()));
          }
        });
      }else{
        _homeController.animateTo(
          200.0,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
        setState(() {
          if(!errors.contains(body['head_message'])){
            errors.add(body['head_message']);
          }
          if(errors.contains(body['head_message']) != errors.contains(body['head_message'])){
            errors.remove(body['head_message']);
          }
          _isLoading = false;
        });
      }
    }, onError: (error) {
      print("Error == $error");
      _homeController.animateTo(
        200.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
      setState(() {
        if(!errors.contains("$error")){
          errors.add("$error");
        }
        if(errors.contains("$error") != errors.contains("$error")){
          errors.remove("$error");
        }
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData _mediaQueryData;
    _mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg",),
            fit: BoxFit.cover,
          )
        ),
        child: SafeArea(
          child:
            GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: ListView(
                controller: _homeController,
                children: [
                  Container(
                    height: _mediaQueryData.size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Image.asset(
                          "assets/images/gif_logo.png",
                          width: double.maxFinite,
                          height: 100.0,
                        ),
                        // SizedBox(height: 20,),
                        Text("Login", style: TextStyle(color: Colors.white, fontSize: 24,),),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Text("GIF ID / Mobile / Email", style: TextStyle(color: Colors.white, fontSize: 16),),
                              SizedBox(height: 2,),
                              SizedBox(
                                width: 180,
                                height: 30,
                                child: TextFormField(
                                  onSaved: (newValue) => email = newValue,
                                  onChanged: (value) {
                                    if (emailController.text.isNotEmpty) {
                                      setState(() {
                                        _validEmail = false;
                                        errors.clear();
                                        removeError(error: kCountID);
                                      });
                                    }
                                    return null;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      setState(() {
                                        _validEmail = true;
                                      });
                                      if(_validEmail || _validPass) {
                                        addError(error: "Form is Empty");
                                      }
                                      return "";
                                    }
                                    return null;
                                  },
                                  controller: emailController,
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(height: 0),
                                      contentPadding: new EdgeInsets.symmetric(vertical: 0.5, horizontal: 10.0),
                                      filled: true,
                                      fillColor: Colors.white,
                                      border: OutlineInputBorder(),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.blueAccent,
                                          )
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.red,
                                          )
                                      ),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.red,
                                          )
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: AppTheme.white,
                                          ))),
                                  style: TextStyle(color: Colors.black),
                                  onFieldSubmitted: (_) {
                                    // FocusScope.of(this.context).requestFocus(passNode);
                                  },
                                  maxLines: 1,
                                ),
                              ),
                              SizedBox(height: 15,),
                              Text("Password", style: TextStyle(color: Colors.white, fontSize: 16),),
                              SizedBox(height: 2,),
                              SizedBox(
                                width: 180,
                                height: 30,
                                child: TextFormField(
                                  focusNode: passNode,
                                  onSaved: (newValue) => password = newValue,
                                  onChanged: (value) {
                                    if (value.isNotEmpty) return null;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      setState(() {
                                        _validPass = true;
                                      });
                                      if(_validEmail || _validPass) {
                                        addError(error: "Form is Empty");
                                      }
                                      return "";
                                    }
                                    return null;
                                  },
                                  controller: passwordController,
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(height: 0),
                                      contentPadding: new EdgeInsets.symmetric(vertical: 0.5, horizontal: 10.0),
                                      filled: true,
                                      fillColor: Colors.white,
                                      border: OutlineInputBorder(),
                                      suffixIcon: InkWell(
                                        onTap: (){
                                          setState(() {
                                            _obscureText = !_obscureText;
                                          });
                                        },
                                        child: Icon(_obscureText ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.blueAccent,
                                          )
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.red,
                                          )
                                      ),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: Colors.red,
                                          )
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            color: AppTheme.white,
                                          ))),
                                  style: TextStyle(color: Colors.black),
                                  autofocus: false,
                                  obscureText: _obscureText,
                                ),
                              ),
                              SizedBox(height: 15,),
                              RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                                splashColor: Colors.grey.withOpacity(0.5),
                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  errors.clear();
                                  if (_formKey.currentState.validate()) {
                                    _formKey.currentState.save();
                                    if(!_isLoading){
                                      signIn();
                                    }
                                  }else{
                                    _homeController.animateTo(
                                      200.0,
                                      curve: Curves.easeOut,
                                      duration: const Duration(milliseconds: 300),
                                    );
                                    return "";
                                  }
                                },
                                padding: EdgeInsets.zero,
                                child: Ink(
                                    decoration: BoxDecoration(
                                        color: AppTheme.gBlueButton,
                                        borderRadius: BorderRadius.circular(15)
                                    ),
                                    child: Container(
                                        constraints: const BoxConstraints(maxWidth: 90, minHeight: 36),
                                        alignment: Alignment.center,
                                        child: _isLoading
                                            ? Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),))
                                            : Text("Login", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              buttonForgotAndRegister(text: "Forgot Password", link: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPassword()))),
                              buttonForgotAndRegister(text: "Register", link: () => Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage()))),
                            ],
                          ),
                        ),
                        FormError(errors: errors),
                        Container(alignment: Alignment.bottomRight, padding: EdgeInsets.only(right: 10), child: Image.asset("assets/images/Agyle Logo.png"),)
                      ],
                    ),
                  )
              ],
          ),
            ),
        ),
      ),
    );
  }

  RaisedButton buttonForgotAndRegister({link, String text}) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      splashColor: Colors.grey.withOpacity(0.5),
      padding: EdgeInsets.zero,
      color: Colors.black.withOpacity(0.5),
      onPressed: link,
      child: Ink(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5)
          ),
          child: Container(
              constraints: const BoxConstraints(maxWidth: 100, minHeight: 45),
              alignment: Alignment.center,
              child: Text("$text", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
    );
  }
}
