import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:gif/api/province.dart';
import 'package:gif/auth/constrant.dart';
import 'package:gif/auth/login/login_page.dart';
// import 'package:gif/auth/profile/new_profile_page.dart';
// import 'package:gif/auth/profile/rework_profile_page.dart';
import 'package:gif/auth/register/register_form.dart';
import 'package:gif/caddie/home_caddie.dart';
import 'package:gif/create_games/pairing/view_profile.dart';
import 'package:gif/models/NavItem.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/size_config.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart';
import 'package:gif/main.dart';
import 'package:provider/provider.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../app_theme.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key, this.userId, this.forCaddie, this.privateProfile}) : super(key: key);
  final userId, forCaddie, privateProfile;
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final List<String> errors = [];
  bool _loading = false;
  bool _isLoading = false;
  bool _edit = false;
  bool _changepassword = false;
  final _formKey = GlobalKey<FormState>();

  String curr;
  String newpass;
  String match;
  String _selectedCountry;
  String _selectedCity;
  String _selectedCourse;

  bool _obscureCurrent = false;
  bool _obscureNew = false;
  bool _obscureReenter = false;

  void getCountry() async {
    var data = {};

    Utils.postAPI(data, "get_negara").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CountryServices.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void getProvince() async {
    var data = {};

    Utils.postAPI(data, "get_kota").then((body) {
      if (body['status'] == 'success') {
        try {
          List<dynamic> lPro = body["data"];
          CitiesService.cities = lPro;
          print('Count: ${body.length}, Datas: ${lPro}');
        } catch (e) {
          print(e);
        }
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  List<dynamic> _dataCourse = [];
  void getCourse() async {
    var data = {
      'kota': homeCountry.text,
    };

    Utils.postAPI(data, "get_lapangan").then((body) {
      CourseService.course.clear();
      if (body['status'] == 'success') {
        _dataCourse = body['data'];
      } else {
        print(body['message']);
        // var snackbar = SnackBar(
        //   content: Text(body['body_message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        // setState(() {
        //   _loading = false;
        // });
      }
      print(body);
    }, onError: (error) {
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  SharedPreferences sharedPreferences;
  var dataUser;

  TextEditingController fullName = TextEditingController();
  TextEditingController nickName = TextEditingController();
  TextEditingController homeCountry = TextEditingController();
  TextEditingController homeCity = TextEditingController();
  TextEditingController homeGolf = TextEditingController();
  TextEditingController curPass = TextEditingController();
  TextEditingController curNew = TextEditingController();
  TextEditingController curReenter = TextEditingController();
  int courseId;

  FocusNode fullNode = FocusNode();
  FocusNode nickNode = FocusNode();
  FocusNode countryNode = FocusNode();
  FocusNode cityNode = FocusNode();
  FocusNode golfNode = FocusNode();
  FocusNode courseNode = FocusNode();
  FocusNode fotoNode = FocusNode();

  var token;

  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    _getDataTour();
    getCountry();
    getProvince();
    getCourse();
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  void _getDataTour() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var dataJson = localStorage.getString('user');
    var tokenJson = localStorage.getString('usertoken');
    var data = json.decode(dataJson);
    var datatoken = json.decode(tokenJson);
    if(widget.privateProfile == null){
      getDataGolfer();
    }else{
      setState(() {
        dataUser = data;
        print("DATAS $data");
        fullName.text = dataUser['data']['nama_lengkap'];
        nickName.text = dataUser['data']['nama_pendek'];
        homeCountry.text = dataUser['data']['negara'];
        homeCity.text = dataUser['data']['kota'];
        homeGolf.text = dataUser['data']['nama_lapangan'];
        courseId = dataUser['data']['id_lapangan'];
        token = datatoken;
        print("ID USER ${dataUser['data']['id']}");
      });
    }
  }

  getDataGolfer() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'id_user' : widget.userId,
    };

    Utils.postAPI(data, "get_profil").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          dataUser = body;
        });
      }else{
        Utils.showToast(this.context, "erroasdr", "${body['message']}");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      Utils.showToast(this.context, "errasdor", "Ops, something went wrong, please try again later.");
      setState(() {
        print("Error == $error");
        // _isLoading = false;
      });
    });
  }

  File _image;
  final snackbarKey = GlobalKey<ScaffoldState>();

  Future<void> chooseImage(ImageSource source) async {
    var imageFile = await ImagePicker.pickImage(source: source);
    //set source: ImageSource.camera to get image from camera
    // setState(() {
    //   _image = imageFile;
    // });
    _cropImage(imageFile);
    // Navigator.pop(this.context);
  }

  _cropImage(File picked) async {
    File cropped = await ImageCropper().cropImage(sourcePath: picked.path, aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0));
    setState(() {
      _image = cropped;
    });
  }

  Future<void> updateProfile(File ImageFile, BuildContext context) async {
    if (ImageFile != null) {
      var stream = new http.ByteStream(DelegatingStream.typed(ImageFile.openRead()));
      var length = await ImageFile.length();
      var uri = Uri.parse(Utils.url_API + "edit_profil");

      var request = new http.MultipartRequest("POST", uri);

      var multipartFile = new http.MultipartFile("foto", stream, length,
          filename: basename(ImageFile.path));
      request.fields['id'] = dataUser['data']['id'].toString();
      request.fields['email'] = dataUser['data']['email'];
      request.fields['jenis_kelamin'] = dataUser['data']['jenis_kelamin'];
      request.fields['no_telp'] = dataUser['data']['no_telp'];
      request.fields['tgl_lahir'] = dataUser['data']['tgl_lahir'];
      request.fields['sebagai'] = dataUser['data']['level'];
      request.fields['nama_lengkap'] = fullName.text;
      request.fields['nama_pendek'] = nickName.text;
      request.fields['negara'] = homeCountry.text;
      request.fields['kota'] = homeCity.text;
      request.fields['lapangan_id'] = courseId.toString();
      request.fields['token'] = token;
      if (ImageFile == null) {
        request.fields['foto'] = '0';
      } else {
        request.files.add(multipartFile);
      }

      var response = await request.send();
      //Get the response from the server
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      if (response.statusCode == 200) {
        _loading = false;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove('user');
        setState(() {
          prefs.setString('user', responseString);
          _edit = false;
          _changepassword = false;
          _image = null;
        });
        dataUser = json.decode(prefs.getString("user"));
      } else {
        var snackbar = SnackBar(
          content: Text('Ops, something wrong, please try again later.'),
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
    } else {
      var data = {
        'id': dataUser['data']['id'],
        'email': dataUser['data']['email'],
        'jenis_kelamin': dataUser['data']['jenis_kelamin'],
        'tgl_lahir': dataUser['data']['tgl_lahir'],
        'no_telp': dataUser['data']['no_telp'],
        'sebagai': dataUser['data']['level'],
        'nama_lengkap': fullName.text,
        'nama_pendek': nickName.text,
        'negara': homeCountry.text,
        'kota': homeCity.text,
        'lapangan_id': courseId,
        'token': token,
        'foto': '0'
      };

      setState(() {
        _loading = true;
      });
      Utils.postAPI(data, "edit_profil").then((body) async {
        if (body['status'] == 'success') {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.remove('user');
          setState(() {
            prefs.setString("user", json.encode(body));
            _loading = false;
            _changepassword = false;
            _edit = false;
            _image = null;
          });
          dataUser = json.decode(prefs.getString("user"));
        } else {
          var snackbar = SnackBar(
            content: Text(body['body_message']),
            backgroundColor: Colors.red,
          );
          snackbarKey.currentState.showSnackBar(snackbar);
          setState(() {
            _loading = false;
          });
        }
        print(body);
      }, onError: (error) {
        var snackbar = SnackBar(
          content: Text('Ops, something went wrong, please try again later.'),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          print("Error == $error");
          _loading = false;
        });
      });
    }
  }

  Future<void> _changePassword() async {
    var data = {
      'email': dataUser['data']['email'],
      'password_lama': curPass.text,
      'password_baru': curNew.text
    };

    setState(() {
      _loading = true;
    });
    Utils.postAPI(data, "edit_password").then((body) async {
      if (body['status'] == 'success') {
        setState(() {
          var snackbar = SnackBar(
            content: Text(body['body_message']),
            backgroundColor: Colors.blueAccent,
          );
          snackbarKey.currentState.showSnackBar(snackbar);
          _loading = false;
          _changepassword = false;
        });
      } else {
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _loading = false;
        });
      }
      print(body);
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // print("HEIGHT ${getProportionateScreenHeight(200)}");
    // var size = MediaQuery.of(context).size;
    return Scaffold(
        key: snackbarKey,
        bottomNavigationBar: widget.privateProfile == null
            ? null
            : BottomNavBar(page: dataUser==null?null:dataUser['data']['level'] == "Caddie" ? "caddie" : null,),
        body: widget.privateProfile == null
            ? _isLoading ? Center(child: LoadingMenu()) : bodyPage(context)
            : DoubleBackToCloseApp(
          snackBar: const SnackBar(
            content: Text('Tap back again to leave the app'),
          ),
          child: bodyPage(context),
        ),
    );
  }

  Widget bodyPage(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  backgroundHeaderAndPhotoProfile(context),
                  widget.privateProfile == null ? Column(
                    children: [
                      IndexProfileGolfer(dataUser: dataUser),
                      ViewProfilePage(
                        userId: widget.userId,
                        forCaddie: widget.forCaddie,
                      ),
                    ],
                  ) :
                  AnimatedCrossFade(
                    firstChild: indexProfile(),
                    secondChild: _changepassword
                        ? changePassword() : _edit ? editProfile() : Container(),
                    duration: Duration(milliseconds: 300),
                    crossFadeState: _changepassword || _edit ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                    sizeCurve: Curves.ease,
                  ),
                ],
              ),
            )
        ),
        _connectionStatus != ""
            ? TextErrorNoConnection(connectionStatus: _connectionStatus)
            : Container(),
      ],
    );
  }


  Widget backgroundHeaderAndPhotoProfile(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/2.5,
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: [
                  Colors.blue[900],
                  Colors.lightBlue,
                ],
              ),
              color: Colors.blue,
            ),
            height: MediaQuery.of(context).size.height/3.5,
          ),
          Positioned.fill(
            top: MediaQuery.of(context).size.height/5-20,
            child: Center(
              child: dataUser == null ? Container() : dataUser['data']['foto'] != null || (_image != null) ? Stack(
                children: [
                  InkWell(
                    onTap: () async {
                      await showDialog(
                          context: snackbarKey.currentContext,
                          builder: (BuildContext context) => NamePopUp(
                            name: "${dataUser==null?"":dataUser['data']['nama_pendek']!=null?dataUser['data']['nama_pendek']+", ":""}${dataUser==null?"":dataUser['data']['nama_lengkap']}",
                            photo: ClipRRect(borderRadius: BorderRadius.circular(10.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) => CircularProgressIndicator(
                              value: progress.progress,),
                              imageUrl: Utils.url + 'upload/user/' + dataUser['data']['foto'], height: 300, width: double.maxFinite, fit: BoxFit.cover,)),
                          )
                      );
                    },
                    child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 70,
                        child: (_image != null) ? Container(
                          margin: EdgeInsets.all(3),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: Image.file(
                              _image,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ) : CachedNetworkImage(
                          imageUrl: Utils.url +
                              'upload/user/' +
                              dataUser['data']['foto'],
                          imageBuilder: (context, imageProvider) =>
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                  shape: BoxShape.circle,
                                ),
                              ),
                          placeholder: (context, url) => Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white),
                                shape: BoxShape.circle,
                                color: Colors.white
                            ),
                            child: CircularProgressIndicator(),
                          ),
                          errorWidget: (context, url, error) => Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white),
                              image: DecorationImage(
                                image: AssetImage(
                                    "assets/images/userImage.png"),
                                fit: BoxFit.cover,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                        )),
                  ),
                  buttonEditImage(),
                ],
              ) : Stack(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 70,
                    child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        image: DecorationImage(
                          image:
                          AssetImage("assets/images/userImage.png"),
                          // fit: BoxFit.cover,
                        ),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  buttonEditImage()
                ],
              ),
            ),
          ),
          Positioned(
            right: 0,
            top: 20,
            child: Container(
              padding: EdgeInsets.all(18),
              child: Image.asset(
                'assets/images/gif_logo.png',
                isAntiAlias: true,
                width: 65,
                height: 65,
              ),
            ),),
          widget.privateProfile == null ? Positioned(
            left: 10,
            top: MediaQuery.of(context).size.height / 12,
            child: InkWell(onTap: () => Navigator.pop(context), child: Icon(Icons.arrow_back_ios_rounded, color: Colors.white,)),
          ) : Container()
        ],
      ),
    );
  }

  Positioned buttonEditImage() {
    return Positioned(
      right: 0,
      bottom: 0,
      child: InkWell(
        onTap: () {
          chooseImage(ImageSource.gallery);
        },
        child: _edit ? Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Color(0xFFD3D3D3),
          ),
          child: Center(child: Icon(Icons.add_a_photo)),
        ) : Container(),
      ),
    );
  }

  Widget indexProfile() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5),
          alignment: Alignment.center,
          child: Text(
            "${dataUser == null ? "" : dataUser['data']['nama_lengkap']}",
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 24),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15),
          alignment: Alignment.center,
          child: Text(
            "Home Golf Course",
          ),
        ),
        SizedBox(height: 5,),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "${dataUser == null ? "" : dataUser['data']['nama_lapangan']}\n${dataUser == null ? "" : dataUser['data']['kota']}, ${dataUser == null ? "" : dataUser['data']['negara']}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 5,),
        InkWell(
          // onTap: () => Navigator.push(this.context, MaterialPageRoute(builder: (context) => ReworkProfilePage())),
          child: Text(
            "GIF ID: ${dataUser == null ? "" : dataUser['data']['kode_user']}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: AppTheme.gFont),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: Text("${dataUser == null ? "" : dataUser['data']['email']}\n ${dataUser == null ? "" : dataUser['data']['no_telp']}", textAlign: TextAlign.center, style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack),),
        ),
        Container(
          margin: EdgeInsets.only(top: 30, bottom: 20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _edit = true;
                        });
                      },
                      child: Text(
                        "Edit\n Profile",
                        style: TextStyle(color: Colors.green),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _changepassword = true;
                        });
                      },
                      child: Text(
                        "Change\n Password",
                        style: TextStyle(color: Colors.green),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 30),
                child: InkWell(
                  onTap: () async {
                    showDialog(
                        context: snackbarKey.currentContext,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            contentPadding: EdgeInsets.all(8),
                            title: Text("Are you sure want to logout?", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                            content: Container(
                              height: 60,
                              width: double.maxFinite,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                      splashColor: Colors.grey.withOpacity(0.5),
                                      onPressed: () { Navigator.pop(context);},
                                      padding: const EdgeInsets.all(0.0),
                                      child: Ink(
                                          height: 40,
                                          decoration: BoxDecoration(
                                              color: AppTheme.gRed,
                                              borderRadius: BorderRadius.circular(100)
                                          ),
                                          child: Container(
                                              constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                              alignment: Alignment.center,
                                              child: Text("Cancel", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                    ),
                                  ),
                                  Container(
                                    child: RaisedButton(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                                      splashColor: Colors.grey.withOpacity(0.5),
                                      onPressed: () async {
                                        SharedPreferences prefs =
                                        await SharedPreferences.getInstance();
                                        prefs.remove('token');
                                        prefs.remove('id');
                                        Navigator.of(this.context).pushAndRemoveUntil(
                                            MaterialPageRoute(
                                                builder: (BuildContext context) => LoginPage()),
                                                (Route<dynamic> route) => false);
                                      },
                                      padding: const EdgeInsets.all(0.0),
                                      child: Ink(
                                          height: 40,
                                          decoration: BoxDecoration(
                                              color: AppTheme.gButton,
                                              borderRadius: BorderRadius.circular(100)
                                          ),
                                          child: Container(
                                              constraints: const BoxConstraints(maxWidth: 90, minHeight: 20),
                                              alignment: Alignment.center,
                                              child: Text("Yes", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,))),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }
                    );
                  },
                  child: Container(
                    width: 101,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: AppTheme.gRed
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.logout, size: 15, color: Colors.white, textDirection: TextDirection.rtl,),
                        SizedBox(width: 2,),
                        Text(
                          "Logout",
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Container editProfile() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              formAndLabel(label: "Full Name", form: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please input this field.";
                  }
                  return null;
                },
                maxLines: null,
                focusNode: fullNode,
                controller: fullName,
                onFieldSubmitted: (_) {
                  FocusScope.of(this.context).requestFocus(nickNode);
                },
              ),),
              formAndLabel(label: "Nick Name", form: TextFormField(
                focusNode: nickNode,
                controller: nickName,
                maxLines: null,
                decoration: InputDecoration(hintText: "(Optional)"),
                onFieldSubmitted: (_) {
                  FocusScope.of(this.context).requestFocus(countryNode);
                },
              ),),
              formAndLabel(label: "Home Country", form: TypeAheadFormField(
                textFieldConfiguration: TextFieldConfiguration(
                    controller: this.homeCountry, focusNode: cityNode),
                suggestionsCallback: (String pattern) {
                  return CountryServices.getSuggestions(pattern);
                },
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text(suggestion['nama']),
                  );
                },
                transitionBuilder:
                    (context, suggestionsBox, controller) {
                  return suggestionsBox;
                },
                onSuggestionSelected: (suggestion) {
                  this.homeCountry.text = suggestion["nama"];
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please select a country';
                  }
                  return null;
                },
                onSaved: (value) => this._selectedCity = value,
              ),),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      // margin: EdgeInsets.only(left: 10),
                      child: Text(
                        "Home Golf Course\n(Most Frequent)", textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    width: 180,
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(right: 50),
                    child: SearchableDropdown.single(
                      items: _dataCourse.map((data) {
                        return (DropdownMenuItem(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${data['nama_lapangan']}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                              Text("${data['nama']}")
                            ],
                          ),
                          value: data,
                        ));
                      }).toList(),
                      hint: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("${homeGolf.text}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                          Text("${homeCity.text}")
                        ],
                      ),
                      underline: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(color: Colors.grey)
                            )
                        ),
                      ),
                      assertUniqueValue: true,
                      onChanged: (value) {
                        if(value != null){
                          homeCity.text = value['nama'];
                          courseId = value['id_lapangan'];
                          homeGolf.text = value['nama_lapangan'];
                        }
                      },
                      dialogBox: true,
                      isExpanded: true,
                      displayClearIcon: false,
                      onClear: () {},
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                    splashColor: Colors.grey.withOpacity(0.5),
                    onPressed: () {
                      setState(() {
                        _changepassword = false;
                        _edit = false;
                        _image = null;
                      });
                    },
                    padding: const EdgeInsets.all(0.0),
                    child: Ink(
                        decoration: BoxDecoration(
                            color: AppTheme.gButton,
                            borderRadius: BorderRadius.circular(100)
                        ),
                        child: Container(
                            constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                            alignment: Alignment.center,
                            child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                    splashColor: Colors.grey.withOpacity(0.5),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        updateProfile(_image, this.context);
                        setState(() {
                          _loading = true;
                        });
                      }
                    },
                    padding: const EdgeInsets.all(0.0),
                    child: Ink(
                        decoration: BoxDecoration(
                            color: AppTheme.gButton,
                            borderRadius: BorderRadius.circular(100)
                        ),
                        child: Container(
                            constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                            alignment: Alignment.center,
                            child: _loading ? Center(
                                child: Container(width: 20, height: 20, child: CircularProgressIndicator(),)) :
                            Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
                  ),
                ],
              ),
              SizedBox(height: 30,),

            ],
          ),
        ),
      ),
    );
  }

  Row formAndLabel({String label, Widget form}) {
    return Row(
      children: [
        Expanded(
          child: Container(
            // margin: EdgeInsets.only(left: 10),
            child: Text(
              "$label", textAlign: TextAlign.right,
            ),
          ),
        ),
        Container(
            padding: EdgeInsets.only(left: 10),
            width: 180,
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(right: 50),
            child: form
        )
      ],
    );
  }

  Wrap changePassword() {
    return Wrap(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5),
          alignment: Alignment.center,
          child: Text(
            "${dataUser == null ? "" : dataUser['data']['nama_lengkap']}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
        ),
        FormErrorRegister(errors: errors),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Current Password",
                          style: TextStyle(color: AppTheme.gButton),
                        )),
                  ),
                  TextFormField(
                    controller: curPass,
                    onSaved: (newValue) => curr = newValue,
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        removeError(error: kEmpty);
                      }
                      return null;
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        addError(error: kEmpty);
                        return "";
                      }
                      return null;
                    },
                    obscureText: !_obscureCurrent,
                    decoration: InputDecoration(
                        suffixIcon: InkWell(
                          onTap: (){
                            setState(() {
                              _obscureCurrent = !_obscureCurrent;
                            });
                          },
                          child: Icon(_obscureCurrent ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                        ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "New Password",
                          style: TextStyle(color: AppTheme.gButton),
                        )),
                  ),
                  TextFormField(
                    controller: curNew,
                    onSaved: (newValue) => newpass = newValue,
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        removeError(error: kEmpty);
                      } else if (value == curReenter.text) {
                        removeError(error: kReenterPass);
                      }
                      return null;
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        addError(error: kEmpty);
                        return "";
                      } else if (value != curReenter.text) {
                        addError(error: kReenterPass);
                        return "";
                      }
                      return null;
                    },
                    obscureText: !_obscureNew,
                    decoration: InputDecoration(
                      suffixIcon: InkWell(
                        onTap: (){
                          setState(() {
                            _obscureNew = !_obscureNew;
                          });
                        },
                        child: Icon(_obscureNew ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Reenter Password",
                            style: TextStyle(color: AppTheme.gButton))),
                  ),
                  TextFormField(
                    controller: curReenter,
                    onSaved: (newValue) => match = newValue,
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        removeError(error: kEmpty);
                      } else if (value == curNew.text) {
                        removeError(error: kReenterPass);
                      }
                      return null;
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        addError(error: kEmpty);
                        return "";
                      } else if (value != curNew.text) {
                        addError(error: kReenterPass);
                      }
                      return null;
                    },
                    obscureText: !_obscureReenter,
                    decoration: InputDecoration(
                      suffixIcon: InkWell(
                        onTap: (){
                          setState(() {
                            _obscureReenter = !_obscureReenter;
                          });
                        },
                        child: Icon(_obscureReenter ? Icons.remove_red_eye : Icons.remove_red_eye_outlined),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 30, bottom: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  setState(() {
                    _changepassword = false;
                    _edit = false;
                  });
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: Text("Back", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                splashColor: Colors.grey.withOpacity(0.5),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    _changePassword();
                    setState(() {
                      _loading = true;
                    });
                  }
                },
                padding: const EdgeInsets.all(0.0),
                child: Ink(
                    decoration: BoxDecoration(
                        color: AppTheme.gButton,
                        borderRadius: BorderRadius.circular(100)
                    ),
                    child: Container(
                        constraints: const BoxConstraints(maxWidth: 116, minHeight: 51),
                        alignment: Alignment.center,
                        child: _loading ? Center(child: Container(width: 20,height: 20,child: CircularProgressIndicator(),)) :
                        Text("Save", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16), textAlign: TextAlign.center,))),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
