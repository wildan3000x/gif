import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/caddie/profile_caddie.dart';
import 'package:gif/caddie/search_golfer.dart';
import 'package:gif/caddie/widget/widget_feedback.dart';
import 'package:gif/main.dart';
import 'package:gif/my_games/caddie/all_recent_caddie.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/caddie_card.dart';
import 'package:gif/widget/list_container_home.dart';

class FindCaddie extends StatefulWidget {
  const FindCaddie({Key key, this.snackBar, this.forAllRecentCaddiePage}) : super(key: key);
  final snackBar, forAllRecentCaddiePage;

  @override
  _FindCaddieState createState() => _FindCaddieState();
}

class _FindCaddieState extends State<FindCaddie> {
  bool _isLoading = false;

  @override
  void initState() {
    getRecentCaddie();
    super.initState();
  }

  var dataRecentCaddie;
  Future<void> getRecentCaddie() async {
    setState(() {
      _isLoading = true;
    });
    final token = await Utils.getToken(context);
    var data = {
      'token': token,
    };
    Utils.postAPI(data, "recent_caddie").then((body) {
      if (body['status'] == 'success') {
        dataRecentCaddie = body['data'];
      } else {
        // Utils.showToast(context, "err", "${body['message']}");
        print("ERROR ${body['message']}");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      widget.snackBar.currentState.showSnackBar(snackbar);
      setState(() {
        _isLoading = false;
        print("Error == $error");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: ButtonSquareFindCaddie(
              textButton: "Find Caddie",
              link: () => Utils.navLink(context, SearchRepresent(findLevel: "Caddie", forBookCaddie: true,)),
              cusMargin: EdgeInsets.only(top: 16, bottom: 5),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TitleListMenu(
                text: "Recent Caddies",
                color: AppTheme.gButtonFindCaddie,
              ),
              widget.forAllRecentCaddiePage == true || dataRecentCaddie == null || dataRecentCaddie.length <= 3 ? Container() :
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  onTap: () => Utils.navLink(context, AllRecentCaddie()),
                    child: Text("See All")),
              )
            ],
          ),
          _isLoading ? LoadingMenu(withoutMargin: true,) :
          dataRecentCaddie == null || dataRecentCaddie.length <= 0
              ? Container(
            height: MediaQuery.of(context).size.height/2,
            child: Center(child: Text("have you ever been accompanied with someone else during your plays?", style: TextStyle(fontSize: 13, color: AppTheme.gFontBlack, fontStyle: FontStyle.italic), textAlign: TextAlign.center,)),
          )
              : ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: dataRecentCaddie==null?0:dataRecentCaddie.length <= 3 || widget.forAllRecentCaddiePage == true ? dataRecentCaddie.length : 3,
            itemBuilder: (context, i) {
              return RecentCaddieCard(dataRecentCaddie: dataRecentCaddie[i]);
            },
          ),
        ],
      ),
    );
  }
}

class RecentCaddieCard extends StatelessWidget {
  const RecentCaddieCard({
    Key key,
    @required this.dataRecentCaddie,
  }) : super(key: key);

  final dataRecentCaddie;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: EdgeInsets.only(bottom: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF9F9F9),
          border: Border.all(
              color: Color(0xFFE5E5E5)
          )
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: () => Utils.navLink(context, CaddieProfile(dataCaddie: dataRecentCaddie, rating: dataRecentCaddie['feedback']['bintang'],)),
        child: Column(
          children: [
            CaddieCard(
              dataCaddie: dataRecentCaddie,
              dataTournament: dataRecentCaddie,
              withRequestedText: "no",
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text("My Feedback", style: TextStyle(color: AppTheme.gcreate, fontWeight: FontWeight.bold,),),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 2.0, bottom: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text("${dataRecentCaddie['feedback']['feedback']}", style: TextStyle(color: AppTheme.gFontBlack, fontStyle: FontStyle.italic),),
                  ),
                  // SizedBox(width: 3,),
                  RatingAndStar(rating: dataRecentCaddie['feedback']['bintang']),
                  SizedBox(width: 12,)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ButtonSquareFindCaddie extends StatelessWidget {
  const ButtonSquareFindCaddie({
    Key key, @required this.textButton, this.cusMargin, this.link, this.isLoading
  }) : super(key: key);

  final textButton, link, cusMargin, isLoading;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: cusMargin == null ? EdgeInsets.zero : cusMargin,
      height: 30,
      child: ElevatedButton(
        onPressed: link,
        style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.all(8)),
          backgroundColor: MaterialStateProperty.all(AppTheme.gButtonFindCaddie),
        ),
        child: isLoading == true
            ? Center(child: Container(width: 15, height: 15, child: CircularProgressIndicator(),),)
            : Text("$textButton", style: TextStyle(color: Colors.white),),
      ),
    );
  }
}
