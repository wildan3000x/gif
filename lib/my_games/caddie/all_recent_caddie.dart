import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/join_games/join_games.dart';
import 'package:gif/my_games/caddie/find_caddie.dart';

class AllRecentCaddie extends StatefulWidget {
  const AllRecentCaddie({Key key}) : super(key: key);

  @override
  _AllRecentCaddieState createState() => _AllRecentCaddieState();
}

class _AllRecentCaddieState extends State<AllRecentCaddie> {
  final snackbarKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(13),
          child: Column(
            children: [
              HeaderCreate(title: "Recent Caddies", backSummary: true,),
              SizedBox(height: 10,),
              Center(
                child: TitleCenter(
                  textTitle: "All of Your Recent Caddies",
                  color: AppTheme.gFontBlack,
                ),
              ),
              FindCaddie(
                snackBar: snackbarKey,
                forAllRecentCaddiePage: true,
              )
            ],
          ),
        ),
      ),
    );
  }
}
