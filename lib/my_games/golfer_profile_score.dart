import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/profile_golfercaddie.dart';
import 'package:intl/intl.dart';
import 'package:simple_rich_text/simple_rich_text.dart';

import '../app_theme.dart';
import 'my_games.dart';
import 'scorecards/my_score.dart';
import 'widget/scorecard_box.dart';

class GolferProfileScore extends StatefulWidget {
  const GolferProfileScore({Key key, this.dataGolfer, this.fromLeaderboards = false}) : super(key: key);
  final dataGolfer;
  final bool fromLeaderboards;

  @override
  _GolferProfileScoreState createState() => _GolferProfileScoreState();
}

class _GolferProfileScoreState extends State<GolferProfileScore> with SingleTickerProviderStateMixin{
  var dataGolfer;
  TabController _tabController;
  bool _isLoading = false;
  bool _isLoadingProfile = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  double sizeRatio;
  int _activeIndex = 0;

  @override
  void initState() {
    dataGolfer = widget.dataGolfer;
    getMyRecentScoreCards();
    if(widget.fromLeaderboards){
      getProfileGolfer();
    }
    super.initState();
    _tabController = TabController(length: 2, vsync: this)
      ..addListener(() {
        setState(() {
          _activeIndex = _tabController.index;
        });
      });
  }

  var myFormat = DateFormat('d MMM yyyy');

  var dataRecentScore;
  var dataTrophies;
  getMyRecentScoreCards() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : widget.fromLeaderboards ? dataGolfer : dataGolfer['token'],
    };
    Utils.postAPI(data, "my_recent_scorecards").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          dataRecentScore = body['data'];
          dataTrophies = body['trophy_room'];
        });
      }else{
        print("failed ${body['message']}");
        // Utils.showToast(context, "err", "${body['message']}");
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  getProfileGolfer() async {
    setState(() {
      _isLoadingProfile = true;
    });
    var data = {
      'token' : dataGolfer,
    };
    Utils.postAPI(data, "get_profil").then((body) {
      if(body['status'] == 'success'){
        setState(() {
          dataGolfer = body['data'];
        });
      }else{
        print("failed ${body['message']}");
        // Utils.showToast(context, "err", "${body['message']}");
      }
      setState(() {
        _isLoadingProfile = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoadingProfile = false;
      });
    });
  }

  String trophyText;

  void linkToScoreCard(data) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyScore(
                  idTour: data['id_turnamen'],
                  buddyScore: true,
                  buddyToken: data['profil']['token'],
                )));
  }

  @override
  Widget build(BuildContext context) {
    sizeRatio = MediaQuery.of(context).size.height;
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.all(13),
          child: Column(
            children: [
              HeaderCreate(
                title: "Golfer Profile",
              ),
              SizedBox(height: 10,),
              _isLoadingProfile ? Center(child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: CircularProgressIndicator(),
              )) : ProfilePicForCaddieGolfer(
                data: dataGolfer,
              ),
              SizedBox(height: 8,),
              Stack(
                fit: StackFit.passthrough,
                alignment: Alignment.bottomCenter,
                children: [
                  Row(
                    children: [
                      unselectedIndicatorTabColor(),
                      unselectedIndicatorTabColor(),
                    ],
                  ),
                  DefaultTabController(
                    length: 2,
                    child: Builder(builder: (BuildContext context){
                      return Column(
                        children: [
                          Container(
                            height: 35,
                            child: TabBar(
                              controller: _tabController,
                              indicatorColor: Colors.grey,
                              // overlayColor: MaterialStateProperty.all(Colors.grey),
                              labelColor: Colors.white,
                              unselectedLabelColor: AppTheme.gFontBlack,
                              indicatorSize: TabBarIndicatorSize.tab,
                              indicator: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20)),
                                  color: _activeIndex == 0 ? AppTheme.gButton : AppTheme.gBlueBoxTrophies),
                              tabs: [
                                Tab(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text("Scorecards"),
                                  ),
                                ),
                                Tab(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text("Trophies"),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },)
                  ),
                ],
              ),
              SizedBox(height: 12,),
              Expanded(
                // height: 100,
                child: _isLoading ? Center(
                  child: Container(width: 20, height: 20, child: CircularProgressIndicator()),
                ) : TabBarView(
                  controller: _tabController,
                  children: [
                    dataRecentScore==null||dataRecentScore.length < 1 ?
                    Center(
                      child: Text("this person doesn't have any score yet.", style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                    ) :
                    SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: List.generate(dataRecentScore==null?0:dataRecentScore.length, (index) =>
                          ScoreBoxCard(
                            dataRecentScore: dataRecentScore[index],
                            onTap: () => linkToScoreCard(dataRecentScore[index]),
                            myFormat: myFormat,
                          )),
                      ),
                    ),
                    dataTrophies==null||dataTrophies.length < 1 ?
                    Center(
                      child: Text("this person has never won any trophy yet.", style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
                    ) :
                    SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: List.generate(dataTrophies==null?0:dataTrophies.length, (index) {
                          return cardGolferTrophys(index);
                        }),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  InkWell cardGolferTrophys(int index) {
    return InkWell(
      onTap: () => linkToScoreCard(dataTrophies[index]),
      child: IntrinsicHeight(
        child: Container(
          margin: EdgeInsets.only(bottom: 8),
          // height: sizeRatio/4-10,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: AppTheme.gGrey2)
          ),
          child: Column(
            children: [
              IntrinsicHeight(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppTheme.gBlueBoxTrophies
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${dataTrophies[index]['nama_turnamen']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),),
                              Text("${dataTrophies[index]['nama_lapangan']}, ${myFormat.format(DateTime.parse(dataTrophies[index]['tanggal_turnamen']))}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(3),
                        padding: EdgeInsets.all(3),
                        width: 91,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              child: IntrinsicHeight(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    grossAndNett(dataTrophies[index]['gross'], "Gross"),
                                    VerticalDivider(
                                      color: AppTheme.gGrey2,
                                      width: 5,
                                      thickness: 2,
                                    ),
                                    grossAndNett(dataTrophies[index]['net'], "Nett"),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  color: AppTheme.gGrey2,
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Center(child: Text("HC ${dataTrophies[index]['hc']}", style: TextStyle(fontSize: 11),),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Row(
                    children: [
                      Image.asset(dataTrophies[index]['trophy'].length > 1 ? "assets/images/many trophy.png"
                          : "assets/images/trophy_without_rank.png", width: 58, height: 66,),
                      SizedBox(width: 10,),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(dataTrophies[index]['trophy'].length, (index2) {
                            var mDataTro = dataTrophies[index]['trophy'][index2];
                            if(mDataTro['tipe_winner'] == "Nearest To The Pin"
                                || mDataTro['tipe_winner'] == "Nearest To The Line"
                                || mDataTro['tipe_winner'] == "Longest Drive")
                            {
                              trophyText = mDataTro['tipe_winner']+" ${mDataTro['distance']}"+" (hole #${mDataTro['hole']})";
                            }else if(mDataTro['tipe_winner'] == "Hole In One"){
                              trophyText = mDataTro['tipe_winner']+" (hole #${mDataTro['hole']})";
                            } else{
                              trophyText = "#"+mDataTro['rank'].toString()+" "+mDataTro['tipe_winner'];
                            }
                            return Row(
                              children: [
                                Icon(Icons.star, color: AppTheme.gStar, size: 18,),
                                SizedBox(width: 5,),
                                Expanded(
                                  child: Text("$trophyText",
                                    style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                                )
                              ],
                            );}
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget grossAndNett(var data, var type) {
    return Container(
      width: 40,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("$type", style: TextStyle(color: type == "Gross" ? AppTheme.gBlueBoxTrophies : AppTheme.gButton, fontSize: 13, fontWeight: FontWeight.bold),),
          Text("$data", style: TextStyle(color: type == "Gross" ? AppTheme.gBlueBoxTrophies : AppTheme.gButton, fontSize: 16, fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }

  Expanded unselectedIndicatorTabColor() {
    return Expanded(
      child: Container(
        // width: double.maxFinite,
        height: 35,
        decoration: BoxDecoration(
          color: AppTheme.gGrey2,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20)),
        ),
      ),
    );
  }
}

