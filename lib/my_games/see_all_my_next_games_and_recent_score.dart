import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/pairing/pairing_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/my_games/widget/scorecard_box.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'scorecards/my_score.dart';

class SeeAllMyNextGamesAndRecentScore extends StatefulWidget {
  final screenCategory;
  const SeeAllMyNextGamesAndRecentScore({Key key, this.screenCategory}) : super(key: key);
  @override
  _SeeAllMyNextGamesAndRecentScoreState createState() => _SeeAllMyNextGamesAndRecentScoreState();
}

class _SeeAllMyNextGamesAndRecentScoreState extends State<SeeAllMyNextGamesAndRecentScore> {
  SharedPreferences sharedPreferences;
  final snackbarKey = GlobalKey<ScaffoldState>();
  var dataTournament;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      if(widget.screenCategory == "my next games"){
        getAllMyNextGames();
      }else{
        getAllMyRecentScorecards();
      }
    }
  }

  getAllMyNextGames() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "main_tournament").then((body) {
      if(body['status'] == 'success'){
        dataTournament = body;
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataRecentScore;
  getAllMyRecentScorecards() async {
    setState(() {
      _isLoading = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "my_recent_scorecards").then((body) {
      if(body['status'] == 'success'){
        dataRecentScore = body['data'];
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
      }
      setState(() {
        _isLoading = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getAllMyNextGames();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1000));
    getAllMyNextGames();
    if(mounted)
      setState(() {
        getAllMyNextGames();
      });
    _refreshController.loadComplete();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: _isLoading,
          child: SmartRefresher(
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Column(
                  children: [
                    HeaderCreate(title: widget.screenCategory == "my next games" ? "My Next Games" : "My Recent Scorecards", backSummary: true,),
                    SizedBox(height: 10,),
                    Text(
                      widget.screenCategory == "my next games" ? "All of My Next Games" : "All of My Recent Scorecards",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF4F4F4F)),
                    ),
                    Text("will be displayed here!", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),),
                    SizedBox(height: 8,),
                    widget.screenCategory == "my next games" ? ConfirmedNotConfirmedText(withPencilIcon: true,) : Container(),
                    // SizedBox(height: 5,),
                    widget.screenCategory == "my next games" ? ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: dataTournament==null?0:dataTournament['nextgames'].length,
                      itemBuilder: (context, index) => ListContainerHome(
                        date: dataTournament['nextgames'][index]['tanggal_turnamen'],
                        nameTour: dataTournament['nextgames'][index]['nama_turnamen'],
                        courseTour: dataTournament['nextgames'][index]['nama_lapangan'],
                        cityTour: dataTournament['nextgames'][index]['kota_turnamen'],
                        orgaTour: dataTournament['nextgames'][index]['penyelenggara_turnamen'],
                        link: () {
                          if (dataTournament['nextgames'][index]["status"] == "Invited") {
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                JoinGames(data: dataTournament['nextgames'][index])));
                          } else if(dataTournament['nextgames'][index]["status"] == "Applying" || dataTournament['nextgames'][index]['status'] == "Not Confirmed"){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                PairingGames(data: dataTournament['nextgames'][index]['status'], msg: dataTournament['nextgames'][index]['status'] == "Applying" ? "Waiting For Approval" : dataTournament['nextgames'][index]['status'] == "Not Confirmed" ? "You have been Rejected" : null, sub_msg: dataTournament['nextgames'][index]['status'] == "Applying" ? "You still need approval from Organizer before join this game" : dataTournament['nextgames'][index]['status'] == "Not Confirmed" ? "your request to enter this game has been denied\nso you cannot enter this game." : null, id: dataTournament['nextgames'][index]['id_turnamen'], userId: dataTournament['nextgames'][index]['user_id'],)));
                          } else if(dataTournament['nextgames'][index]["status_create"] == true){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                SummaryGames(tourId: dataTournament['nextgames'][index]['id_turnamen'])));
                          } else{
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                StartGame(data: dataTournament['nextgames'][index], status: dataTournament['nextgames'][index]['status'], id: dataTournament['nextgames'][index]['id_turnamen'], userId: dataTournament['nextgames'][index]['user_id'],)));
                          }
                        },
                        widgetCorner: dataTournament['nextgames'][index]["status_create"] == true ?
                        Icon(Icons.circle, color: Color(0xFFff8000), size: 20,) :
                        SvgPicture.asset("assets/svg/check.svg", width: 20, height: 20, color: dataTournament['nextgames'][index]["status"] ==  "Confirmed" ? AppTheme.gFont : Color(0xFFF15411),),
                      ),
                    ) : ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: dataRecentScore==null?0:dataRecentScore.length,
                      itemBuilder: (context, index){
                        return ScoreBoxCard(
                          dataRecentScore: dataRecentScore[index],
                          myFormat: myFormat,
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                MyScore(idTour: dataRecentScore[index]['turnamen_id'], buddyScore: false))).then((value) {
                              getAllMyRecentScorecards();
                            });
                          },
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  var myFormat = DateFormat('d MMM yyyy');
}
