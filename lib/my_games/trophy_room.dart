import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/caddie/widget/button_rectangle_rounded_caddie.dart';
import 'package:gif/create_games/headerCreate.dart';
import 'package:gif/my_games/my_games.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';
import 'scorecards/my_score.dart';
import 'widget/scorecard_box.dart';

class TrophyRoom extends StatefulWidget {
  const TrophyRoom({Key key, this.checkData, this.dataTrophy}) : super (key: key);
  final checkData, dataTrophy;
  @override
  _TrophyRoomState createState() => _TrophyRoomState();
}

class _TrophyRoomState extends State<TrophyRoom> {
  final snackbarKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  var myFormat = DateFormat('d MMM yyyy');


  @override
  void initState(){
    checkLoginStatus();
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      // getAllBuddy();
    }
  }


  @override
  Widget build(BuildContext context) {
    return widget.checkData == null || widget.checkData.isEmpty ? Scaffold(
      body: Container(
        color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/eyes.png"),
                  SizedBox(height: 20,),
                  Text("Trophy Room", style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 24),),
                  SizedBox(height: 20,),
                  Text("It’s lonely and scary here", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Image.asset("assets/images/ghost.png", width: 120, height: 120,),
                      Image.asset("assets/images/emoticon.png", width: 60, height: 60,),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 30),
              alignment: Alignment.bottomCenter,
              child: ButtonRectangleRoundedCaddie(color: AppTheme.bogies, textButton: "Back", link: () => Navigator.pop(context),),
            )
          ],
        ),
      ),
    ) : Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(13),
            child: Column(
              children: [
                HeaderCreate(title: "Trophy Room", backSummary: true,),
                SizedBox(height: 5,),
                Image.asset("assets/images/winner.png", width: 60, height: 60, isAntiAlias: true,),
                Container(
                  padding: EdgeInsets.only(bottom: 8, top: 3),
                  child: Center(
                    child: Text(
                      "My Trophy",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF4F4F4F)),
                    ),
                  ),
                ),
                listScoreCard(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Column listScoreCard() {
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: widget.dataTrophy==null?0:widget.dataTrophy.length,
          itemBuilder: (context, index){
            return ScoreBoxCard(
              dataRecentScore: widget.dataTrophy[index],
              myFormat: myFormat,
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyScore(idTour: widget.dataTrophy[index]['turnamen_id'], buddyScore: false,)));
              },
            );
          },
        ),
      ],
    );
  }
}
