import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gif/auth/login/login_page.dart';
import 'package:gif/create_games/create_games.dart';
import 'package:gif/create_games/pairing/join_games.dart';
import 'package:gif/create_games/summary/games_summary.dart';
import 'package:gif/header_home.dart';
import 'package:gif/join_games/join_games.dart';
import 'package:gif/main.dart';
import 'package:gif/my_games/caddie/find_caddie.dart';
import 'package:gif/my_games/scorecards/my_score.dart';
import 'package:gif/my_games/see_all_my_next_games_and_recent_score.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/no_conn.dart';
import 'package:gif/the_game/leaderboard/running_leaderboard.dart';
import 'package:gif/the_game/scoring_page.dart';
import 'package:gif/the_game/start_game.dart';
import 'package:gif/utils/Utils.dart';
import 'package:gif/widget/custom_button.dart';
import 'package:gif/widget/list_container_home.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_database/firebase_database.dart';

import '../app_theme.dart';
import 'trophy_room.dart';
import 'widget/scorecard_box.dart';

class MyGames extends StatefulWidget {
  // MyGames({this.app});
  // final FirebaseApp app;
  @override
  _MyGamesState createState() => _MyGamesState();
}

class _MyGamesState extends State<MyGames> {

  SharedPreferences sharedPreferences;
  bool _isLoading = false;
  bool _isLoading2 = false;
  final snackbarKey = GlobalKey<ScaffoldState>();
  var dataGames;
  bool _isCheck = false;
  int countNotifGolfer;
  int countNotifOrga;
  // DatabaseError _error;
  String _connectionStatus = '';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
    // initConnectivity();
    // _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    checkLoginStatus();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          _connectionStatus = '';
        });
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = '';
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = 'Failed Connect To The Internet';
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "No Internet Connection");
        // setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get Connectivity.');
        break;
    }
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getRunningGames();
    }
  }

  var dataHistory;

  void getRunningGames() async {
    setState(() {
      _isLoading2 = true;
    });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "main_tournament").then((body) {
      if(body['status'] == 'success'){
        dataGames = body['turnamen_berjalan'];
        dataHistory = body['lastgames'];
        countRunningNotif = body['notifikasi_rg_count'];
        countNotifGolfer = body['notifikasi_count'];
        if(dataGames.isNotEmpty) {
          getStartGames(dataGames[0]['id_turnamen']);
        }else{
          getMyRecentScoreCards();
          print("NO TOURNAMENT RUNNING");
        }
      }else{
        var snackbar = SnackBar(
          content: Text(body['body_message']+", please try again later."),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading2 = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading2 = false;
      });
    });
  }


  var myFormat = DateFormat('d MMM yyyy');

  var dataRecentScore;
  var dataTrophyRoom;
  getMyRecentScoreCards() async {
    // setState(() {
    //   _isLoading2 = true;
    // });
    var data = {
      'token' : sharedPreferences.getString("token"),
    };
    Utils.postAPI(data, "my_recent_scorecards").then((body) {
      if(body['status'] == 'success'){
        dataRecentScore = body['data'];
        dataTrophyRoom = body;
      }else{
        print("failed ${body['message']}");
        // var snackbar = SnackBar(
        //   content: Text(body['message']),
        //   backgroundColor: Colors.red,
        // );
        // snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading2 = false;
        });
      }
      setState(() {
        _isLoading2 = false;
      });
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading2 = false;
      });
    });
  }

  var dataStart;

  void getStartGames(int id) async {
    var data = {
      'token': sharedPreferences.getString("token"),
      'id_turnamen': id
    };
    Utils.postAPI(data, "get_SG").then((body) {
      if (body['status'] == 'success') {
        dataStart = body['data'];
      } else {
        print("failed ${body['message']}");
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        // snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading2 = false;
        });
      }
      getMyRecentScoreCards();
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
      });
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    _isLoading2 = false;
    super.dispose();
  }

  var dataClub;

  var countRunningNotif;

  bool _seeAll = false;
  bool _seeAll2 = false;

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    getRunningGames();
    getMyRecentScoreCards();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    getRunningGames();
    getMyRecentScoreCards();
    if(mounted)
      setState(() {
        getRunningGames();
        getMyRecentScoreCards();
      });
    _refreshController.loadComplete();
  }

  void linkHead(link){
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => link));
  }

  bool _isFindCaddie = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: snackbarKey,
      bottomNavigationBar: BottomNavBar(runningGamesGolfer: true,),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave the app'),
        ),
        child: SafeArea(
          child: Stack(
            children: [
              SmartRefresher(
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    margin: EdgeInsets.all(13),
                    child: Column(
                      children: [
                        HeaderHome(
                          countNotifGolfer: countNotifGolfer,
                          countRunningNotif: countRunningNotif,
                          count1: countNotifGolfer,
                          context: context,
                          text: "My Games${_isFindCaddie ? " - Caddie" : ""}",
                          page: "my games",
                          otherOnTapFunction: [
                            () {
                              setState(() {
                                _isFindCaddie = !_isFindCaddie;
                              });
                            },
                            () => linkHead(SearchJoinGames()),
                            () {
                              if(dataTrophyRoom['trophy_room'] != null) {
                                linkHead(TrophyRoom(
                                  dataTrophy: dataTrophyRoom['trophy_room'],
                                  checkData: dataTrophyRoom['trophy_room'],));
                              }
                            }
                          ],
                          forMyGamesCaddie: _isFindCaddie,
                        ),
                        AnimatedCrossFade(
                          firstChild: _isLoading2 ? LoadingMenu(withoutMargin: true,) : Column(
                            children: [
                              listRunningGames(),
                              listScoreCard(),
                              // dataHistory == null || dataHistory.length <= 0 ? Container() : listHistory()
                            ],
                          ),
                          secondChild: _isFindCaddie ? FindCaddie(
                            snackBar: snackbarKey,
                          ) : Container(),
                          duration: Duration(milliseconds: 300),
                          crossFadeState: _isFindCaddie ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                          sizeCurve: Curves.ease,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _connectionStatus != "" ? TextErrorNoConnection(connectionStatus: _connectionStatus) : Container(),
            ],
          ),
        ),
      ),
    );
  }

  Column listHistory() {
    return Column(
      children: [
        textRunningGames(text: "My Last Games"),
        dataHistory != null ? dataHistory.length > 0 ? dataHistoryGames() : Container(margin: EdgeInsets.all(45), child: _isLoading2
            ? Center(
            child: Container(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(),
            ))
            : Text("no games."),) : Container(margin: EdgeInsets.all(45),
          child: _isLoading2
              ? Center(
              child: Container(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(),
              ))
              : Text("no games."),)
      ],
    );
  }

  Column listScoreCard() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  margin: EdgeInsets.only(top:10),
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Text("My Recent Scorecards", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
                    ],
                  )
              ),
            ),
            dataRecentScore==null||dataRecentScore.length<4
                ? Container()
                : SeeAllButton(
              onPressed: () => Utils.navLink(context, SeeAllMyNextGamesAndRecentScore(screenCategory: "my recent scorecards",))
            )
          ],
        ),
        // textRunningGames(text: "My Recent Scorecards"),
        dataRecentScore == null || dataRecentScore.length <= 0 ?Container(
          margin: EdgeInsets.all(45),
          child: Text("Go play some game first!"),
        ) : ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: dataRecentScore==null?0:dataRecentScore.length<4?dataRecentScore.length:_seeAll?dataRecentScore.length:3,
          itemBuilder: (context, index){
            return ScoreBoxCard(
              dataRecentScore: dataRecentScore[index],
              myFormat: myFormat,
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    MyScore(idTour: dataRecentScore[index]['turnamen_id'], buddyScore: false))).then((value) {
                  getRunningGames();
                  getMyRecentScoreCards();
                });
              },
            );
          },
        ),
      ],
    );
  }

  Column listRunningGames() {
    return Column(
      children: [
        textRunningGames(),
        dataGames == null || dataGames.length <= 0 ? Container(margin: EdgeInsets.all(45), child: _isLoading ? Center(
            child: Container(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(),
            )) : Text("no running games at this moment."),) : Column(
          children: [
            dataRunningGames(),
            Text("*only one running tournament can be shown at a time.", style: TextStyle(color: Colors.grey, fontSize: 13, fontStyle: FontStyle.italic), textAlign: TextAlign.center,),
          ],
        )
      ],
    );
  }

  ListView dataRunningGames() {
    return ListView.builder(
    // scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      itemCount: dataGames == null ? 0 : 1,
      itemBuilder: (context, index) => ListContainerHome(
        date: dataGames[index]['tanggal_turnamen'],
        nameTour: dataGames[index]['nama_turnamen'],
        courseTour: dataGames[index]['nama_lapangan'],
        cityTour: dataGames[index]['kota_turnamen'],
        orgaTour: dataGames[index]['penyelenggara_turnamen'],
        link: (){
          if(dataGames[index]['status_create'] == true){
            showDialog(context: context, builder: (context){
              return AlertDialog(
                titlePadding: EdgeInsets.fromLTRB(15, 20, 15, 0),
                title: Row(
                  children: [
                    SvgPicture.asset("assets/svg/cart.svg", width: 25, height: 30,),
                    SizedBox(width: 5,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Actions", style: TextStyle(fontSize: 18),),
                        Text("as the owner/organizer of this game",
                            style: TextStyle(fontSize: 12, fontStyle: FontStyle.italic, color: Colors.grey)),
                      ],
                    ),
                  ],
                ),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CustomButton(
                      textButton: "Go to game summary",
                      width: 200,
                      link: () => Utils.navLink(context, SummaryGames(tourId: dataGames[index]['id_turnamen'])),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text("or", style: TextStyle(fontStyle: FontStyle.italic, color: AppTheme.gFontBlack),),
                    ),
                    CustomButton(
                      textButton: dataGames[index].containsKey('organizer') && dataGames[index]['organizer'] == true
                          ? "Live Score"
                          : "Straight to input score!",
                      width: 200,
                      customColor: AppTheme.par,
                      link: () => actionRunningGames(index),
                    ),
                  ],
                ),
              );
            });
          }else if(dataGames[index]["status"] == "Applying" || dataGames[index]['status'] == "Not Confirmed"){
            Utils.showToast(context, "error", "You still need approval from Organizer before join this game");
          } else if(dataGames[index]["status"] == "Invited"){
            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                JoinGames(data: dataGames[index]))).then((value) => setState(() {
              getRunningGames();
              getMyRecentScoreCards();
            }));
          } else{
            actionRunningGames(index);
          }
        },
        widgetCorner: Image.asset("assets/images/arrow left.png", color: AppTheme.gFont,),
      ));
  }

  void actionRunningGames(int index){
    if(dataGames[index].containsKey('organizer') && dataGames[index]['organizer'] == true){
      Navigator.push(context, MaterialPageRoute(builder: (context) => RunningLeaderBoards(dataHeader: dataGames[index],)));
    }else if(dataGames[index]['skip']==true){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>
          ScorePage(dataId: dataGames[index]['id_turnamen'], dataSG: dataStart,))).then((value) {
        getRunningGames();
        getMyRecentScoreCards();
      });
    }else{
      Navigator.push(context, MaterialPageRoute(builder: (context) =>
          StartGame(
            data: dataGames[index],
            status: "Confirmed",
            id: dataGames[index]['id_turnamen'],
            userId: dataGames[index]['user_id'],))).then((value) {
        getRunningGames();
        getMyRecentScoreCards();
      });
    }
  }

  ListView dataHistoryGames() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: _seeAll2 ? dataHistory.length : 0,
      itemBuilder: (context, index) => ListContainerHome(
        date: dataHistory[index]['tanggal_turnamen'],
        nameTour: dataHistory[index]['nama_turnamen'],
        courseTour: dataHistory[index]['nama_lapangan'],
        cityTour: dataHistory[index]['kota_turnamen'],
        orgaTour: dataHistory[index]['penyelenggara_turnamen'],
        forColorHistory: true,
        link: (){
          Fluttertoast.showToast(
              msg: "The tournament has ended.",
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.black54,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1
          );
        },
        widgetCorner: Icon(Icons.access_time, color: Color(0xFF4f4f4f),),
      )
    );
  }


  Row textRunningGames({String text}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
              margin: EdgeInsets.only(top:8),
              padding: EdgeInsets.all(5),
              child: Text(text == null ? "Running Games" : text, style: TextStyle(color: text == null?Color(0xFFF15411):text=="My Recent Scorecards"?Color(0xFF2D9CDB):Color(0xFF4F4F4F), fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.start,)
          ),
        ),
        dataHistory != null ? dataHistory.length > 0 ? Container(
          padding: EdgeInsets.only(right: 5),
          child: InkWell(
              onTap: () {
                setState(() {
                    _seeAll2 = !_seeAll2;
                });
              },
              child: text == null ? Container() : _seeAll2 ? Text("Hide") : Text("See All"),
          ),
        ) : Container() : Container()
      ],
    );
  }
}
