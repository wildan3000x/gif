import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../app_theme.dart';

class ScoreBoxCard extends StatelessWidget {
  ScoreBoxCard({
    Key key,
    @required this.dataRecentScore,
    @required this.myFormat,
    @required this.onTap,
  }) : super(key: key);

  var dataRecentScore;
  final DateFormat myFormat;
  final onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      // padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Color(0xFFF9F9F9),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFFE5E5E5))
      ),
      child: FlatButton(
        padding: EdgeInsets.all(5),
        splashColor: Colors.grey.withOpacity(0.2),
        textTheme: ButtonTextTheme.normal,
        onPressed: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              // padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      colors: [Color(0xFF1857B7), Color(0xFF5DEED4)]),
                  shape: BoxShape.circle
              ),
              width: 40,
              height: 40,
              child: Center(child: Text("${dataRecentScore['net']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,)),
            ),
            SizedBox(width: 8,),
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${dataRecentScore['nama_turnamen']}", style: TextStyle(color: AppTheme.gFontBlack,  fontSize: 16, fontWeight: FontWeight.bold), overflow: TextOverflow.ellipsis,),
                  SizedBox(height: 2,),
                  Text("${dataRecentScore['nama_lapangan']}, ${dataRecentScore['kota_turnamen']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.normal),),
                  SizedBox(height: 2,),
                  Row(
                    children: [
                      Text("${myFormat.format(DateTime.parse(dataRecentScore['tanggal_turnamen']))},", style: TextStyle(color: AppTheme.gFont, fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                      SizedBox(width: 4,),
                      Text("${dataRecentScore['penyelenggara_turnamen']}", style: TextStyle(color: Color(0xFF2D9CDB), fontStyle: FontStyle.italic, fontWeight: FontWeight.normal),),
                    ],
                  )
                ],
              ),
            ),
            dataRecentScore['trophy'].isEmpty? Container() :
            dataRecentScore['trophy'].length>1?Container(
              width: 55,
              height: 55,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(left: 0, child: Image.asset("assets/images/${dataRecentScore['trophy'][0]['rank']==2?"medal#2":dataRecentScore['trophy'][0]['rank']==3?"medal#3":"medal"}.png", width: 40, height: 40, isAntiAlias: true,)),
                  Positioned(right: 0, child: Image.asset("assets/images/${dataRecentScore['trophy'][1]['rank']==2?"medal#2":dataRecentScore['trophy'][1]['rank']==3?"medal#3":"medal"}.png", width: 40, height: 40, isAntiAlias: true,)),
                  dataRecentScore['trophy'].length>2?
                  Positioned(bottom: 0, child: Image.asset("assets/images/${dataRecentScore['trophy'][2]['rank']==2?"medal#2":dataRecentScore['trophy'][2]['rank']==3?"medal#3":"medal"}.png", width: 40, height: 40, isAntiAlias: true,))
                      : Container()
                ],
              ),
            ) : Container(
              width: 55,
              child: Column(
                children: [
                  Text("${dataRecentScore['trophy'][0]['tipe_winner']}", style: TextStyle(color: AppTheme.gFont), overflow: TextOverflow.clip, textAlign: TextAlign.center,),
                  SizedBox(height: 1.5,),
                  Image.asset("assets/images/${dataRecentScore['trophy'][0]['rank']==2?"medal#2":dataRecentScore['trophy'][0]['rank']==3?"medal#3":"medal"}.png", width: 40, height: 40, isAntiAlias: true,)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}