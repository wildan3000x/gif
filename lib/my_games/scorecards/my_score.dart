import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gif/app_theme.dart';
import 'package:gif/auth/login/login_page.dart';
// import 'package:gif/my_games/golfer_profile_score.dart';
import 'package:gif/navbar/bottomNavBar.dart';
import 'package:gif/the_game/final_scoring_page.dart';
import 'package:gif/the_game/header_scoring.dart';
import 'package:gif/the_game/leaderboard/final_leaderboard/final_leaderboard.dart';
import 'package:gif/the_game/remark.dart';
import 'package:gif/utils/Utils.dart';
import 'package:intl/intl.dart';
import 'package:gif/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../golfer_profile_score.dart';


class MyScore extends StatefulWidget {
  MyScore({Key key, this.dataTour, this.idTour, this.buddyScore, this.buddyToken}) : super (key : key);
  final dataTour, idTour, buddyScore, buddyToken;
  @override
  _MyScoreState createState() => _MyScoreState();
}

class _MyScoreState extends State<MyScore> {
  final GlobalKey<ScaffoldState> snackbarKey = GlobalKey<ScaffoldState>();

  var myFormat = DateFormat('d MMM yyyy');

  bool _isLoading = false;
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    checkLoginStatus();
    WidgetsBinding.instance.addPostFrameCallback((_) => getSizeAndPosition());
    super.initState();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
              (Route<dynamic> route) => false);
    }else {
      getMyClub();
      getStartGames();
    }
  }

  var dataMyScore;

  void getMyClub() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'token' : widget.buddyScore == true ? widget.buddyToken : sharedPreferences.getString("token"),
      'id_turnamen': widget.idTour
    };

    Utils.postAPI(data, "detail_my_score").then((body) {
      if(body['status'] == 'success'){
        dataMyScore = body['data'];
        setState(() {
          _isLoading = false;
        });
      }else{
        var snackbar = SnackBar(
          content: Text(body['message']),
          backgroundColor: Colors.red,
        );
        snackbarKey.currentState.showSnackBar(snackbar);
        setState(() {
          _isLoading = false;
        });
      }
    }, onError: (error) {
      var snackbar = SnackBar(
        content: Text('Ops, something wrong, please try again later.'),
        backgroundColor: Colors.red,
      );
      snackbarKey.currentState.showSnackBar(snackbar);
      setState(() {
        print("Error == $error");
        _isLoading = false;
      });
    });
  }

  var dataTour;
  var dataPairing;
  var dataStart;

  getStartGames() async {
    var data = {
      'token': widget.buddyScore ? widget.buddyToken : sharedPreferences.getString("token"),
      'id_turnamen': widget.idTour
    };
    Utils.postAPI(data, "get_SG").then((body) {
      if (body['status'] == 'success') {
        dataTour = body['data']['turnamen'];
        dataPairing = body['data']['turnamen_pairing_group'];
        dataStart = body['data'];
      } else {
        print("MESSAGE ${body['message']}");
      }
    }, onError: (error) {
      print("Error == $error");
    });
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    print("SIZEEE ${queryData.size.width}");
    return Scaffold(
      bottomNavigationBar: BottomNavBar(clickable: true,),
      key: snackbarKey,
      body: SafeArea(
        child: _isLoading ? Center(
          child: Container(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(),
          ),
        ) : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(13),
            child: Column(
              children: [
                HeaderScoring(dataPairing: dataMyScore, myFormat: myFormat, backButton: true,),
                SizedBox(height: 5,),
                textAndLinkOnTop(),
                SizedBox(height: 20,),
                profilePicture(),
                SizedBox(height: 15,),
                infoRankAndScore(),
                SizedBox(height: 10,),
                scoreBoard(),
                SizedBox(height: 10,),
                scoreBoard2(),
                SizedBox(height: 10,),
                Container(
                  padding: EdgeInsets.only(top: 2),
                  width: queryData.size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          width: queryData.size.width/6.6*1,
                          child: Text("Remark", style: TextStyle(color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold),)
                      ),
                      Container(
                        width: queryData.size.width/6.6*5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 1),
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                  color: Color(0xFFF2C94C),
                                  borderRadius: BorderRadius.circular(3)
                              ),
                            ),
                            Text("Eagle"),
                            Container(
                              margin: EdgeInsets.only(left: 3, right: 1),
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                  color: AppTheme.gFont,
                                  borderRadius: BorderRadius.circular(3)
                              ),
                            ),
                            Text("Birdie"),
                            Container(
                              margin: EdgeInsets.only(left: 3, right: 1),
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                  color: Color(0xFF2D9CDB),
                                  borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            Text("Par"),
                            Container(
                              margin: EdgeInsets.only(left: 3, right: 1),
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                  color: Color(0xFFF15411),
                                  borderRadius: BorderRadius.circular(3)
                              ),
                            ),
                            Flexible(child: Text("3 Bogies/Worse", overflow: TextOverflow.ellipsis,)),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  final GlobalKey _listOneKey = GlobalKey();
  Size _listOneSize = Size.zero;
  final GlobalKey _listTwoKey = GlobalKey();
  Size _listTwoSize = Size.zero;

  getSizeAndPosition() {
    // if(_listOneKey.currentContext.findRenderObject() != null && _listTwoKey.currentContext.findRenderObject() != null) {
      RenderBox _cardBox1 = _listOneKey.currentContext.findRenderObject();
      _listOneSize = _cardBox1.size;
      // RenderBox _cardBox2 = _listTwoKey.currentContext.findRenderObject();
      // _listTwoSize = _cardBox2.size;
      setState(() {});
    // }
  }

  Column scoreBoard() {
    return Column(
      children: [
        Text("First ${dataMyScore==null?"":dataMyScore['data_score']['data_score1'].length}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 64, child: Text("Hole", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                key: _listOneKey,
                child: Container(
                  // margin: EdgeInsets.only(left: 17),
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score1'].length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index){
                          return Container(padding: EdgeInsets.only(left: 5),
                              width: _listOneSize.width / (dataMyScore['data_score']['data_score1'].length), child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score1'][index]['hole']}", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18),));
                        },),
                    ],
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("T", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              border: Border(
                  left: BorderSide(color: Color(0xFFE5E5E5)),
                  right: BorderSide(color: Color(0xFFE5E5E5))
              )
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 64, child: Text("Par", style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                child: Container(
                  // margin: EdgeInsets.only(left: 17),
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score1'].length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index){
                          return Container(padding: EdgeInsets.only(left: 5), width: _listOneSize.width / (dataMyScore['data_score']['data_score1'].length), child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score1'][index]['par']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 18), textAlign: TextAlign.justify,));
                        },),
                    ],
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("${dataMyScore==null?"":dataMyScore['data_score']['total_par1']}", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 59, child: Text("Stroke", style: TextStyle(color: Color(0xFF828282), fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                child: Container(
                  // margin: EdgeInsets.only(left: 3),
                  height: 23,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score1'].length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index){
                      return Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 5),
                        // width: index == 9 ? 22 : 20,
                        constraints: BoxConstraints(maxWidth: _listOneSize.width / (dataMyScore['data_score']['data_score1'].length),),
                        child: Container(
                          width: 20,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(dataMyScore==null||dataMyScore['data_score']['data_score1'][index]['warna']=="Par"?1000:5),
                              color: dataMyScore==null||dataMyScore['data_score']['data_score1'][index]['warna']==""? Color(0xFFF9F9F9) :
                              dataMyScore['data_score']['data_score1'][index]['warna']=="Eagle"?AppTheme.eagle :
                              dataMyScore['data_score']['data_score1'][index]['warna']=="Birdie"?AppTheme.birdie :
                              dataMyScore['data_score']['data_score1'][index]['warna']=="Par"?AppTheme.par :
                              dataMyScore['data_score']['data_score1'][index]['warna']=="Bogies"?AppTheme.bogies :
                              Color(0xFFF9F9F9)
                          ),
                          child: Center(
                            child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score1'][index]['score']}", style: TextStyle(
                                color: dataMyScore==null||dataMyScore['data_score']['data_score1'][index]['warna']==""? AppTheme.gFontBlack : Colors.white,
                                fontSize: dataMyScore==null?0:dataMyScore['data_score']['data_score1'][index]['score'].toString().length>1?11:18),),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("${dataMyScore==null?"":dataMyScore['data_score']['total_score1']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Column scoreBoard2() {
    return Column(
      children: [
        Text("Second ${dataMyScore==null?"":dataMyScore['data_score']['data_score2'].length}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 64, child: Text("Hole", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                // key: _listOneKey,
                child: Container(
                  // margin: EdgeInsets.only(left: 17),
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score2'].length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index){
                          return Container(width: _listOneSize.width / (dataMyScore['data_score']['data_score2'].length), child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score2'][index]['hole']}", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18),));
                        },),
                    ],
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("T", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              border: Border(
                  left: BorderSide(color: Color(0xFFE5E5E5)),
                  right: BorderSide(color: Color(0xFFE5E5E5))
              )
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 64, child: Text("Par", style: TextStyle(color: Color(0xFF2D9CDB), fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                child: Container(
                  // margin: EdgeInsets.only(left: 17),
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score2'].length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index){
                          return Container(padding: EdgeInsets.only(left: 5), width: _listOneSize.width / (dataMyScore['data_score']['data_score2'].length), child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score2'][index]['par']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 18), textAlign: TextAlign.left,));
                        },),
                    ],
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("${dataMyScore==null?"":dataMyScore['data_score']['total_par2']}", style: TextStyle(color: AppTheme.gFont, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              color: Color(0xFFF9F9F9),
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
              border: Border.all(color: Color(0xFFE5E5E5))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(width: 59, child: Text("Stroke", style: TextStyle(color: Color(0xFF828282), fontWeight: FontWeight.bold, fontSize: 18),)),
              Expanded(
                child: Container(
                  // margin: EdgeInsets.only(left: 3),
                  height: 23,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: dataMyScore==null?0:dataMyScore['data_score']['data_score2'].length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index){
                      return Container(
                        alignment: Alignment.centerLeft,
                        // width: index == 9 ? 22 : 20,
                        padding: EdgeInsets.only(left: 5),
                        constraints: BoxConstraints(maxWidth: _listOneSize.width / (dataMyScore['data_score']['data_score2'].length),),
                        child: Container(
                          width: 20,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(dataMyScore==null||dataMyScore['data_score']['data_score2'][index]['warna']=="Par"?1000:5),
                              color: dataMyScore==null||dataMyScore['data_score']['data_score2'][index]['warna']==""? Color(0xFFF9F9F9) :
                              dataMyScore['data_score']['data_score2'][index]['warna']=="Eagle"?AppTheme.eagle :
                              dataMyScore['data_score']['data_score2'][index]['warna']=="Birdie"?AppTheme.birdie :
                              dataMyScore['data_score']['data_score2'][index]['warna']=="Par"?AppTheme.par :
                              dataMyScore['data_score']['data_score2'][index]['warna']=="Bogies"?AppTheme.bogies :
                              Color(0xFFF9F9F9)
                          ),
                          child: Center(
                            child: Text("${dataMyScore==null?"":dataMyScore['data_score']['data_score2'][index]['score']}", style: TextStyle(
                                color: dataMyScore==null||dataMyScore['data_score']['data_score2'][index]['warna']==""? AppTheme.gFontBlack : Colors.white,
                                fontSize: dataMyScore==null?0:dataMyScore['data_score']['data_score2'][index]['score'].toString().length>1?11:18),),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                width: 30,
                padding: const EdgeInsets.only(right: 3.0),
                child: Text("${dataMyScore==null?"":dataMyScore['data_score']['total_score2']}", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
      ],
    );
  }

  String ranking, textRankPerType;
  Row infoRankAndScore() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          // width: 102,
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text("Total Score", style: TextStyle(color: Color(0xFF1A478A), fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
                Container(
                  // width: /*dataMyScore==null?0:dataMyScore['turnamen']['net'].length==3?129:*/109,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Text("Gross", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 18, fontWeight: FontWeight.bold),),
                          Text("${dataMyScore==null?"":dataMyScore['turnamen']['gross']}", style: TextStyle(color: Color(0xFF2D9CDB), fontSize: 30, fontWeight: FontWeight.bold),),
                        ],
                      ),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(color: Color(0xFFBDBDBD))
                          )
                        ),
                      ),
                      // VerticalDivider(color: Color(0xFFBDBDBD), thickness: 1, indent: 5, endIndent: 5,),
                      Container(
                        child: Column(
                          children: [
                            Text("Nett", style: TextStyle(color: AppTheme.gFont, fontSize: 18, fontWeight: FontWeight.bold),),
                            Text("${dataMyScore==null?"":dataMyScore['turnamen']['net']}", style: TextStyle(color: AppTheme.gFont, fontSize: 30, fontWeight: FontWeight.bold),),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  // width: 102,
                  height: 18,
                  decoration: BoxDecoration(
                      color: Color(0xFFE5E5E5),
                      borderRadius: BorderRadius.circular(5)
                  ),
                  child: Center(child: Text("HC ${dataMyScore==null?"":dataMyScore['turnamen']['hc']}", style: TextStyle(color: AppTheme.gFontBlack),)),
                )
              ],
            ),
          ),
        ),
        Flexible(
          // height: 83,
          child: Container(
            // color: Colors.yellowAccent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Rank", style: TextStyle(color: Color(0xFF1A478A), fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
                SizedBox(height: 5,),
                Text("#${dataMyScore==null?"":dataMyScore['rank']['gross']} Gross", style: TextStyle(color: AppTheme.gFontBlack),),
                Text("#${dataMyScore==null?"":dataMyScore['rank']['nett']} Nett", style: TextStyle(color: AppTheme.gFontBlack),),
                dataMyScore==null||dataMyScore['rank']['flight']==""?Text(""):
                Text("#${dataMyScore==null?"":dataMyScore["rank"]["flight"]} ${dataMyScore==null?"":dataMyScore['rank']['nama_flight']}", style: TextStyle(color: AppTheme.gFontBlack),),
              ],
            ),
          ),
        ),
        Expanded(
          // width: 90,
          child: dataMyScore==null?Container():dataMyScore['turnamen']['trophy'].isEmpty?Column(
            children: [
              Text("Trophy", style: TextStyle(color: Color(0xFF1A478A), fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              SizedBox(height: 2,),
              dataMyScore['data_user']['token'] == sharedPreferences.getString('token') ? Column(
                children: [
                  Text("Really ?", style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold),),
                  Text("Buy yourself some trophies online", style: TextStyle(color: AppTheme.gFontBlack,), textAlign: TextAlign.center,)
                ],
              ) : Padding(
                padding: const EdgeInsets.only(top: 13.0),
                child: Text("(none)", style: TextStyle(color: AppTheme.gFontBlack,), textAlign: TextAlign.center,),
              ),
            ],
          ):Column(
            children: [
              Text("Trophy", style: TextStyle(color: Color(0xFF1A478A), fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.center,),
              SizedBox(height: 2,),
              // Image.asset("assets/images/trophy#1.png", width: 48, height: 56,),
              dataMyScore==null?Container():dataMyScore['turnamen']['trophy'].length>1?
              FlatButton(
                height: 48,
                onPressed: (){
                  showDialog(
                      context: snackbarKey.currentContext,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          contentPadding: EdgeInsets.only(bottom: 15, right: 15, left: 15),
                          titlePadding: EdgeInsets.all(8),
                          title: Image.asset("assets/images/winner.png", width: 40, height: 40, isAntiAlias: true,),
                          content: Container(
                            width: double.maxFinite,
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: dataMyScore==null?0:dataMyScore['turnamen']['trophy'].length,
                              itemBuilder: (context, index){
                                if(dataMyScore['turnamen']['trophy'][index]['tipe_winner'] == "Hole In One"
                                    || dataMyScore['turnamen']['trophy'][index]['tipe_winner'] == "Nearest To The Pin"
                                    || dataMyScore['turnamen']['trophy'][index]['tipe_winner'] == "Nearest To The Line"
                                    || dataMyScore['turnamen']['trophy'][index]['tipe_winner'] == "Longest Drive")
                                {
                                  // setState(() {
                                    ranking = dataMyScore['turnamen']['trophy'][index]['hole'].toString();
                                    textRankPerType = " ${dataMyScore['turnamen']['trophy'][index]['distance'] ?? ""}";
                                  // });
                                }else{
                                  // setState(() {
                                    ranking = dataMyScore['turnamen']['trophy'][index]['rank'].toString();
                                    textRankPerType = "";
                                  // });
                                }
                                return Container(
                                  margin: EdgeInsets.only(top: index == 0 ? 0 : 4),
                                  child: Text("#${textRankPerType==""?ranking+" ":""}"
                                      "${dataMyScore['turnamen']['trophy'][index]['tipe_winner']}"
                                      "${textRankPerType!=""?textRankPerType+" (hole "+ranking+")":""}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: textRankPerType == "" ? AppTheme.bogies : AppTheme.par,
                                    ),
                                    textAlign: TextAlign.center,),
                                );
                              },
                            ),
                          ),
                        );
                      }
                  );
                },
                splashColor: Colors.grey.withOpacity(0.2),
                child: Column(
                  children: [
                    Image.asset("assets/images/many trophy.png", width: 48, height: 56,),
                    Text("tap to see all trophy's", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontSize: 11), textAlign: TextAlign.center,)
                  ],
                ),) : Container(
                child: Image.asset("assets/images/trophy_without_rank.png", width: 48, height: 56,),
              ),
              dataMyScore==null?Container():dataMyScore['turnamen']['trophy'].length>1?Container():Text("${dataMyScore==null?"":dataMyScore['turnamen']['trophy'][0]['tipe_winner']}", style: TextStyle(color: AppTheme.gFontBlack), textAlign: TextAlign.center,),
            ],
          ),
        )
      ],
    );
  }

  Widget profilePicture() {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
            builder: (context) => GolferProfileScore(
              dataGolfer: dataMyScore['data_user'],
            )
        ));
      },
      // onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => GolferProfileScore(dataGolfer: dataMyScore,))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.centerRight,
            child: dataMyScore==null?Container(): dataMyScore['data_user']['foto'] == null ? ClipRRect(
              borderRadius: BorderRadius.circular(10000.0),
              child: Image.asset(
                "assets/images/userImage.png",
                width: 80,
                height: 80,
              ),
            ) : ClipRRect(borderRadius: BorderRadius.circular(10000.0), child: CachedNetworkImage(progressIndicatorBuilder: (context, url, progress) =>
                CircularProgressIndicator(
                  value: progress.progress,
                ), imageUrl: Utils.url + 'upload/user/' + dataMyScore['data_user']['foto'], width: 80, height: 80, fit: BoxFit.cover,)),),
          // SizedBox(width: 45,),
          SizedBox(width: 8,),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(dataMyScore==null?"":dataMyScore['data_user']['nama_lengkap'], style: TextStyle(color: AppTheme.gFontBlack, fontWeight: FontWeight.bold, fontSize: 32),),
                // Text("Mickelsen", style: TextStyle(color: AppTheme.gFontBlack, fontSize: 32),),
              ],
            ),
          ),
          // SizedBox(width: 30,)
        ],
      ),
    );
  }

  Row textAndLinkOnTop() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        InkWell(
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => FinalLeaderBoards(dataHeader: dataStart,))),
            child: Text("Tournament Result", style: TextStyle(color: Color(0xFF2D9CDB)),)),
        InkWell(onTap: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => FinalScorePage(dataId: widget.idTour, dataSG: dataStart, tokenFromScoreCard: widget.buddyScore ? widget.buddyToken : null,))),
            child: Text("Pairing Scorecard", style: TextStyle(color: Color(0xFF2D9CDB)),))
      ],
    );
  }
}
