import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gif/models/NavItem.dart';
import 'package:gif/size_config.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show Platform;

class BottomNavBar extends StatelessWidget {
  BottomNavBar({
    Key key,
    this.page,
    this.main,
    this.runningGamesGolfer,
    this.appointment,
    this.rating,
    this.playCaddie,
    this.clickable
  }) : super(key: key);

  final page;
  final main, runningGamesGolfer;
  final appointment, rating, playCaddie;
  final clickable;

  SharedPreferences sharedPreferences;

  @override
  Widget build(BuildContext context) {
    double defaultSize = SizeConfig.defaultSize;
    return Consumer<NavItems>(
      builder: (context, navItems, child) {
        return Container(
          padding: EdgeInsets.only(top: 10, bottom: Platform.isIOS ? 30 : 10, left: Platform.isIOS ? 20 : 15, right: Platform.isIOS ? 20 : 15),
          height: Platform.isIOS ? 90 : 60,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  Colors.grey[350],
                  Colors.white,
                ],
              ),
//                border: Border(top: BorderSide(color: Colors.grey)),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15.0),
                  topLeft: Radius.circular(15.0)),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -2),
                  blurRadius: 1,
                  color: Color(0xFF4B1A39).withOpacity(0.1),
                )
              ]),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(page == null ? navItems.items.length : navItems.itemsCaddie.length, (index) {
                  return page == null ? buildIconNavBarItem(
                      isActive: main == true ? index == 0 ? true : false : runningGamesGolfer == true ? index == 1 ? true : false: navItems.selectedIndex == index
                          ? true
                          : false,
                      icon: navItems.items[index].icon,
                      text: navItems.items[index].text,
                      press: () {
                        navItems.changeNavIndex(index: index);
                        if (navItems.items[index].destinationChecker())
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) =>
                              navItems.items[index].destination,
                            ),
                          );
                      }
                  ) : buildIconNavBarItem(
                      isActive: main == true ? index == 0 ? true : false : playCaddie == true ? index == 1 ? true : false : appointment == true ? index == 3 ? true : false : rating == true ? index == 2 ? true : false : navItems.selectedIndex == index ? true : false,
                      icon: navItems.itemsCaddie[index].icon,
                      text: navItems.itemsCaddie[index].text,
                      press: () {
                        navItems.changeNavIndex(index: index);
                        if (navItems.itemsCaddie[index].destinationChecker())
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) =>
                              navItems.itemsCaddie[index].destination,
                            ),
                          );
                      }
                  );
              }
          ),
        ));
      });
  }

  Widget buildIconNavBarItem(
      {String text, String icon, Function press, bool isActive = false}) {
    return InkWell(
      onTap: isActive && clickable == null ? null : press,
      child: Container(
        child: GestureDetector(
          onTap: isActive && clickable == null ? null : press,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SvgPicture.asset(
                icon,
                height: 20,
                width: 20,
                color: isActive ? page!=null ? Colors.green : Colors.orange : Colors.black,
              ),
              Text(
                text,
                style: TextStyle(
                    color: isActive ? page!=null ? Colors.green : Colors.orange : Colors.black, fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}
